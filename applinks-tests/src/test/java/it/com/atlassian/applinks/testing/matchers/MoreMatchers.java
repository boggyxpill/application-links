package it.com.atlassian.applinks.testing.matchers;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import java.util.List;

/**
 * Extensions to the core Hamcrest matchers library.
 *
 * @see Matchers
 */
public final class MoreMatchers {

    private MoreMatchers() {
        throw new AssertionError("Don't instantiate me");
    }


    // work-around for bad generic APIs and overall crapiness of Java generics
    @SuppressWarnings("unchecked")
    public static <T> Matcher<List<T>> contains(Matcher<T>... itemMatchers) {
        return (Matcher) Matchers.contains(itemMatchers);
    }

    @SuppressWarnings("unchecked")
    public static <T> Matcher<Iterable<T>> iterableContains(Matcher<T>... itemMatchers) {
        return (Matcher) Matchers.contains(itemMatchers);
    }

    @SuppressWarnings("unchecked")
    public static <T> Matcher<List<T>> listWithSize(int expectedSize) {
        return (Matcher) Matchers.iterableWithSize(expectedSize);
    }

    @SuppressWarnings("unchecked")
    public static <T> Matcher<List<T>> emptyList(Class<T> elementType) {
        return (Matcher) Matchers.emptyCollectionOf(elementType);
    }
}
