package it.com.atlassian.applinks.smoke.jira;

import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.applinks.pageobjects.LoginClient;
import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.webdriver.applinks.externalpage.GenericAdminPage;
import com.atlassian.webdriver.applinks.page.AbstractApplicationLinkPage;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.smoke.AbstractApplinksSmokeTest;
import it.com.atlassian.applinks.testing.backdoor.features.AtlassianDarkFeaturesBackdoor;
import it.com.atlassian.applinks.testing.categories.SmokeTest;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.product.Products;
import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertTrue;

/**
 * Verifies that Applinks admin links are present in JIRA for both V2 and V3 (depending on the NEW_UI feature), as well
 * as in both Renaissance and non-Renaissance mode.
 *
 * @since 5.0
 */
@Category(SmokeTest.class)
@Products(SupportedProducts.JIRA)
@Ignore("The Renaissance toggle is borked. Waiting on confirmation from the Jira team before removing it")
public class JiraApplinksAdminLinkSmokeTest extends AbstractApplinksSmokeTest<JiraTestedProduct> {
    private static final String RENAISSANCE_FEATURE_KEY = "com.atlassian.jira.config.CoreFeatures.LICENSE_ROLES_ENABLED";

    @Rule
    public final ApplinksFeaturesRule applinksFeatures;

    private final AtlassianDarkFeaturesBackdoor darkFeaturesBackdoor;

    public JiraApplinksAdminLinkSmokeTest(@Nonnull JiraTestedProduct mainProduct,
                                          @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
        applinksFeatures = new ApplinksFeaturesRule(mainInstance);
        darkFeaturesBackdoor = new AtlassianDarkFeaturesBackdoor(mainInstance);
    }

    @After
    public void disableRenaissance() {
        darkFeaturesBackdoor.disable(RENAISSANCE_FEATURE_KEY);
    }

    @Test
    public void shouldShowApplinksAdminLinkInV2WithRenaissanceOff() {
        applinksFeatures.disable(ApplinksFeatures.V3_UI);

        GenericAdminPage adminPage = loginAndGoToAdmin();
        assertTrue("Configure Applinks link is missing for V2", adminPage.hasConfigureApplinksLink());

        ListApplicationLinkPage v2ApplinksAdminPage = adminPage.goToConfigureApplinks(ListApplicationLinkPage.class);
        waitUntilTrue("Configure Applinks link dit not follow to the Applinks Admin page",
                v2ApplinksAdminPage.isApplinksListFullyLoaded());
    }

    @Test
    public void shouldShowApplinksAdminLinkInV3WithRenaissanceOff() {
        GenericAdminPage adminPage = loginAndGoToAdmin();
        assertTrue("Configure Applinks link is missing for V3", adminPage.hasConfigureApplinksLink());

        V3ListApplicationLinksPage v3ApplinksAdminPage = adminPage.goToConfigureApplinks(
                V3ListApplicationLinksPage.class);
        waitUntilTrue("Configure Applinks link dit not follow to the Applinks Admin page",
                v3ApplinksAdminPage.isLoaded());
    }

    @Test
    public void shouldShowApplinksAdminLinkInV2WithRenaissanceOn() {
        darkFeaturesBackdoor.enable(RENAISSANCE_FEATURE_KEY);
        applinksFeatures.disable(ApplinksFeatures.V3_UI);

        GenericAdminPage adminPage = loginAndGoToAdmin();
        assertTrue("Configure Applinks link is missing for V2", adminPage.hasConfigureApplinksLink());

        ListApplicationLinkPage v2ApplinksAdminPage = adminPage.goToConfigureApplinks(ListApplicationLinkPage.class);
        waitUntilTrue("Configure Applinks link dit not follow to the Applinks Admin page",
                v2ApplinksAdminPage.isApplinksListFullyLoaded());
    }

    @Test
    public void shouldShowApplinksAdminLinkInV3WithRenaissanceOn() {
        darkFeaturesBackdoor.enable(RENAISSANCE_FEATURE_KEY);

        GenericAdminPage adminPage = loginAndGoToAdmin();
        assertTrue("Configure Applinks link is missing for V3", adminPage.hasConfigureApplinksLink());

        V3ListApplicationLinksPage v3ApplinksAdminPage = adminPage.goToConfigureApplinks(
                V3ListApplicationLinksPage.class);
        waitUntilTrue("Configure Applinks link dit not follow to the Applinks Admin page",
                v3ApplinksAdminPage.isLoaded());
    }

    private GenericAdminPage loginAndGoToAdmin() {
        return LoginClient.loginAsSysadminAndGoTo(mainProduct, GenericAdminPage.class, AbstractApplicationLinkPage.URL);
    }
}
