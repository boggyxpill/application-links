package it.com.atlassian.applinks.testing.rules;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.google.common.collect.ImmutableList;
import org.junit.internal.AssumptionViolatedException;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import static java.util.Arrays.asList;

/**
 * An abstract rule for test rules that have to do the same thing before and/or after the test for multiple
 * tested products.
 */
public abstract class MultipleProductTestWatcher extends TestWatcher {

    private final Iterable<TestedProduct<WebDriverTester>> products;

    protected MultipleProductTestWatcher(Iterable<TestedProduct<WebDriverTester>> products) {
        this.products = ImmutableList.copyOf(products);
    }

    protected MultipleProductTestWatcher(TestedProduct<WebDriverTester>... products) {
        this(asList(products));
    }


    protected final Iterable<TestedProduct<WebDriverTester>> allProducts() {
        return products;
    }

    @Override
    protected final void succeeded(Description description) {
        for (TestedProduct<WebDriverTester> product : products) {
            onSucceeded(product, description);
        }
    }

    @Override
    protected final void failed(Throwable e, Description description) {
        for (TestedProduct<WebDriverTester> product : products) {
            onFailed(product, description);
        }
    }

    @Override
    protected final void starting(Description description) {
        for (TestedProduct<WebDriverTester> product : products) {
            onStarting(product, description);
        }
    }

    protected final void skipped(AssumptionViolatedException e, Description description) {
        for (TestedProduct<WebDriverTester> product : products) {
            onSkipped(product, description);
        }
    }

    @Override
    protected final void finished(Description description) {
        for (TestedProduct<WebDriverTester> product : products) {
            onFinished(product, description);
        }
    }


    protected void onStarting(TestedProduct<WebDriverTester> product, Description description) {
    }

    protected void onSucceeded(TestedProduct<WebDriverTester> product, Description description) {
    }

    protected void onFailed(TestedProduct<WebDriverTester> product, Description description) {
    }

    protected void onSkipped(TestedProduct<WebDriverTester> product, Description description) {
    }

    protected void onFinished(TestedProduct<WebDriverTester> product, Description description) {
    }
}
