package it.com.atlassian.applinks.core.rest;

import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.core.net.HttpClientResponse;
import com.atlassian.webdriver.refapp.RefappTestedProduct;
import it.com.atlassian.applinks.core.RestTestHelper;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Integration tests for Applink creation v1.0 REST endpoints.
 *
 * @since v3.11
 */
@RunWith(Parameterized.class)
@Category(RefappTest.class)
public class ManifestResourceTest {
    protected static final RefappTestedProduct PRODUCT = TestedProductFactory.create(RefappTestedProduct.class, "refapp1", null);

    private static final String REST_URL = "/rest";
    private static final String MANIFEST_REST_ENDPOINT_FORMAT = REST_URL + "/applinks/%s/manifest";
    private static final String PRODUCT_CAPABILITIES_REST_ENDPOINT_FORMAT = "%s" + MANIFEST_REST_ENDPOINT_FORMAT;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{"1.0"}, {"2.0"}, {"latest"}};
        return Arrays.asList(data);
    }

    private String productManifestRestEndPoint;

    public ManifestResourceTest(String apiVersion) {
        productManifestRestEndPoint = String.format(PRODUCT_CAPABILITIES_REST_ENDPOINT_FORMAT, PRODUCT.getProductInstance().getBaseUrl(), apiVersion);

    }

    @Test
    public void manifest() throws IOException, JSONException, ResponseException {
        final HttpClientResponse manifestResponse = RestTestHelper.getRestResponse(RestTestHelper.getDefaultUser(), productManifestRestEndPoint);

        assertThat(manifestResponse.getStatusCode(), is(200));
        JSONObject manifest = new JSONObject(manifestResponse.getResponseBodyAsString());
        assertTrue(manifest.has("id"));
        assertThat(manifest.get("typeId").toString(), is("refapp"));
        assertThat(manifest.get("buildNumber").toString(), is("123"));
        assertThat((JSONArray) manifest.get("inboundAuthenticationTypes"), any(JSONArray.class));
        assertThat((JSONArray) manifest.get("outboundAuthenticationTypes"), any(JSONArray.class));
        assertThat(manifest.get("iconUrl").toString(), endsWith("refapp.png"));
        assertThat(manifest.get("iconUri").toString(), endsWith("refapp.png"));
    }
}
