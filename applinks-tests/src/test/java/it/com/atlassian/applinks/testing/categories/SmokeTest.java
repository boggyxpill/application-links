package it.com.atlassian.applinks.testing.categories;

import it.com.atlassian.applinks.testing.runner.ApplinksSmokeTestRunner;

/**
 * JUnit Category to classify smoke tests. Tests annotated with this category should use {@link ApplinksSmokeTestRunner}
 * and follow the requirements described in its javadocs.
 *
 * @since 4.3
 */
public interface SmokeTest {
}
