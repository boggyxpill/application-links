package it.com.atlassian.applinks.testing.categories;

/**
 * JUnit Category to classify tests validating the creation process.
 *
 * @since 4.0.15
 */
public interface CreationTest {
}
