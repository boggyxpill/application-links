package it.com.atlassian.applinks.rest.migration;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.migration.AuthenticationConfig;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.backdoor.TrustedAppsBackdoor;
import com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator;
import com.atlassian.applinks.test.rest.migration.ApplinksMigrationRequest;
import com.atlassian.applinks.test.rest.migration.ApplinksMigrationRestTester;
import com.atlassian.applinks.test.rest.model.RestTrustedAppsStatus;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRequest;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRestTester;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.ApplinksCapabilitiesRule;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestRule;

import javax.ws.rs.core.Response;

import static com.atlassian.applinks.test.data.applink.config.BasicAuthenticationApplinkConfigurator.enableBasic;
import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.enableTrustedApps;
import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.setUpTrustedApps;
import static com.atlassian.applinks.test.data.applink.config.UrlApplinkConfigurator.setUrl;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectLocalAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectRemoteAuthentication;
import static com.atlassian.applinks.test.rest.specification.AuthenticationStatusExpectations.expectCapability;
import static com.atlassian.applinks.test.rest.specification.AuthenticationStatusExpectations.expectIncoming;
import static com.atlassian.applinks.test.rest.specification.AuthenticationStatusExpectations.expectOutgoing;
import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forProduct;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_USER_BARNEY;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.SYSADMIN;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static org.junit.Assert.assertFalse;
import static org.openqa.selenium.net.PortProber.findFreePort;

@Category(RefappTest.class)
public class MigrateAuthenticationResourceTest {
    // We pick just any OAuthConfig that's not disabled. Other non-disabled config also work here.
    private static final OAuthConfig ENABLED = OAuthConfig.createDefaultOAuthConfig();
    private static final OAuthConfig DISABLED = OAuthConfig.createDisabledConfig();
    private static final TestAuthentication DEFAULT_AUTH_ADMIN = REFAPP_ADMIN_BETTY;

    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);
    @Rule
    public final TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);
    @Rule
    public final ApplinksCapabilitiesRule applinksCapabilitiesRule = new ApplinksCapabilitiesRule(REFAPP1);

    private final ApplinksMigrationRestTester migrationRestTester = new ApplinksMigrationRestTester(forProduct(REFAPP1));
    private final ApplinkStatusRestTester refapp1statusTester = new ApplinkStatusRestTester(REFAPP1);
    private final TrustedAppsBackdoor trustedAppsBackdoor = new TrustedAppsBackdoor();

    @Test
    public void migrateIncomingAndOutgoingTrustedApp() {
        applink.configure(enableTrustedApps());

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectIncoming(
                        new AuthenticationConfig().oauth(ENABLED).trustedConfigured(true)
                ))
                .specification(expectOutgoing(
                        new AuthenticationConfig().oauth(ENABLED).trustedConfigured(false)
                ))
                .specification(expectCapability(ApplinksCapabilities.MIGRATION_API))
                .build());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .build());

        assertNoRemoteTrusted();
    }

    @Test
    public void migrateIncomingAndOutgoingTrustedAppWithNonSysAdminBasic() {
        applink.configureFrom(enableBasic(REFAPP_USER_BARNEY));
        applink.configure(enableTrustedApps());

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectIncoming(
                        new AuthenticationConfig().oauth(ENABLED).trustedConfigured(true)
                ))
                .specification(expectOutgoing(
                        new AuthenticationConfig().oauth(ENABLED).trustedConfigured(false)
                ))
                .build());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .build());

        assertNoRemoteTrusted();
    }

    @Test
    public void migrateOutgoingBasic() {
        applink.configureFrom(enableBasic(SYSADMIN));

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectIncoming(new AuthenticationConfig().oauth(DISABLED)))
                .specification(expectOutgoing(new AuthenticationConfig().oauth(ENABLED)))
                .specification(expectCapability(ApplinksCapabilities.MIGRATION_API))
                .build());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectLocalAuthentication(new ApplinkOAuthStatus(OAuthConfig.createDisabledConfig(), OAuthConfig.createDefaultOAuthConfig())))
                .specification(expectRemoteAuthentication(new ApplinkOAuthStatus(OAuthConfig.createDefaultOAuthConfig(), OAuthConfig.createDisabledConfig())))
                .build());
    }

    @Test
    public void migrateIncomingAndOutgoingBasic() {
        applink.configureFrom(enableBasic(SYSADMIN));
        applink.configureTo(enableBasic(SYSADMIN));

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectIncoming(new AuthenticationConfig().oauth(ENABLED).basicConfigured(true)))
                .specification(expectOutgoing(new AuthenticationConfig().oauth(ENABLED)))
                .specification(expectCapability(ApplinksCapabilities.MIGRATION_API))
                .build());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectLocalAuthentication(new ApplinkOAuthStatus(OAuthConfig.createDefaultOAuthConfig(), OAuthConfig.createDefaultOAuthConfig())))
                .specification(expectRemoteAuthentication(new ApplinkOAuthStatus(OAuthConfig.createDefaultOAuthConfig(), OAuthConfig.createDefaultOAuthConfig())))
                .build());
    }

    @Test
    public void migrateOneDirectionOutgoingTrustedApp() {
        applink.configureFrom(setUpTrustedApps(false, true));
        applink.configureTo(setUpTrustedApps(true, false));

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectIncoming(new AuthenticationConfig().oauth(DISABLED)))
                .specification(expectOutgoing(new AuthenticationConfig().oauth(ENABLED)))
                .specification(expectCapability(ApplinksCapabilities.MIGRATION_API))
                .build());

        assertNoRemoteTrusted();
    }

    @Test
    public void migrateOneDirectionIncomingTrustedAppShouldNotChangeTheAuthenticationStatus() {
        applink.configureFrom(setUpTrustedApps(true, false));
        applink.configureTo(setUpTrustedApps(false, true));

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectIncoming(new AuthenticationConfig().oauth(DISABLED).trustedConfigured(true)))
                .specification(expectOutgoing(new AuthenticationConfig().oauth(DISABLED)))
                .specification(expectCapability(ApplinksCapabilities.MIGRATION_API))
                .build());
    }

    @Test
    public void migrateShouldUseOAuthToRemoveRemoteTrusted() {
        applink.configure(OAuthApplinkConfigurator.enableOAuthWithImpersonation());
        applink.configureFrom(setUpTrustedApps(true, false));
        applink.configureTo(setUpTrustedApps(false, true));

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectIncoming(new AuthenticationConfig().oauth(ENABLED).trustedConfigured(true)))
                .specification(expectOutgoing(new AuthenticationConfig().oauth(ENABLED)))
                .specification(expectCapability(ApplinksCapabilities.MIGRATION_API))
                .build());

        assertNoRemoteTrusted();
    }

    @Test
    public void migrateShouldMigrateToOAuthEvenWhenIncomingAlreadyIsOAuth() {
        applink.configureFrom(OAuthApplinkConfigurator.enableOAuth(OAuthConfig.createOAuthWithImpersonationConfig(), OAuthConfig.createDisabledConfig()));
        applink.configureTo(OAuthApplinkConfigurator.enableOAuth(OAuthConfig.createDisabledConfig(), OAuthConfig.createOAuthWithImpersonationConfig()));
        applink.configure(setUpTrustedApps(true, true));

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectIncoming(new AuthenticationConfig().oauth(ENABLED).trustedConfigured(true)))
                .specification(expectOutgoing(new AuthenticationConfig().oauth(ENABLED)))
                .specification(expectCapability(ApplinksCapabilities.MIGRATION_API))
                .build());

        assertNoRemoteTrusted();
    }

    @Test
    public void migrateWithoutRemoteSysAdminPermissionShouldNotChangeAuthentications() {
        applink.configureFrom(setUpTrustedApps(false, true));

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectIncoming(new AuthenticationConfig().oauth(DISABLED)))
                .specification(expectOutgoing(new AuthenticationConfig().oauth(DISABLED).trustedConfigured(true)))
                .specification(expectCapability(ApplinksCapabilities.MIGRATION_API))
                .build());
    }

    @Test
    public void migrateWithoutLocalSysAdminPermissionForTrustedShouldBeForbidden() {
        applink.configureFrom(setUpTrustedApps(true, true));

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(REFAPP_ADMIN_BETTY)
                .expectStatus(Response.Status.FORBIDDEN)
                .build());
    }

    @Test
    public void migrateWithoutLocalAdminPermissionForBasicShouldBeForbidden() {
        applink.configureFrom(enableBasic(SYSADMIN));

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(REFAPP_USER_BARNEY)
                .expectStatus(Response.Status.FORBIDDEN)
                .build());
    }

    @Test
    public void migrateWithExistingOAuthWillDropOutgoingTrustedAndLeaveOAuthAlone() {
        applink.configure(enableTrustedApps());
        applink.configure(OAuthApplinkConfigurator.enableDefaultOAuth());

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.OK)
                .specification(expectIncoming(new AuthenticationConfig().oauth(ENABLED).trustedConfigured(true)))
                .specification(expectOutgoing(new AuthenticationConfig().oauth(ENABLED)))
                .specification(expectCapability(ApplinksCapabilities.MIGRATION_API))
                .build());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.DEFAULT))
                .build());

        assertNoRemoteTrusted();
    }

    @Test
    public void migrateBrokenApplink() {
        applink.configure(enableTrustedApps()).configureFrom(setUrl(loopBackUrlWithPort(REFAPP2, findFreePort())));

        migrationRestTester.post(new ApplinksMigrationRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .expectStatus(Response.Status.CONFLICT)
                .build());
    }

    private void assertNoRemoteTrusted() {
        final RestTrustedAppsStatus trustedAppsStatus = trustedAppsBackdoor.getStatus(applink.to());
        assertFalse("Unexpected incoming trusted apps config", trustedAppsStatus.getIncoming());
        assertFalse("Unexpected outgoing trusted apps config", trustedAppsStatus.getOutgoing());
    }

    private static String loopBackUrlWithPort(ProductInstances instance, int port) {
        return instance.getLoopbackUrl().replace(Integer.toString(instance.getHttpPort()), Integer.toString(port));
    }
}
