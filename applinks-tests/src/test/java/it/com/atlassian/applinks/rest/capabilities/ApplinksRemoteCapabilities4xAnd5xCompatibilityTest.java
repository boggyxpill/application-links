package it.com.atlassian.applinks.rest.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.test.rest.capabilities.ApplinksCapabilitiesRestTester;
import com.atlassian.applinks.test.rest.capabilities.ApplinksRemoteCapilitiesRequest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.ApplinksVersionOverrideRule;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestRule;

import static com.atlassian.applinks.test.rest.matchers.RestApplicationVersionMatchers.expectValidVersion;
import static com.atlassian.applinks.test.rest.matchers.RestApplicationVersionMatchers.expectVersion;
import static com.atlassian.applinks.test.rest.matchers.RestMatchers.containsAllEnumValues;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.applicationVersion;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.applinksVersion;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.capabilities;
import static com.atlassian.applinks.test.rest.specification.ApplinksRemoteCapabilitiesExpectations.expectNoError;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static org.hamcrest.Matchers.emptyIterable;

/**
 * Test correct response from Applinks capabilities resource given Applinks versions pre capabilities (4.0.x and
 * pre-5.0.5)
 */
@Category(RefappTest.class)
public class ApplinksRemoteCapabilities4xAnd5xCompatibilityTest {
    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);
    @Rule
    public final TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);
    @Rule
    public final ApplinksVersionOverrideRule refapp2ApplinksVersion = new ApplinksVersionOverrideRule(REFAPP2);

    private final ApplinksCapabilitiesRestTester capabilitiesRestTester = new ApplinksCapabilitiesRestTester(REFAPP1);

    @Test
    public void emptyCapabilitiesGivenApplinks40() {
        refapp2ApplinksVersion.setApplinksVersion("4.3.5");

        // need to get fresh capabilities to make sure the Applinks version change is picked up
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .maxAge(0)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectVersion(4, 3, 5)))
                .specification(capabilities(emptyIterable()))
                .specification(expectNoError())
                .build());
    }

    @Test
    public void emptyCapabilitiesGivenApplinks504() {
        refapp2ApplinksVersion.setApplinksVersion("5.0.4");

        // need to get fresh capabilities to make sure the Applinks version change is picked up
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .maxAge(0)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectVersion(5, 0, 4)))
                .specification(capabilities(emptyIterable()))
                .specification(expectNoError())
                .build());
    }

    @Test
    public void nonEmptyCapabilitiesGivenApplinks505() {
        refapp2ApplinksVersion.setApplinksVersion("5.0.5");

        // need to get fresh capabilities to make sure the Applinks version change is picked up
        capabilitiesRestTester.getRemote(new ApplinksRemoteCapilitiesRequest.Builder(applink.from().id())
                .authentication(REFAPP_ADMIN_BETTY)
                .maxAge(0)
                .specification(applicationVersion(expectValidVersion()))
                .specification(applinksVersion(expectVersion(5, 0, 5)))
                .specification(capabilities(containsAllEnumValues(ApplinksCapabilities.class)))
                .specification(expectNoError())
                .build());
    }
}
