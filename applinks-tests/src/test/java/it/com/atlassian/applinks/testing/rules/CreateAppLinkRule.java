package it.com.atlassian.applinks.testing.rules;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import com.atlassian.applinks.pageobjects.SimpleApplinksClient;
import com.atlassian.pageobjects.TestedProduct;

import org.junit.rules.RuleChain;

/**
 * Creates new links with no auth between the specified instances.
 *
 * @since 5.0.0
 */
public class CreateAppLinkRule extends AbstractProductLinkingRule {
    /**
     * Apply the rule to each product.
     */
    public static RuleChain forProducts(TestedProduct<?>... products) {
        RuleChain chain = RuleChain.emptyRuleChain();

        for (final TestedProduct<?> product : products) {
            // apply the rule to each localProduct, linking to the other products.
            chain = chain.around(new CreateAppLinkRule(product, getOtherProducts(product, products)));
        }
        return chain;
    }

    @Inject
    public CreateAppLinkRule(@Nonnull TestedProduct<?> localProduct, final Iterable<TestedProduct<?>> remoteProducts) {
        super(localProduct, remoteProducts);
    }

    @Override
    protected void createLink(final TestedProduct<?> localProduct, final TestedProduct<?> remoteProduct) {
        SimpleApplinksClient.createAtlassianAppLinkViaRest(localProduct, remoteProduct);
    }
}

