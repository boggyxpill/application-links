package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.api.application.refapp.RefAppCharlieEntityType;
import com.atlassian.applinks.pageobjects.OAuthApplinksClient;

import com.atlassian.webdriver.applinks.page.adg.entitylinks.ConfigureEntityLinksPage;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.rules.CreateAppLinkWith3LO2LORule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Category(RefappTest.class)
public class CreateAndDeleteUALEntityLinkWithOAuthTestRefApp extends V2AbstractApplinksTest {

    @Rule
    public RuleChain ruleChain = CreateAppLinkWith3LO2LORule.forProducts(PRODUCT, PRODUCT2);

    private static final String REMOTE_PROJECT_NAME = "Schultz";

    @Before
    public void setUp() throws Exception {
        login(PRODUCT, PRODUCT2);
        OAuthApplinksClient.createCharlie("sheen", "Hot Shots II");
        OAuthApplinksClient.createCharlie(PRODUCT2, "brown", REMOTE_PROJECT_NAME, ADMIN_PASSWORD);
    }

    @Test
    public void createAndDeleteProjectLink() {
        ConfigureEntityLinksPage.EntityLinkRow firstRow =
                PRODUCT.visit(ConfigureEntityLinksPage.class, RefAppCharlieEntityType.class, "sheen")
                        .clickAddLink()
                        .selectApplication("RefApp")
                        .authorize()
                        .confirmHandlingWebLoginIfRequired(ADMIN_USERNAME, ADMIN_PASSWORD)
                        .selectProject(REMOTE_PROJECT_NAME)
                        .create()
                        .getRows()
                        .get(0);

        assertEquals(firstRow.getName(), REMOTE_PROJECT_NAME);
        assertTrue(firstRow.isPrimary());

        boolean emptyState = firstRow.delete()
                .confirm()
                .isEmptyState();
        assertTrue(emptyState);
    }
}
