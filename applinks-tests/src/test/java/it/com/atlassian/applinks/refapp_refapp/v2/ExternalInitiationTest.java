package it.com.atlassian.applinks.refapp_refapp.v2;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.Tester;
import com.atlassian.webdriver.applinks.component.v2.CreateUalDialog;
import com.atlassian.webdriver.applinks.component.v2.InformationDialog;
import com.atlassian.webdriver.applinks.component.v2.PauseDialog;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;
import java.net.URLEncoder;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasDisplayUrl;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

/**
 * Test the external initiation of the create applink process.
 *
 * @since v4.0
 */
@Category({RefappTest.class})
public class ExternalInitiationTest extends V2AbstractApplinksTest {
    @Inject
    Tester tester;

    @Inject
    protected PageBinder pageBinder;

    final String initiatorRedirect = PRODUCT2.getProductInstance().getBaseUrl() + "/invalid/url";

    @Test
    public void verifyExternalInitiationIsNotTriggeredWithOutParams()
            throws Exception {
        loginAsSysadmin(PRODUCT);

        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);

        // application url is not set
        assertThat(listApplicationLinkPage.getApplicationUrl(), is(""));

        // dialog is not showing
        assertFalse(listApplicationLinkPage.getCreateUalDialog().isPresent());
    }

    @Test
    public void verifyExternalInitiationIsTriggeredWithInitiationParameters()
            throws Exception {
        loginAsSysadmin(PRODUCT);

        String externalInitiationUrl = PRODUCT.getProductInstance().getBaseUrl()
                + new com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage().getUrl()
                + "?initiatorTargetUrl="
                + PRODUCT2.getProductInstance().getBaseUrl();
        PRODUCT.getTester().gotoUrl(externalInitiationUrl);
        CreateUalDialog createUalDialog = PRODUCT.getPageBinder().bind(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class)
                .getCreateUalDialog();

        // dialog is showing
        assertTrue(createUalDialog.isPresent());

        // application url is set
        assertThat(createUalDialog.getFromAppDetail().getApplicationUrl(), is(PRODUCT2.getProductInstance().getBaseUrl()));
    }

    @Test
    public void verifyExternalInitiationTriggeredProcessWorks() {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        String externalInitiationUrl = PRODUCT.getProductInstance().getBaseUrl()
                + new com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage().getUrl()
                + "?initiatorTargetUrl="
                + PRODUCT2.getProductInstance().getBaseUrl();
        PRODUCT.getTester().gotoUrl(externalInitiationUrl);
        ListApplicationLinkPage listApplicationLink = PRODUCT.getPageBinder().bind(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);
        listApplicationLink.getCreateUalDialog()
                .clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplicationCreateUalDialog(PRODUCT2.getProductInstance().getBaseUrl())
                .clickContinueToCreateUalLink();

        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(PRODUCT.getProductInstance().getBaseUrl());

        waitUntil(listApplicationLink.getApplicationLinksTimed(), contains(hasDisplayUrl(PRODUCT2.getProductInstance().getBaseUrl())));
    }

    @Test
    public void verifyExternalInitiationOfDuplicateInformsUserAndRedirects() throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);
        // create a link
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);
        // enter the target url
        listApplicationLinkPage.setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())
                // click "create link"
                .clickCreateNewLinkAndOpenDialog()
                // view and confirm the link details
                .clickContinueToCreateUalLink()
                // redirected to the target application
                .handleRedirectionToRemoteApplicationCreateUalDialog(PRODUCT2.getProductInstance().getBaseUrl())
                // view and confirm the link details
                .clickContinueToCreateUalLink();

        // make sure we are back in product 1
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(PRODUCT2.getProductInstance().getBaseUrl());

        // try and create a duplicate
        String externalInitiationUrl = PRODUCT.getProductInstance().getBaseUrl()
                + new com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage().getUrl()
                + "?initiatorTargetUrl="
                + PRODUCT2.getProductInstance().getBaseUrl()
                + "&initiatorRedirect="
                + initiatorRedirect;
        PRODUCT.getTester().gotoUrl(externalInitiationUrl);
        PRODUCT.getPageBinder().bind(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class)
                .getInformationDialog()
                .clickContinue();

        // back to product 1
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(PRODUCT.getProductInstance().getBaseUrl());
        // then on to final destination
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(initiatorRedirect);

        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("applinksCreationStatus=%22failed%22"));
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("applinksCreationStatusReason=%22localDuplicateLinkExists%22"));
        // no local link was created
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), not(containsString("applinksCreatedId")));

    }

    @Test
    public void verifyExternalInitiationTriggeredProcessWorksWithFinalRedirect() throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        String externalInitiationUrl = PRODUCT.getProductInstance().getBaseUrl()
                + new com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage().getUrl()
                + "?initiatorTargetUrl="
                + PRODUCT2.getProductInstance().getBaseUrl()
                + "&initiatorRedirect="
                + initiatorRedirect;
        PRODUCT.getTester().gotoUrl(externalInitiationUrl);
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.getPageBinder().bind(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);
        listApplicationLinkPage.getCreateUalDialog()
                .clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplicationNoClose()
                .clickContinueToCreateUalLink();

        // back to product 1
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(PRODUCT.getProductInstance().getBaseUrl());
        // then on to final destination
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(initiatorRedirect);

        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("applinksCreationStatus=%22created%22"));
    }

    @Test
    public void verifyExternalInitiationTriggeredProcessWorksWithRedirectWithoutProtocol() throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        String externalInitiationUrl = PRODUCT.getProductInstance().getBaseUrl()
                + new com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage().getUrl()
                + "?initiatorTargetUrl="
                + PRODUCT2.getProductInstance().getBaseUrl()
                + "&initiatorRedirect="
                + initiatorRedirect.substring("http://".length());
        PRODUCT.getTester().gotoUrl(externalInitiationUrl);
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.getPageBinder().bind(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);

        listApplicationLinkPage.getCreateUalDialog()
                .clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplicationNoClose()
                .clickContinueToCreateUalLink();

        // back to product 1
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(PRODUCT.getProductInstance().getBaseUrl());
        // then on to final destination
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(initiatorRedirect);

        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), startsWith(initiatorRedirect + "?"));
    }

    @Test
    public void verifyExternalInitiationTriggeredProcessWorksWithEncodedComplexFinalRedirect() throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        String externalInitiationUrl = PRODUCT.getProductInstance().getBaseUrl()
                + new com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage().getUrl()
                + "?initiatorTargetUrl="
                + PRODUCT2.getProductInstance().getBaseUrl()
                + "&initiatorRedirect="
                + URLEncoder.encode(initiatorRedirect + "?a=1&url=http://www.google.com", "UTF-8");
        PRODUCT.getTester().gotoUrl(externalInitiationUrl);
        ListApplicationLinkPage listApplicationLink = PRODUCT.getPageBinder().bind(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);
        listApplicationLink.getCreateUalDialog()
                .clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplicationNoClose()
                .clickContinueToCreateUalLink();

        // back to product 1
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(PRODUCT.getProductInstance().getBaseUrl());
        // then on to final destination
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(initiatorRedirect);

        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("applinksCreationStatus=%22created%22"));
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("a=1"));
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("url=http://www.google.com"));
    }

    @Test
    public void verifyExternalInitiationTriggeredProcessLosesParametersWithUnencodedComplexFinalRedirect() throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        String externalInitiationUrl = PRODUCT.getProductInstance().getBaseUrl()
                + new com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage().getUrl()
                + "?initiatorTargetUrl="
                + PRODUCT2.getProductInstance().getBaseUrl()
                + "&initiatorRedirect="
                + URLEncoder.encode(initiatorRedirect + "?a=1&url=http://www.google.com", "UTF-8");
        PRODUCT.getTester().gotoUrl(externalInitiationUrl);
        ListApplicationLinkPage listApplicationLink = PRODUCT.getPageBinder().bind(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);

        listApplicationLink.getCreateUalDialog()
                .clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplicationNoClose()
                .clickContinueToCreateUalLink();

        // back to product 1
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(PRODUCT.getProductInstance().getBaseUrl());
        // then on to final destination
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(initiatorRedirect);

        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("applinksCreationStatus=%22created%22"));
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("a=1"));
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("url=http://www.google.com"));
    }

    @Test
    public void verifyUncheckedAdminReturnsLocalApplicationLinkId() throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        String externalInitiationUrl = PRODUCT.getProductInstance().getBaseUrl()
                + new com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage().getUrl()
                + "?initiatorTargetUrl="
                + PRODUCT2.getProductInstance().getBaseUrl()
                + "&initiatorRedirect="
                + URLEncoder.encode(initiatorRedirect + "?a=1&url=http://www.google.com", "UTF-8");
        PRODUCT.getTester().gotoUrl(externalInitiationUrl);
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.getPageBinder().bind(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);
        CreateUalDialog createUalDialog = listApplicationLinkPage.getCreateUalDialog();

        createUalDialog.unCheckAdmin()
                .clickContinueToCreateUalLink()
                .clickClose();

        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(initiatorRedirect);

        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("applinksCreationStatus=%22failed%22"));
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("applinksCreationStatusReason=%22reciprocalCreationSkipped%22"));
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("a=1"));
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("url=http://www.google.com"));
        // local link was created
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("applinksCreatedId"));
    }

    @Test
    public void verifyRemoteDuplicateReturnsLocalApplicationLinkId() throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        // create the first 1 ended link at the remote end
        PRODUCT2.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class)
                .setApplicationUrl(PRODUCT.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenDialog()
                .unCheckAdmin()
                .clickContinueToCreateUalLink();

        String externalInitiationUrl = PRODUCT.getProductInstance().getBaseUrl()
                + new com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage().getUrl()
                + "?initiatorTargetUrl="
                + PRODUCT2.getProductInstance().getBaseUrl()
                + "&initiatorRedirect="
                + URLEncoder.encode(initiatorRedirect + "?a=1&url=http://www.google.com", "UTF-8");
        PRODUCT.getTester().gotoUrl(externalInitiationUrl);
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.getPageBinder().bind(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);

        listApplicationLinkPage.getCreateUalDialog()
                .clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplication(PRODUCT2.getProductInstance().getBaseUrl());

        InformationDialog informationDialog = listApplicationLinkPage.getInformationDialog();

        assertThat(informationDialog.singleDialogIsPresent(), is(true));
        assertThat(informationDialog.getWarning(), containsString(String.format("An Application Link to this URL '%s' has already been configured.", PRODUCT.getProductInstance().getBaseUrl())));

        informationDialog.clickContinue();
        PauseDialog pauseDialog = PRODUCT.getPageBinder().bind(PauseDialog.class);

        // wait for redirect back to Product 1
        pauseDialog.handleRedirectionToRemoteApplication(PRODUCT.getProductInstance().getBaseUrl());

        // wait for redirect to final destination in Product 2
        pauseDialog.handleRedirectionToRemoteApplication(initiatorRedirect);

        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("applinksCreationStatus=%22failed%22"));
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("applinksCreationStatusReason=%22reciprocalDuplicateLinkExists%22"));
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("a=1"));
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("url=http://www.google.com"));
        // local link was created
        assertThat(PRODUCT.getTester().getDriver().getCurrentUrl(), containsString("applinksCreatedId"));
    }
}
