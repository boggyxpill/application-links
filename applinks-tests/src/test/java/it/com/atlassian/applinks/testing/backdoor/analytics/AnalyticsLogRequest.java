package it.com.atlassian.applinks.testing.backdoor.analytics;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.rest.AbstractRestRequest;
import com.google.common.collect.ImmutableMap;
import com.jayway.restassured.specification.ResponseSpecification;

import javax.annotation.Nonnull;
import java.util.Map;

import static java.util.Objects.requireNonNull;


/**
 * Request to get analytics logs.
 *
 * @since 4.3
 */
public class AnalyticsLogRequest extends AbstractRestRequest {
    @Nonnull
    public static AnalyticsLogRequest expecting(@Nonnull ResponseSpecification... specifications) {
        requireNonNull(specifications, "specification");
        return new AnalyticsLogRequest.Builder().specifications(specifications).build();
    }

    private final AnalyticsLogMode logMode;

    private AnalyticsLogRequest(@Nonnull Builder builder) {
        super(builder);
        this.logMode = builder.logMode;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.EMPTY;
    }

    @Nonnull
    @Override
    protected Map<String, Object> getExtraQueryParams() {
        return ImmutableMap.<String, Object>of("mode", logMode.modeId);
    }

    public static class Builder extends AbstractBuilder<Builder, AnalyticsLogRequest> {
        private AnalyticsLogMode logMode = AnalyticsLogMode.DEFAULT;

        @Nonnull
        @Override
        public AnalyticsLogRequest build() {
            return new AnalyticsLogRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
