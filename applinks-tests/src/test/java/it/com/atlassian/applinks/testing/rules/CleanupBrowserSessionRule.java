package it.com.atlassian.applinks.testing.rules;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.applinks.page.EchoPage;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.testing.rule.FailsafeExternalResource;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;

import static java.util.Arrays.asList;

/**
 * A rule to clean-up cookies in the browser for a number of products.
 *
 * @since 3.10.7
 */
public final class CleanupBrowserSessionRule extends FailsafeExternalResource {
    private final Iterable<TestedProduct<WebDriverTester>> products;

    public CleanupBrowserSessionRule(Iterable<TestedProduct<WebDriverTester>> products) {
        this.products = products;
    }

    public CleanupBrowserSessionRule(TestedProduct<WebDriverTester>... products) {
        this(asList(products));
    }

    @Override
    protected void before() throws Throwable {
        cleanUp();
    }

    private void cleanUp() {
        for (TestedProduct<WebDriverTester> product : products) {
            final WebDriver driver = product.getTester().getDriver();
            product.visit(EchoPage.class);
            try {
                driver.manage().deleteAllCookies();
            } catch (UnhandledAlertException uhaex) {
                // if there is an alert, dismiss it and then try to remove the cookies again.
                try {
                    driver.switchTo().alert().dismiss();
                } catch (NoAlertPresentException nopex) {
                    // do nothing it seems to have gone...
                } finally {
                    driver.manage().deleteAllCookies();
                }
            }
        }
    }
}



