package it.com.atlassian.applinks;

import com.atlassian.applinks.pageobjects.LoginClient;
import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.webdriver.refapp.RefappTestedProduct;
import com.atlassian.webdriver.refapp.page.RefappHomePage;
import com.atlassian.webdriver.refapp.page.RefappLoginPage;
import it.com.atlassian.applinks.testing.ApplinksAuthentications;
import it.com.atlassian.applinks.testing.product.ProductInstances;

import static junit.framework.Assert.assertTrue;

/**
 * Class contains static helper methods coe interacting with an application test instance
 *
 * @see LoginClient
 * @see ApplinksAuthentications
 * @since v4.0.0
 * @deprecated for logging in operations use {@link LoginClient} instead
 */
@Deprecated
public class ApplicationTestHelper {
    // RefApp's default user set.
    public static final String ADMIN_USERNAME = "betty";
    public static final String ADMIN_PASSWORD = "betty";
    public static final String SYSADMIN_USERNAME = "admin";
    public static final String SYSADMIN_PASSWORD = "admin";
    public static final String REFAPP_PRODUCT_NAME_FORMAT = "RefApp - %s";

    private ApplicationTestHelper() {
    }

    public static void loginAsAdmin(RefappTestedProduct... products) {
        login(ADMIN_USERNAME, ADMIN_PASSWORD, products);
    }

    public static void loginAsAdmin(RefappTestedProduct product) {
        login(ADMIN_USERNAME, ADMIN_PASSWORD, product);
    }

    public static void loginAsSysadmin(RefappTestedProduct... products) {
        login(SYSADMIN_USERNAME, SYSADMIN_PASSWORD, products);
    }

    public static void loginAsSysadmin(RefappTestedProduct product) {
        login(SYSADMIN_USERNAME, SYSADMIN_PASSWORD, product);
    }

    public static void login(String username, String password, RefappTestedProduct... products) {
        for (RefappTestedProduct product : products) {
            login(username, password, product);
        }
    }

    public static void login(String username, String password, RefappTestedProduct product) {
        RefappLoginPage loginPage = product.visit(RefappLoginPage.class);
        RefappHomePage homePage = loginPage.login(username, password, RefappHomePage.class);
        assertTrue("Must have logged in.", homePage.getHeader().isLoggedIn());
    }

    public static String getProductName(ProductInstance productInstance) {
        if (ProductInstances.matches(SupportedProducts.REFAPP, productInstance)) {
            return String.format(REFAPP_PRODUCT_NAME_FORMAT, ProductInstances.getLocalHostName(productInstance));
        }

        throw new UnsupportedOperationException("Not implemented");
    }
}
