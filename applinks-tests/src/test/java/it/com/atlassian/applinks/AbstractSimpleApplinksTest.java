package it.com.atlassian.applinks;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;

import org.junit.Rule;
import org.junit.rules.RuleChain;

import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;

/**
 * Abstract class providing scaffolding for integration tests against the basic Applinks plugin.
 *
 * @since 5.0.0
 */
public abstract class AbstractSimpleApplinksTest {
    protected TestedProduct productOne;
    protected TestedProduct productTwo;

    public static final String SYSADMIN_USERNAME = "admin";
    public static final String SYSADMIN_PASSWORD = "admin";

    @Rule
    public final RuleChain testRules = ApplinksRuleChain.forProducts(getProductOne(), getProductTwo());

    public TestedProduct getProductTwo() {
        if (productTwo == null) {
            String productClassName = System.getProperty("product.two.classname", getDefaultProductTwoClass().getCanonicalName());
            productTwo = getTestedProduct(productClassName);
        }
        return productTwo;
    }

    public TestedProduct getProductOne() {
        if (productOne == null) {
            String productClassName = System.getProperty("product.one.classname", getDefaultProductOneClass().getCanonicalName());
            productOne = getTestedProduct(productClassName);
        }
        return productOne;
    }

    public Class<? extends TestedProduct> getDefaultProductOneClass() {
        return Refapp4TestedProduct.class;
    }

    public Class<? extends TestedProduct> getDefaultProductTwoClass() {
        return Refapp5TestedProduct.class;
    }

    private TestedProduct getTestedProduct(String productClassName) {
        final Class c;
        try {
            c = Class.forName(productClassName);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Unable to determine tested product", e);
        }

        return TestedProductFactory.create(c);
    }
}
