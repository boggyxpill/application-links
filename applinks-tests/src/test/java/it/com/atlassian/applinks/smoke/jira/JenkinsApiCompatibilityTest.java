package it.com.atlassian.applinks.smoke.jira;

import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.applinks.test.rest.applink.ApplinkV2Request;
import com.atlassian.applinks.test.rest.applink.ApplinksV2RestTester;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.applinks.test.rest.backdoor.BackdoorCreateSideApplinkRequest;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import it.com.atlassian.applinks.smoke.AbstractApplinksSmokeTest;
import it.com.atlassian.applinks.testing.categories.SmokeTest;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.product.Products;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.test.authentication.TestAuthentication.admin;
import static com.atlassian.applinks.test.data.util.TestUrls.createRandomUrl;
import static javax.ws.rs.core.Response.Status.OK;

/**
 * This class uses the jira-jenkins-plugin to ensure that changes that are made do not break API.
 * There should hopefully be no cases where non-api changes break the jira-jenkins-plugin
 */
@Category(SmokeTest.class)
@Products(SupportedProducts.JIRA)
public class JenkinsApiCompatibilityTest extends AbstractApplinksSmokeTest<JiraTestedProduct> {
    private final ApplinksBackdoor applinksBackdoor;

    public JenkinsApiCompatibilityTest(@Nonnull JiraTestedProduct mainProduct,
                                       @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
        applinksBackdoor = new ApplinksBackdoor(mainInstance);
    }

    /**
     * This tests for the failure in JRA-44951
     */
    @Test
    public void listingApplicationLinksShouldWorkWhenJenkinsApplinkExists() {
        TestApplink.Side jenkinsLink = applinksBackdoor.createSide(
                new BackdoorCreateSideApplinkRequest.Builder(createRandomUrl()).typeId("jenkins").build());

        // in the event of failure, this request will return with a 500 error and result in AssertionError
        new ApplinksV2RestTester(mainInstance).get(new ApplinkV2Request.Builder(jenkinsLink)
                .authentication(admin())
                .expectStatus(OK)
                .build());
    }
}
