package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.backdoor.SystemPropertiesBackdoor;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import java.util.EnumSet;

import static com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilitiesKeys.capabilityKeyFor;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Allows for disabling Applinks capabilities during tests. Restores all capabilities after the test has run.
 *
 * @since 4.3
 */
public class ApplinksCapabilitiesRule extends TestWatcher {
    private static final EnumSet<ApplinksCapabilities> EMPTY_CAPABILITIES = EnumSet.noneOf(ApplinksCapabilities.class);

    private final TestedInstance instance;
    private final SystemPropertiesBackdoor systemPropertiesBackdoor;
    private final EnumSet<ApplinksCapabilities> capabilitiesToDisable;

    private final EnumSet<ApplinksCapabilities> disabledCapabilities = EnumSet.noneOf(ApplinksCapabilities.class);

    public ApplinksCapabilitiesRule(@Nonnull TestedInstance instance) {
        this(instance, EMPTY_CAPABILITIES);
    }

    private ApplinksCapabilitiesRule(@Nonnull TestedInstance instance, EnumSet<ApplinksCapabilities> disabled) {
        this.instance = checkNotNull(instance, "instance");
        this.systemPropertiesBackdoor = new SystemPropertiesBackdoor(instance);
        this.capabilitiesToDisable = checkNotNull(disabled);
    }

    @Nonnull
    public ApplinksCapabilitiesRule withDisabledCapabilities(@Nonnull ApplinksCapabilities first,
                                                             @Nonnull ApplinksCapabilities... more) {
        return new ApplinksCapabilitiesRule(instance, EnumSet.of(first, more));
    }

    public final void disable(@Nonnull ApplinksCapabilities capability) {
        disabledCapabilities.add(capability);
        systemPropertiesBackdoor.setProperty(capabilityKeyFor(capability), Boolean.FALSE.toString());
    }

    @Override
    protected void starting(Description description) {
        for (ApplinksCapabilities toDisable : capabilitiesToDisable) {
            disable(toDisable);
        }
    }

    @Override
    protected void finished(Description description) {
        for (ApplinksCapabilities capability : disabledCapabilities) {
            systemPropertiesBackdoor.clearProperty(capabilityKeyFor(capability));
        }
    }
}
