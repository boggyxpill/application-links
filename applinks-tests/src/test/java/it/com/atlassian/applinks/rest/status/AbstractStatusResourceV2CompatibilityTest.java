package it.com.atlassian.applinks.rest.status;

import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRequest;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRestTester;
import com.atlassian.applinks.test.rule.UserManagementRule;
import it.com.atlassian.applinks.testing.ApplinksAuthentications;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static com.atlassian.applinks.internal.common.net.Uris.utf8Encode;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDefaultOAuthConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDisabledConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createOAuthWithImpersonationConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createThreeLoOnlyConfig;
import static com.atlassian.applinks.test.rest.data.applink.DeleteApplinkConfigurator.deleteApplink;
import static com.atlassian.applinks.test.rest.data.applink.config.DeleteAccessTokenApplinkConfigurator.deleteServiceProviderToken;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.disableOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enable3LoOnly;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableOAuthWithImpersonation;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthDanceConfigurator.setUp3LoAccess;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectAuthorisationUri;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectErrorDetails;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectLocalAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectNoAuthorisationUri;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectNoRemoteAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectRemoteAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectStatusError;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectStatusWorking;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertTrue;

/**
 * Base class for Status resource tests running against an instance without Status API. This requires making authenticated requests and may
 * result in additional error conditions.
 *
 * @since 5.0
 */
public abstract class AbstractStatusResourceV2CompatibilityTest {
    protected static final TestAuthentication DEFAULT_AUTH_ADMIN = REFAPP_ADMIN_BETTY;
    protected static final TestAuthentication USER_BOB = TestAuthentication.authenticatedWith("bob");

    private static ApplinkOAuthStatus STATUS_THREE_LO_ONLY = new ApplinkOAuthStatus(
            createThreeLoOnlyConfig(), createThreeLoOnlyConfig());

    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);
    @Rule
    public final TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);

    @Rule
    public final UserManagementRule refapp1Users = new UserManagementRule(REFAPP1);
    @Rule
    public final UserManagementRule refapp2Users = new UserManagementRule(REFAPP2);

    protected final ApplinkStatusRestTester refapp1statusTester = new ApplinkStatusRestTester(REFAPP1);

    private boolean verificationCallbackInvoked = false;

    @After
    public void assertVerificationCallbackInvoked() {
        // make sure the verification callback was invoked in each test
        assertTrue("Verification callback has not been invoked. You need to invoke either internalVerifyRequestToV2Made " +
                "or internalVerifyRequestToV2NotMade in each test in this class", verificationCallbackInvoked);
    }

    @Test
    public void statusConnectedDefaultOAuth() {
        // configure default OAuth on both ends and create access for the admin user
        applink.configure(enableDefaultOAuth(), setUp3LoAccess(DEFAULT_AUTH_ADMIN));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusWorking(applink.from().id()))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.DEFAULT))
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusConnectedOAuthWithImpersonation() {
        // Impersonation should enable communication on both ends without requiring OAuth dance
        applink.configure(enableOAuthWithImpersonation());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusWorking(applink.from().id()))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusConnectedDefaultOAuthOneWayOnly() {
        // Impersonation should enable communication on both ends without requiring OAuth dance
        applink.configureFrom(enableOAuth(createDisabledConfig(), createDefaultOAuthConfig()))
                .configureTo(enableOAuth(createDefaultOAuthConfig(), createDisabledConfig()))
                .configure(setUp3LoAccess(DEFAULT_AUTH_ADMIN));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusWorking(applink.from().id()))
                .specification(expectLocalAuthentication(
                        new ApplinkOAuthStatus(createDisabledConfig(), createDefaultOAuthConfig())))
                .specification(expectRemoteAuthentication(
                        new ApplinkOAuthStatus(createDefaultOAuthConfig(), createDisabledConfig())
                ))
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusConnectedOAuthWithImpersonationOneWayOnly() {
        // Impersonation should enable communication on both ends without requiring OAuth dance
        applink.configureFrom(enableOAuth(createDisabledConfig(), createOAuthWithImpersonationConfig()))
                .configureTo(enableOAuth(createOAuthWithImpersonationConfig(), createDisabledConfig()));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusWorking(applink.from().id()))
                .specification(expectLocalAuthentication(
                        new ApplinkOAuthStatus(createDisabledConfig(), createOAuthWithImpersonationConfig())))
                .specification(expectRemoteAuthentication(
                        new ApplinkOAuthStatus(createOAuthWithImpersonationConfig(), createDisabledConfig())
                ))
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusConnectedOutgoingDefaultOAuthIncomingOAuthWithImpersonation() {
        // Impersonation should enable communication on both ends without requiring OAuth dance
        applink.configureFrom(enableOAuth(createOAuthWithImpersonationConfig(), createDefaultOAuthConfig()))
                .configureTo(enableOAuth(createDefaultOAuthConfig(), createOAuthWithImpersonationConfig()))
                .configure(setUp3LoAccess(DEFAULT_AUTH_ADMIN));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusWorking(applink.from().id()))
                .specification(expectLocalAuthentication(
                        new ApplinkOAuthStatus(createOAuthWithImpersonationConfig(), createDefaultOAuthConfig())))
                .specification(expectRemoteAuthentication(
                        new ApplinkOAuthStatus(createDefaultOAuthConfig(), createOAuthWithImpersonationConfig())
                ))
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusConnectedOutgoingOAuthWithImpersonationIncomingDefaultOAuth() {
        // Impersonation should enable communication on both ends without requiring OAuth dance
        applink.configureFrom(enableOAuth(createDefaultOAuthConfig(), createOAuthWithImpersonationConfig()))
                .configureTo(enableOAuth(createOAuthWithImpersonationConfig(), createDefaultOAuthConfig()));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusWorking(applink.from().id()))
                .specification(expectLocalAuthentication(
                        new ApplinkOAuthStatus(createDefaultOAuthConfig(), createOAuthWithImpersonationConfig())))
                .specification(expectRemoteAuthentication(
                        new ApplinkOAuthStatus(createOAuthWithImpersonationConfig(), createDefaultOAuthConfig())
                ))
                .build());

        internalVerifyRequestToV2Made();
    }


    @Test
    public void statusErrorNoOutgoingAuth() {
        // no outgoing authentication provider configured

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.NO_OUTGOING_AUTH))
                .build());

        internalVerifyRequestToV2NotMade();
    }

    @Test
    public void statusErrorDefaultOAuthRemoteAuthRequiredNoLocalToken() {
        // enable default OAuth level on both sides - requires OAuth dance, local token missing
        applink.configure(enableDefaultOAuth());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .authorisationCallback(REFAPP1.getBaseUrl() + "/auth-test")
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.LOCAL_AUTH_TOKEN_REQUIRED))
                .specification(expectAuthorisationUri(allOf(
                        startsWith(REFAPP1.getBaseUrl()), // authorisationUri base URL
                        containsString(applink.from().id()), // should contain applink ID as query param
                        containsString(utf8Encode(REFAPP1.getBaseUrl() + "/auth-test"))))) // should contain callback as query param (encoded)
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());

        internalVerifyRequestToV2NotMade();
    }

    @Test
    public void statusErrorDefaultOAuthRemoteAuthRequiredNoRemoteToken() {
        // enable default OAuth level on both sides, configure 3LO access, then remove remote service provider token
        applink.configure(enableDefaultOAuth())
                .configure(setUp3LoAccess(REFAPP_ADMIN_BETTY))
                .configureTo(deleteServiceProviderToken(REFAPP_ADMIN_BETTY));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .authorisationCallback(REFAPP1.getBaseUrl() + "/auth-test")
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.REMOTE_AUTH_TOKEN_REQUIRED))
                .specification(expectAuthorisationUri(allOf(
                        startsWith(REFAPP1.getBaseUrl()), // authorisationUri base URL
                        containsString(applink.from().id()), // should contain applink ID as query param
                        containsString(utf8Encode(REFAPP1.getBaseUrl() + "/auth-test"))))) // should contain callback as query param (encoded)
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusErrorDefaultOAuthInsufficientRemotePermission() {
        // enable default OAuth level on both sides, configure 3LO access to admin as an ordinary user on the remote end
        applink.configure(
                enableDefaultOAuth(),
                setUp3LoAccess(DEFAULT_AUTH_ADMIN, ApplinksAuthentications.REFAPP_USER_BARNEY)
        );

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .authorisationCallback(REFAPP1.getBaseUrl() + "/auth-test")
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.INSUFFICIENT_REMOTE_PERMISSION))
                .specification(expectAuthorisationUri(allOf(
                        startsWith(REFAPP1.getBaseUrl()), // authorisationUri base URL
                        containsString(applink.from().id()), // should contain applink ID as query param
                        containsString(utf8Encode(REFAPP1.getBaseUrl() + "/auth-test"))))) // should contain callback as query param (encoded)
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusErrorOAuthWithImpersonationNoRemoteUser() {
        // create user "bob" with no corresponding user on REFAPP2
        refapp1Users.createUser(USER_BOB, PermissionLevel.ADMIN);
        // enable OAuth with impersonation
        applink.configure(enableOAuthWithImpersonation());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(USER_BOB)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.INSUFFICIENT_REMOTE_PERMISSION))
                .specification(expectNoAuthorisationUri()) // 2LOi should not provide authorisationUri
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .specification(expectNoRemoteAuthentication())
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusErrorOAuthWithImpersonationInsufficientRemotePermission() {
        // create user "bob" with insufficient permission level on REFAPP2
        refapp1Users.createUser(USER_BOB, PermissionLevel.ADMIN);
        refapp2Users.createUser(USER_BOB, PermissionLevel.USER);
        // enable OAuth with impersonation
        applink.configure(enableOAuthWithImpersonation());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(USER_BOB)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.INSUFFICIENT_REMOTE_PERMISSION))
                .specification(expectNoAuthorisationUri()) // 2LOi should not provide authorisationUri
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .specification(expectNoRemoteAuthentication())
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusErrorDefaultOAuthNoExistingTrustNoRemoteApplink() {
        // configure 2LOi and then remove applink remotely
        applink.configure(enableDefaultOAuth())
                .configureTo(deleteApplink());

        // generic OAUTH_PROBLEM at the moment, will be fixed by APLDEV-251
        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_PROBLEM))
                .specification(expectErrorDetails(is(ApplinksOAuth.PROBLEM_CONSUMER_KEY_UNKNOWN)))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());

        // the only request made here is to manifest resource to check 2LO
        internalVerifyRequestToV2NotMade();
    }

    @Test
    public void statusErrorDefaultOAuthExistingTrustNoRemoteApplink() {
        // configure 2LOi and then remove applink remotely and set up access token
        applink.configure(enableDefaultOAuth())
                .configure(setUp3LoAccess(DEFAULT_AUTH_ADMIN))
                .configureTo(deleteApplink());

        // generic OAUTH_PROBLEM at the moment, will be fixed by APLDEV-251
        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_PROBLEM))
                .specification(expectErrorDetails(is(ApplinksOAuth.PROBLEM_CONSUMER_KEY_UNKNOWN)))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusErrorOAuthWithImpersonationNoRemoteApplink() {
        // configure 2LOi and then remove applink remotely
        applink.configure(enableOAuthWithImpersonation())
                .configureTo(deleteApplink());

        // generic OAUTH_PROBLEM at the moment, will be fixed by APLDEV-251
        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_PROBLEM))
                .specification(expectErrorDetails(is(ApplinksOAuth.PROBLEM_CONSUMER_KEY_UNKNOWN)))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .specification(expectNoRemoteAuthentication())
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusErrorDefaultOAuthNoExistingTrustDisabledRemotely() {
        // enable default OAuth, then disable OAuth remotely
        applink.configure(enableDefaultOAuth())
                .configureTo(disableOAuth());

        // generic OAUTH_PROBLEM at the moment, will be fixed by APLDEV-251
        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_PROBLEM))
                .specification(expectErrorDetails(is(ApplinksOAuth.PROBLEM_CONSUMER_KEY_UNKNOWN)))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());

        // the only request made here is to manifest resource to check 2LO
        internalVerifyRequestToV2NotMade();
    }

    @Test
    public void statusErrorDefaultOAuthExistingTrustDisabledRemotely() {
        // enable default OAuth and set up access token, then disable OAuth remotely
        applink.configure(enableDefaultOAuth())
                .configure(setUp3LoAccess(DEFAULT_AUTH_ADMIN))
                .configureTo(disableOAuth());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_PROBLEM))
                .specification(expectErrorDetails(is(ApplinksOAuth.PROBLEM_CONSUMER_KEY_UNKNOWN)))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusErrorAuthUnsupportedLocalDefaultOAuthVsRemote3LoOnly() {
        // OAuth with impersonation locally, default OAuth remotely
        applink.configureFrom(enableDefaultOAuth())
                .configureTo(enable3LoOnly())
                .configure(setUp3LoAccess(DEFAULT_AUTH_ADMIN));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectRemoteAuthentication(STATUS_THREE_LO_ONLY))
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusErrorAuthMismatchLocalDefaultOAuthVsRemoteOAuthImpersonation() {
        // OAuth with impersonation locally, default OAuth remotely
        applink.configureFrom(enableDefaultOAuth())
                .configureTo(enableOAuthWithImpersonation())
                .configure(setUp3LoAccess(DEFAULT_AUTH_ADMIN));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.AUTH_LEVEL_MISMATCH))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusErrorAuthUnsupportedLocalOAuthImpersonationVsRemote3LoOnly() {
        // OAuth with impersonation locally, 3LO-only remotely (-> unsupported level)
        applink.configureFrom(enableOAuthWithImpersonation())
                .configureTo(enable3LoOnly());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .specification(expectNoRemoteAuthentication()) // the status is "deducted" without actually knowing the exact remote OAuth setting
                .build());

        internalVerifyRequestToV2Made();
    }

    @Test
    public void statusErrorAuthUnsupportedLocalOAuthImpersonationVsRemoteDefaultOAuth() {
        // OAuth with impersonation locally, default OAuth remotely
        applink.configureFrom(enableOAuthWithImpersonation())
                .configureTo(enableDefaultOAuth());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.AUTH_LEVEL_MISMATCH))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.DEFAULT))
                .build());

        internalVerifyRequestToV2Made();
    }

    protected void verifyRequestToV2Made() {
    }

    protected void verifyRequestToV2NotMade() {
    }

    private void internalVerifyRequestToV2Made() {
        verificationCallbackInvoked = true;
        verifyRequestToV2Made();
    }

    private void internalVerifyRequestToV2NotMade() {
        verificationCallbackInvoked = true;
        verifyRequestToV2NotMade();
    }
}
