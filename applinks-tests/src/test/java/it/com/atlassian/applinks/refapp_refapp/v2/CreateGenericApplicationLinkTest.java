package it.com.atlassian.applinks.refapp_refapp.v2;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.webdriver.applinks.component.v2.ConfigureUrlDialog;
import com.atlassian.webdriver.applinks.component.v2.CreateNonUalDialog;
import com.atlassian.webdriver.applinks.component.v2.CreateNonUalIncomingDialog;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.google.common.collect.Lists;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.categories.SingleProductTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Tests for the v2 applink creation workflow
 * Tests
 * - basic UI components that don't require another application running
 * - 3rd Party/Generic links.
 *
 * @since 4.0.0
 */
@Category({RefappTest.class, SingleProductTest.class})
public class CreateGenericApplicationLinkTest extends V2AbstractApplinksTest {
    private static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDdT7fuLU/4/ywtEineIEUANty6JpjQybsa5slp8PWSHVg1/pqmaeTE6LCF44hX53VXYn6IqNR+L4pptBNBJEuhsZH+CltLT5ogOpYPRdgju4eIiINQ7Dt8OQlhPilP0ZTc2hYKxt1EfuaazKc7Y0t8K4rkeY6z9G2L7yPTP8ZAewIDAQAB";

    @Before
    public void setup() {
        // login to the product
        login(PRODUCT);
    }

    @Test
    public void verifyListApplicationLinkPageLayoutOnFirstEntry() throws Exception {
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        OAuthApplinksClient.verifyListApplicationLinkPageLayoutFormIsReset(page);
    }

    @Test
    public void verifyConfigureUrlDialogLayoutUnresponsiveUrl() throws Exception {
        String testUrl = "invalid.url";
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        ConfigureUrlDialog dialog = page.getConfigureUrlDialog();

        OAuthApplinksClient.verifyConfigureUrlDialogLayout(dialog, testUrl, null);

        dialog.clickCancel();

        OAuthApplinksClient.verifyListApplicationLinkPageLayoutWithUrl(page, testUrl);
    }

    @Test
    public void verifyConfigureUrlDialogLayoutWhitespaceUrl() throws Exception {
        String testUrl = "   ";
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        ConfigureUrlDialog dialog = page.getConfigureUrlDialog();

        OAuthApplinksClient.verifyConfigureUrlDialogLayout(dialog, testUrl.trim(), null);

        dialog.clickCancel();

        OAuthApplinksClient.verifyListApplicationLinkPageLayoutWithUrl(page, testUrl);
    }

    @Test
    public void verifyConfigureUrlDialogLayoutUrlWithoutManifest() throws Exception {
        String testUrl = PRODUCT.getProductInstance().getBaseUrl() + "/invalid/url";
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        ConfigureUrlDialog dialog = page.getConfigureUrlDialog();

        OAuthApplinksClient.verifyConfigureUrlDialogLayout(dialog, testUrl, null);

        dialog.clickCancel();

        OAuthApplinksClient.verifyListApplicationLinkPageLayoutWithUrl(page, testUrl);
    }

    @Test
    public void verifyConfigureUrlDialogLayoutUrlWithRedirect() throws Exception {
        String testUrl = "www.twitter.com";
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        ConfigureUrlDialog dialog = page.getConfigureUrlDialog();

        OAuthApplinksClient.verifyConfigureUrlDialogLayout(dialog, testUrl, format("https://%s/", testUrl));

        dialog.clickCancel();

        OAuthApplinksClient.verifyListApplicationLinkPageLayoutWithUrl(page, testUrl);
    }

    @Test
    public void verifyCreateApplicationLinkLayoutUnresponsiveUrl() throws Exception {
        String testUrl = "http://www.foo" + new Date().getTime() + ".com";
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        ConfigureUrlDialog configureUrlDialog = page.getConfigureUrlDialog();

        configureUrlDialog.clickContinue();
        CreateNonUalDialog createNonUalDialog = configureUrlDialog.getCreateNonUalDialog();

        OAuthApplinksClient.verifyCreateLinkDialogLayout(createNonUalDialog, ProductInstances.REFAPP1);

        createNonUalDialog.clickCancel();

        OAuthApplinksClient.verifyListApplicationLinkPageLayoutWithUrl(page, testUrl);
    }

    @Test
    public void verifyCreateApplicationLinkLayoutWhitespaceUrl() throws Exception {
        String testUrl = "    ";
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        ConfigureUrlDialog configureUrlDialog = page.getConfigureUrlDialog();

        configureUrlDialog.clickContinue();
        CreateNonUalDialog createNonUalDialog = configureUrlDialog.getCreateNonUalDialog();

        OAuthApplinksClient.verifyCreateLinkDialogLayout(createNonUalDialog, ProductInstances.REFAPP1);

        createNonUalDialog.clickCancel();

        OAuthApplinksClient.verifyListApplicationLinkPageLayoutWithUrl(page, testUrl);
    }

    @Test
    public void verifyCreateApplicationLinkLayoutUrlWithoutManifest() throws Exception {
        String testUrl = "http://www.google.com";
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        ConfigureUrlDialog configureUrlDialog = page.getConfigureUrlDialog();

        configureUrlDialog.clickContinue();
        CreateNonUalDialog createNonUalDialog = configureUrlDialog.getCreateNonUalDialog();

        OAuthApplinksClient.verifyCreateLinkDialogLayout(createNonUalDialog, ProductInstances.REFAPP1);

        createNonUalDialog.clickCancel();

        OAuthApplinksClient.verifyListApplicationLinkPageLayoutWithUrl(page, testUrl);
    }

    @Test
    public void verifyCreateApplicationLinkIncomingLayout() throws Exception {
        String testUrl = "http://www.foo" + new Date().getTime() + ".com";
        String name = "foo-bar";
        // go to the applinks admin screen in the source application
        ListApplicationLinkPage page = PRODUCT.visit(ListApplicationLinkPage.class);

        page.setApplicationUrl(testUrl).clickCreateNewLink();

        ConfigureUrlDialog configureUrlDialog = page.getConfigureUrlDialog();

        configureUrlDialog.clickContinue();
        CreateNonUalDialog createNonUalDialog = configureUrlDialog.getCreateNonUalDialog();

        OAuthApplinksClient.verifyCreateLinkDialogLayout(createNonUalDialog, ProductInstances.REFAPP1);

        createNonUalDialog.setName(name);
        createNonUalDialog.checkCreateIncomingCheckBox();
        createNonUalDialog.clickContinue();
        CreateNonUalIncomingDialog createNonUalIncomingDialog = createNonUalDialog.getCreateNonUalIncomingDialog();

        OAuthApplinksClient.verifyCreateNonUalIncomingDialogLayout(createNonUalIncomingDialog, ProductInstances.REFAPP1);

        createNonUalIncomingDialog.clickCancel();

        OAuthApplinksClient.verifyConfiguredAppLinksTableContents(page, Lists.newArrayList(testUrl));
        OAuthApplinksClient.verifyConfiguration(PRODUCT, testUrl, name, "", "", "", "", "", "", "", "", "");

        //check the form has reset
        OAuthApplinksClient.verifyListApplicationLinkPageLayoutFormIsReset(page);
    }

    @Test
    public void verifyCreateBasicGenericApplicationLink() throws Exception {
        Long timeStamp = new Date().getTime();
        String genericRpcUrl = "http://www.foo" + timeStamp + ".com";
        String genericName = "search";
        String expectedFinalStatusMessage = "Application Link '" + genericName + "' created successfully";

        // go to the applinks admin screen in the source application
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);
        // enter the target url
        listApplicationLinkPage.setApplicationUrl(genericRpcUrl)
                // click "create link". NB. deliberately using fake urls so will have to go through the url check screen.
                .clickCreateNewLinkAndOpenConfigureUrlDialog()
                // click "continue"
                .clickContinueAndOpenNonUalDialog()
                // set the app link name
                .setName(genericName)
                // view and confirm the link details
                .clickContinueToCreateNonUalLink();

        assertThat(listApplicationLinkPage.getFirstMessage(), containsString(expectedFinalStatusMessage));

        OAuthApplinksClient.verifyConfiguredAppLinksTableContents(listApplicationLinkPage, Lists.newArrayList(genericRpcUrl));
        OAuthApplinksClient.verifyConfiguration(PRODUCT, genericRpcUrl, genericName, "", "", "", "", "", "",
                "", "", "");
    }

    @Test
    public void verifyCreateGenericApplicationLinkWithOutgoingConfiguration() throws Exception {
        Long timeStamp = new Date().getTime();
        String genericRpcUrl = "http://www.foo" + timeStamp + ".com";
        String genericName = "search";
        String expectedFinalStatusMessage = "Application Link '" + genericName + "' created successfully";

        // go to the applinks admin screen in the source application
        String serviceProvider = "a";
        String consumerkey = "b";
        String sharedSecret = "c";
        String requestTokenUrl = "d";
        String accessTokenUrl = "e";
        String authorizeUrl = "f";
        String consumerName = "";
        String consumerkeyIncoming = "";
        String publicKey = "";


        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);
        // enter the target url
        listApplicationLinkPage.setApplicationUrl(genericRpcUrl)
                // click "create link". NB. deliberately using fake urls so will have to go through the url check screen.
                .clickCreateNewLinkAndOpenConfigureUrlDialog()
                // click "continue"
                .clickContinueAndOpenNonUalDialog()
                // set the app link name
                .setName(genericName)
                .setServiceProvider(serviceProvider)
                .setConsumerKey(consumerkey)
                .setSharedSecret(sharedSecret)
                .setRequestTokenUrl(requestTokenUrl)
                .setAccessTokenUrl(accessTokenUrl)
                .setAuthorizeUrl(authorizeUrl)
                // view and confirm the link details
                .clickContinueToCreateNonUalLink();

        assertThat(listApplicationLinkPage.getFirstMessage(), containsString(expectedFinalStatusMessage));

        OAuthApplinksClient.verifyConfiguredAppLinksTableContents(listApplicationLinkPage, Lists.newArrayList(genericRpcUrl));
        OAuthApplinksClient.verifyConfiguration(PRODUCT, genericRpcUrl, genericName, serviceProvider,
                consumerkey, sharedSecret, requestTokenUrl, accessTokenUrl, authorizeUrl,
                consumerkeyIncoming, consumerName, publicKey);
    }

    @Test
    public void verifyCreateGenericApplicationLinkWithIncomingConfiguration() throws Exception {
        Long timeStamp = new Date().getTime();
        String genericRpcUrl = "http://www.foo" + timeStamp + ".com";
        String genericName = "search";
        String expectedFinalStatusMessage = "Application Link '" + genericName + "' created successfully";

        // go to the applinks admin screen in the source application
        String serviceProvider = "";
        String consumerkey = "";
        String sharedSecret = "";
        String requestTokenUrl = "";
        String accessTokenUrl = "";
        String authorizeUrl = "";
        String consumerName = "x";
        String consumerkeyIncoming = "y";

        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);
        // enter the target url
        listApplicationLinkPage.setApplicationUrl(genericRpcUrl)
                // click "create link". NB. deliberately using fake urls so will have to go through the url check screen.
                .clickCreateNewLinkAndOpenConfigureUrlDialog()
                // click "continue"
                .clickContinueAndOpenNonUalDialog()
                // set the app link name
                .setName(genericName)
                .checkCreateIncomingCheckBox()
                // view and confirm the link details
                .clickContinue();

        CreateNonUalIncomingDialog createNonUalIncomingDialog = PRODUCT.getPageBinder().bind(CreateNonUalIncomingDialog.class);

        //make sure the dialog is visible.
        waitUntilTrue(createNonUalIncomingDialog.isAt());

        createNonUalIncomingDialog
                .setConsumerKey(consumerkeyIncoming)
                .setConsumerName(consumerName)
                .setPublicKey(PUBLIC_KEY)
                .clickContinue();

        assertThat(listApplicationLinkPage.getFirstMessage(), containsString(expectedFinalStatusMessage));

        OAuthApplinksClient.verifyConfiguredAppLinksTableContents(listApplicationLinkPage, Lists.newArrayList(genericRpcUrl));
        OAuthApplinksClient.verifyConfiguration(PRODUCT, genericRpcUrl, genericName, serviceProvider,
                consumerkey, sharedSecret, requestTokenUrl, accessTokenUrl, authorizeUrl,
                consumerkeyIncoming, consumerName, PUBLIC_KEY);
    }

    @Test
    public void verifyDuplicateRpcUrlApplicationLinkIsNotAllowed() throws Exception {
        Long timeStamp = new Date().getTime();
        String genericRpcUrl1 = "http://www.foo" + timeStamp + ".com";
        String genericName1 = "search1";
        String genericName2 = "search2";

        // go to the applinks admin screen in the source application
        PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class)
                // enter the target url
                .setApplicationUrl(genericRpcUrl1)
                // click "create link". NB. deliberately using fake urls so will have to go through the url check screen.
                .clickCreateNewLinkAndOpenConfigureUrlDialog()
                // click "continue"
                .clickContinueAndOpenNonUalDialog()
                // set the app link name
                .setName(genericName1)
                // view and confirm the link details
                .clickContinueToCreateNonUalLink();

        // go to the applinks admin screen in the source application
        CreateNonUalDialog createNonUalDialog = PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class)
                // enter the target url
                .setApplicationUrl(genericRpcUrl1)
                // click "create link". NB. deliberately using fake urls so will have to go through the url check screen.
                .clickCreateNewLinkAndOpenConfigureUrlDialog()
                // click "continue"
                .clickContinueAndOpenNonUalDialog()
                // set the app link name
                .setName(genericName2)
                .clickContinueExpectingAnError();

        assertThat(createNonUalDialog.getHeadlineError(), is("There is already one Application Link with URL '" + genericRpcUrl1 + "'."));
    }

    @Test
    public void verifyDuplicateNamedApplicationLinkCanBeCorrected() throws Exception {
        Long timeStamp = new Date().getTime();
        String genericRpcUrl1 = "http://www.foo" + timeStamp + ".com";
        String genericRpcUrl2 = "http://www.foo" + timeStamp + ".com.au";
        String nonUniqueName = "search";
        String uniqueName = "search - 2";

        // go to the applinks admin screen in the source application
        PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class)
                // enter the target url
                .setApplicationUrl(genericRpcUrl1)
                // click "create link". NB. deliberately using fake urls so will have to go through the url check screen.
                .clickCreateNewLinkAndOpenConfigureUrlDialog()
                // click "continue"
                .clickContinueAndOpenNonUalDialog()
                // set the app link name
                .setName(nonUniqueName)
                // view and confirm the link details
                .clickContinueToCreateNonUalLink();

        // try to create a new link with a duplicate name
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);
        // enter the target url
        listApplicationLinkPage.setApplicationUrl(genericRpcUrl2)
                // click "create link". NB. deliberately using fake urls so will have to go through the url check screen.
                .clickCreateNewLinkAndOpenConfigureUrlDialog()
                // click "continue"
                .clickContinueAndOpenNonUalDialog()
                // set the app link name
                .setName(nonUniqueName)
                .clickContinue();

        String expectedFinalStatusMessage = "Application Link '" + uniqueName + "' created successfully";
        assertThat(listApplicationLinkPage.getFirstMessage(), containsString(expectedFinalStatusMessage));

        OAuthApplinksClient.verifyConfiguredAppLinksTableContents(listApplicationLinkPage, Lists.newArrayList(genericRpcUrl1, genericRpcUrl2));
        OAuthApplinksClient.verifyConfiguration(PRODUCT, genericRpcUrl2, uniqueName, "", "", "", "", "", "", "", "", "");
    }

    @Test
    public void verifyCancellingCreateNonUalDialogResetsCreateNewLinkButton() throws Exception {
        Long timeStamp = new Date().getTime();
        String genericRpcUrl = "http://www.foo" + timeStamp + ".com";

        // go to the applinks admin screen in the source application
        com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage listApplicationLinkPage =
                PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);

        // create button is disabled by default
        waitUntilFalse(listApplicationLinkPage.createNewLinkButtonIsEnabledTimed());

        com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage listApplicationLinkPage2 =
                listApplicationLinkPage.setApplicationUrl(genericRpcUrl)
                // click "create link". NB. deliberately using fake urls so will have to go through the url check screen.
                .clickCreateNewLinkAndOpenConfigureUrlDialog()
                // click "continue"
                .clickContinueAndOpenNonUalDialog()
                .clickCancel();

        // create button is enabled after cancel.
        waitUntilTrue(listApplicationLinkPage2.createNewLinkButtonIsEnabledTimed());
    }

    @Test
    public void verifyCancellingCreateConfigureUrlDialogResetsCreateNewLinkButton() throws Exception {
        Long timeStamp = new Date().getTime();
        String genericRpcUrl = "http://www.foo" + timeStamp + ".com";

        // go to the applinks admin screen in the source application
        ConfigureUrlDialog configureUrlDialog = PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class)
                // enter the target url
                .setApplicationUrl(genericRpcUrl)
                // click "create link". NB. deliberately using fake urls so will have to go through the url check screen.
                .clickCreateNewLinkAndOpenConfigureUrlDialog();

        waitUntilTrue(configureUrlDialog.isAt());
        com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage listApplicationLinkPage2 = configureUrlDialog.clickCancel();

        // create button is enabled after cancel.
        waitUntilTrue(listApplicationLinkPage2.createNewLinkButtonIsEnabledTimed());
    }

    @Test
    public void verifyCreateApplicationLinkViaDescriptor() throws Exception {
        Long timeStamp = new Date().getTime();
        String rpcUrl = "http://www.foo" + timeStamp + ".com";
        String applinkName = "search";
        String consumerName = "x";
        String consumerKey = "y";
        String applinkDescriptor = String.format(
                "{\"name\":\"%s\",\"url\":\"%s\",\"publicKey\":\"%s\",\"consumerName\":\"%s\",\"consumerKey\":\"%s\"}",
                applinkName ,rpcUrl, PUBLIC_KEY, consumerName, consumerKey);

        createApplicationLinkViaDescriptorAndVerify(rpcUrl, applinkName, applinkDescriptor, consumerName, consumerKey);
    }

    @Test
    public void verifyCreateApplicationLinkViaDescriptorShortForm() throws Exception {
        Long timeStamp = new Date().getTime();

        String rpcUrl = "http://www.foo" + timeStamp + ".com";
        String applinkName = "search";

        String applinkDescriptor = String.format("{\"name\":\"%s\",\"url\":\"%s\",\"publicKey\":\"%s\"}",applinkName, rpcUrl, PUBLIC_KEY);

        createApplicationLinkViaDescriptorAndVerify(rpcUrl, applinkName, applinkDescriptor, null, null);
    }


    public void createApplicationLinkViaDescriptorAndVerify(@Nonnull String rpcUrl,
                                                            @Nonnull String applinkName,
                                                            @Nonnull String applinkDescriptor,
                                                            @Nullable String consumerName,
                                                            @Nullable String consumerKey){
        consumerName = consumerName == null ? applinkName :consumerName;
        consumerKey = consumerKey == null ? applinkName : consumerKey;

        final ListApplicationLinkPage listApplinksPage = PRODUCT.visit(ListApplicationLinkPage.class);
        listApplinksPage.setApplicationUrl(applinkDescriptor).clickCreateNewLink();

        OAuthApplinksClient.verifyConfiguredAppLinksTableContents(listApplinksPage, Lists.newArrayList(rpcUrl));

        OAuthApplinksClient.verifyConfiguration(PRODUCT, rpcUrl, applinkName, "", "", "", "", "", "",
                consumerKey, consumerName, PUBLIC_KEY);//if no consumer name or key specified, the name of the applink is used for the consumer name and key
    }

    @Test
    public void verifyMissingNameIsHighlighted() throws Exception {
        Long timeStamp = new Date().getTime();
        String genericRpcUrl = "http://www.foo" + timeStamp + ".com";
        String genericName = "search";
        String expectedFinalStatusMessage = "Application Link '" + genericName + "' created successfully";

        // go to the applinks admin screen in the source application
        String serviceProvider = "a";
        String consumerkey = "b";
        String sharedSecret = "c";
        String requestTokenUrl = "d";
        String accessTokenUrl = "e";
        String authorizeUrl = "f"   ;
        String consumerName = "";
        String consumerkeyIncoming = "";
        String publicKey = "";

        CreateNonUalDialog createNonUalDialog = PRODUCT.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class)
                // enter the target url
                .setApplicationUrl(genericRpcUrl)
                // click "create link". NB. deliberately using fake urls so will have to go through the url check screen.
                .clickCreateNewLinkAndOpenConfigureUrlDialog()
                // click "continue"
                .clickContinueAndOpenNonUalDialog()
                // DO NOT set the app link name
                .setServiceProvider(serviceProvider)
                .setConsumerKey(consumerkey)
                .setSharedSecret(sharedSecret)
                .setRequestTokenUrl(requestTokenUrl)
                .setAccessTokenUrl(accessTokenUrl)
                .setAuthorizeUrl(authorizeUrl)
                // view and confirm the link details
                .clickContinueExpectingAnError();

        assertThat(createNonUalDialog.getVisibleErrors(), hasItem("The application name should not be empty."));
    }
}