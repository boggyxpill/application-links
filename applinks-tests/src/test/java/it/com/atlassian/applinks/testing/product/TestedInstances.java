package it.com.atlassian.applinks.testing.product;

import com.atlassian.applinks.test.product.DefaultTestedInstance;
import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProduct;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.function.Function;

import static com.google.common.base.Preconditions.checkState;

/**
 * @since 4.3
 */
public class TestedInstances {
    public static final Function<ProductInstance, TestedInstance> FROM_PRODUCT_INSTANCE =
            new Function<ProductInstance, TestedInstance>() {
                @Nullable
                @Override
                public TestedInstance apply(ProductInstance productInstance) {
                    return fromProductInstance(productInstance);
                }
            };

    private TestedInstances() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }


    @Nonnull
    public static TestedInstance fromProductInstance(@Nonnull ProductInstance productInstance) {
        if (productInstance instanceof TestedInstance) {
            return (TestedInstance) productInstance;
        } else {
            SupportedProducts product = SupportedProducts.fromId(productInstance.getInstanceId());
            checkState(product != null, "Unsupported product instance. Cannot find product for : " + productInstance);
            return new DefaultTestedInstance(productInstance.getInstanceId(), productInstance.getBaseUrl(), product);
        }
    }

    @Nonnull
    public static TestedInstance fromProduct(@Nonnull TestedProduct<?> product) {
        return fromProductInstance(product.getProductInstance());
    }
}
