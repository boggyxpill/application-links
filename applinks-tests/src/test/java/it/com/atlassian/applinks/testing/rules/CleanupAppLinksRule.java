package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.pageobjects.ProductInstance;
import it.com.atlassian.applinks.testing.product.TestedInstances;
import org.junit.rules.RuleChain;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toImmutableList;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;

/**
 * Removes all applinks from the target application after the test.
 *
 * @since 4.0
 */
public class CleanupAppLinksRule extends TestWatcher {
    @Nonnull
    public static RuleChain forProducts(@Nonnull ProductInstance... products) {
        return forTestedInstances(stream(products).map(TestedInstances::fromProductInstance).collect(toImmutableList()));
    }

    @Nonnull
    public static RuleChain forTestedInstances(@Nonnull TestedInstance... products) {
        return forTestedInstances(asList(checkNotNull(products, "products")));
    }

    @Nonnull
    public static RuleChain forTestedInstances(@Nonnull Iterable<TestedInstance> products) {
        checkNotNull(products, "products");
        RuleChain chain = RuleChain.emptyRuleChain();
        for (TestedInstance product : products) {
            chain = chain.around(new CleanupAppLinksRule(product));
        }
        return chain;
    }

    private final ApplinksBackdoor backdoor;

    @Inject
    public CleanupAppLinksRule(@Nonnull ApplinksBackdoor backdoor) {
        this.backdoor = checkNotNull(backdoor);
    }

    public CleanupAppLinksRule(@Nonnull TestedInstance product) {
        this(new ApplinksBackdoor(product));
    }

    @Override
    protected void finished(Description description) {
        backdoor.cleanUp();
    }
}


