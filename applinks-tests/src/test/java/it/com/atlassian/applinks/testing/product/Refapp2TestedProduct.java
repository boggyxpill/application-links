package it.com.atlassian.applinks.testing.product;

import com.atlassian.pageobjects.Defaults;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.refapp.RefappTestedProduct;

@Defaults(instanceId = "refapp2", contextPath = "/refapp2", httpPort = 5992)
public class Refapp2TestedProduct extends RefappTestedProduct {
    public Refapp2TestedProduct(TestedProductFactory.TesterFactory<WebDriverTester> testerFactory, ProductInstance productInstance) {
        super(testerFactory, productInstance);
    }
}
