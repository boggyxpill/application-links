package it.com.atlassian.applinks;

import com.atlassian.pageobjects.TestedProduct;
import it.com.atlassian.applinks.testing.product.Refapp1TestedProduct;
import it.com.atlassian.applinks.testing.product.Refapp2TestedProduct;

/**
 * Extension to {@link it.com.atlassian.applinks.AbstractSimpleApplinksTest} for tests that require the full applinks suite, applinks-plugin + auth provider plugins.
 *
 * @since 5.0.0
 */
public class AbstractFullApplinksTest extends AbstractSimpleApplinksTest {
    @Override
    public Class<? extends TestedProduct> getDefaultProductOneClass() {
        return Refapp1TestedProduct.class;
    }

    @Override
    public Class<? extends TestedProduct> getDefaultProductTwoClass() {
        return Refapp2TestedProduct.class;
    }
}
