package it.com.atlassian.applinks.testing;

import com.atlassian.applinks.test.authentication.TestAuthentication;

/**
 * Common users to authenticate into tested applications.
 *
 * @since 4.3
 */
public final class ApplinksAuthentications {
    /**
     * Default System Administrator user, should work across all tested applications.
     */
    public static final TestAuthentication SYSADMIN = TestAuthentication.admin();

    /**
     * Betty is the default admin user on RefApp.
     */
    public static final TestAuthentication REFAPP_ADMIN_BETTY = TestAuthentication.authenticatedWith("betty");

    /**
     * Barney is the default user on RefApp.
     */
    public static final TestAuthentication REFAPP_USER_BARNEY = TestAuthentication.authenticatedWith("barney");

    private ApplinksAuthentications() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }
}
