package it.com.atlassian.applinks.refapp_refapp.v3;

import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;
import com.atlassian.webdriver.applinks.page.v3.V3ListApplicationLinksPage;
import it.com.atlassian.applinks.AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

@Category(RefappTest.class)
public class V3PrimaryLinkTest extends AbstractApplinksTest {
    private final ApplinksBackdoor applinksBackdoor = new ApplinksBackdoor(INSTANCE1);

    @Test
    public void primaryLinkChangesWhenSetPrimaryClicked() {
        applinksBackdoor.createRandomGeneric();
        TestApplink.Side generic2 = applinksBackdoor.createRandomGeneric();

        V3ListApplicationLinksPage page = loginAsSysadminAndGoTo(V3ListApplicationLinksPage.class);
        page.getApplinkRow(generic2.applicationId()).setPrimary();
        waitUntilTrue("Primary application link was not able to be changed",
                page.getApplinkRow(generic2.applicationId()).isPrimary());
    }
}
