package com.atlassian.applinks.pageobjects;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.refapp.page.RefappLoginPage;

public class LoginRequiredPage extends RefappLoginPage {
    @ElementBy(name = "os_username")
    protected PageElement username;

    public String getUrl() {
        return "/plugins/servlet/applinks/listApplicationLinks";
    }

    @WaitUntil
    protected void waitForPageFullyLoaded() {
        // have to wait until the loading finish before we can extract information.
        Poller.waitUntilTrue(username.timed().isVisible());
    }
}
