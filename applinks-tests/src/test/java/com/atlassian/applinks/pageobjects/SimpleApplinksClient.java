package com.atlassian.applinks.pageobjects;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.core.rest.ManifestResourceClient;
import com.atlassian.applinks.core.rest.model.ApplicationLinkEntity;
import com.atlassian.applinks.core.v2.rest.ApplicationLinkResourceClient;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.applinks.component.v2.CreateUalDialog;
import com.atlassian.webdriver.applinks.component.v2.PauseDialog;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasDisplayUrl;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;

/**
 * Utility class containing methods to drive application link creation via pageobjects.
 * Only expects the applinks-plugin to be present no authentication provider plugins
 *
 * @since 5.0.0
 */
public class SimpleApplinksClient {
    /**
     * Create a default 2-way link between the products.
     *
     * @param product1
     * @param product2
     */
    public static ListApplicationLinkPage createTwoWayLink(TestedProduct<WebDriverTester> product1, TestedProduct<WebDriverTester> product2) {
        final ListApplicationLinkPage product1ApplinksPage = product1.visit(ListApplicationLinkPage.class);

        // creation dance.
        final CreateUalDialog product1CreateUalDialog = product1ApplinksPage.setApplicationUrl(product2.getProductInstance().getBaseUrl()).clickCreateNewLinkAndOpenDialog();
        final PauseDialog product1RedirectReciprocalDialog = product1CreateUalDialog.clickContinueToCreateUalLink();
        final CreateUalDialog product2CreateUalDialog = product1RedirectReciprocalDialog.handleRedirectionToRemoteApplicationCreateUalDialog(product2.getProductInstance().getBaseUrl());
        product2CreateUalDialog.clickContinueToCreateUalLink()
                //redirection back to app 1
                .handleRedirectionToRemoteApplication(product1.getProductInstance().getBaseUrl());

        // as there is no outgoing auth to configure there is no dialog shown by the remote instance when the browser si redirected back

        // check if the list is loading
        Poller.waitUntilTrue(product1ApplinksPage.getLinksLoadingMessageTimed().isPresent());
        Poller.waitUntilFalse(product1ApplinksPage.getLinksLoadingMessageTimed().isVisible());

        // check the new link is visible.
        Poller.waitUntilTrue(product1ApplinksPage.isApplinksListFullyLoaded());
        Poller.waitUntil(product1ApplinksPage.getApplicationLinksTimed(), contains(hasDisplayUrl(product2.getProductInstance().getBaseUrl())));

        return product1ApplinksPage;
    }

    /**
     * Create a simple reciprocal link between the products via the REST API
     *
     * @since 5.0.0
     */
    public static void createReciprocalAtlassianAppLinkViaRest(final TestedProduct productOne, final TestedProduct productTwo) {
        SimpleApplinksClient.createAtlassianAppLinkViaRest(productOne, productTwo);
        SimpleApplinksClient.createAtlassianAppLinkViaRest(productTwo, productOne);
    }

    /**
     * Create a simple AppLink between the products via the REST API.
     *
     * @since 4.0.15
     */
    public static ApplicationId createAtlassianAppLinkViaRest(final TestedProduct fromProduct, final TestedProduct toProduct) {
        try {
            Manifest manifest = ManifestResourceClient.forProduct(toProduct).get();

            final ApplicationLinkEntity applicationLinkEntity = new ApplicationLinkEntity(manifest.getId(),
                    manifest.getTypeId(),
                    manifest.getName(),
                    manifest.getUrl(),
                    manifest.getIconUrl(),
                    manifest.getIconUri(),
                    manifest.getUrl(),
                    false,
                    false,
                    null);

            // add the link
            return ApplicationLinkResourceClient.forProduct(fromProduct).addApplicationLink(applicationLinkEntity);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Unable to create applink between '%s' and '%s'", fromProduct, toProduct), e);
        }
    }
}
