package com.atlassian.applinks.pageobjects;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.core.rest.model.ApplicationLinkEntity;
import com.atlassian.applinks.core.rest.model.AuthenticationProviderEntity;
import com.atlassian.applinks.core.v2.rest.ApplicationLinkResourceClient;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.oauth.Consumer;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.applinks.component.ApplicationDetailsSection;
import com.atlassian.webdriver.applinks.component.OauthIncomingAuthenticationSection;
import com.atlassian.webdriver.applinks.component.OauthIncomingAuthenticationWithAutoConfigSection;
import com.atlassian.webdriver.applinks.component.OauthOutgoingAuthenticationEditModeSection;
import com.atlassian.webdriver.applinks.component.OauthOutgoingAuthenticationWithAutoConfigSection;
import com.atlassian.webdriver.applinks.component.v2.AbstractDialog;
import com.atlassian.webdriver.applinks.component.v2.ConfigureUrlDialog;
import com.atlassian.webdriver.applinks.component.v2.ConfirmUrlDialog;
import com.atlassian.webdriver.applinks.component.v2.CreateNonUalDialog;
import com.atlassian.webdriver.applinks.component.v2.CreateNonUalIncomingDialog;
import com.atlassian.webdriver.applinks.component.v2.CreateUalDialog;
import com.atlassian.webdriver.applinks.component.v2.IncompatibleApplinksVersionDialog;
import com.atlassian.webdriver.applinks.component.v2.PauseDialog;
import com.atlassian.webdriver.applinks.externalpage.CharlieManagementPage;
import com.atlassian.webdriver.applinks.page.AbstractApplicationLinkPage;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.refapp.RefappTestedProduct;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import it.com.atlassian.applinks.ApplicationTestHelper;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import org.hamcrest.Matcher;

import java.net.URI;
import java.util.HashMap;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Conditions.isEqual;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static it.com.atlassian.applinks.V2AbstractApplinksTest.ADMIN_PASSWORD;
import static it.com.atlassian.applinks.V2AbstractApplinksTest.PRODUCT;
import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasDisplayUrl;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * Class of static helper methods used across tests for Application Links creation.
 *
 * @since v4.0.0
 */
public class OAuthApplinksClient {
    /**
     * Create a default 2-way link between the products.
     *
     * @param product1
     * @param product2
     */
    public static ListApplicationLinkPage createTwoWayLink(TestedProduct<WebDriverTester> product1, TestedProduct<WebDriverTester> product2) {
        ListApplicationLinkPage product1ApplinksPage = product1.visit(ListApplicationLinkPage.class);
        final CreateUalDialog product1CreateUalDialog = product1ApplinksPage.setApplicationUrl(product2.getProductInstance().getBaseUrl()).clickCreateNewLinkAndOpenDialog();
        final PauseDialog product1RedirectReciprocalDialog = product1CreateUalDialog.clickContinueToCreateUalLink();
        final CreateUalDialog product2CreateUalDialog = product1RedirectReciprocalDialog.handleRedirectionToRemoteApplicationCreateUalDialog(product2.getProductInstance().getBaseUrl());
        product2CreateUalDialog.clickContinueToCreateUalLink()
                //redirection back to app 1
                .handleRedirectionToRemoteApplication(product1.getProductInstance().getBaseUrl());

        // wait until product1 applinks page fully loads after returning from product2 (bind will do that)
        product1ApplinksPage = product1.getPageBinder().bind(ListApplicationLinkPage.class);
        waitUntil(product1ApplinksPage.getApplicationLinksTimed(), contains(hasDisplayUrl(product2.getProductInstance().getBaseUrl())));

        return product1ApplinksPage;
    }

    /**
     * Create a default 1-way link between the products.
     *
     * @param product1 product 1
     * @param product2 product 2
     */
    public static ListApplicationLinkPage createOneWayLink(TestedProduct<WebDriverTester> product1, TestedProduct<WebDriverTester> product2) {
        final ListApplicationLinkPage product1ApplinksPage = product1.visit(ListApplicationLinkPage.class);
        final CreateUalDialog product1CreateUalDialog = product1ApplinksPage.setApplicationUrl(product2.getProductInstance().getBaseUrl()).clickCreateNewLinkAndOpenDialog();
        final PauseDialog product1ManualReciprocalDialog = product1CreateUalDialog.unCheckAdmin().clickContinueToCreateUalLink();
        product1ApplinksPage.clearDialogs();
        product1ManualReciprocalDialog.clickClose();
        return product1ApplinksPage;
    }

    public static ListApplicationLinkPage createGenericLink(TestedProduct<WebDriverTester> product1, String linkRpcUrl, String linkName) {
        return createGenericLink(product1, linkRpcUrl, linkName, contains(hasDisplayUrl(linkRpcUrl)));
    }

    public static ListApplicationLinkPage createGenericLink(final TestedProduct<WebDriverTester> product1,
                                                            final String linkRpcUrl,
                                                            final String linkName,
                                                            final Matcher<List<AbstractApplicationLinkPage.ApplicationLinkEntryRow>> links) {
        final ListApplicationLinkPage product1ApplinksPage = product1.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);

        // enter the target url
        // deliberately using fake URLs so will have to go through the URL check screen.
        product1ApplinksPage.setApplicationUrl(linkRpcUrl)
                .clickCreateNewLinkAndOpenConfigureUrlDialog()
                .clickContinueAndOpenNonUalDialog()
                .setName(linkName)
                .clickContinueToCreateNonUalLink();

        // wait until applinks page fully loads creating the applink
        waitUntilTrue(product1ApplinksPage.isApplinksListFullyLoaded());

        if (links != null) {
            waitUntil(product1ApplinksPage.getApplicationLinksTimed(), links);
        }

        return product1ApplinksPage;
    }

    public static void createCharlie(final String key, final String name) {
        createCharlie(PRODUCT, key, name, ADMIN_PASSWORD);
    }

    public static void createCharlie(RefappTestedProduct product, final String key, final String name, final String webSudoPassword) {
        PageElement createdCharlieLink = product.visit(CharlieManagementPage.class) //
                .handleWebSudoIfRequired(webSudoPassword) //
                .add(key, name) //
                .getLandingPageLink(key);
        assertNotNull(createdCharlieLink);
    }

    /**
     * check the main form for applink creation is working as expected.
     *
     * @param page
     */
    public static void verifyCreateNewLinkFormBehavior(ListApplicationLinkPage page) {
        // check the main form is active
        String url = "any old rubbish";
        page.setApplicationUrl(url);
        OAuthApplinksClient.verifyListApplicationLinkPageLayoutWithUrl(page, url);

        page.clearApplicationUrl();
        OAuthApplinksClient.verifyListApplicationLinkPageLayoutFormIsReset(page);
    }

    public static void verifyConfiguredAppLinksTableContents(ListApplicationLinkPage hostListApplicationLinkPage, List<String> linkUrls) {
        // confirm the expect link was created.
        final Iterable<Matcher> matchers = Iterables.transform(linkUrls, new Function<String, Matcher>() {
            public Matcher apply(final String url) {
                return hasDisplayUrl(url);
            }
        });

        waitUntil(hostListApplicationLinkPage.getApplicationLinksTimed(), contains(Iterables.toArray(matchers, Matcher.class)));

    }

    public static void verifyConfiguredAppLinksTableIsEmpty(TestedProduct<WebDriverTester> hostProduct) {
        int numApplinks = hostProduct.visit(com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage.class) //
                .inApplicationLinks().allRows().size();
        assertEquals(0, numApplinks);
    }

    public static void verifyConfiguration(TestedProduct<WebDriverTester> hostProduct,
                                           String genericRpcUrl, String genericName,
                                           String serviceProvider, String consumerKey, String sharedSecret, String requestTokenUrl, String accessTokenUrl, String authorizeUrl,
                                           String consumerKeyIncoming, String consumerName, String publicKey) {
        ListApplicationLinkPage hostListApplicationLinkPage = hostProduct.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);

        ApplicationDetailsSection applicationDetailsSection = hostListApplicationLinkPage.configureApplicationLink(genericRpcUrl);
        verifyBasicConfiguration(applicationDetailsSection, genericRpcUrl, genericName);
        verifyOutgoingOauthConfiguration(applicationDetailsSection, serviceProvider, consumerKey, sharedSecret, requestTokenUrl, accessTokenUrl, authorizeUrl);
        verifyIncomingOauthConfiguration(applicationDetailsSection, consumerKeyIncoming, consumerName, publicKey);

        applicationDetailsSection.close();
    }

    public static void verifyBasicConfiguration(ApplicationDetailsSection applicationDetailsSection, String genericRpcUrl, String genericName) {
        assertThat(applicationDetailsSection.getApplicationUrl(), is(genericRpcUrl));
        assertThat(applicationDetailsSection.getApplicationName(), is(genericName));
    }

    public static void verifyOutgoingOauthConfiguration(ApplicationDetailsSection applicationDetailsSection,
                                                        String serviceProvider, String consumerKey, String sharedSecret, String requestTokenUrl, String accessTokenUrl, String authorizeUrl) {
        OauthOutgoingAuthenticationEditModeSection oauthOutgoingAuthenticationEditModeSection = applicationDetailsSection.openOutgoingOauth();
        assertThat(oauthOutgoingAuthenticationEditModeSection.getServiceProvider(), is(serviceProvider));
        assertThat(oauthOutgoingAuthenticationEditModeSection.getConsumerKey(), is(consumerKey));
        // Don't check sharedSecret as its only shown as *******
        // assertThat(oauthOutgoingAuthenticationEditModeSection.getSharedSecret(), is(sharedSecret));
        assertThat(oauthOutgoingAuthenticationEditModeSection.getRequestTokenUrl(), is(requestTokenUrl));
        assertThat(oauthOutgoingAuthenticationEditModeSection.getAccessTokenUrl(), is(accessTokenUrl));
        assertThat(oauthOutgoingAuthenticationEditModeSection.getAuthorizeUrl(), is(authorizeUrl));
    }

    public static void verifyIncomingOauthConfiguration(ApplicationDetailsSection applicationDetailsSection,
                                                        String consumerKey, String consumerName, String publicKey) {
        OauthIncomingAuthenticationSection incomingAuthenticationSection = applicationDetailsSection.openIncomingOauth();
        assertThat(incomingAuthenticationSection.getConsumerKey(), is(consumerKey));
        assertThat(incomingAuthenticationSection.getConsumerName(), is(consumerName));
        assertThat(incomingAuthenticationSection.getPublicKey(), is(publicKey));
    }

    public static void verifyListApplicationLinkPageLayoutFormIsReset(ListApplicationLinkPage page) {
        waitUntilTrue(page.getApplicationUrlTextBoxTimed().isPresent());
        Poller.waitUntilEquals("textbox should be empty on entry", "", page.getApplicationUrlTextBoxTimed().getValue());

        waitUntilTrue(page.getCreateNewLinkButtonTimed()
                .isVisible());
        waitUntilFalse("the create link button should be disabled, if the textbox is empty", page.getCreateNewLinkButtonTimed()
                .isEnabled());
        assertThat("the create link button should not have a spinner", page.spinnerIsPresentInCreateApplinkButton(), is(false));
    }

    public static void verifyListApplicationLinkPageLayoutWithUrl(ListApplicationLinkPage page, String url) {
        waitUntilTrue(page.getApplicationUrlTextBoxTimed().isPresent());
        Poller.waitUntilEquals("textbox should contain url", url, page.getApplicationUrlTextBoxTimed().getValue());
        waitUntilTrue(page.getCreateNewLinkButtonTimed()
                .isVisible());
        waitUntilTrue("the create link button should be enabled, if the textbox is NOT empty", page.getCreateNewLinkButtonTimed()
                .isEnabled());
        assertThat("the create link button should not have a spinner", page.spinnerIsPresentInCreateApplinkButton(), is(false));
    }

    public static void verifyConfigureUrlDialogLayout(final ConfigureUrlDialog dialog, final String url, final String redirectedUrl) {
        // first, let's wait for the dialog to be open.
        waitUntilTrue(dialog.isAt());

        // url should have been updated to include protocol
        String visibleUrl = url.startsWith("http") ? url : "http://" + url;
        String correctedUrl = redirectedUrl != null ? redirectedUrl : visibleUrl;

        // check fields
        TimedElement userDefinedUrlEl = dialog.getUserDefinedUrlTextBoxTimed();
        waitUntilTrue(isEqual(visibleUrl, userDefinedUrlEl.getValue()));

        TimedElement correctedUrlEl = dialog.getCorrectedUrlTextBoxTimed();
        waitUntilTrue(isEqual(correctedUrl, correctedUrlEl.getValue()));

        if (!correctedUrlEl.getValue().now().equals(userDefinedUrlEl.getValue().now())) {
            // url has changed, e.g. a redirect
            // so user should see option to continue using their entered url.
            waitUntilTrue(dialog.getUseEnteredUrlCheckBoxTimed().isPresent());
        }

        verifyDialogControls(dialog);
    }

    public static void verifyConfirmUrlDialogLayout(final ConfirmUrlDialog dialog, final String rpcUrl, final String displayUrl) {
        // application url
        waitUntilTrue(dialog.getApplicationUrlRpcTextBoxTimed().isPresent());
        Poller.waitUntilEquals(rpcUrl, dialog.getApplicationUrlRpcTextBoxTimed().getValue());

        // display url
        waitUntilTrue(dialog.getApplicationUrlDisplayTextBoxTimed().isPresent());
        Poller.waitUntilEquals(displayUrl, dialog.getApplicationUrlDisplayTextBoxTimed().getValue());

        verifyDialogControls(dialog);
    }

    private static void verifyDialogControls(AbstractDialog page) {
        // check controls
        waitUntilTrue(page.getContinueButtonTimed().isPresent());
        waitUntilTrue(page.getContinueButtonTimed().isEnabled());
        waitUntilTrue(page.getCancelLinkTimed().isPresent());
        waitUntilTrue(page.getCancelLinkTimed()
                .isEnabled());
    }

    public static void verifyCreateNonUalIncomingDialogLayout(final CreateNonUalIncomingDialog dialog, ProductInstances productInstance) {
        waitUntilTrue(dialog.isAt());

        //check status text
        assertThat(dialog.getLocalApplicationUrl(), endsWith(productInstance.getBaseUrl()));
        assertThat(dialog.getLocalApplicationName(), endsWith(ApplicationTestHelper.getProductName(productInstance)));
        assertThat(dialog.getLocalApplicationApplication(), endsWith(productInstance.getProduct().getApplicationTypeName()));

        // check fields

        verifyDialogControls(dialog);
    }

    public static void verifyCreateLinkDialogLayout(final CreateNonUalDialog dialog, ProductInstances productInstance) {
        waitUntilTrue(dialog.isAt());

        //check status text
        assertThat(dialog.getLocalApplicationUrl(), endsWith(productInstance.getBaseUrl()));
        assertThat(dialog.getLocalApplicationName(), endsWith(ApplicationTestHelper.getProductName(productInstance)));
        assertThat(dialog.getLocalApplicationApplication(), endsWith(productInstance.getProduct().getApplicationTypeName()));

        // check fields
        waitUntilTrue(dialog.getApplicationNameTextBoxTimed().isPresent());
        Poller.waitUntilEquals("", dialog.getApplicationNameTextBoxTimed().getValue());

        waitUntilTrue(dialog.getApplicationTypeSelectTimed().isPresent());
        Poller.waitUntilEquals("generic", dialog.getApplicationTypeSelectTimed().getValue());

        // serviceProvider
        waitUntilTrue(dialog.getServiceProviderTextBoxTimed().isPresent());
        Poller.waitUntilEquals("", dialog.getServiceProviderTextBoxTimed().getValue());
        // consumerkey
        waitUntilTrue(dialog.getConsumerKeyTextBoxTimed().isPresent());
        Poller.waitUntilEquals("", dialog.getConsumerKeyTextBoxTimed().getValue());
        // sharedSecret
        waitUntilTrue(dialog.getSharedSecretTextBoxTimed().isPresent());
        Poller.waitUntilEquals("", dialog.getSharedSecretTextBoxTimed().getValue());
        // RequestTokenUrl
        waitUntilTrue(dialog.getRequestTokenUrlTextBoxTimed().isPresent());
        Poller.waitUntilEquals("", dialog.getRequestTokenUrlTextBoxTimed().getValue());
        // accessTokeUrl
        waitUntilTrue(dialog.getAccessTokenUrlTextBoxTimed().isPresent());
        Poller.waitUntilEquals("", dialog.getAccessTokenUrlTextBoxTimed().getValue());
        // authorizeUrl
        waitUntilTrue(dialog.getAuthorizeUrlTextBoxTimed().isPresent());
        Poller.waitUntilEquals("", dialog.getAuthorizeUrlTextBoxTimed().getValue());
        // createIncoming
        waitUntilTrue(dialog.getCreateIncomingCheckBoxTimed().isPresent());
        Poller.waitUntilEquals("on", dialog.getCreateIncomingCheckBoxTimed().getValue());

        verifyDialogControls(dialog);
    }

    public static void verifyIncompatibleApplinksVersionDialogLayout(final IncompatibleApplinksVersionDialog dialog, ProductInstance productInstance) {
        String warning = String.format("The remote application, '%s', is running an older version of the Application Links plugin. It will not be possible to create the remote end of the link from this application. However both ends of the link can be created using the older process in '%s'.",
                ApplicationTestHelper.getProductName(productInstance), ApplicationTestHelper.getProductName(productInstance));
        assertThat(dialog.getWarning(), is(warning));
        String information = String.format("Click here to transfer to '%s' to create the link. Alternatively click Continue to create the local end of the link only.", ApplicationTestHelper.getProductName(productInstance), ApplicationTestHelper.getProductName(productInstance));
        assertThat(dialog.getInformation(), is(information));
        verifyDialogControls(dialog);
    }

    public static void assertNoApplicationLinksAreConfigured(TestedProduct<WebDriverTester> product) {
        int numApplinks = product.visit(com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage.class) //
                .inApplicationLinks().allRows().size();
        assertEquals(0, numApplinks);
    }

    /**
     * Check the configuration of the created link.
     */
    public static void verifyAtlassianLinkConfigurationOneEnded(final RefappTestedProduct hostProduct, final RefappTestedProduct targetProduct, final boolean threeLOConfigured, final boolean twoLOConfigured, final boolean twoLOiConfigured) {
        ListApplicationLinkPage hostListApplicationLinkPage = hostProduct.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);

        // confirm the expect link was created.
        waitUntil(hostListApplicationLinkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(targetProduct
                .getProductInstance().getBaseUrl())));

        ApplicationDetailsSection applicationDetailsSection = hostListApplicationLinkPage.configureApplicationLink(targetProduct.getProductInstance().getBaseUrl());
        verifyAtlassianLinkIncomingOauthConfiguration(threeLOConfigured, twoLOConfigured, twoLOiConfigured, applicationDetailsSection);

        verifyAtlassianLinkConfigurationOneEndedOutgoingOauth(applicationDetailsSection);

    }

    /**
     * Check the configuration of the created link.
     */
    public static void verifyAtlassianLinkConfigurationTwoEnded(final RefappTestedProduct hostProduct, final RefappTestedProduct targetProduct, final boolean threeLOConfigured, final boolean twoLOConfigured, final boolean twoLOiConfigured/*, final String rpcUrl*/) {
        ListApplicationLinkPage hostListApplicationLinkPage = hostProduct.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);

        // confirm the expect link was created.
        waitUntil(hostListApplicationLinkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(targetProduct
                .getProductInstance().getBaseUrl())));

        ApplicationDetailsSection applicationDetailsSection = hostListApplicationLinkPage.configureApplicationLink(targetProduct.getProductInstance().getBaseUrl());
        verifyAtlassianLinkIncomingOauthConfiguration(threeLOConfigured, twoLOConfigured, twoLOiConfigured, applicationDetailsSection);

        verifyAtlassianLinkOutgoingOauthConfiguration(threeLOConfigured, twoLOConfigured, twoLOiConfigured, applicationDetailsSection);
    }

    /**
     * Check the configuration of the created link.
     */
    public static void verifyAtlassianLinkConfigurationDoesNotExist(final RefappTestedProduct hostProduct, final RefappTestedProduct targetProduct) {
        ListApplicationLinkPage hostListApplicationLinkPage = hostProduct.visit(com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage.class);

        // confirm the expect link was created.
        waitUntil(hostListApplicationLinkPage
                .getApplicationLinksTimed(), not(contains(hasDisplayUrl(targetProduct.getProductInstance()
                .getBaseUrl()))));
    }

    public static void verifyAtlassianLinkIncomingOauthConfiguration(boolean threeLOConfigured, boolean twoLOConfigured, boolean twoLOiConfigured, ApplicationDetailsSection applicationDetailsSection) {
        OauthIncomingAuthenticationWithAutoConfigSection oauthIncomingAuthenticationWithAutoConfigSection = applicationDetailsSection.openIncomingOauthExpectingAutoConfig();

        // 3LO configured as expected
        if (!threeLOConfigured) {
            assertThat(oauthIncomingAuthenticationWithAutoConfigSection.isNotConfigured(), is(true));
            assertThat(oauthIncomingAuthenticationWithAutoConfigSection.enableButtonIsVisible(), is(true));
            return;
        }

        assertThat(oauthIncomingAuthenticationWithAutoConfigSection.disableButtonIsVisible(), is(true));

        assertThat(oauthIncomingAuthenticationWithAutoConfigSection.isConfigured(), is(true));

        // 2LO configured as expected
        waitUntil(oauthIncomingAuthenticationWithAutoConfigSection.is2LOSectionPresentTimed(), is(true));
        waitUntil(oauthIncomingAuthenticationWithAutoConfigSection.is2LOEnabledTimed(), is(twoLOConfigured));

        if (twoLOiConfigured) {
            // 2LOi configured as expected
            waitUntil(oauthIncomingAuthenticationWithAutoConfigSection.is2LOWithImpersonationSectionPresentTimed(), is(true));
            waitUntil(oauthIncomingAuthenticationWithAutoConfigSection.is2LOImpersonationEnabledTimed(), is(twoLOiConfigured));
        }
    }

    public static void verifyAtlassianLinkOutgoingOauthConfiguration(boolean threeLOConfigured, boolean twoLOConfigured, boolean twoLOiConfigured, ApplicationDetailsSection applicationDetailsSection) {
        OauthOutgoingAuthenticationWithAutoConfigSection oauthOutgoingAuthenticationWithAutoConfigSection = applicationDetailsSection.openOutgoingOauthExpectingAutoConfig();

        // 3LO configured as expected
        if (!threeLOConfigured) {
            assertThat(oauthOutgoingAuthenticationWithAutoConfigSection.isNotConfigured(), is(true));
            assertThat(oauthOutgoingAuthenticationWithAutoConfigSection.enableButtonIsVisible(), is(true));
            return;
        }

        assertThat(oauthOutgoingAuthenticationWithAutoConfigSection.disableButtonIsVisible(), is(true));

        assertThat(oauthOutgoingAuthenticationWithAutoConfigSection.isConfigured(), is(true));

        // 2LO configured as expected
        waitUntil(oauthOutgoingAuthenticationWithAutoConfigSection.isOutgoing2LOEnabledTimed(), is(twoLOConfigured));

        if (!oauthOutgoingAuthenticationWithAutoConfigSection.outgoing2LOiCheckboxIsVisible()) {
            // outgoing 2LOi checkbox may not be present when link to a pre 4.0.15 applinks instance.
            return;
        }

        // 2LOi configured as expected
        waitUntil(oauthOutgoingAuthenticationWithAutoConfigSection
                .isOutgoing2LOiEnabledTimed(), is(twoLOiConfigured));
    }

    /**
     * One ended atlassian 2 atlassian links current show an error when trying to config oauth.
     */
    public static void verifyAtlassianLinkConfigurationOneEndedOutgoingOauth(ApplicationDetailsSection applicationDetailsSection) {
        OauthOutgoingAuthenticationWithAutoConfigSection oauthOutgoingAuthenticationWithAutoConfigSection = applicationDetailsSection.openOutgoingOauthExpectingAutoConfig();

        assertThat(oauthOutgoingAuthenticationWithAutoConfigSection.isOutgoingErrorMessagePresent(), is(true));
    }

    /**
     * Create a reciprocal 3LO + 2LO link using the REST API.
     *
     * @since 4.0.15
     */
    public static void createReciprocal3LO2LOLinkViaRest(final TestedProduct product, final TestedProduct product2) {
        create3LO2LOLinkViaRest(product2, product);

        create3LO2LOLinkViaRest(product, product2);
    }

    /**
     * Create a 2LOi link using the REST API.
     *
     * @since 4.0.15
     */
    public static void create2LOiLinkViaRest(final TestedProduct fromProduct, final TestedProduct toProduct) {
        ApplicationId toProductId = SimpleApplinksClient.createAtlassianAppLinkViaRest(fromProduct, toProduct);

        // outgoing

        // 3LO
        OAuthApplinksClient.registerAuthenticationProviderViaRest(fromProduct, toProductId, OAuthAuthenticationProvider.class);
        // 2LO
        OAuthApplinksClient.registerAuthenticationProviderViaRest(fromProduct, toProductId, TwoLeggedOAuthAuthenticationProvider.class);
        // 2LOI
        OAuthApplinksClient.registerAuthenticationProviderViaRest(fromProduct, toProductId, TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);

        // incoming

        // 3LO
        OAuthApplinksClient.registerConsumerViaRest(fromProduct, toProductId);

        // 2LO
        Consumer toProductConsumer = OAuthApplinksClient.getConsumerViaRest(fromProduct, toProductId);
        OAuthApplinksClient.registerConsumerViaRest(fromProduct, toProductId,
                Consumer
                        .key(toProductConsumer.getKey())
                        .name(toProductConsumer.getName())
                        .signatureMethod(toProductConsumer.getSignatureMethod())
                        .publicKey(toProductConsumer.getPublicKey())
                        .twoLOAllowed(true)
                        .twoLOImpersonationAllowed(true)
                        .build()
        );
    }

    /**
     * Create a 3LO + 2LO link using the REST API.
     *
     * @since 4.0.15
     */
    public static void create3LO2LOLinkViaRest(final TestedProduct fromProduct, final TestedProduct toProduct) {
        ApplicationId toProductId = SimpleApplinksClient.createAtlassianAppLinkViaRest(fromProduct, toProduct);

        // outgoing

        // 3LO
        OAuthApplinksClient.registerAuthenticationProviderViaRest(fromProduct, toProductId, OAuthAuthenticationProvider.class);
        // 2LO
        OAuthApplinksClient.registerAuthenticationProviderViaRest(fromProduct, toProductId, TwoLeggedOAuthAuthenticationProvider.class);

        // incoming

        // 3LO
        OAuthApplinksClient.registerConsumerViaRest(fromProduct, toProductId);

        // 2LO
        Consumer toProductConsumer = OAuthApplinksClient.getConsumerViaRest(fromProduct, toProductId);
        OAuthApplinksClient.registerConsumerViaRest(fromProduct, toProductId,
                Consumer
                        .key(toProductConsumer.getKey())
                        .name(toProductConsumer.getName())
                        .signatureMethod(toProductConsumer.getSignatureMethod())
                        .publicKey(toProductConsumer.getPublicKey())
                        .twoLOAllowed(true)
                        .twoLOImpersonationAllowed(false)
                        .build()
        );
    }

    /**
     * Get the existing OAuth consumer associated with the specified application link.
     *
     * @since 4.0.15
     */
    public static Consumer getConsumerViaRest(final TestedProduct product, final ApplicationId applicationId) {
        return ApplicationLinkResourceClient.forProduct(product).getConsumer(applicationId);
    }

    /**
     * Register a default OAuth Consumer for the the specified application link, this turns on incoming 3LO.
     *
     * @since 4.0.15
     */
    public static void registerConsumerViaRest(final TestedProduct product, final ApplicationId applicationId) {
        ApplicationLinkResourceClient.forProduct(product).putConsumer(applicationId);
    }

    /**
     * Register an OAuth Consumer for the the specified application link.
     *
     * @since 4.0.15
     */
    public static void registerConsumerViaRest(final TestedProduct product, final ApplicationId applicationId, final Consumer consumer) {
        ApplicationLinkResourceClient.forProduct(product).putConsumer(applicationId, consumer);
    }

    /**
     * Create a generic link using the REST API.
     *
     * @since 4.0.15
     */
    public static ApplicationId createGenericAppLinkViaRest(final TestedProduct fromProduct, final String baseUrl, final String name) {
        try {
            final ApplicationLinkEntity applicationLinkEntity = new ApplicationLinkEntity(null,
                    new TypeId("generic"),
                    name,
                    new URI(baseUrl),
                    null,
                    new URI(baseUrl),
                    new URI(baseUrl),
                    false,
                    false,
                    null);

            // add the link
            return ApplicationLinkResourceClient.forProduct(fromProduct).addApplicationLink(applicationLinkEntity);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Register the specified auth provider for the specified product, this adds outgoing configuration.
     *
     * @since 4.0.15
     */
    public static void registerAuthenticationProviderViaRest(final TestedProduct product, final ApplicationId applicationId, final Class<? extends AuthenticationProvider> authenticationProviderClass) {
        final AuthenticationProviderEntity applicationLinkEntity = new AuthenticationProviderEntity(null,
                null,
                authenticationProviderClass.getCanonicalName(),
                new HashMap<String, String>());

        ApplicationLinkResourceClient.forProduct(product).putAuthenticationProvider(applicationId, applicationLinkEntity);
    }
}
