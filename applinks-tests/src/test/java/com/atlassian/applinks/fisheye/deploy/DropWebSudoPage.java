package com.atlassian.applinks.fisheye.deploy;

import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;

public class DropWebSudoPage extends ApplinkAbstractPage {

    public String getUrl() {
        return "/plugins/servlet/dropwebsudo";
    }

    public <T> T navigateBack(Class<T> nextPageClass) {
        driver.navigate().back();
        return pageBinder.bind(nextPageClass);
    }
}
