package com.atlassian.applinks;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.application.refapp.RefAppApplicationType;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.internal.test.oauth.AccessTokenBackdoorResource;
import com.atlassian.applinks.internal.test.utils.TrustedUtils;
import com.atlassian.applinks.oauth.auth.servlets.serviceprovider.AbstractConsumerServlet;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsConfigurationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @see ApplinksTestResource
 * @see AccessTokenBackdoorResource
 * @deprecated use specific backdoor resources to create test data.
 */
@AnonymousAllowed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("testdata")
@Deprecated
@SuppressWarnings("all") // This code is deprecated and ready for removal in next major version
public class AgentTestResource {

    /** @see AbstractConsumerServlet#OAUTH_INCOMING_CONSUMER_KEY */
    private static final String OAUTH_INCOMING_CONSUMER_KEY = "oauth.incoming.consumerkey";
    private static final Logger log = LoggerFactory.getLogger(AgentTestResource.class);
    private static final int MAXMOCKS = 150;

    private final MutatingApplicationLinkService linkService;
    private final TypeAccessor typeAccessor;
    private final ManifestRetriever manifestRetriever;
    private final ServiceProviderConsumerStore serviceProviderConsumerStore;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final TrustedApplicationsConfigurationManager trustedApplicationsConfigurationManager;

    // Incrementing identifier for newly created mock links
    private int mockcount = 0;
    private List<LinkOptions> mocks = new ArrayList<LinkOptions>();

    public AgentTestResource(
            MutatingApplicationLinkService linkService,
            TypeAccessor typeAccessor,
            ManifestRetriever manifestRetriever,
            ServiceProviderConsumerStore serviceProviderConsumerStore,
            AuthenticationConfigurationManager authenticationConfigurationManager,
            AuthenticationConfigurationManager configurationManager,
            TrustedApplicationsConfigurationManager trustedApplicationsConfigurationManager) {
        this.linkService = linkService;
        this.typeAccessor = typeAccessor;
        this.manifestRetriever = manifestRetriever;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.serviceProviderConsumerStore = serviceProviderConsumerStore;
        this.trustedApplicationsConfigurationManager = trustedApplicationsConfigurationManager;
    }

    private enum OAUTHCOMBOS {L1, L2, L3, L4, BL1, BL2, BL3}

    private enum OTHERCOMBOS {O0, O1, O2, O3}

    /**
     * This class is capable of representing all combinations of authentication options. It is
     * loosely divided into 'oauth' and 'other' options for authentication.
     */
    private class LinkOptions {
        // other authentication options
        boolean addBasic;
        boolean addBasicOut;
        boolean addTrusted;
        boolean addTrustedOut;

        // oauth authentication options
        boolean add3LO;
        boolean add2LO;
        boolean add2LOi;
        boolean add3LOOut;
        boolean add2LOOut;
        boolean add2LOiOut;

        String description;
        int id;
        ApplicationId aid;


        // Other config
        public void setOtherIN(OTHERCOMBOS other) {
            setOther(true, other);
        }

        public void setOtherOUT(OTHERCOMBOS other) {
            setOther(false, other);
        }

        private void setOther(boolean in, OTHERCOMBOS other) {
            switch (other) {
                case O0:
                    setOtherFlags(in, false, false);
                    break;
                case O1:
                    setOtherFlags(in, true, false);
                    break;
                case O2:
                    setOtherFlags(in, false, true);
                    break;
                case O3:
                    setOtherFlags(in, true, true);
                    break;
            }
        }

        public void setOtherFlags(boolean in, boolean basic, boolean trusted) {
            if (in) {
                this.addBasic = basic;
                this.addTrusted = trusted;
            } else {
                this.addBasicOut = basic;
                this.addTrustedOut = trusted;
            }
        }


        // OAuth config
        public void setOAuthIn(OAUTHCOMBOS combo) {
            setOAuth(true, combo);
        }

        public void setOAuthOut(OAUTHCOMBOS combo) {
            setOAuth(false, combo);
        }

        public void setOAuth(boolean in, OAUTHCOMBOS combo) {
            switch (combo) {
                case L1:
                    setOAuthFlags(in, false, false, false);
                    break;
                case L2:
                    setOAuthFlags(in, true, false, false);
                    break;
                case L3:
                    setOAuthFlags(in, true, true, false);
                    break;
                case L4:
                    setOAuthFlags(in, true, true, true);
                    break;
                case BL1:
                    setOAuthFlags(in, false, true, false);
                    break;
                case BL2:
                    setOAuthFlags(in, false, false, true);
                    break;
                case BL3:
                    setOAuthFlags(in, false, true, true);
                    break;
            }
        }

        public void setOAuthFlags(boolean in, boolean add3LO, boolean add2LO, boolean add2LOi) {
            if (in) // do inbound
            {
                this.add3LO = add3LO;
                this.add2LO = add2LO;
                this.add2LOi = add2LOi;
            } else // do outbound
            {
                this.add3LOOut = add3LO;
                this.add2LOOut = add2LO;
                this.add2LOiOut = add2LOi;
            }
        }


    }


    /**
     * This method will populate the applinks system with preconfigured, dummy links. The
     * set of configurations is designed to cover all correct and incorrect scenarios.
     *
     * @return successful or internal error response in case of failure
     */
    @GET
    public Response populate() {
        if (mockcount > 0) {
            log.debug("agent test: already populated");
            return Response.ok().build();
        }

        log.debug("agent test: populating test links");
        try {

            for (OTHERCOMBOS otherin : OTHERCOMBOS.values()) {
                for (OTHERCOMBOS otherout : OTHERCOMBOS.values()) {
                    for (OAUTHCOMBOS oauthin : OAUTHCOMBOS.values()) {
                        for (OAUTHCOMBOS oauthout : OAUTHCOMBOS.values()) {
                            mockcount++;
                            LinkOptions options = new LinkOptions();
                            options.setOAuthIn(oauthin);
                            options.setOAuthOut(oauthout);
                            options.setOtherIN(otherin);
                            options.setOtherOUT(otherout);
                            options.id = mockcount;
                            options.description =
                                    "Mock-" + mockcount + " " +
                                            oauthin.toString() + "-" +
                                            oauthout.toString() + "-" +
                                            otherin.toString() + "-" +
                                            otherout.toString();
                            mocks.add(options);
                        }
                    }
                }
            }

            for (int i = 0; i < MAXMOCKS; i++) {
                if (mocks.size() >= MAXMOCKS) {
                    create(mocks.get(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
        return Response.ok().build();
    }

    /**
     * This method will create a dummy application link with the specified incoming
     * and outgoing configuration ( currently outgoing is all enabled)
     *
     * @param options An object containing all authentication options
     * @return
     * @throws Exception
     */
    private int create(LinkOptions options) throws Exception {
        // Create the link
        ApplicationLink link = null;
        try {
            ApplicationId aid = new ApplicationId("550e8400-e29b-41d4-a716-446655" + String.format("%06d", options.id));
            link =
                    linkService.addApplicationLink(
                            aid,
                            typeAccessor.loadApplicationType(new TypeId(RefAppApplicationType.class.getCanonicalName())),
                            ApplicationLinkDetails.builder()
                                    .displayUrl(new URI("http://unvoodoo.com"))
                                    .rpcUrl(new URI("http://unvoodoo.com/rpc"))
                                    .name(options.description)
                                    .isPrimary(true)
                                    .build()
                    );
            options.aid = aid;

        } catch (Exception e) {
            if (link == null) {
                throw e;
            } else {
                log.info("agenttest: ignoring creation Exception: " + e.getMessage());
            }
        }

        if (options.addBasic) {
            // It isn't possible to turn this off
        }

        // Add trusted apps in/out
        if (options.addTrusted) {
            new TrustedUtils(authenticationConfigurationManager, trustedApplicationsConfigurationManager)
                    .issueOfflineInboundTrust(link);
        }

        // Add oauth in
        if (options.add3LO || options.add2LO || options.add2LOi) {
            PublicKey publickey = KeyPairGenerator.getInstance("RSA").generateKeyPair().getPublic();
            Consumer updatedConsumer = new Consumer.InstanceBuilder("consumerkey-" + options.id)
                    .name("consumername-" + options.id)
                    .description("consumerdesc-" + options.id)
                    .publicKey(publickey)
                    .signatureMethod(Consumer.SignatureMethod.HMAC_SHA1)
                    .callback(new URI("http://callback.com"))
                    .twoLOAllowed(options.add2LO)
                    .executingTwoLOUser("2LOuser")
                    .twoLOImpersonationAllowed(options.add2LOi)
                    .build();
            serviceProviderConsumerStore.put(updatedConsumer);
            link.putProperty(OAUTH_INCOMING_CONSUMER_KEY, updatedConsumer.getKey());
            // This didn't work, the component import couldn't resolve it from the applinks plugin
            //serviceProviderStoreService.addConsumer(updatedConsumer, link);
        }

        // Add oauth out
        if (options.addBasicOut)
            authenticationConfigurationManager.registerProvider(link.getId(), BasicAuthenticationProvider.class, Collections.<String, String>emptyMap());
        if (options.addTrustedOut)
            authenticationConfigurationManager.registerProvider(link.getId(), TrustedAppsAuthenticationProvider.class, Collections.<String, String>emptyMap());
        if (options.add3LOOut)
            authenticationConfigurationManager.registerProvider(link.getId(), OAuthAuthenticationProvider.class, Collections.<String, String>emptyMap());
        if (options.add2LOOut)
            authenticationConfigurationManager.registerProvider(link.getId(), TwoLeggedOAuthAuthenticationProvider.class, Collections.<String, String>emptyMap());
        if (options.add2LOiOut)
            authenticationConfigurationManager.registerProvider(link.getId(), TwoLeggedOAuthWithImpersonationAuthenticationProvider.class, Collections.<String, String>emptyMap());

        return options.id;
    }


}
