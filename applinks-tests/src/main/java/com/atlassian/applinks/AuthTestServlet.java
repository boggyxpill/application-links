package com.atlassian.applinks;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ApplicationLinkUIService;
import com.atlassian.applinks.api.ApplicationLinkUIService.MessageFormat;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * This servlet tests the authorization state of each applink (or a specific applink,
 * if the application type is specified in the request parameter "app").  If the user
 * is authorized, it displays the username (span ID "username").  If not, it displays
 * an authentication request message using ApplicationLinkUIService - the banner
 * version by default, or the inline (lozenge) version if the request parameter
 * "inline" is present.
 * <p>
 * The "please authenticate" link in this banner begins the OAuth dance in a new
 * window/tab, and, if successful, refreshes the document (and adds a confirmation
 * banner at the top, if it's in an iframe).  This is all provided for us by the JS
 * code in applinks-public.
 * <p>
 * Mapped to {@code /plugins/servlet/applinks/applinks-tests/auth-test}.
 *
 * @since 3.6
 */
@SuppressWarnings("serial")
public class AuthTestServlet extends HttpServlet {
    private final ApplicationLinkService applicationLinkService;
    private final ApplicationLinkUIService applicationLinkUIService;
    private final TemplateRenderer templateRenderer;
    private final WebResourceManager webResourceManager;

    public AuthTestServlet(ApplicationLinkService applicationLinkService,
                           ApplicationLinkUIService applicationLinkUIService,
                           TemplateRenderer templateRenderer,
                           WebResourceManager webResourceManager) {
        this.applicationLinkService = applicationLinkService;
        this.applicationLinkUIService = applicationLinkUIService;
        this.templateRenderer = templateRenderer;
        this.webResourceManager = webResourceManager;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html");
        webResourceManager.requireResource("com.atlassian.applinks.applinks-tests:test-resources");

        boolean anyFound = false;
        String selectedType = req.getParameter("app");
        boolean useInlineFormat = (req.getParameter("inline") != null);
        String inlineDescription = req.getParameter("description");
        if (inlineDescription == null) {
            inlineDescription = "description";
        }

        ImmutableList.Builder<ItemRepresentation> items = ImmutableList.builder();

        for (ApplicationLink appLink : applicationLinkService.getApplicationLinks()) {
            if (selectedType != null && !selectedType.equals("") && !selectedType.equals(appLink.getType().getI18nKey())) {
                continue;
            }
            anyFound = true;
            String appName = appLink.getType().getI18nKey();
            String authenticatedUser;
            try {
                authenticatedUser = getAuthenticatedUsername(appLink);
            } catch (ResponseException e) {
                resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                resp.getWriter().write(e.toString());
                return;
            }
            if (authenticatedUser != null) {
                items.add(new ItemRepresentation(appName, authenticatedUser, null));
            } else {
                ApplicationLinkUIService.MessageBuilder builder = applicationLinkUIService.authorisationRequest(appLink);
                if (useInlineFormat) {
                    builder.format(MessageFormat.INLINE);
                    builder.contentHtml(inlineDescription);
                } else {
                    builder.format(MessageFormat.BANNER);
                }
                items.add(new ItemRepresentation(appName, null, builder.getHtml()));
            }
        }

        if (!anyFound) {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            resp.getWriter().write("Application link not found");
            return;
        }

        templateRenderer.render("auth-test.vm",
                ImmutableMap.<String, Object>of("items", items.build(),
                        "useInlineFormat", useInlineFormat,
                        "inlineDescription", inlineDescription),
                resp.getWriter());
    }

    private String getAuthenticatedUsername(ApplicationLink appLink) throws ResponseException {
        ApplicationLinkRequestFactory requestFactory = appLink.createAuthenticatedRequestFactory(OAuthAuthenticationProvider.class);

        try {
            ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET, "/plugins/servlet/applinks/whoami");
            String response = request.execute(new ApplicationLinkResponseHandler<String>() {
                public String handle(final Response response) throws ResponseException {
                    return response.getResponseBodyAsString();
                }

                public String credentialsRequired(final Response response) throws ResponseException {
                    return null;
                }
            });
            if (response != null && response.equals("")) {
                response = null;
            }
            return response;
        } catch (CredentialsRequiredException e) {
            return null;
        }
    }

    public static class ItemRepresentation {
        private String appName;
        private String authenticatedUser;
        private String messageHtml;

        public ItemRepresentation(String appName, String authenticatedUser, String messageHtml) {
            super();
            this.appName = appName;
            this.authenticatedUser = authenticatedUser;
            this.messageHtml = messageHtml;
        }

        public String getAppName() {
            return appName;
        }

        public String getAuthenticatedUser() {
            return authenticatedUser;
        }

        public String getMessageHtml() {
            return messageHtml;
        }
    }
}
