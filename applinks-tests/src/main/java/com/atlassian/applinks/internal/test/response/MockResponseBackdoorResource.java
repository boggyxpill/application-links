package com.atlassian.applinks.internal.test.response;

import com.atlassian.applinks.internal.common.rest.interceptor.RestRepresentationInterceptor;
import com.atlassian.applinks.internal.common.rest.util.RestResponses;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.test.rest.model.RestMockResponseRequest;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.test.response.RequestPredicates.forRestRequestPredicate;

/**
 * @since 5.0
 */
@AnonymousAllowed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("mock-response")
@InterceptorChain({NoCacheHeaderInterceptor.class, RestRepresentationInterceptor.class})
public class MockResponseBackdoorResource {
    private final MockResponseSetup mockResponseSetup;

    public MockResponseBackdoorResource(MockResponseSetup mockResponseSetup) {
        this.mockResponseSetup = mockResponseSetup;
    }

    @POST
    public Response addMockSetup(RestMockResponseRequest request) {
        mockResponseSetup.addResponseDefinition(
                forRestRequestPredicate(request.getRequestPredicate()),
                request.getResponseDefinition().asDomain());

        return RestResponses.created(request.getResponseDefinition());
    }

    @DELETE
    public Response clearMockSetup() {
        mockResponseSetup.removeAll();

        return Response.noContent().build();
    }
}
