package com.atlassian.applinks;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.application.refapp.RefAppApplicationType;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.security.auth.trustedapps.TrustedApplicationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base class for test trusted applinks request servlets.
 * This takes a url that is requested via TA.
 * Be sure to set the url parameter when subclassing and using.
 *
 * @since 3.11.0
 */
public abstract class AbstractTrustedRequestServlet extends HttpServlet {
    protected static final Logger log = LoggerFactory.getLogger(AbstractTrustedRequestServlet.class);

    protected final MutatingApplicationLinkService applicationLinkService;

    @SuppressWarnings("squid:S2226") // Updating the underlying field may result in unexpected behaviour
    private String url;

    public AbstractTrustedRequestServlet(final MutatingApplicationLinkService applicationLinkService) {
        this.applicationLinkService = applicationLinkService;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    @SuppressWarnings("squid:S1989") // resp.sendError can throw IOException but this is already handled
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        log.debug("doGet - Getting trusted apps link signature:{}", req.getHeader(TrustedApplicationUtils.Header.Request.SIGNATURE));
        final ApplicationLink primaryApplicationLink = applicationLinkService.getPrimaryApplicationLink(RefAppApplicationType.class);
        final ApplicationLinkRequestFactory requestFactory = primaryApplicationLink.createAuthenticatedRequestFactory();
        try {
            final String[] responseBodyHolder = new String[1];
            final ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET, url + req.getMethod());
            request.execute(new ApplicationLinkResponseHandler<Response>() {
                @Override
                public Response credentialsRequired(final Response response) throws ResponseException {
                    responseBodyHolder[0] = response.getResponseBodyAsString();
                    return response;
                }

                @Override
                public Response handle(final Response response) throws ResponseException {
                    return credentialsRequired(response);
                }
            });

            resp.setContentType("text/html");
            final PrintWriter writer = resp.getWriter();
            writer.print(responseBodyHolder[0]);
        } catch (CredentialsRequiredException e) {
            log.error("doGet - Caught a credentials exception:{}", e.getMessage());
            resp.sendError(404, "A credentials error occurred when executing a request to the remote application");
        } catch (ResponseException e) {
            log.error("doGet - Caught a response exception:{}", e.getMessage());
            resp.sendError(404, "An error occurred when executing a request to the remote application");
        }
    }
}