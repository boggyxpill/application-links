package com.atlassian.applinks;

import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;

/**
 * Trusted application links redirect servlet.
 *
 * @since 3.11.0
 */
public class TrustedTestRedirectServlet extends AbstractTrustedRequestServlet {
    public TrustedTestRedirectServlet(final MutatingApplicationLinkService applicationLinkService) {
        super(applicationLinkService);
        setUrl("/plugins/servlet/applinks/applinks-tests/redirecttosomwhere/abc");
    }
}