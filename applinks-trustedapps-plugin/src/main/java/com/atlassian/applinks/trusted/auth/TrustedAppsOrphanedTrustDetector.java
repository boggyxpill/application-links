package com.atlassian.applinks.trusted.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.core.auth.InternalOrphanedTrustDetector;
import com.atlassian.applinks.core.auth.OrphanedTrustCertificate;
import com.atlassian.security.auth.trustedapps.TrustedApplication;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsConfigurationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Finds orphaned Trusted Applications certificates (that is, they are stored
 * locally but are not related to a configured {@link ApplicationLink})
 *
 * @since 3.0
 */
@Component
public class TrustedAppsOrphanedTrustDetector implements InternalOrphanedTrustDetector {
    private final ApplicationLinkService applicationLinkService;
    private final TrustedApplicationsConfigurationManager trustedApplicationsConfigurationManager;
    private final TrustConfigurator trustConfigurator;
    private static final Logger log = LoggerFactory.getLogger(TrustedAppsOrphanedTrustDetector.class);

    @Autowired
    public TrustedAppsOrphanedTrustDetector(final ApplicationLinkService applicationLinkService,
                                            final TrustedApplicationsConfigurationManager trustedApplicationsConfigurationManager,
                                            final TrustConfigurator trustConfigurator) {
        this.applicationLinkService = applicationLinkService;
        this.trustedApplicationsConfigurationManager = trustedApplicationsConfigurationManager;
        this.trustConfigurator = trustConfigurator;
    }

    @Override
    public List<OrphanedTrustCertificate> findOrphanedTrustCertificates() {
        final List<OrphanedTrustCertificate> orphanedTrustCertificates = new ArrayList<OrphanedTrustCertificate>();

        final Set<String> recognisedIds = new HashSet<String>();
        for (final ApplicationLink link : applicationLinkService.getApplicationLinks()) {
            final String id = (String) link.getProperty(AbstractTrustedAppsServlet.TRUSTED_APPS_INCOMING_ID);
            if (id != null) {
                recognisedIds.add(id);
            }
        }

        for (final TrustedApplication trustedApp : trustedApplicationsConfigurationManager.getTrustedApplications()) {
            if (!recognisedIds.contains(trustedApp.getID())) {
                orphanedTrustCertificates.add(
                        new OrphanedTrustCertificate(trustedApp.getID(), trustedApp.getName(), OrphanedTrustCertificate.Type.TRUSTED_APPS)
                );
            }
        }

        return orphanedTrustCertificates;
    }

    @Override
    public void deleteTrustCertificate(final String id, final OrphanedTrustCertificate.Type type) {
        checkCertificateType(type);

        trustedApplicationsConfigurationManager.deleteApplication(id);
    }

    private void checkCertificateType(final OrphanedTrustCertificate.Type type) {
        if (!canHandleCertificateType(OrphanedTrustCertificate.Type.TRUSTED_APPS)) {
            throw new IllegalArgumentException("Unsupported type: " + type);
        }
    }

    @Override
    public boolean canHandleCertificateType(final OrphanedTrustCertificate.Type type) {
        return type == OrphanedTrustCertificate.Type.TRUSTED_APPS;
    }

    @Override
    public void addOrphanedTrustToApplicationLink(final String id, final OrphanedTrustCertificate.Type type, final ApplicationId applicationId) {
        checkCertificateType(type);
        try {
            ApplicationLink applicationLink = applicationLinkService.getApplicationLink(applicationId);
            trustConfigurator.issueInboundTrust(applicationLink);
            log.debug("Associated Trusted Apps configuration for Application Link id='" + applicationLink.getId() + "' and name='" + applicationLink.getName() + "'");
        } catch (TypeNotInstalledException e) {
            throw new RuntimeException("An application of the type " + e.getType() + " is not installed!", e);
        } catch (TrustConfigurator.ConfigurationException e) {
            throw new RuntimeException("Failed to add Trusted Apps configuration for Application Link with id '" + applicationId + '"', e);
        }
    }
}
