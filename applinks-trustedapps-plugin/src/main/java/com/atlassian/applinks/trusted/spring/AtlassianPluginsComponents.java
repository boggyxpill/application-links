package com.atlassian.applinks.trusted.spring;

import com.atlassian.applinks.core.auth.InternalOrphanedTrustDetector;
import com.atlassian.applinks.trusted.auth.TrustedAppsOrphanedTrustDetector;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

@Configuration
public class AtlassianPluginsComponents {

    @Bean
    @SuppressWarnings("rawtypes")
    public FactoryBean<ServiceRegistration> exportTrustedAppsOrphanedTrustDetector(
            final TrustedAppsOrphanedTrustDetector trustedAppsOrphanedTrustDetector) {
        return exportOsgiService(trustedAppsOrphanedTrustDetector, as(InternalOrphanedTrustDetector.class));
    }
}
