package com.atlassian.applinks.trusted.spring;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsConfigurationManager;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class AtlassianPluginsComponentImports {

    @Bean
    public ApplicationLinkService applicationLinkService() {
        return importOsgiService(ApplicationLinkService.class);
    }

    @Bean
    public ApplicationProperties applicationProperties() {
        return importOsgiService(ApplicationProperties.class);
    }

    @Bean
    public AuthenticationConfigurationManager authenticationConfigurationManager() {
        return importOsgiService(AuthenticationConfigurationManager.class);
    }

    @Bean
    public EventPublisher eventPublisher() {
        return importOsgiService(EventPublisher.class);
    }

    @Bean
    public I18nResolver i18nResolver() {
        return importOsgiService(I18nResolver.class);
    }

    @Bean
    public InternalHostApplication internalHostApplication() {
        return importOsgiService(InternalHostApplication.class);
    }

    @Bean
    public InternalTypeAccessor internalTypeAccessor() {
        return importOsgiService(InternalTypeAccessor.class);
    }

    @Bean
    public LocaleResolver localeResolver() {
        return importOsgiService(LocaleResolver.class);
    }

    @Bean
    public LoginUriProvider loginUriProvider() {
        return importOsgiService(LoginUriProvider.class);
    }

    @Bean
    public PluginAccessor pluginAccessor() {
        return importOsgiService(PluginAccessor.class);
    }

    @Bean
    public RequestFactory<?> requestFactory() {
        return importOsgiService(RequestFactory.class);
    }

    @Bean
    public TemplateRenderer templateRenderer() {
        return importOsgiService(TemplateRenderer.class);
    }

    @Bean
    public TrustedApplicationsConfigurationManager trustedApplicationsConfigurationManager() {
        return importOsgiService(TrustedApplicationsConfigurationManager.class);
    }

    @Bean
    public TrustedApplicationsManager trustedApplicationsManager() {
        return importOsgiService(TrustedApplicationsManager.class);
    }

    @Bean
    public UserManager userManager() {
        return importOsgiService(UserManager.class);
    }

    @Bean
    public WebResourceManager webResourceManager() {
        return importOsgiService(WebResourceManager.class);
    }

    @Bean
    public WebResourceUrlProvider webResourceUrlProvider() {
        return importOsgiService(WebResourceUrlProvider.class);
    }

    @Bean
    public WebSudoManager webSudoManager() {
        return importOsgiService(WebSudoManager.class);
    }

    @Bean
    public XsrfTokenAccessor xsrfTokenAccessor() {
        return importOsgiService(XsrfTokenAccessor.class);
    }

    @Bean
    public XsrfTokenValidator xsrfTokenValidator() {
        return importOsgiService(XsrfTokenValidator.class);
    }
}
