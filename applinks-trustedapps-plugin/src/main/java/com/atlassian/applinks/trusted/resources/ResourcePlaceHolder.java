package com.atlassian.applinks.trusted.resources;

/**
 * Place holder class used to force the BND Import-Package to pull in the resources from applinks-plugin
 *
 * @since 5.0.0
 */
public class ResourcePlaceHolder extends com.atlassian.applinks.core.common.ResourcePlaceHolder {
}
