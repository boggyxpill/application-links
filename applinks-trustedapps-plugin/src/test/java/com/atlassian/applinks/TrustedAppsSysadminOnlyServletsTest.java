package com.atlassian.applinks;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.RedirectController;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.trusted.auth.ConsumerConfigurationServlet;
import com.atlassian.applinks.trusted.auth.ProviderConfigurationServlet;
import com.atlassian.applinks.trusted.auth.TrustConfigurator;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.applinks.ui.velocity.ListApplicationLinksContext;
import com.atlassian.applinks.ui.velocity.VelocityContextFactory;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsConfigurationManager;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Before;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Theories.class)
public class TrustedAppsSysadminOnlyServletsTest {
    @DataPoints
    public static SysadminOnlyTestServlet[] testSysadminOnlyServlets = SysadminOnlyTestServlet.values();

    private static I18nResolver i18nResolver;
    private static MessageFactory messageFactory;
    private static TemplateRenderer templateRenderer;
    private static WebResourceManager webResourceManager;
    private static ApplicationLinkService applicationLinkService;
    private static AdminUIAuthenticator adminUIAuthenticator;
    private static InternalHostApplication internalHostApplication;
    private static DocumentationLinker documentationLinker;
    private static LoginUriProvider loginUriProvider;
    private static WebSudoManager webSudoManager;
    private static VelocityContextFactory velocityContextFactory;
    private static TrustedApplicationsManager trustedApplicationsManager;
    private static AuthenticationConfigurationManager configurationManager;
    private static TrustedApplicationsConfigurationManager trustedAppsManager;
    private static TrustConfigurator trustConfigurator;
    private static XsrfTokenAccessor xsrfTokenAccessor;
    private static XsrfTokenValidator xsrfTokenValidator;
    private static UserManager userManager;
    private static RedirectController redirectController;

    private HttpServletRequest request;
    private HttpServletResponse response;

    @Before
    public void setUp() {
        i18nResolver = mock(I18nResolver.class);
        messageFactory = mock(MessageFactory.class);
        templateRenderer = mock(TemplateRenderer.class);
        webResourceManager = mock(WebResourceManager.class);
        applicationLinkService = mock(ApplicationLinkService.class);
        adminUIAuthenticator = mock(AdminUIAuthenticator.class);
        internalHostApplication = mock(InternalHostApplication.class);
        documentationLinker = mock(DocumentationLinker.class);
        loginUriProvider = mock(LoginUriProvider.class);
        webSudoManager = mock(WebSudoManager.class);
        velocityContextFactory = mock(VelocityContextFactory.class);
        trustedApplicationsManager = mock(TrustedApplicationsManager.class);
        configurationManager = mock(AuthenticationConfigurationManager.class);
        trustedAppsManager = mock(TrustedApplicationsConfigurationManager.class);
        trustConfigurator = mock(TrustConfigurator.class);
        xsrfTokenAccessor = mock(XsrfTokenAccessor.class);
        xsrfTokenValidator = mock(XsrfTokenValidator.class);
        userManager = mock(UserManager.class);
        redirectController = mock(RedirectController.class);

        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);

        ListApplicationLinksContext listApplicationLinksContext = mock(ListApplicationLinksContext.class);

        when(request.getPathInfo()).thenReturn("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        when(request.getServletPath()).thenReturn("servlet-path");
        when(xsrfTokenValidator.validateFormEncodedToken(request)).thenReturn(true);
        when(userManager.getRemoteUsername()).thenReturn("admin");
        when(userManager.isSystemAdmin(any(String.class))).thenReturn(true);
        when(velocityContextFactory.buildListApplicationLinksContext(request)).thenReturn(listApplicationLinksContext);
    }

    @Theory
    public void verifyThatSysadminAccessIsCheckedForSysadminOnlyServlet(SysadminOnlyTestServlet servlet) throws Exception {
        when(request.getMethod()).thenReturn(servlet.getMethod());
        whenUserIsLoggedInAsSysadmin();
        servlet.getServlet().service(request, response);

        verify(adminUIAuthenticator).checkSysadminUIAccessBySessionOrCurrentUser(request);
    }

    @Theory
    public void verifyThatWebSudoRequestIsExecutedForSysadminOnlyServlet(SysadminOnlyTestServlet servlet) throws Exception {
        when(request.getMethod()).thenReturn(servlet.getMethod());
        whenUserIsLoggedInAsSysadmin();
        servlet.getServlet().service(request, response);

        verify(webSudoManager).willExecuteWebSudoRequest(request);
    }

    @Theory
    public void verifyWebSudoGivenControlWhenRequestRequiresSudoAuth(SysadminOnlyTestServlet servlet) throws Exception {
        when(request.getMethod()).thenReturn(servlet.getMethod());
        whenUserIsLoggedInAsSysadmin();
        doThrow(new WebSudoSessionException("blah")).when(webSudoManager).willExecuteWebSudoRequest(request);
        servlet.getServlet().service(request, response);

        verify(webSudoManager).enforceWebSudoProtection(request, response);
    }

    private static HttpServlet createConsumerConfigServlet() {
        return new ConsumerConfigurationServlet(i18nResolver, templateRenderer, adminUIAuthenticator,
                webResourceManager, configurationManager, applicationLinkService, messageFactory,
                trustedApplicationsManager, trustedAppsManager, internalHostApplication, trustConfigurator,
                loginUriProvider, documentationLinker, webSudoManager,
                xsrfTokenAccessor, xsrfTokenValidator, userManager, redirectController);
    }

    private static HttpServlet createProviderConfigServlet() {
        return new ProviderConfigurationServlet(i18nResolver, templateRenderer, adminUIAuthenticator,
                webResourceManager, applicationLinkService, messageFactory, trustedAppsManager,
                configurationManager, trustedApplicationsManager, internalHostApplication, trustConfigurator,
                loginUriProvider, documentationLinker, webSudoManager, xsrfTokenAccessor,
                xsrfTokenValidator, userManager);
    }

    private void whenUserIsLoggedInAsSysadmin() {
        when(adminUIAuthenticator.checkSysadminUIAccessBySessionOrCurrentUser(request)).thenReturn(true);
    }

    enum SysadminOnlyTestServlet {
        TRUSTED_CONSUMER_CONFIGURATION_SERVLET_GET {
            @Override
            HttpServlet getServlet() {
                return createConsumerConfigServlet();
            }

            @Override
            String getMethod() {
                return "GET";
            }
        },

        TRUSTED_CONSUMER_CONFIGURATION_SERVLET_POST {
            @Override
            HttpServlet getServlet() {
                return createConsumerConfigServlet();
            }

            @Override
            String getMethod() {
                return "POST";
            }
        },

        TRUSTED_PROVIDER_CONFIGURATION_SERVLET_GET {
            @Override
            HttpServlet getServlet() {
                return createProviderConfigServlet();
            }

            @Override
            String getMethod() {
                return "GET";
            }
        },

        TRUSTED_PROVIDER_CONFIGURATION_SERVLET_POST {
            @Override
            HttpServlet getServlet() {
                return createProviderConfigServlet();
            }

            @Override
            String getMethod() {
                return "POST";
            }
        };

        abstract HttpServlet getServlet();

        abstract String getMethod();
    }
}