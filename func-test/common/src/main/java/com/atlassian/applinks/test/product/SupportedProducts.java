package com.atlassian.applinks.test.product;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

/**
 * Enumeration of AMPS products used in Applinks tests.
 *
 * @since 4.3
 */
public enum SupportedProducts {
    JIRA("jira", "jira", "Jira"),
    CONFLUENCE("confluence", "confluence", "Confluence"),
    BITBUCKET("stash", "bitbucket", "Bitbucket Server"),
    BAMBOO("bamboo", "bamboo", "Bamboo"),
    FECRU("fecru", "fecru", "FishEye / Crucible"),
    REFAPP("refapp", "refapp", "Reference Application"),
    GENERIC("generic", "generic", "Generic Application");

    @Nullable
    public static SupportedProducts fromId(@Nonnull String id) {
        requireNonNull(id, "id");
        for (SupportedProducts product : values()) {
            if (id.startsWith(product.getProductId())) {
                return product;
            }
        }
        return null;
    }

    private final String applicationTypeId;
    private final String productId;
    private final String typeName;

    SupportedProducts(String applicationTypeId, String productId, String typeName) {
        this.applicationTypeId = applicationTypeId;
        this.productId = productId;
        this.typeName = typeName;
    }

    @Nonnull
    public String getProductId() {
        return productId;
    }

    /**
     * @return expected human-readable application type name
     */
    @Nonnull
    public String getApplicationTypeName() {
        return typeName;
    }

    @Nonnull
    public String getApplicationTypeId() {
        return applicationTypeId;
    }
}
