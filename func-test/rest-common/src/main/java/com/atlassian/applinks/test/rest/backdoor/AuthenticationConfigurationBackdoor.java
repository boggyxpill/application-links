package com.atlassian.applinks.test.rest.backdoor;

import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forTestRestModule;
import static java.util.Objects.requireNonNull;

/**
 * See {@code AuthenticationConfigurationBackdoorResource}.
 *
 * @since 5.1
 */
public class AuthenticationConfigurationBackdoor {
    private final BaseRestTester authConfigTester;

    public AuthenticationConfigurationBackdoor(@Nonnull TestedInstance instance) {
        this.authConfigTester = new BaseRestTester(forTestRestModule(instance).path("auth-config"));
    }

    public Response configureAuthentication(@Nonnull BackdoorConfigureAuthenticationRequest request) {
        requireNonNull(request, "request");

        return authConfigTester.put(request);
    }

    public Response disableAuthentication(@Nonnull BackdoorConfigureAuthenticationRequest request) {
        requireNonNull(request, "request");

        return authConfigTester.delete(request);
    }
}
