package com.atlassian.applinks.test.rest;

import javax.annotation.Nonnull;
import java.net.URI;

import static java.util.Objects.requireNonNull;

/**
 * {@link AbstractRestRequest} extension that allows to provide Applinks-specific parameters, e.g.
 * {@code authorisationCallback}.
 *
 * @since 4.3
 */
public abstract class AbstractApplinksRestRequest extends AbstractRestRequest {
    private static final String AUTHORISATION_CALLBACK = "authorisationCallback";

    protected AbstractApplinksRestRequest(AbstractBuilder<?, ?> builder) {
        super(builder);
    }

    protected abstract static class AbstractApplinksBuilder<B extends AbstractApplinksBuilder<B, R>,
            R extends AbstractApplinksRestRequest> extends AbstractBuilder<B, R> {
        /**
         * Add {@code authorisationCallback} query parameter to the request. This is picked up by REST resources that
         * may return {@code authorisationUri} in their response, so it can be customized with the callback to call
         * after the authorisation is complete
         *
         * @param callback URI pointing to the authorisation callback
         * @return this builder instance
         * @see com.atlassian.applinks.api.AuthorisationURIGenerator
         */
        public B authorisationCallback(@Nonnull String callback) {
            return queryParam(AUTHORISATION_CALLBACK, callback);
        }

        /**
         * @param callback URI pointing to the authorisation callback
         * @return this builder instance
         * @see com.atlassian.applinks.api.AuthorisationURIGenerator
         */
        public B authorisationCallback(@Nonnull URI callback) {
            return authorisationCallback(requireNonNull(callback, "callback").toString());
        }
    }
}
