package com.atlassian.applinks.test.rest.backdoor;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.AbstractRestRequest;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls.RestModules;
import com.jayway.restassured.response.Response;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.test.rest.matchers.StatusMatchers.noContent;
import static com.atlassian.applinks.test.rest.matchers.StatusMatchers.okOrNotFound;
import static com.atlassian.applinks.test.rest.url.ApplinksRestUrls.forRestModule;
import static java.util.Collections.singletonMap;
import static java.util.Objects.requireNonNull;

/**
 * Backdoor to access the {@code systemProperties} resource in the {@code applinks-tests} backdoor plugin.
 * <p>
 * NOTE: this will only work against applications that are running the {@code applinks-tests} backdoor plugin.
 *
 * @since 4.3
 */
public class SystemPropertiesBackdoor {
    static final String RESOURCE_PATH = "systemProperties";
    static final String VALUE_PROPERTY = "value";

    private final BaseRestTester systemPropertiesTester;

    public SystemPropertiesBackdoor(@Nonnull String baseUrl) {
        // no need for authentication, the resource allows anonymous access
        this.systemPropertiesTester = new BaseRestTester(getRestPath(baseUrl));
    }

    public SystemPropertiesBackdoor(@Nonnull TestedInstance instance) {
        this(instance.getBaseUrl());
    }

    public void setProperty(@Nonnull String key, @Nullable String value) {
        requireNonNull(key, "key");
        if (value == null) {
            clearProperty(key);
        } else {
            systemPropertiesTester.put(new PropertyRequest.Builder(key)
                    .value(value)
                    .expectStatus(noContent())
                    .build());
        }
    }

    public void clearProperty(@Nonnull String key) {
        systemPropertiesTester.delete(new PropertyRequest.Builder(key)
                .expectStatus(noContent())
                .build());
    }

    @Nullable
    public String getProperty(@Nonnull String key) {
        Response response = systemPropertiesTester.get(new PropertyRequest.Builder(key)
                .expectStatus(okOrNotFound())
                .build());

        return response.getStatusCode() == Status.OK.getStatusCode() ?
                response.as(BaseRestEntity.class).getString(VALUE_PROPERTY) :
                null;
    }

    private static RestUrl getRestPath(@Nonnull String baseUrl) {
        return forRestModule(baseUrl, RestModules.APPLINKS_TESTS).path(RESOURCE_PATH);
    }

    private static final class PropertyRequest extends AbstractRestRequest {
        private final String key;
        private final String value;

        protected PropertyRequest(Builder builder) {
            super(builder);
            this.key = builder.key;
            this.value = builder.value;
        }

        @Nonnull
        @Override
        protected RestUrl getPath() {
            return RestUrl.forPath(key);
        }

        @Nullable
        @Override
        protected Object getBody() {
            if (value != null) {
                return singletonMap(VALUE_PROPERTY, value);
            } else {
                return null;
            }
        }

        static class Builder extends AbstractBuilder<Builder, PropertyRequest> {
            private final String key;
            private String value;

            Builder(@Nonnull String key) {
                Validate.notBlank(key,"Property key should not be blank");
                this.key = key;
            }

            public Builder value(@Nullable String value) {
                this.value = value;
                return this;
            }

            @Nonnull
            @Override
            public PropertyRequest build() {
                return new PropertyRequest(this);
            }

            @Nonnull
            @Override
            protected Builder self() {
                return this;
            }
        }
    }
}
