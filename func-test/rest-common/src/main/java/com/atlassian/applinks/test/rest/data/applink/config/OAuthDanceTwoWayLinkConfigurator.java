package com.atlassian.applinks.test.rest.data.applink.config;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Applink configurator that extends {@link OAuthDanceConfigurator} to provide two way applink configuration.
 *
 * @see OAuthDanceConfigurator
 * @since 5.1
 */
public class OAuthDanceTwoWayLinkConfigurator extends OAuthDanceConfigurator {
    @Nonnull
    public static OAuthDanceTwoWayLinkConfigurator setUp3LoAccessForTwoWayLink(@Nonnull TestAuthentication authentication) {
        return setUp3LoAccessForTwoWayLink(authentication, authentication);
    }

    @Nonnull
    public static OAuthDanceTwoWayLinkConfigurator setUp3LoAccessForTwoWayLink(@Nonnull String username) {
        return setUp3LoAccessForTwoWayLink(username, username);
    }

    @Nonnull
    @SuppressWarnings("ConstantConditions")
    public static OAuthDanceTwoWayLinkConfigurator setUp3LoAccessForTwoWayLink(@Nonnull TestAuthentication fromAuthentication,
                                                                               @Nonnull TestAuthentication toAuthentication) {
        return setUp3LoAccessForTwoWayLink(fromAuthentication.getUsername(), toAuthentication.getUsername());
    }

    @Nonnull
    public static OAuthDanceTwoWayLinkConfigurator setUp3LoAccessForTwoWayLink(@Nonnull String fromUsername, @Nonnull String toUsername) {
        return new OAuthDanceTwoWayLinkConfigurator(fromUsername, toUsername);
    }

    protected OAuthDanceTwoWayLinkConfigurator(@Nonnull String fromUsername, @Nonnull String toUsername) {
        super(fromUsername, toUsername);
    }

    @Override
    public void configure(@Nonnull TestApplink applink) {
        requireNonNull(applink, "applink");
        super.configure(applink);
        super.configure(applink.reverse());
    }
}
