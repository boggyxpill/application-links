package com.atlassian.applinks.test.rest.matchers;

import com.atlassian.applinks.internal.rest.model.applink.RestExtendedApplicationLink;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import java.util.Map;

import static com.atlassian.applinks.test.rest.matchers.RestMatchers.hasPropertyThat;

/**
 * @see RestExtendedApplicationLink
 * @since 5.0
 */
public final class RestExtendedApplicationLinkMatchers {
    private RestExtendedApplicationLinkMatchers() {
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withPropertyThat(@Nonnull String propertyKey,
                                                                @Nonnull Matcher<?> valueMatcher) {
        return hasPropertyThat(RestExtendedApplicationLink.PROPERTIES, hasPropertyThat(propertyKey, valueMatcher));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withDataThat(@Nonnull String dataKey,
                                                            @Nonnull Matcher<?> valueMatcher) {
        return hasPropertyThat(RestExtendedApplicationLink.DATA, hasPropertyThat(dataKey, valueMatcher));
    }
}
