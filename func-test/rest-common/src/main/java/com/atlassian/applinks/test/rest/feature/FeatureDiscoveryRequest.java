package com.atlassian.applinks.test.rest.feature;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.rest.AbstractRestRequest;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Request to the feature discovery resource, either to check or manipulate a specific feature key
 *
 * @since 4.3
 */
public class FeatureDiscoveryRequest extends AbstractRestRequest {
    private final String featureKey;

    private FeatureDiscoveryRequest(Builder builder) {
        super(builder);
        this.featureKey = builder.featureKey;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.forPath(featureKey);
    }

    public static final class Builder extends AbstractBuilder<Builder, FeatureDiscoveryRequest> {
        private final String featureKey;

        public Builder(@Nonnull String featureKey) {
            this.featureKey = requireNonNull(featureKey, "feature");
        }

        @Nonnull
        @Override
        public FeatureDiscoveryRequest build() {
            return new FeatureDiscoveryRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
