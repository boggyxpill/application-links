package com.atlassian.applinks.test.rest;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.jayway.restassured.specification.ResponseSpecification;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * Base class for REST testers. REST tester is a lightweight wrapper around the {@code RestAssured} library that allows
 * building re-usable test clients for REST resources.
 * <br>
 * This class can be used standalone given a {@link AbstractRestRequest request} implementation that encapsulates all
 * REST resource and request-specific data. For extra DRY-ness it is recommended to extend this class to build a
 * resource-specific builder, paired with a request class that encapsulates request-specific data.
 *
 * @since 4.3
 */
public class BaseRestTester {
    protected final RestUrl resourceUrl;
    protected final TestAuthentication defaultAuthentication;

    public BaseRestTester(@Nonnull RestUrl resourceUrl) {
        this(resourceUrl, null);
    }

    public BaseRestTester(@Nonnull RestUrl resourceUrl, @Nullable TestAuthentication defaultAuthentication) {
        this.resourceUrl = requireNonNull(resourceUrl, "resourceUrl");
        this.defaultAuthentication = defaultAuthentication == null ?
                TestAuthentication.anonymous() :
                defaultAuthentication;
    }

    @Nonnull
    public final Response delete(@Nonnull AbstractRestRequest request) {
        return buildRequest(requireNonNull(request, "request")).delete(buildUrl(request));
    }

    @Nonnull
    public final Response get(@Nonnull AbstractRestRequest request) {
        return buildRequest(requireNonNull(request, "request")).get(buildUrl(request));
    }

    @Nonnull
    public final Response post(@Nonnull AbstractRestRequest request) {
        //POST requests can include a body
        return buildRequest(requireNonNull(request, "request"), true).post(buildUrl(request));
    }

    @Nonnull
    public final Response put(@Nonnull AbstractRestRequest request) {
        //PUT requests can include a body
        return buildRequest(requireNonNull(request, "request"), true).put(buildUrl(request));
    }

    protected final ResponseSpecification buildRequest(AbstractRestRequest request) {
        return buildRequest(request, false);
    }

    protected final ResponseSpecification buildRequest(AbstractRestRequest request, boolean withBody) {
        RequestSpecification builder = authenticatedRequest(request);
        if (withBody && request.hasBody()) {
            builder.body(request.getBody());
        }

        if (request.hasQueryParams()) {
            for (Map.Entry<String, Collection<?>> param : request.getQueryParams().entrySet()) {
                builder.queryParam(param.getKey(), param.getValue());
            }
        }

        return builder.expect()
                .spec(request.getSpecification())
                .log().ifValidationFails();
    }

    private static RequestSpecification authenticatedRequest(TestAuthentication authentication) {
        RequestSpecification builder = jsonRequest();
        if (authentication.isAuthenticated()) {
            builder.auth().preemptive().basic(authentication.getUsername(), authentication.getPassword());
        } else {
            builder.auth().none();
        }

        return builder;
    }

    private static RequestSpecification jsonRequest() {
        return RestAssured.given().contentType(MediaType.APPLICATION_JSON);
    }

    private RequestSpecification authenticatedRequest(AbstractRestRequest request) {
        return authenticatedRequest(request.getAuthentication() == null ? defaultAuthentication : request.getAuthentication());
    }

    private String buildUrl(AbstractRestRequest request) {
        return resourceUrl.add(request.getPath()).toString();
    }
}
