package com.atlassian.applinks.test.rest.applink;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * REST tester for the V3 Application Links REST API. This requires admin privileges to interact with.
 *
 * @since 5.0
 */
public class ApplinksV3RestTester extends BaseRestTester {
    public ApplinksV3RestTester(@Nonnull TestedInstance instance) {
        super(getRestUrl(instance));
    }

    public ApplinksV3RestTester(@Nonnull TestedInstance instance, @Nullable TestAuthentication defaultAuthentication) {
        super(getRestUrl(instance), defaultAuthentication);
    }

    public Response getAll(@Nonnull ApplinksV3Request request) {
        return super.get(request);
    }

    public Response get(@Nonnull ApplinkV3Request request) {
        return super.get(request);
    }

    private static RestUrl getRestUrl(@Nonnull TestedInstance instance) {
        return ApplinksRestUrlsFactory.forProduct(instance).applinks();
    }
}
