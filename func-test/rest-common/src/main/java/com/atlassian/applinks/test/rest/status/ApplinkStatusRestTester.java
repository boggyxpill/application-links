package com.atlassian.applinks.test.rest.status;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forProduct;

public class ApplinkStatusRestTester extends BaseRestTester {
    public ApplinkStatusRestTester(@Nonnull TestedInstance instance) {
        super(forProduct(instance).status());
    }

    public ApplinkStatusRestTester(@Nonnull TestedInstance instance, @Nullable TestAuthentication defaultAuthentication) {
        super(forProduct(instance).status(), defaultAuthentication);
    }

    @Nonnull
    public Response get(@Nonnull ApplinkStatusRequest request) {
        return super.get(request);
    }

    @Nonnull
    public Response getOAuth(@Nonnull ApplinkOAuthStatusRequest request) {
        return super.get(request);
    }

    @Nonnull
    public Response putOAuth(@Nonnull ApplinkOAuthStatusRequest request) {
        return super.put(request);
    }
}
