package com.atlassian.applinks.test.rest.applink;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.AbstractRestRequest;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Request to interact with a single Applinks via V1 applinks REST.
 *
 * @since 4.3
 */
public class ApplinkV1Request extends AbstractRestRequest {
    @Nonnull
    public static ApplinkV1Request forApplink(@Nonnull TestApplink.Side applinkSide) {
        return new Builder(applinkSide).build();
    }

    // TODO this is delete-specific, should be moved to a dedicated ApplinksV1DeleteRequest
    private static final String PARAM_RECIPROCATE = "reciprocate";

    protected final String applinkId;

    protected ApplinkV1Request(BaseBuilder<?, ?> builder) {
        super(builder);
        this.applinkId = builder.applinkId;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.forPath(applinkId);
    }

    public static final class Builder extends BaseBuilder<Builder, ApplinkV1Request> {
        public Builder(@Nonnull String applinkId) {
            super(applinkId);
        }

        public Builder(@Nonnull TestApplink.Side side) {
            super(side);
        }

        @Nonnull
        @Override
        public ApplinkV1Request build() {
            return new ApplinkV1Request(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }

    public static abstract class BaseBuilder<B extends BaseBuilder<B, R>, R extends ApplinkV1Request>
            extends AbstractBuilder<B, R> {
        protected final String applinkId;

        protected BaseBuilder(@Nonnull String applinkId) {
            this.applinkId = requireNonNull(applinkId, "applinkId");
        }

        protected BaseBuilder(@Nonnull TestApplink.Side side) {
            this(side.id());
        }

        @Nonnull
        public BaseBuilder reciprocalDelete(boolean reciprocal) {
            return queryParam(PARAM_RECIPROCATE, reciprocal);
        }
    }
}
