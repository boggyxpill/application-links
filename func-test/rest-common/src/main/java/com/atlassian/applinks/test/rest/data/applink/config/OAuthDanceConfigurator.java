package com.atlassian.applinks.test.rest.data.applink.config;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.data.applink.config.TestApplinkConfigurator;
import com.atlassian.applinks.test.rest.backdoor.AccessTokenBackdoor;
import com.atlassian.applinks.test.rest.model.RestAccessToken;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Applink configurator that sets up a 3LO access token for a {@code fromUsername} on the {@code from} side of the
 * Applink to the {@code toUsername} on the {@code to} side of the applink.
 * <p>
 * NOTE: this configurator is stateful and relies on an odering of the calls to {@link #configureSide(TestApplink.Side)},
 * so it is recommended to only use its {@link #configure(TestApplink)} method.
 * </p>
 *
 * @since 5.0
 */
public class OAuthDanceConfigurator implements TestApplinkConfigurator {
    @Nonnull
    public static OAuthDanceConfigurator setUp3LoAccess(@Nonnull TestAuthentication authentication) {
        return setUp3LoAccess(authentication, authentication);
    }

    @Nonnull
    public static OAuthDanceConfigurator setUp3LoAccess(@Nonnull String username) {
        return setUp3LoAccess(username, username);
    }

    @Nonnull
    @SuppressWarnings("ConstantConditions")
    public static OAuthDanceConfigurator setUp3LoAccess(@Nonnull TestAuthentication fromAuthentication,
                                                        @Nonnull TestAuthentication toAuthentication) {
        return setUp3LoAccess(fromAuthentication.getUsername(), toAuthentication.getUsername());
    }

    @Nonnull
    public static OAuthDanceConfigurator setUp3LoAccess(@Nonnull String fromUsername, @Nonnull String toUsername) {
        return new OAuthDanceConfigurator(fromUsername, toUsername);
    }

    private final AccessTokenBackdoor accessTokenBackdoor = new AccessTokenBackdoor();

    private final String fromUsername;
    private final String toUsername;

    private RestAccessToken token;

    protected OAuthDanceConfigurator(@Nonnull String fromUsername, @Nonnull String toUsername) {
        this.fromUsername = requireNonNull(fromUsername, "fromUsername");
        this.toUsername = requireNonNull(toUsername, "toUsername");
    }

    @Override
    public void configure(@Nonnull TestApplink applink) {
        requireNonNull(applink, "applink");
        // note "to" must come first
        configureSide(applink.to());
        configureSide(applink.from());
    }

    @Override
    public void configureSide(@Nonnull TestApplink.Side side) {
        if (token == null) {
            // first call - should be to the "to" side of the applink
            token = accessTokenBackdoor.createServiceProviderToken(side, toUsername);
        } else {
            // already created/retrieved service provider token, now put it as consumer token on the "from" side
            accessTokenBackdoor.addConsumerToken(side, fromUsername, token);
            // reset the token
            token = null;
        }
    }
}
