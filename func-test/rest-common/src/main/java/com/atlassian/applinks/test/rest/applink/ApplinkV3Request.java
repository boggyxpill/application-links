package com.atlassian.applinks.test.rest.applink;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.rest.model.applink.RestApplicationLink;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.data.applink.TestApplink;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

/**
 * V3 request to a single applink.
 *
 * @since 5.0
 */
public class ApplinkV3Request extends AbstractApplinksV3Request {
    private final String applinkId;
    private final RestApplicationLink applicationLink;

    private ApplinkV3Request(Builder builder) {
        super(builder);
        this.applinkId = builder.applinkId;
        this.applicationLink = builder.applicationLink;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.forPath(applinkId);
    }

    @Nullable
    @Override
    protected Object getBody() {
        return applicationLink;
    }

    public static class Builder extends AbstractApplinksV3Builder<Builder, ApplinkV3Request> {
        private final String applinkId;
        private RestApplicationLink applicationLink;

        public Builder(@Nonnull String applinkId) {
            this.applinkId = requireNonNull(applinkId, "applinkId");
        }

        public Builder(@Nonnull TestApplink.Side side) {
            this.applinkId = requireNonNull(side, "side").id();
        }

        @Nonnull
        public Builder applicationLink(@Nullable RestApplicationLink applink) {
            this.applicationLink = applink;
            return this;
        }

        @Nonnull
        public Builder applicationLink(@Nullable ApplicationLink applink) {
            return applink != null ? applicationLink(new RestApplicationLink(applink)) : this;
        }

        @Nonnull
        @Override
        public ApplinkV3Request build() {
            return new ApplinkV3Request(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
