package com.atlassian.applinks.test.rest.capabilities;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.AbstractApplinkRequest;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrls.REMOTE_PATH;

/**
 * Request to the remote "capabilities" resource for a given Applink.
 *
 * @since 5.0
 */
public class ApplinksRemoteCapilitiesRequest extends AbstractApplinkRequest {
    private ApplinksRemoteCapilitiesRequest(Builder builder) {
        super(builder);
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.forPath(REMOTE_PATH).add(applinkId);
    }

    public static final class Builder extends AbstractApplinkRequestBuilder<Builder, ApplinksRemoteCapilitiesRequest> {
        public Builder(@Nonnull String applinkId) {
            super(applinkId);
        }

        public Builder(@Nonnull TestApplink.Side applink) {
            super(applink);
        }

        @Nonnull
        public Builder maxAge(long maxAge) {
            return queryParam("maxAge", maxAge);
        }

        @Nonnull
        public Builder timeUnit(@Nullable TimeUnit timeUnit) {
            if (timeUnit != null) {
                return queryParam("timeUnit", timeUnit.name());
            } else {
                return this;
            }
        }

        @Nonnull
        @Override
        public ApplinksRemoteCapilitiesRequest build() {
            return new ApplinksRemoteCapilitiesRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
