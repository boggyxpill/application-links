package com.atlassian.applinks.test.rest.matchers;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.internal.common.rest.model.applink.RestApplicationLink;
import com.atlassian.applinks.internal.common.rest.model.applink.RestMinimalApplicationLink;
import com.atlassian.applinks.spi.application.TypeId;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

import static com.atlassian.applinks.test.rest.matchers.RestMatchers.hasPropertyThat;
import static org.hamcrest.Matchers.is;

/**
 * @see RestMinimalApplicationLink
 * @see RestApplicationLink
 * @since 5.0
 */
public final class RestApplicationLinkMatchers {
    private RestApplicationLinkMatchers() {
    }

    @Nonnull
    public static Matcher<Map<String, Object>> applink(@Nonnull Matcher<?> idMatcher) {
        return hasPropertyThat(RestMinimalApplicationLink.ID, idMatcher);
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withIdThat(@Nonnull Matcher<?> idMatcher) {
        return hasPropertyThat(RestMinimalApplicationLink.ID, idMatcher);
    }

    @Nonnull
    public static Matcher<?> withId(@Nullable String id) {
        return withIdThat(is(id));
    }

    @Nonnull
    public static Matcher<?> withId(@Nonnull ApplicationId id) {
        return withId(id.get());
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withNameThat(@Nonnull Matcher<?> nameMatcher) {
        return hasPropertyThat(RestMinimalApplicationLink.NAME, nameMatcher);
    }

    @Nonnull
    public static Matcher<?> withName(@Nullable String name) {
        return withNameThat(is(name));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withDisplayUrlThat(@Nonnull Matcher<?> urlMatcher) {
        return hasPropertyThat(RestMinimalApplicationLink.DISPLAY_URL, urlMatcher);
    }

    @Nonnull
    public static Matcher<?> withDisplayUrl(@Nullable String url) {
        return withDisplayUrlThat(is(url));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withRpcUrlThat(@Nonnull Matcher<?> urlMatcher) {
        return hasPropertyThat(RestMinimalApplicationLink.RPC_URL, urlMatcher);
    }

    @Nonnull
    public static Matcher<?> withRpcUrl(@Nullable String url) {
        return withRpcUrlThat(is(url));
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withTypeThat(@Nonnull Matcher<?> typeMatcher) {
        return hasPropertyThat(RestMinimalApplicationLink.TYPE, typeMatcher);
    }

    @Nonnull
    public static Matcher<?> withType(@Nullable String type) {
        return withTypeThat(is(type));
    }

    @Nonnull
    public static Matcher<?> withType(@Nonnull TypeId type) {
        return withType(type.get());
    }

    @Nonnull
    public static Matcher<Map<String, Object>> withPrimaryThat(@Nonnull Matcher<?> primaryMatcher) {
        return hasPropertyThat(RestApplicationLink.PRIMARY, primaryMatcher);
    }

    @Nonnull
    public static Matcher<?> withPrimary(@Nullable Boolean primary) {
        return withPrimaryThat(is(primary));
    }
}
