package com.atlassian.applinks.test.rest.backdoor;

import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.spi.application.IdentifiableType;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.AbstractRestRequest;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

public class BackdoorCreateSideApplinkRequest extends AbstractRestRequest {
    private final String baseUrl;
    private final String type;
    private final String name;

    private BackdoorCreateSideApplinkRequest(Builder builder) {
        super(builder);
        this.baseUrl = builder.baseUrl;
        this.type = builder.type;
        this.name = builder.name;
    }

    @Nullable
    @Override
    protected Object getBody() {
        return new BaseRestEntity.Builder()
                .add("name", name)
                .add("baseUrl", baseUrl)
                .add("typeId", type)
                .add("twoWay", false)
                .build();
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.EMPTY;
    }

    public static class Builder extends AbstractBuilder<Builder, BackdoorCreateSideApplinkRequest> {
        private final String baseUrl;
        private String name;
        private String type;

        public Builder(@Nonnull String remoteBaseUrl) {
            this.baseUrl = requireNonNull(remoteBaseUrl, "remoteBaseUrl");
            this.name = "Test link to: " + remoteBaseUrl;
        }

        public Builder(@Nonnull TestedInstance to) {
            this.baseUrl = requireNonNull(to, "to").getBaseUrl();
            this.name = "Test link to: " + to.getInstanceId();
        }

        @Nonnull
        public Builder name(@Nonnull String name) {
            this.name = requireNonNull(name, "name");

            return this;
        }

        /**
         * @param typeClass applink type, as specified by its class
         * @return this builder instance
         */
        @Nonnull
        public Builder type(@Nonnull Class<? extends ApplicationType> typeClass) {
            return typeId(requireNonNull(typeClass, "type").getCanonicalName());
        }

        /**
         * @param typeId applink type, as specified by {@link IdentifiableType#getId()}.
         * @return this builder instance
         */
        @Nonnull
        public Builder typeId(@Nonnull String typeId) {
            this.type = requireNonNull(typeId, "typeId");

            return this;
        }

        @Nonnull
        @Override
        public BackdoorCreateSideApplinkRequest build() {
            return new BackdoorCreateSideApplinkRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
