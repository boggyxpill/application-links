package com.atlassian.applinks.test.rest.backdoor;

import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.application.generic.GenericApplicationType;
import com.atlassian.applinks.internal.common.rest.model.applink.RestMinimalApplicationLink;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.internal.rest.model.RestErrors;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.DefaultTestApplink;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.product.DefaultTestedInstance;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.CustomizableRestRequest;
import com.atlassian.applinks.test.rest.SimpleRestRequest;
import com.atlassian.applinks.test.rest.model.RestProperty;
import com.google.common.collect.ImmutableList;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.test.authentication.TestAuthentication.admin;
import static com.atlassian.applinks.test.data.util.TestUrls.createRandomUri;
import static com.atlassian.applinks.test.rest.matchers.RestMatchers.matchesEntity;
import static com.atlassian.applinks.test.rest.specification.RestExpectations.expectBody;
import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forTestRestModule;
import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;

/**
 * Backdoor to access the {@code applinks} resource in the {@code applinks-tests} backdoor plugin. See
 * {@code ApplinksTestResource} and {@code CleanupResource} for the REST resources implementations that this class is
 * communicating with.
 * <p>
 * NOTE: this will only work against applications that are running the {@code applinks-tests} backdoor plugin.
 *
 * @since 4.3
 */
public class ApplinksBackdoor {
    private static final String APPLINKS_PATH = "applinks";
    private static final String CLEANUP_PATH = "cleanup";
    private static final String PATH_SYSTEM = "system";
    private static final String PATH_PROPERTY = "property";

    private final TestedInstance instance;

    private final BaseRestTester applinksTester;
    private final BaseRestTester cleanUpTester;

    public ApplinksBackdoor(@Nonnull String baseUrl) {
        this(DefaultTestedInstance.fromBaseUrl(baseUrl));
    }

    public ApplinksBackdoor(@Nonnull TestedInstance instance) {
        this.instance = requireNonNull(instance, "instance");
        // no need for authentication, the resource allows anonymous access
        this.applinksTester = new BaseRestTester(getApplinksRestPath(instance));
        this.cleanUpTester = new BaseRestTester(getCleanUpRestPath(instance));
    }

    @Nonnull
    public Iterable<RestMinimalApplicationLink> getAllApplinks() {
        return ImmutableList.copyOf(applinksTester.get(SimpleRestRequest.DEFAULT_INSTANCE)
                .as(RestMinimalApplicationLink[].class));
    }

    @Nonnull
    public RestMinimalApplicationLink getApplink(String id) {
        return applinksTester.get(new CustomizableRestRequest.Builder().addPath(id).build())
                .as(RestMinimalApplicationLink.class);
    }

    @Nonnull
    public RestMinimalApplicationLink getApplink(TestApplink.Side side) {
        checkSide(side);
        return getApplink(side.id());
    }

    /**
     * Create a reciprocal applink without any auth enabled.
     * Uses {@link TestAuthentication#admin() default admin authentication} to create the link on remote end, which may
     * not work for some tested applications
     *
     * @param to target of the applink
     * @return created applink
     */
    @Nonnull
    public TestApplink create(@Nonnull TestedInstance to) {
        requireNonNull(to, "to");
        return create(new BackdoorCreateApplinkRequest.Builder(to).build());
    }

    /**
     * Create a reciprocal applink without any auth enabled.
     *
     * @param request request to create the applink
     * @return created applink
     */
    @Nonnull
    public TestApplink create(@Nonnull BackdoorCreateApplinkRequest request) {
        Response response = applinksTester.post(request);
        JsonPath result = response.getBody().jsonPath();
        if (result.get("errors") != null) {
            throw new RuntimeException("Error response " + response.as(RestErrors.class));
        }
        return new DefaultTestApplink.Builder()
                .from(result.getString("linkId"), instance)
                .to(result.getString("localId"), request.to)
                .build();
    }

    /**
     * Create a one-side applink without any auth enabled.
     *
     * @param request request to create the applink
     * @return created applink
     */
    @Nonnull
    public TestApplink.Side createSide(@Nonnull BackdoorCreateSideApplinkRequest request) {
        JsonPath result = applinksTester.post(request).getBody().jsonPath();
        return new DefaultTestApplink.DefaultSide(result.getString("linkId"), instance);
    }

    /**
     * Create a one-side generic applink without any auth enabled, with a given {@code baseUrl}.
     *
     * @param baseUrl base URL of the applink
     * @return created applink
     */
    @Nonnull
    public TestApplink.Side createGeneric(@Nonnull String baseUrl) {
        return create(baseUrl, GenericApplicationType.class);
    }

    /**
     * Create a one-side generic applink (no auth enabled) to a random base URL that is not likely to be contactable.
     *
     * @return created applink
     */
    @Nonnull
    public TestApplink.Side createRandomGeneric() {
        return createRandom(GenericApplicationType.class);
    }

    /**
     * Create a one-side generic applink without any auth enabled, with a given {@code baseUrl} and
     * {@code applicationType}.
     *
     * @param baseUrl         base URL of the applink
     * @param applicationType application type
     * @return created applink
     */
    @Nonnull
    public TestApplink.Side create(@Nonnull String baseUrl, @Nonnull Class<? extends ApplicationType> applicationType) {
        return createSide(new BackdoorCreateSideApplinkRequest.Builder(baseUrl).type(applicationType).build());
    }

    /**
     * Create a one-side generic applink without any auth enabled, with a random URL and given {@code applicationType}.
     *
     * @param applicationType application type
     * @return created applink
     */
    @Nonnull
    public TestApplink.Side createRandom(@Nonnull Class<? extends ApplicationType> applicationType) {
        return create(createRandomUri().toString(), applicationType);
    }

    /**
     * Set applink name.
     *
     * @param side applink side to update
     * @param name new name
     * @return response
     */
    @Nonnull
    public Response setName(@Nonnull TestApplink.Side side, @Nonnull String name) {
        checkSide(side);
        return setName(side.id(), name);
    }

    /**
     * Set applink name.
     *
     * @param applinkId applink ID
     * @param name      new name
     * @return response
     */
    @Nonnull
    public Response setName(@Nonnull String applinkId, @Nonnull String name) {
        requireNonNull(applinkId, "applinkId can't be null");
        requireNonNull(name, "name can't be null");
        return applinksTester.put(createApplinkUpdateRequest(applinkId, new BaseRestEntity.Builder()
                .add("name", name)
                .build()));
    }

    /**
     * Set application and display URL to {@code url}.
     *
     * @param side applink side to update
     * @param url  new url
     * @return response
     */
    @Nonnull
    public Response setUrl(@Nonnull TestApplink.Side side, @Nonnull String url) {
        checkSide(side);
        return setUrl(side.id(), url);
    }

    /**
     * Set application and display URL to {@code url}.
     *
     * @param applinkId applink ID
     * @param url       new url
     * @return response
     */
    @Nonnull
    public Response setUrl(@Nonnull String applinkId, @Nonnull String url) {
        requireNonNull(applinkId, "applinkId can't be null");
        requireNonNull(url, "url can't be null");
        return applinksTester.put(createApplinkUpdateRequest(applinkId, new BaseRestEntity.Builder()
                .add("rpcUrl", url)
                .add("displayUrl", url)
                .build()));
    }

    /**
     * Set display URL to {@code url}.
     *
     * @param side applink side to update
     * @param url  new display url
     * @return response
     */
    @Nonnull
    public Response setDisplayUrl(@Nonnull TestApplink.Side side, @Nonnull String url) {
        checkSide(side);
        return setDisplayUrl(side.id(), url);
    }

    /**
     * Set display URL to {@code url}.
     *
     * @param applinkId applink ID
     * @param url       new display url
     * @return response
     */
    @Nonnull
    public Response setDisplayUrl(@Nonnull String applinkId, @Nonnull String url) {
        requireNonNull(applinkId, "applinkId can't be null");
        requireNonNull(url, "url can't be null");
        return applinksTester.put(createApplinkUpdateRequest(applinkId, new BaseRestEntity.Builder()
                .add("displayUrl", url)
                .build()));
    }

    @Nonnull
    public Response setSystem(@Nonnull TestApplink.Side side, boolean system) {
        checkSide(side);
        return setSystem(side.id(), system);
    }

    @Nonnull
    public Response setSystem(@Nonnull String applinkId, boolean system) {
        requireNonNull(applinkId);
        if (system) {
            return applinksTester.put(createSystemRequest(applinkId));
        } else {
            return applinksTester.delete(createSystemRequest(applinkId));
        }
    }

    @Nonnull
    public Response setProperty(@Nonnull TestApplink.Side side, @Nonnull String key, @Nonnull Object value) {
        checkSide(side);
        return setProperty(side.id(), key, value);
    }

    @Nonnull
    public Response setProperty(@Nonnull String applinkId, @Nonnull String key, @Nonnull Object value) {
        requireNonNull(applinkId, "applinkId");
        requireNonNull(key, "key");
        return applinksTester.put(createSetPropertyRequest(applinkId, key, value));
    }

    @Nonnull
    public Response removeProperty(@Nonnull String applinkId, @Nonnull String key) {
        requireNonNull(applinkId, "applinkId");
        requireNonNull(key, "key");
        return applinksTester.delete(createRemovePropertyRequest(applinkId, key));
    }

    @Nonnull
    public Response cleanUp() {
        // authenticated request as Bitbucket enforces restrictions on some of the TA APIs used to clean up
        return cleanUpTester.get(new SimpleRestRequest.Builder().authentication(admin()).build());
    }


    private void checkSide(@Nonnull TestApplink.Side side) {
        requireNonNull(side, "side");
        checkState(side.product().equals(instance), "Applink from different instance: " + side.product());
    }


    private static RestUrl getApplinksRestPath(TestedInstance instance) {
        return forTestRestModule(instance).path(APPLINKS_PATH);
    }

    private static RestUrl getCleanUpRestPath(TestedInstance instance) {
        return forTestRestModule(instance).path(CLEANUP_PATH);
    }

    private static CustomizableRestRequest createApplinkUpdateRequest(@Nonnull String applinkId,
                                                                      BaseRestEntity applinkData) {
        return new CustomizableRestRequest.Builder()
                .addPath(applinkId)
                .expectStatus(Status.OK)
                .specification(expectBody(matchesEntity(applinkData)))
                .body(applinkData)
                .build();
    }

    private static CustomizableRestRequest createSystemRequest(@Nonnull String applinkId) {
        return new CustomizableRestRequest.Builder()
                .addPath(applinkId)
                .addPath(PATH_SYSTEM)
                .expectStatus(Status.NO_CONTENT)
                .build();
    }

    private static CustomizableRestRequest createSetPropertyRequest(@Nonnull String applinkId, @Nonnull String key,
                                                                    @Nonnull Object body) {
        RestProperty fullBody = new RestProperty(body);
        return new CustomizableRestRequest.Builder()
                .addPath(applinkId)
                .addPath(PATH_PROPERTY)
                .addPath(key)
                .body(fullBody)
                .expectStatus(Status.OK)
                .specification(expectBody(matchesEntity(fullBody)))
                .build();
    }

    private static CustomizableRestRequest createRemovePropertyRequest(@Nonnull String applinkId, @Nonnull String key) {
        return new CustomizableRestRequest.Builder()
                .addPath(applinkId)
                .addPath(PATH_PROPERTY)
                .addPath(key)
                .expectStatus(Status.NO_CONTENT)
                .build();
    }
}
