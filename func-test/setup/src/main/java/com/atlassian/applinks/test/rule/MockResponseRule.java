package com.atlassian.applinks.test.rule;

import com.atlassian.applinks.test.backdoor.MockResponseBackdoor;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.response.MockResponseDefinition;
import com.atlassian.applinks.test.rest.model.RestRequestPredicate;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * JUnit rule set up mock responses in the target refapp.
 *
 * @since 5.0
 */
public class MockResponseRule extends TestWatcher {
    private final MockResponseBackdoor backdoor;

    private boolean mockResponsesAdded;

    private final Map<RestRequestPredicate, MockResponseDefinition> setUp = new LinkedHashMap<>();

    public MockResponseRule(@Nonnull TestedInstance instance) {
        this.backdoor = new MockResponseBackdoor(instance);
    }

    public MockResponseRule withMockResponse(@Nonnull RestRequestPredicate predicate,
                                             @Nonnull MockResponseDefinition responseDefinition) {
        requireNonNull(predicate, "predicate");
        requireNonNull(responseDefinition, "responseDefinition");

        setUp.put(predicate, responseDefinition);
        return this;
    }

    public void addMockResponse(@Nonnull RestRequestPredicate predicate,
                                @Nonnull MockResponseDefinition responseDefinition) {
        backdoor.addResponse(predicate, responseDefinition);
        mockResponsesAdded = true;
    }

    @Override
    protected void starting(Description description) {
        for (Map.Entry<RestRequestPredicate, MockResponseDefinition> toAdd : setUp.entrySet()) {
            addMockResponse(toAdd.getKey(), toAdd.getValue());
        }
    }

    @Override
    protected void finished(Description description) {
        if (mockResponsesAdded) {
            backdoor.reset();
        }
    }
}
