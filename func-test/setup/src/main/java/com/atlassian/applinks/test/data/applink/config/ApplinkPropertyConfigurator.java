package com.atlassian.applinks.test.data.applink.config;

import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Configurator that sets a property on an applink.
 *
 * @since 5.0
 */
public class ApplinkPropertyConfigurator extends AbstractTestApplinkConfigurator {
    @Nonnull
    public static ApplinkPropertyConfigurator setProperty(@Nonnull String key, @Nonnull Object value) {
        return new ApplinkPropertyConfigurator(key, value);
    }

    private final String key;
    private final Object value;

    private ApplinkPropertyConfigurator(@Nonnull String key, @Nonnull Object value) {
        this.key = requireNonNull(key, "key");
        this.value = requireNonNull(value, "value");
    }

    @Override
    public void configureSide(@Nonnull TestApplink.Side side) {
        new ApplinksBackdoor(side.product()).setProperty(side, key, value);
    }
}
