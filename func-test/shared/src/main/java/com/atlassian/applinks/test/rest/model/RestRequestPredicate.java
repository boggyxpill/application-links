package com.atlassian.applinks.test.rest.model;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

import static com.atlassian.applinks.internal.rest.RestUrlBuilder.REST_CONTEXT;

/**
 * @since 5.0
 */
public class RestRequestPredicate extends BaseRestEntity {
    public static final String SERVLET_PATH = "servletPath";
    public static final String PATH_INFO_PATTERN = "pathInfoPattern";
    public static final String HAS_OAUTH_HEADER = "hasOAuthHeader";

    @SuppressWarnings("unused") // for deserialization
    public RestRequestPredicate() {
    }

    @SuppressWarnings("unused") // for deserialization
    public RestRequestPredicate(Map<String, Object> originalValue) {
        super(originalValue);
    }

    @Nonnull
    public static Builder restRequestTo(@Nonnull RestUrl restUrl) {
        return new Builder().servletPath(RestUrl.COMPONENT_SEPARATOR + REST_CONTEXT.toString())
                .pathInfoPattern(RestUrl.COMPONENT_SEPARATOR + restUrl.toString());
    }

    public static class Builder {
        private String servletPath;
        private String pathInfoPattern;
        private boolean hasOAuthHeader;

        @Nonnull
        public Builder servletPath(@Nullable String servletPath) {
            this.servletPath = servletPath;
            return this;
        }

        @Nonnull
        public Builder pathInfoPattern(String pathInfoPattern) {
            this.pathInfoPattern = pathInfoPattern;
            return this;
        }

        @Nonnull
        public Builder hasOAuthHeader(boolean hasOAuthHeader) {
            this.hasOAuthHeader = hasOAuthHeader;
            return this;
        }

        public RestRequestPredicate build() {
            RestRequestPredicate predicate = new RestRequestPredicate();
            predicate.putIfNotNull(SERVLET_PATH, servletPath);
            predicate.putIfNotNull(PATH_INFO_PATTERN, pathInfoPattern);
            predicate.put(HAS_OAUTH_HEADER, hasOAuthHeader);

            return predicate;
        }
    }
}
