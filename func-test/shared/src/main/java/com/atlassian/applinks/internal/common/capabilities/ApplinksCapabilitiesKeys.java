package com.atlassian.applinks.internal.common.capabilities;

import javax.annotation.Nonnull;

/**
 * Expose {@link ApplinksCapabilities} system property keys for testing.
 *
 * @since 5.0
 */
public class ApplinksCapabilitiesKeys {
    private ApplinksCapabilitiesKeys() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static String capabilityKeyFor(@Nonnull ApplinksCapabilities capability) {
        return capability.key;
    }
}
