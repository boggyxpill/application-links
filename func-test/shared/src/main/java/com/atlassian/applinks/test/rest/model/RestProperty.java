package com.atlassian.applinks.test.rest.model;

import com.atlassian.applinks.internal.rest.model.BaseRestEntity;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
public class RestProperty extends BaseRestEntity {
    public static final String VALUE = "value";

    @SuppressWarnings("unused") // for deserialization
    public RestProperty() {
    }

    public RestProperty(@Nonnull Object value) {
        put(VALUE, requireNonNull(value, "value"));
    }

    @Nonnull
    public Object getValue() {
        return get(VALUE);
    }
}
