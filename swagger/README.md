# Application Links REST API v3.0

## Notes
This is the initial public release of the Applinks REST API. It has quite a lot of rough edges, as it was documented
without modification in the interests of backwards compatibility. It's functionality is also less than what can be done
through the UI, this is also intentional: much of the REST resources that are used by the API can result in an Application
Link which is in an inconsistent state. In the future releases of the API, resources that can be used in a safe way will be added.

## Compatibility
The entirety of the API documented in this release is compatible with Applinks versions 5.0.x+

The REST resources manifest, applicationlink and relocateApplicationlink are all backwards compatible to Applinks versions 3.0.x+
