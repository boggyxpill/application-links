package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.AuthorisationAdminURIGenerator;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.core.rest.context.CurrentContext;
import com.atlassian.applinks.core.util.RequestUtil;
import com.atlassian.applinks.core.util.URIUtil;
import com.atlassian.applinks.host.spi.HostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.velocity.htmlsafe.HtmlSafe;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.Map;

import static com.atlassian.applinks.core.util.URIUtil.utf8Encode;
import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.Validate.isTrue;

/**
 * atlassian-plugin.xml excerpt:
 * <pre>
 *     &lt;applinks-authentication-provider class="ThreeLeggedOAuthRequestFactoryImpl"&gt;
 *     &lt;/applinks-authentication-provider&gt;
 * </pre>
 *
 * @since 3.10
 */
public class ThreeLeggedOAuthRequestFactoryImpl implements ApplicationLinkRequestFactory, AuthorisationAdminURIGenerator {
    // Note that this URI path belongs to the OAuth plugin, not the Applinks plugin.
    // It would be better to discover it programmatically from the OAuth plugin; it's
    // hard-coded here to avoid introducing a dependency on a new OAuth plugin API in
    // Applinks 3.6, which is otherwise compatible with OAuth 1.2.
    private static final String OAUTH_ACCESS_TOKENS_ADMIN_SERVLET_LOCATION =
            "/plugins/servlet/oauth/users/access-tokens";

    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final ApplicationLink applicationLink;
    private final ConsumerService consumerService;
    private final ConsumerTokenStoreService consumerTokenStoreService;
    private final RequestFactory requestFactory;
    private final UserManager userManager;
    private final HostApplication hostApplication;

    public ThreeLeggedOAuthRequestFactoryImpl(final ApplicationLink applicationLink,
                                              final AuthenticationConfigurationManager authenticationConfigurationManager,
                                              final ConsumerService consumerService, final ConsumerTokenStoreService consumerTokenStoreService,
                                              final RequestFactory requestFactory, final UserManager userManager,
                                              final HostApplication hostApplication) {
        this.applicationLink = requireNonNull(applicationLink, "applicationLink can't be null");
        this.authenticationConfigurationManager = requireNonNull(authenticationConfigurationManager, "authenticationConfigurationManager can't be null");
        this.consumerService = requireNonNull(consumerService, "consumerService can't be null");
        this.consumerTokenStoreService = requireNonNull(consumerTokenStoreService, "consumerTokenStoreService can't be null");
        this.requestFactory = requireNonNull(requestFactory, "requestFactory can't be null");
        this.userManager = requireNonNull(userManager, "userManager can't be null");
        this.hostApplication = requireNonNull(hostApplication, "hostApplication can't be null");
    }

    public ApplicationLinkRequest createRequest(final Request.MethodType methodType, final String uri) throws CredentialsRequiredException {
        final Map<String, String> config = authenticationConfigurationManager
                .getConfiguration(applicationLink.getId(), OAuthAuthenticationProvider.class);

        isTrue(config != null, format("OAuth Authentication is not configured for application link %s", applicationLink));

        final ServiceProvider serviceProvider = ServiceProviderUtil.getServiceProvider(config, applicationLink);
        final Request request = requestFactory.createRequest(methodType, uri);

        // current user must be logged-in but the username is not necessarily used since OAuth will get username on remote application from the dance anyway.
        final String username = requireNonNull(userManager.getRemoteUsername(), "You have to be logged in to use oauth authentication.");

        return new ThreeLeggedOAuthRequest(uri, methodType, request, serviceProvider, consumerService,
                retrieveConsumerToken(username), consumerTokenStoreService, applicationLink.getId(), username);
    }

    private ConsumerToken retrieveConsumerToken(final String username) throws CredentialsRequiredException {
        final ConsumerToken consumerToken = consumerTokenStoreService.getConsumerToken(applicationLink, username);
        // token should never be a request token, we only store access tokens.
        if (consumerToken == null || consumerToken.isRequestToken()) {
            throw new CredentialsRequiredException(this, "You do not have an authorized access token for the remote resource.");
        }
        return consumerToken;
    }

    @HtmlSafe
    public URI getAuthorisationURI() {
        final HttpServletRequest request = CurrentContext.getHttpServletRequest();
        URI baseUrl;
        if (request != null) {
            baseUrl = RequestUtil.getBaseURLFromRequest(request, hostApplication.getBaseUrl());
        } else {
            baseUrl = hostApplication.getBaseUrl();
        }
        return URIUtil.uncheckedConcatenate(baseUrl,
                "/plugins/servlet/applinks/oauth/login-dance/authorize?applicationLinkID=" + utf8Encode(applicationLink.getId().get()));
    }

    @HtmlSafe
    public URI getAuthorisationURI(final URI callback) {
        return URIUtil.uncheckedToUri(getAuthorisationURI().toString() +
                "&redirectUrl=" + utf8Encode(requireNonNull(callback)));
    }

    @HtmlSafe
    public URI getAuthorisationAdminURI() {
        return URIUtil.uncheckedConcatenate(applicationLink.getDisplayUrl(),
                OAUTH_ACCESS_TOKENS_ADMIN_SERVLET_LOCATION);
    }
}