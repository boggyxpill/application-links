package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.AuthorisationAdminURIGenerator;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.core.RedirectController;
import com.atlassian.applinks.core.ServletPathConstants;
import com.atlassian.applinks.core.util.RequestUtil;
import com.atlassian.applinks.core.util.WebResources;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthMessageProblemException;
import com.atlassian.applinks.internal.common.net.ResponseHeaderUtil;
import com.atlassian.applinks.oauth.auth.servlets.consumer.AddServiceProviderManuallyServlet;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.ui.validators.CallbackParameterValidator;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import net.oauth.OAuth;
import net.oauth.OAuthProblemException;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.ADMIN_ERROR_MESSAGE;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.APPLINK_ID;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.AUTH_ADMIN_URI;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.ERROR;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.ERROR_DETAILS;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.REDIRECT_URL;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.USER_ERROR_MESSAGE;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.WARNING_MESSAGE;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.WARNING_TITLE;
import static com.atlassian.applinks.oauth.auth.OAuthApplinksServlet.TemplateVariable.WEB_RESOURCES;

/**
 * This servlet 'manages' the OAuth dance, consisting of the following steps:
 * 1) Obtain a request token from the remote application
 * 2) Redirect the user to the remote application and ask for authorization
 * 3) Wait for the redirect by the remote application and swap the request token for an access token afterwards
 * 4) Notify the user that authorization was successfully and he can continue with his operation.
 *
 * @since 3.0
 */
@SuppressWarnings("serial")
public class OAuthApplinksServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(OAuthApplinksServlet.class);

    public static final String AUTHORIZE_PATH = "authorize";
    public static final String ACCESS_PATH = "access";
    @VisibleForTesting
    protected static final String APPLICATION_LINK_ID_PARAM = "applicationLinkID";
    private static final String REDIRECT_URL_PARAM = "redirectUrl";
    private static final String TEMPLATE = "com/atlassian/applinks/oauth/auth/oauth_dance.vm";

    // i18n keys
    private static final String ADMIN_ERROR_CHECK_CONFIG = "applinks.admin.error.message.check.for.misconfig";
    private static final String ADMIN_ERROR_CHECK_LINK = "applinks.admin.error.message.check.link";
    private static final String USER_ERROR_NOT_LOGGED_IN = "applinks.user.error.message.not.logged.in";
    private static final String USER_ERROR_ACCESS_DENIED = "applinks.user.error.message.access.denied";
    private static final String ERROR_NOT_LOGGED_IN = "auth.oauth.config.error.not.loggedin";
    private static final String ERROR_APPLINK_ID_REQUIRED = "auth.oauth.config.error.link.id.empty";
    private static final String ERROR_TYPE_NOT_LOADED = "auth.oauth.config.error.link.type.not.loaded";
    private static final String ERROR_NO_LINK_FOUND_FOR_ID = "auth.oauth.config.error.link.id";
    private static final String ERROR_OAUTH_DANCE = "auth.oauth.config.error.dance";
    private static final String ERROR_OATH_NOT_CONFIGURED = "auth.oauth.config.error.not.configured";
    private static final String ERROR_CONSUMER_UNKNOWN = "auth.oauth.config.error.dance.oauth.problem.consumer.unknown";
    private static final String ERROR_TOKEN_REJECTED = "auth.oauth.config.error.dance.oauth.problem.token.rejected";
    private static final String ERRROR_OAUTH_DANCE_PROBLEM = "auth.oauth.config.error.dance.oauth.problem";
    private static final String WARNING_TITLE_ACCESS_DENIED = "auth.oauth.config.dance.denied.title";
    private static final String WARNING_MESSAGE_ACCESS_DENIED = "auth.oauth.config.dance.denied.message";

    // velocity variable names - ordering not important, just added for type-safety
    @VisibleForTesting
    enum TemplateVariable {
        ADMIN_ERROR_MESSAGE("adminError"),
        APPLINK_ID("applinkId"),
        AUTH_ADMIN_URI("authAdminUri"),
        ERROR("error"),
        ERROR_DETAILS("errorDetails"),
        REDIRECT_URL("redirectUrl"),
        USER_ERROR_MESSAGE("userError"),
        WARNING_MESSAGE("warningMessage"),
        WARNING_TITLE("warningTitle"),
        WEB_RESOURCES("webResources");

        final String key;

        TemplateVariable(String key) {
            this.key = key;
        }
    }

    private final ConsumerTokenStoreService consumerTokenStoreService;
    private final OAuthTokenRetriever oAuthTokenRetriever;
    private final UserManager userManager;
    private final I18nResolver i18nResolver;
    private final WebResourceManager webResourceManager;
    private final TemplateRenderer templateRenderer;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final ConsumerService consumerService;
    private final InternalHostApplication internalHostApplication;
    private final CallbackParameterValidator callbackParameterValidator;
    private final ApplicationLinkService applicationLinkService;
    private final RedirectController redirectController;


    public OAuthApplinksServlet(final ConsumerTokenStoreService consumerTokenStoreService,
                                final OAuthTokenRetriever oAuthTokenRetriever,
                                final UserManager userManager,
                                final I18nResolver i18nResolver,
                                final WebResourceManager webResourceManager,
                                final TemplateRenderer templateRenderer,
                                final AuthenticationConfigurationManager authenticationConfigurationManager,
                                final ConsumerService consumerService,
                                final InternalHostApplication internalHostApplication,
                                final CallbackParameterValidator callbackParameterValidator,
                                final ApplicationLinkService applicationLinkService,
                                final RedirectController redirectController) {
        this.consumerTokenStoreService = consumerTokenStoreService;
        this.oAuthTokenRetriever = oAuthTokenRetriever;
        this.userManager = userManager;
        this.i18nResolver = i18nResolver;
        this.webResourceManager = webResourceManager;
        this.templateRenderer = templateRenderer;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.consumerService = consumerService;
        this.internalHostApplication = internalHostApplication;
        this.callbackParameterValidator = callbackParameterValidator;
        this.applicationLinkService = applicationLinkService;
        this.redirectController = redirectController;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final Map<String, Object> context = createVelocityContext(resp);
        final String applicationLinkId = getApplicationLinkId(req);

        // pass the original app link id back to the caller until we have an actual ApplicationLink object
        context.put(APPLINK_ID.key, StringEscapeUtils.escapeHtml4(applicationLinkId));

        ResponseHeaderUtil.preventCrossFrameClickJacking(resp);

        if (userManager.getRemoteUser(req) == null) {
            addNotLoggedInUserErrorToContext(context);
            addErrorToContext(context, ERROR_NOT_LOGGED_IN);
            render(context, resp);

            return;
        }

        if (StringUtils.isBlank(applicationLinkId)) {
            addErrorToContext(context, ERROR_APPLINK_ID_REQUIRED);
            addCheckConfigAdminErrorToContext(context);
            render(context, resp);

            return;
        }

        final ApplicationLink applicationLink;
        try {
            applicationLink = applicationLinkService.getApplicationLink(new ApplicationId(applicationLinkId));
        } catch (TypeNotInstalledException e) {
            LOG.error("Failed to get application link", e);

            addErrorToContext(context, ERROR_TYPE_NOT_LOADED, applicationLinkId, e.getType());
            addCheckConfigAdminErrorToContext(context);
            render(context, resp);

            return;
        }

        if (applicationLink == null) {
            addErrorToContext(context, ERROR_NO_LINK_FOUND_FOR_ID, applicationLinkId);
            addCheckConfigAdminErrorToContext(context);
            render(context, resp);

            return;
        }

        // use the ID of the ApplicationLink we found from now on
        context.put(APPLINK_ID.key, applicationLink.getId().toString());

        if (!authenticationConfigurationManager.isConfigured(applicationLink.getId(), OAuthAuthenticationProvider.class)) {
            addErrorToContext(context, ERROR_OATH_NOT_CONFIGURED, applicationLink.toString());
            addCheckLinkAdminErrorToContext(context, applicationLink.getName());
            render(context, resp);

            return;
        }

        final String token = getToken(req);
        try {
            if (StringUtils.isBlank(token) || req.getPathInfo().endsWith(AUTHORIZE_PATH)) {
                obtainAndAuthorizeRequestToken(applicationLink, resp, req);
            } else if (req.getPathInfo().endsWith(ACCESS_PATH)) {
                getAccessToken(token, applicationLink, req);
                final String redirectUrl = getRedirectUrl(req);

                if (StringUtils.isBlank(redirectUrl)) {
                    URI authAdminUri = null;
                    ApplicationLinkRequestFactory requestFactory = applicationLink.createAuthenticatedRequestFactory();
                    if (requestFactory instanceof AuthorisationAdminURIGenerator) {
                        authAdminUri = ((AuthorisationAdminURIGenerator) requestFactory).getAuthorisationAdminURI();
                    }
                    context.put(AUTH_ADMIN_URI.key, (authAdminUri == null) ? "" : authAdminUri.toString());
                    render(context, resp);
                } else {
                    redirectController.redirectOrPrintRedirectionWarning(resp, redirectUrl);
                }
            }
        } catch (Exception e) {
            handleExceptionThrownDuringTokenRequest(req, resp, applicationLink, e, context);
        }
    }

    private void handleExceptionThrownDuringTokenRequest(
            HttpServletRequest req,
            HttpServletResponse resp,
            @Nonnull final ApplicationLink applicationLink,
            @Nonnull final Exception e,
            @Nonnull final Map<String, Object> context) throws IOException {
        LOG.error("An error occurred when performing the oauth 'dance' for application link '" + applicationLink + "'", e);

        addRedirectUrlToContext(req, context);

        if (e.getCause() instanceof OAuthProblemException) {
            final OAuthProblemException oAuthProblem = (OAuthProblemException) e.getCause();
            final String problem = oAuthProblem.getProblem();

            if (problem.equals(OAuth.Problems.CONSUMER_KEY_UNKNOWN)) {
                addErrorToContext(context, ERROR_CONSUMER_UNKNOWN, internalHostApplication.getName(),
                        applicationLink.getName());
                addCheckLinkAdminErrorToContext(context, applicationLink.getName());
                render(context, resp);
            } else if (problem.equals(OAuth.Problems.TOKEN_REJECTED)) {
                addErrorToContext(context, ERROR_TOKEN_REJECTED);
                addCheckLinkAdminErrorToContext(context, applicationLink.getName());
                render(context, resp);
            } else {
                addErrorToContext(context, ERRROR_OAUTH_DANCE_PROBLEM, applicationLink.toString(),
                        oAuthProblem.getProblem());
                addCheckLinkAdminErrorToContext(context, applicationLink.getName());
                render(context, resp);
            }
        } else if (e instanceof OAuthPermissionDeniedException) {
            addWarningToContext(context, WARNING_TITLE_ACCESS_DENIED, WARNING_MESSAGE_ACCESS_DENIED,
                    applicationLink.getName());
            addAccessDeniedUserErrorToContext(context, applicationLink.getName());
            render(context, resp);
        } else if (e instanceof OAuthMessageProblemException) {
            final List<String> errorDetails =
                    getDetailsFromOAuthMessageProblemException((OAuthMessageProblemException) e);

            LOG.error("Error during OAuth Dance, OAuth Parameters '{}'", StringUtils.join(errorDetails, ","));

            addErrorDetailsToContext(context, errorDetails);
            addErrorToContext(context, ERROR_OAUTH_DANCE, applicationLink.toString());
            addCheckLinkAdminErrorToContext(context, applicationLink.getName());
            render(context, resp);
        } else if (e instanceof ResponseException) {
            addErrorToContext(context, ERROR_OAUTH_DANCE, applicationLink.toString());
            addErrorDetailsToContext(context, Lists.newArrayList(e.getLocalizedMessage()));
            addCheckLinkAdminErrorToContext(context, applicationLink.getName());
            render(context, resp);
        } else {
            addErrorToContext(context, ERROR_OAUTH_DANCE, applicationLink.toString());
            addCheckLinkAdminErrorToContext(context, applicationLink.getName());
            render(context, resp);
        }
    }

    private void addErrorToContext(
            @Nonnull final Map<String, Object> context,
            @Nonnull final String errorKey,
            @Nonnull final String... args) {
        addVariableToContext(context, ERROR, errorKey, args);
    }

    private void addErrorDetailsToContext(
            @Nonnull final Map<String, Object> context,
            @Nonnull final List<String> errorDetails) {
        context.put(ERROR_DETAILS.key, errorDetails);
    }

    private void addWarningToContext(
            @Nonnull final Map<String, Object> context,
            @Nonnull final String warningTitleKey,
            @Nonnull final String warningMessageKey,
            @Nonnull final String... args) {
        addVariableToContext(context, WARNING_TITLE, warningTitleKey);
        addVariableToContext(context, WARNING_MESSAGE, warningMessageKey, args);
    }

    private void addCheckConfigAdminErrorToContext(@Nonnull final Map<String, Object> context) {
        addVariableToContext(context, ADMIN_ERROR_MESSAGE, ADMIN_ERROR_CHECK_CONFIG,
                internalHostApplication.getName());
    }

    private void addCheckLinkAdminErrorToContext(
            @Nonnull final Map<String, Object> context,
            @Nonnull final String remoteAppName) {
        addVariableToContext(context, ADMIN_ERROR_MESSAGE, ADMIN_ERROR_CHECK_LINK,
                internalHostApplication.getName(), remoteAppName);
    }

    private void addNotLoggedInUserErrorToContext(@Nonnull final Map<String, Object> context) {
        addVariableToContext(context, USER_ERROR_MESSAGE, USER_ERROR_NOT_LOGGED_IN, internalHostApplication.getName());
    }

    private void addAccessDeniedUserErrorToContext(
            @Nonnull final Map<String, Object> context,
            @Nonnull final String remoteAppName) {
        addVariableToContext(context, USER_ERROR_MESSAGE, USER_ERROR_ACCESS_DENIED, internalHostApplication.getName(),
                remoteAppName);
    }

    private void addVariableToContext(
            @Nonnull final Map<String, Object> context,
            @Nonnull final TemplateVariable templateVariable,
            @Nonnull final String errorKey,
            @Nonnull final String... args) {

        final String error = args.length > 0 ? i18nResolver.getText(errorKey, args) : i18nResolver.getText(errorKey);
        context.put(templateVariable.key, error);
    }

    private void render(
            @Nonnull final Map<String, Object> context, @Nonnull final HttpServletResponse resp) throws IOException {
        templateRenderer.render(TEMPLATE, context, resp.getWriter());
    }

    private List<String> getDetailsFromOAuthMessageProblemException(@Nonnull final OAuthMessageProblemException e) {
        final List<String> errorDetails = Lists.newArrayList();
        for (Map.Entry<String, String> param : e.getParameters().entrySet()) {
            final String oAuthParam = param.getKey() + ": '" + param.getValue() + "'";
            errorDetails.add(oAuthParam);
        }
        return errorDetails;
    }

    private void addRedirectUrlToContext(
            @Nonnull final HttpServletRequest req,
            @Nonnull final Map<String, Object> context) {
        String redirectUrl = getValidatedRedirectUrl(req);
        if (redirectUrl == null) {
            redirectUrl = "#";
        }
        context.put(REDIRECT_URL.key, redirectUrl);
    }

    private Map<String, Object> createVelocityContext(final HttpServletResponse resp) {
        final Map<String, Object> context = new HashMap<>();
        context.put("i18n", i18nResolver);
        resp.setContentType("text/html");
        webResourceManager.requireResource("com.atlassian.applinks.applinks-oauth-plugin:oauth-dance");
        final StringWriter stringWriter = new StringWriter();
        webResourceManager.includeResources(stringWriter, UrlMode.RELATIVE);
        final WebResources webResources = new WebResources();
        webResources.setIncludedResources(stringWriter.getBuffer().toString());
        context.put(WEB_RESOURCES.key, webResources);
        return context;
    }

    private String getToken(final HttpServletRequest req) {
        return req.getParameter(OAuth.OAUTH_TOKEN);
    }

    private void getAccessToken(String requestToken, final ApplicationLink applicationLink, final HttpServletRequest request)
            throws ResponseException {
        final String username = getRemoteUsername(request);
        final ConsumerToken requestTokenPair = consumerTokenStoreService.getConsumerToken(applicationLink, username);
        if (requestTokenPair == null) {
            throw new ResponseException("Cannot get access token as no request token pair can be found");
        }
        if (requestTokenPair.isAccessToken()) {
            //Already has access token.
            return;
        }
        if (!requestToken.equals(requestTokenPair.getToken())) {
            throw new ResponseException("The oauth_token in the request is not the same as the token persisted in the system.");
        }
        final Map<String, String> config = authenticationConfigurationManager.getConfiguration(applicationLink.getId(),
                OAuthAuthenticationProvider.class);
        final ServiceProvider serviceProvider = ServiceProviderUtil.getServiceProvider(config, applicationLink);
        final String requestVerifier = request.getParameter(OAuth.OAUTH_VERIFIER);
        final String consumerKey = getConsumerKey(applicationLink);
        final ConsumerToken accessToken = oAuthTokenRetriever.getAccessToken(serviceProvider, requestTokenPair,
                requestVerifier, consumerKey);
        consumerTokenStoreService.removeConsumerToken(applicationLink.getId(), username);
        consumerTokenStoreService.addConsumerToken(applicationLink, username, accessToken);
    }

    private void obtainAndAuthorizeRequestToken(final ApplicationLink applicationLink, final HttpServletResponse resp, final HttpServletRequest req)
            throws ResponseException, IOException {
        final Map<String, String> config = authenticationConfigurationManager.getConfiguration(applicationLink.getId(), OAuthAuthenticationProvider.class);
        final ServiceProvider serviceProvider = ServiceProviderUtil.getServiceProvider(config, applicationLink);
        final String consumerKey = getConsumerKey(applicationLink);
        final String redirectUrl = getRedirectUrl(req);
        URI baseUrl = RequestUtil.getBaseURLFromRequest(req, internalHostApplication.getBaseUrl());
        final String redirectToMeUrl = baseUrl + ServletPathConstants.APPLINKS_SERVLETS_PATH + "/oauth/login-dance/" + ACCESS_PATH
                + "?" + APPLICATION_LINK_ID_PARAM + "=" + applicationLink.getId() + (redirectUrl != null ? "&" + REDIRECT_URL_PARAM + "=" + URLEncoder.encode(redirectUrl, "UTF-8") : "");
        final ConsumerToken requestToken = oAuthTokenRetriever.getRequestToken(serviceProvider, consumerKey, redirectToMeUrl);
        consumerTokenStoreService.addConsumerToken(applicationLink, getRemoteUsername(req), requestToken);
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put(OAuth.OAUTH_TOKEN, requestToken.getToken());
        parameters.put(OAuth.OAUTH_CALLBACK, redirectToMeUrl);
        resp.sendRedirect(serviceProvider.getAuthorizeUri() + "?" + OAuth.formEncode(parameters.entrySet()));
    }

    private String getRemoteUsername(final HttpServletRequest request) {
        final UserProfile remoteUser = userManager.getRemoteUser(request);
        return remoteUser == null ? null : remoteUser.getUsername();
    }

    private String getConsumerKey(ApplicationLink applicationLink) {
        final Map<String, String> config = authenticationConfigurationManager.getConfiguration(applicationLink.getId(), OAuthAuthenticationProvider.class);
        if (config.containsKey(AddServiceProviderManuallyServlet.CONSUMER_KEY_OUTBOUND)) {
            return config.get(AddServiceProviderManuallyServlet.CONSUMER_KEY_OUTBOUND);
        }
        return consumerService.getConsumer().getKey();
    }

    private String getApplicationLinkId(HttpServletRequest req) {
        return req.getParameter(APPLICATION_LINK_ID_PARAM);
    }

    private String getRedirectUrl(HttpServletRequest req) {
        return req.getParameter(REDIRECT_URL_PARAM);
    }

    private String getValidatedRedirectUrl(HttpServletRequest req) {
        final String redirectUrl = req.getParameter(REDIRECT_URL_PARAM);
        if (redirectUrl != null) {
            callbackParameterValidator.validate(redirectUrl);
        }

        return redirectUrl;
    }

}
