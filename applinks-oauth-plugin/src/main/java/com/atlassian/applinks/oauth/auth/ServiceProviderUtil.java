package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.oauth.auth.servlets.consumer.AddServiceProviderManuallyServlet;
import com.atlassian.oauth.ServiceProvider;

import java.net.URI;
import java.util.Map;

import static com.atlassian.applinks.core.util.URIUtil.uncheckedConcatenate;
import static java.util.Objects.requireNonNull;

/**
 * Creates service provider URL for a service provider that has the atlassian oauth plugin installed.
 *
 * @since 3.0
 */
public class ServiceProviderUtil {

    public static ServiceProvider getServiceProvider(final URI rpcUrl, final URI displayUrl) {
        final URI requestTokenUri = uncheckedConcatenate(rpcUrl, "/plugins/servlet/oauth/request-token");
        final URI authorizeTokenUri = uncheckedConcatenate(displayUrl, "/plugins/servlet/oauth/authorize");
        final URI accessTokenUri = uncheckedConcatenate(rpcUrl, "/plugins/servlet/oauth/access-token");
        return new ServiceProvider(requestTokenUri, authorizeTokenUri, accessTokenUri);
    }

    public static ServiceProvider getServiceProvider(Map<String, String> config, ApplicationLink applicationLink) {
        if (config.containsKey(AddServiceProviderManuallyServlet.CONSUMER_KEY_OUTBOUND)) {
            final String accessTokenUrl = makeAbsoluteUrl(config.get(AddServiceProviderManuallyServlet.SERVICE_PROVIDER_ACCESS_TOKEN_URL), applicationLink.getRpcUrl());
            final String requestTokenUrl = makeAbsoluteUrl(config.get(AddServiceProviderManuallyServlet.SERVICE_PROVIDER_REQUEST_TOKEN_URL), applicationLink.getRpcUrl());
            final String authorizeUrl = makeAbsoluteUrl(config.get(AddServiceProviderManuallyServlet.SERVICE_PROVIDER_AUTHORIZE_URL), applicationLink.getDisplayUrl());
            return new ServiceProvider(URI.create(requestTokenUrl), URI.create(authorizeUrl), URI.create(accessTokenUrl));
        }
        return ServiceProviderUtil.getServiceProvider(applicationLink.getRpcUrl(), applicationLink.getDisplayUrl());
    }

    /**
     * Creates absolute uri from the given possibly-relative uri and base uri.
     * If the given uri is already absolute, return as it is.
     *
     * @param uri     uri, could be relative or absolute. If relative, it must start with a slash.
     * @param baseUri base uri must be absolute
     * @return an absolute uri
     */
    protected static String makeAbsoluteUrl(String uri, URI baseUri) {
        requireNonNull(uri, "uri can't be null");
        requireNonNull(baseUri, "baseUri can't be null");

        if (uri.startsWith("/")) {
            return baseUri.resolve(uri).toASCIIString();
        } else {
            return uri;
        }
    }
}