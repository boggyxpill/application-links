package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.core.auth.AbstractApplicationLinkResponseHandler;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.sal.api.net.Response;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import net.oauth.OAuth;
import net.oauth.OAuthMessage;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;

/**
 * Base class for OAuth redirecting application link response handlers.
 *
 * @since 3.11.0
 */
public class OAuthRedirectingApplicationLinkResponseHandler extends AbstractApplicationLinkResponseHandler {
    public static final String WWW_AUTH_HEADER = "WWW-Authenticate";

    private static final Logger log = LoggerFactory.getLogger(OAuthRedirectingApplicationLinkResponseHandler.class);

    protected static final Set<String> TOKEN_PROBLEMS = ImmutableSet.of(
            // See: http://wiki.oauth.net/ProblemReporting
            OAuth.Problems.TOKEN_EXPIRED,
            OAuth.Problems.TOKEN_REJECTED,
            OAuth.Problems.TOKEN_REVOKED
    );

    protected final ConsumerTokenStoreService consumerTokenStoreService;
    protected final ApplicationId applicationId;
    protected final String username;

    // Used to indicate a problem with the token and to redirect to credentials required.
    protected boolean hasTokenProblems = false;
    protected String authenticationProblem = null;
    protected String authenticationProblemAdvice = null;
    protected List<OAuth.Parameter> allParameters;

    public OAuthRedirectingApplicationLinkResponseHandler(String url,
                                                          ApplicationLinkRequest wrappedRequest,
                                                          ConsumerTokenStoreService consumerTokenStoreService,
                                                          ApplicationId applicationId,
                                                          String username,
                                                          boolean followRedirects) {
        super(url, wrappedRequest, followRedirects);
        this.consumerTokenStoreService = consumerTokenStoreService;
        this.username = username;
        this.applicationId = applicationId;
    }

    protected void checkForOAuthProblemAndRemoveConsumerTokenIfNecessary(Response response) {
        final String value = response.getHeaders().get(WWW_AUTH_HEADER);
        if (!StringUtils.isBlank(value)) {
            allParameters = ImmutableList.copyOf(OAuthMessage.decodeAuthorization(value));
            for (OAuth.Parameter parameter : allParameters) {
                if ("oauth_problem".equals(parameter.getKey())) {
                    log.debug("OAuth request rejected by peer.\n" +
                            "Our OAuth request header: Authorization: " + wrappedRequest.getHeaders().get("Authorization") + "\n" +
                            "Full OAuth response header: WWW-Authenticate: " + value);

                    if (OAuth.Problems.TIMESTAMP_REFUSED.equals(parameter.getValue())) {
                        log.warn("Peer rejected the timestamp on our OAuth request. " +
                                "This might be due to a replay attack, but it's more " +
                                "likely our system clock is not synchronized with the " +
                                "server's clock. " +
                                "You may turn on debug logging to log the full contents " +
                                "of the OAuth response headers.");
                    }

                    // if the consumer token store is not provided, we just can't automatically delete it.
                    if (consumerTokenStoreService != null) {
                        if (TOKEN_PROBLEMS.contains(parameter.getValue())) {
                            try {
                                consumerTokenStoreService.removeConsumerToken(applicationId, username);
                            } catch (RuntimeException e) {
                                log.error("Failed to delete consumer token for user '" + username + "'.", e);
                            }
                            hasTokenProblems = true;
                        }
                    }

                    authenticationProblem = parameter.getValue();
                }

                if (OAuth.Problems.OAUTH_PROBLEM_ADVICE.equals(parameter.getKey())) {
                    this.authenticationProblemAdvice = parameter.getValue();
                }
            }
        }
    }
}
