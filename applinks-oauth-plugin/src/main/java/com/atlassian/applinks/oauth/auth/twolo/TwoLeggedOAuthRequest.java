package com.atlassian.applinks.oauth.auth.twolo;

import java.net.URI;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.oauth.auth.OAuthRequest;
import com.atlassian.oauth.Request;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerService;

/**
 * Represents a 2-legged OAuth request which is different to ordinary OAuth requests in that the
 * oauth_token field is always hardcoded to empty string. The signing mechanism also take the
 * empty string oauth_token into account to create the signature.
 *
 * @since 3.10
 */
public class TwoLeggedOAuthRequest extends OAuthRequest {
    public TwoLeggedOAuthRequest(final String url,
                                 final MethodType methodType,
                                 final com.atlassian.sal.api.net.Request wrappedRequest,
                                 final ServiceProvider serviceProvider,
                                 final ConsumerService consumerService,
                                 final ApplicationId applicationId) {
        super(url, methodType, wrappedRequest, applicationId, serviceProvider, consumerService);
    }

    @Override
    protected Request createUnsignedRequest() {
        // 2LO means oauth_token must be empty string
        return new com.atlassian.oauth.Request(toOAuthMethodType(methodType), URI.create(url), toOAuthParameters(""));
    }
}