package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.auth.oauth.ConsumerTokenService;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Default implementation of {@link ConsumerTokenService}
 *
 * @since v3.11.0
 */
public class DefaultConsumerTokenService implements ConsumerTokenService {
    private final ConsumerTokenStoreService consumerTokenStoreService;
    private final ApplicationLinkService applicationLinkService;

    @Autowired
    public DefaultConsumerTokenService(ConsumerTokenStoreService consumerTokenStoreService, ApplicationLinkService applicationLinkService) {
        this.consumerTokenStoreService = consumerTokenStoreService;
        this.applicationLinkService = applicationLinkService;
    }

    public void removeAllTokensForUsername(String username) {
        final Iterable<ApplicationLink> applicationLinks = applicationLinkService.getApplicationLinks();
        for (ApplicationLink applicationLink : applicationLinks) {
            final ApplicationId applicationId = applicationLink.getId();
            if (consumerTokenStoreService.isOAuthOutgoingEnabled(applicationId)) {
                consumerTokenStoreService.removeConsumerToken(applicationId, username);
            }
        }
    }
}
