package com.atlassian.applinks.oauth.auth.twolo;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.ServiceProviderStoreService;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.sal.api.net.RequestFactory;

/**
 * Plugin module implemenation for 2-legged OAuth authentication.
 *
 * @since 3.10
 */
public class TwoLeggedOAuthAuthenticatorProviderPluginModule extends AbstractTwoLeggedOAuthAuthenticatorProviderPluginModule {
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final ConsumerService consumerService;
    private final RequestFactory requestFactory;
    private final ServiceProviderStoreService serviceProviderStoreService;

    public TwoLeggedOAuthAuthenticatorProviderPluginModule(
            AuthenticationConfigurationManager authenticationConfigurationManager,
            ConsumerService consumerService,
            InternalHostApplication hostApplication,
            RequestFactory requestFactory,
            ServiceProviderStoreService serviceProviderStoreService) {
        super(hostApplication);
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.consumerService = consumerService;
        this.requestFactory = requestFactory;
        this.serviceProviderStoreService = serviceProviderStoreService;
    }

    public AuthenticationProvider getAuthenticationProvider(final ApplicationLink link) {
        AuthenticationProvider provider = null;

        if (authenticationConfigurationManager.isConfigured(link.getId(), TwoLeggedOAuthAuthenticationProvider.class)) {
            provider = new TwoLeggedOAuthAuthenticationProvider() {
                public ApplicationLinkRequestFactory getRequestFactory() {
                    return new TwoLeggedOAuthRequestFactoryImpl(
                            link,
                            authenticationConfigurationManager,
                            consumerService,
                            requestFactory);
                }
            };
        }
        return provider;
    }

    public boolean incomingEnabled(final ApplicationLink applicationLink) {
        final Consumer consumer = serviceProviderStoreService.getConsumer(applicationLink);
        return consumer != null && consumer.getTwoLOAllowed();
    }

    @Override
    public Class<? extends AuthenticationProvider> getAuthenticationProviderClass() {
        return TwoLeggedOAuthAuthenticationProvider.class;
    }
}
