package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.core.auth.AbstractApplicationLinkRequest;
import com.atlassian.oauth.Request.HttpMethod;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.google.common.collect.ImmutableMap;
import net.oauth.OAuth;
import net.oauth.OAuthMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @since 3.10
 */
public abstract class OAuthRequest extends AbstractApplicationLinkRequest {
    private static final Logger log = LoggerFactory.getLogger(OAuthRequest.class);

    private static final Map<MethodType, HttpMethod> METHOD_TYPE_MAP = ImmutableMap.<MethodType, HttpMethod>builder()
            .put(MethodType.GET, HttpMethod.GET)
            .put(MethodType.DELETE, HttpMethod.DELETE)
            .put(MethodType.POST, HttpMethod.POST)
            .put(MethodType.PUT, HttpMethod.PUT)
            .put(MethodType.HEAD, HttpMethod.HEAD)
            .put(MethodType.OPTIONS, HttpMethod.OPTIONS)
            .put(MethodType.TRACE, HttpMethod.TRACE)
            .build();

    protected final MethodType methodType;
    protected final ApplicationId applicationId;
    protected final ServiceProvider serviceProvider;
    protected final ConsumerService consumerService;

    public OAuthRequest(final String url, final MethodType methodType, final Request wrappedRequest,
                        final ApplicationId applicationId, final ServiceProvider serviceProvider,
                        final ConsumerService consumerService) {
        super(url, wrappedRequest);
        this.methodType = methodType;
        this.applicationId = applicationId;
        this.serviceProvider = serviceProvider;
        this.consumerService = consumerService;
    }

    public <R> R execute(final ApplicationLinkResponseHandler<R> applicationLinkResponseHandler) throws ResponseException {
        signRequest();
        return wrappedRequest.execute(ensureOAuthApplinksResponseHandler(applicationLinkResponseHandler));
    }

    private <R> ApplicationLinkResponseHandler<R> ensureOAuthApplinksResponseHandler(final ApplicationLinkResponseHandler<R> applicationLinkResponseHandler) {
        if (applicationLinkResponseHandler instanceof OAuthApplinksResponseHandler) {
            return applicationLinkResponseHandler;
        }

        return new OAuthApplinksResponseHandler<>(url, applicationLinkResponseHandler, this, applicationId, followRedirects);
    }

    public void execute(final ResponseHandler responseHandler) throws ResponseException {
        signRequest();
        wrappedRequest.execute(ensureOAuthResponseHandler(responseHandler));
    }

    private ResponseHandler ensureOAuthResponseHandler(final ResponseHandler responseHandler) {
        if (responseHandler instanceof OAuthResponseHandler) {
            return responseHandler;
        }

        return new OAuthResponseHandler(url, responseHandler, wrappedRequest, applicationId, followRedirects);
    }

    public <RET> RET executeAndReturn(final ReturningResponseHandler<? super Response, RET> responseHandler) throws ResponseException {
        signRequest();
        return wrappedRequest.executeAndReturn(ensureOAuthApplinksReturningResponseHandler(responseHandler));
    }

    private <R> ReturningResponseHandler<? super Response, R> ensureOAuthApplinksReturningResponseHandler(final ReturningResponseHandler<? super Response, R> returningResponseHandler) {
        if (returningResponseHandler instanceof OAuthApplinksReturningResponseHandler) {
            return returningResponseHandler;
        }

        return new OAuthApplinksReturningResponseHandler<>(url, returningResponseHandler, wrappedRequest, applicationId, followRedirects);
    }

    protected void signRequest() throws ResponseException {
        final com.atlassian.oauth.Request oAuthRequest = createUnsignedRequest();
        final com.atlassian.oauth.Request signedRequest = consumerService.sign(oAuthRequest, serviceProvider);
        final OAuthMessage oAuthMessage = OAuthHelper.asOAuthMessage(signedRequest);
        try {
            wrappedRequest.setHeader("Authorization", oAuthMessage.getAuthorizationHeader(null));
        } catch (IOException e) {
            throw new ResponseException("Unable to generate OAuth Authorization request header.", e);
        }
    }

    /**
     * @return OAuth request before being signed, all the parameters must be available and ready to be signed.
     */
    protected abstract com.atlassian.oauth.Request createUnsignedRequest();

    protected List<com.atlassian.oauth.Request.Parameter> toOAuthParameters(final String accesstoken) {
        final List<com.atlassian.oauth.Request.Parameter> parameters = new ArrayList<com.atlassian.oauth.Request.Parameter>();
        parameters.add(new com.atlassian.oauth.Request.Parameter(OAuth.OAUTH_TOKEN, accesstoken));
        for (final String parameterName : this.parameters.keySet()) {
            final List<String> values = this.parameters.get(parameterName);
            for (final String value : values) {
                parameters.add(new com.atlassian.oauth.Request.Parameter(parameterName, value));
            }
        }
        return parameters;
    }

    protected static HttpMethod toOAuthMethodType(final Request.MethodType methodType) {
        HttpMethod method = METHOD_TYPE_MAP.get(methodType);
        if (method == null) {
            log.warn("Did not find matching OAuth method type for " + methodType + ", returning GET. This will " +
                    "likely lead to signature_invalid error");
            method = HttpMethod.GET;
        }
        return method;
    }
}