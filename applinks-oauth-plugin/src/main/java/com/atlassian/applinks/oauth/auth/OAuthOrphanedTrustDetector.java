package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.core.auth.InternalOrphanedTrustDetector;
import com.atlassian.applinks.core.auth.OrphanedTrustCertificate;
import com.atlassian.applinks.internal.common.auth.oauth.ServiceProviderStoreService;
import com.atlassian.applinks.oauth.auth.servlets.consumer.AddServiceProviderManuallyServlet;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Finds orphaned OAuth consumers (that is, they are stored locally but are not related to a
 * configured {@link ApplicationLink})
 *
 * @since 3.0
 */
public class OAuthOrphanedTrustDetector implements InternalOrphanedTrustDetector {

    private final ApplicationLinkService applicationLinkService;
    private final ServiceProviderConsumerStore serviceProviderConsumerStore;
    private final ServiceProviderStoreService serviceProviderStoreService;
    private final ConsumerService consumerService;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private static final Logger log = LoggerFactory.getLogger(OAuthOrphanedTrustDetector.class);

    @Autowired
    public OAuthOrphanedTrustDetector(final ApplicationLinkService applicationLinkService,
                                      final ServiceProviderConsumerStore serviceProviderConsumerStore,
                                      final ServiceProviderStoreService serviceProviderStoreService,
                                      final ConsumerService consumerService,
                                      final AuthenticationConfigurationManager authenticationConfigurationManager) {
        this.applicationLinkService = applicationLinkService;
        this.serviceProviderConsumerStore = serviceProviderConsumerStore;
        this.serviceProviderStoreService = serviceProviderStoreService;
        this.consumerService = consumerService;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
    }

    @Override
    public List<OrphanedTrustCertificate> findOrphanedTrustCertificates() {
        final List<OrphanedTrustCertificate> orphanedTrustCertificates = new ArrayList<>();

        orphanedTrustCertificates.addAll(findOrphanedOAuthConsumers());
        orphanedTrustCertificates.addAll(findOrphanedOAuthServiceProviders());
        return orphanedTrustCertificates;
    }

    private List<OrphanedTrustCertificate> findOrphanedOAuthServiceProviders() {
        final List<OrphanedTrustCertificate> orphanedTrustCertificates = new ArrayList<>();
        final List<String> registeredServiceProviders = findRegisteredServiceProviders();
        final Iterable<Consumer> allServiceProviders = consumerService.getAllServiceProviders();
        for (Consumer serviceProvider : allServiceProviders) {
            if (!registeredServiceProviders.contains(serviceProvider.getKey())) {
                log.debug("Found orphaned Service Provider with consumer key '{}' and name '{}'",
                        serviceProvider.getKey(), serviceProvider.getName());
                orphanedTrustCertificates.add(
                        new OrphanedTrustCertificate(serviceProvider.getKey(), serviceProvider.getDescription(),
                                OrphanedTrustCertificate.Type.OAUTH_SERVICE_PROVIDER)
                );
            }
        }
        return orphanedTrustCertificates;
    }

    private List<String> findRegisteredServiceProviders() {
        final List<String> serviceProviderConsumerKeys = new ArrayList<>();
        for (final ApplicationLink link : applicationLinkService.getApplicationLinks()) {
            if (authenticationConfigurationManager.isConfigured(link.getId(), OAuthAuthenticationProvider.class)) {
                Map<String, String> configuration = authenticationConfigurationManager.getConfiguration(link.getId(), OAuthAuthenticationProvider.class);
                final String consumerKey = configuration.get(AddServiceProviderManuallyServlet.CONSUMER_KEY_OUTBOUND);
                serviceProviderConsumerKeys.add(consumerKey);
            }
        }
        return serviceProviderConsumerKeys;
    }


    private List<OrphanedTrustCertificate> findOrphanedOAuthConsumers() {
        final List<OrphanedTrustCertificate> orphanedTrustCertificates = new ArrayList<>();
        final Set<String> recognisedConsumerKeys = new HashSet<>();
        for (final ApplicationLink link : applicationLinkService.getApplicationLinks()) {
            final Consumer consumer = serviceProviderStoreService.getConsumer(link);
            if (consumer != null) {
                recognisedConsumerKeys.add(consumer.getKey());
            }
        }

        for (final Consumer consumer : serviceProviderConsumerStore.getAll()) {
            if (!recognisedConsumerKeys.contains(consumer.getKey())) {
                orphanedTrustCertificates.add(
                        new OrphanedTrustCertificate(consumer.getKey(), consumer.getDescription(),
                                OrphanedTrustCertificate.Type.OAUTH)
                );
            }
        }

        return orphanedTrustCertificates;
    }

    @Override
    public void deleteTrustCertificate(final String id, final OrphanedTrustCertificate.Type type) {
        checkCertificateType(type);
        if (type == OrphanedTrustCertificate.Type.OAUTH) {
            serviceProviderConsumerStore.remove(id);
        } else if (type == OrphanedTrustCertificate.Type.OAUTH_SERVICE_PROVIDER) {
            consumerService.removeConsumerByKey(id);
        }
    }

    private void checkCertificateType(final OrphanedTrustCertificate.Type type) {
        if (!canHandleCertificateType(type)) {
            throw new IllegalArgumentException("Unsupported type: " + type);
        }
    }

    @Override
    public boolean canHandleCertificateType(final OrphanedTrustCertificate.Type type) {
        return (type == OrphanedTrustCertificate.Type.OAUTH || type == OrphanedTrustCertificate.Type.OAUTH_SERVICE_PROVIDER);
    }

    @Override
    public void addOrphanedTrustToApplicationLink(final String id, final OrphanedTrustCertificate.Type type, final ApplicationId applicationId) {
        checkCertificateType(type);
        final ApplicationLink applicationLink;
        try {
            applicationLink = applicationLinkService.getApplicationLink(applicationId);
            if (applicationLink == null) {
                throw new NoApplicationIdFoundException("No Application Link with id '" + applicationId + "' found.");
            }
        } catch (TypeNotInstalledException e) {
            throw new IllegalStateException("An application of the type " + e.getType() + " is not installed!", e);
        }

        if (type == OrphanedTrustCertificate.Type.OAUTH) {
            registerOAuthConsumer(id, applicationLink);
        } else if (type == OrphanedTrustCertificate.Type.OAUTH_SERVICE_PROVIDER) {
            registerOAuthServiceProvider(id, applicationLink);
        }
    }

    private void registerOAuthServiceProvider(final String id, final ApplicationLink applicationLink) {
        final Consumer consumer = consumerService.getConsumerByKey(id);
        final String requestTokenUrl = applicationLink.getRpcUrl() + "/request/token";
        final String accessTokenUrl = applicationLink.getRpcUrl() + "/access/token";
        final String authorizeUrl = applicationLink.getDisplayUrl() + "/authorize/token";

        authenticationConfigurationManager.registerProvider(
                applicationLink.getId(),
                OAuthAuthenticationProvider.class,
                ImmutableMap.of(AddServiceProviderManuallyServlet.CONSUMER_KEY_OUTBOUND, consumer.getKey(),
                        AddServiceProviderManuallyServlet.SERVICE_PROVIDER_REQUEST_TOKEN_URL, requestTokenUrl,
                        AddServiceProviderManuallyServlet.SERVICE_PROVIDER_ACCESS_TOKEN_URL, accessTokenUrl,
                        AddServiceProviderManuallyServlet.SERVICE_PROVIDER_AUTHORIZE_URL, authorizeUrl));
        log.debug("Associated OAuth ServiceProvider with consumer key '{}' with Application Link id='{}' and name='{}'",
                consumer.getKey(), applicationLink.getId(), applicationLink.getName());
    }

    private void registerOAuthConsumer(final String id, final ApplicationLink applicationLink) {
        Consumer consumer = serviceProviderConsumerStore.get(id);
        if (consumer == null) {
            throw new NoConsumerFoundException("No consumer with id '" + id + "' registered!");
        }
        serviceProviderStoreService.addConsumer(consumer, applicationLink);
        log.debug("Associated OAuth Consumer with key '{}' with Application Link id='{}' and name='{}'",
                consumer.getKey(), applicationLink.getId(), applicationLink.getName());
    }

    public static class NoApplicationIdFoundException extends RuntimeException {
        public NoApplicationIdFoundException(String message) {
            super(message);
        }
    }

    public static class NoConsumerFoundException extends RuntimeException {
        public NoConsumerFoundException(String message) {
            super(message);
        }
    }
}
