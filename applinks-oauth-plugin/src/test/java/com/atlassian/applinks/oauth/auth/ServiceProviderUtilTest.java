package com.atlassian.applinks.oauth.auth;

import org.junit.Test;

import java.net.URI;

import com.atlassian.applinks.oauth.auth.ServiceProviderUtil;

import static org.junit.Assert.assertEquals;

public class ServiceProviderUtilTest {
    @Test
    public void makeAbsoluteUrlSmokeTest() {
        assertEquals("http://localhost:80/blah/test", ServiceProviderUtil.makeAbsoluteUrl("/blah/test", URI.create("http://localhost:80")));
    }

    @Test
    public void makeAbsoluteUrlWithTrailingSlashInBashUrl() {
        assertEquals("http://localhost:80/blah/test", ServiceProviderUtil.makeAbsoluteUrl("/blah/test", URI.create("http://localhost:80/")));
    }

    @Test
    public void makeAbsoluteUrlFromAlreadyAbsoluteUrl() {
        assertEquals("http://localhost:80/blah/test", ServiceProviderUtil.makeAbsoluteUrl("http://localhost:80/blah/test", URI.create("http://www.atlassian.com")));
    }
}
