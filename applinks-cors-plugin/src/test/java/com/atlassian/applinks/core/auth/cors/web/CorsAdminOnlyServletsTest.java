package com.atlassian.applinks.core.auth.cors.web;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.RedirectController;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.cors.auth.CorsAuthServlet;
import com.atlassian.applinks.cors.auth.CorsService;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.applinks.ui.validators.CallbackParameterValidator;
import com.atlassian.applinks.ui.velocity.ListApplicationLinksContext;
import com.atlassian.applinks.ui.velocity.VelocityContextFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Before;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Theories.class)
public class CorsAdminOnlyServletsTest {
    @DataPoints
    public static SysadminOnlyTestServlet[] testSysadminOnlyServlets = SysadminOnlyTestServlet.values();

    static I18nResolver i18nResolver;
    static MessageFactory messageFactory;
    static TemplateRenderer templateRenderer;
    static WebResourceManager webResourceManager;
    static ApplicationLinkService applicationLinkService;
    static ManifestRetriever manifestRetriever;
    static PluginAccessor pluginAccessor;
    static AdminUIAuthenticator adminUIAuthenticator;
    static InternalHostApplication internalHostApplication;
    static DocumentationLinker documentationLinker;
    static LoginUriProvider loginUriProvider;
    static WebSudoManager webSudoManager;
    static VelocityContextFactory velocityContextFactory;
    static AuthenticationConfigurationManager configurationManager;
    static XsrfTokenAccessor xsrfTokenAccessor;
    static XsrfTokenValidator xsrfTokenValidator;
    static UserManager userManager;
    static CorsService corsService;
    static CallbackParameterValidator callbackParameterValidator;
    static RedirectController redirectController;

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession session;
    PrintWriter writer;

    ListApplicationLinksContext listApplicationLinksContext;

    @Before
    public void setUp() {
        i18nResolver = mock(I18nResolver.class);
        messageFactory = mock(MessageFactory.class);
        templateRenderer = mock(TemplateRenderer.class);
        webResourceManager = mock(WebResourceManager.class);
        applicationLinkService = mock(ApplicationLinkService.class);
        manifestRetriever = mock(ManifestRetriever.class);
        pluginAccessor = mock(PluginAccessor.class);
        adminUIAuthenticator = mock(AdminUIAuthenticator.class);
        internalHostApplication = mock(InternalHostApplication.class);
        documentationLinker = mock(DocumentationLinker.class);
        loginUriProvider = mock(LoginUriProvider.class);
        webSudoManager = mock(WebSudoManager.class);
        velocityContextFactory = mock(VelocityContextFactory.class);
        configurationManager = mock(AuthenticationConfigurationManager.class);
        xsrfTokenAccessor = mock(XsrfTokenAccessor.class);
        xsrfTokenValidator = mock(XsrfTokenValidator.class);
        userManager = mock(UserManager.class);
        corsService = mock(CorsService.class);
        callbackParameterValidator = mock(CallbackParameterValidator.class);
        redirectController = mock(RedirectController.class);

        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
        writer = mock(PrintWriter.class);
        listApplicationLinksContext = mock(ListApplicationLinksContext.class);

        when(request.getPathInfo()).thenReturn("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        when(request.getServletPath()).thenReturn("servlet-path");
        when(xsrfTokenValidator.validateFormEncodedToken(request)).thenReturn(true);
        when(userManager.getRemoteUsername()).thenReturn("admin");
        when(userManager.isSystemAdmin(any(String.class))).thenReturn(true);
        when(velocityContextFactory.buildListApplicationLinksContext(request)).thenReturn(listApplicationLinksContext);
    }

    @Theory
    public void verifyThatSysadminAccessIsCheckedForSysadminOnlyServlet(SysadminOnlyTestServlet servlet) throws Exception {
        when(request.getMethod()).thenReturn(servlet.getMethod());
        whenUserIsLoggedInAsSysadmin();
        servlet.getServlet().service(request, response);

        verify(adminUIAuthenticator).checkSysadminUIAccessBySessionOrCurrentUser(request);
    }

    @Theory
    public void verifyThatWebSudoRequestIsExecutedForSysadminOnlyServlet(SysadminOnlyTestServlet servlet) throws Exception {
        when(request.getMethod()).thenReturn(servlet.getMethod());
        whenUserIsLoggedInAsSysadmin();
        servlet.getServlet().service(request, response);

        verify(webSudoManager).willExecuteWebSudoRequest(request);
    }

    @Theory
    public void verifyWebSudoGivenControlWhenRequestRequiresSudoAuth(SysadminOnlyTestServlet servlet) throws Exception {
        when(request.getMethod()).thenReturn(servlet.getMethod());
        whenUserIsLoggedInAsSysadmin();
        doThrow(new WebSudoSessionException("blah")).when(webSudoManager).willExecuteWebSudoRequest(request);
        servlet.getServlet().service(request, response);

        verify(webSudoManager).enforceWebSudoProtection(request, response);
    }

    private static CorsAuthServlet createCorsServlet() {
        return new CorsAuthServlet(i18nResolver, messageFactory, templateRenderer, webResourceManager,
                applicationLinkService, adminUIAuthenticator, documentationLinker, loginUriProvider,
                internalHostApplication, xsrfTokenAccessor, xsrfTokenValidator,
                corsService, webSudoManager, userManager);
    }

    private void whenUserIsLoggedInAsSysadmin() {
        when(adminUIAuthenticator.checkSysadminUIAccessBySessionOrCurrentUser(request)).thenReturn(true);
    }

    enum SysadminOnlyTestServlet {
        CORS_SERVLET_GET {
            @Override
            HttpServlet getServlet() {
                return createCorsServlet();
            }

            @Override
            String getMethod() {
                return "GET";
            }
        },
        CORS_SERVLET_PUT {
            @Override
            HttpServlet getServlet() {
                return createCorsServlet();
            }

            @Override
            String getMethod() {
                return "PUT";
            }
        },
        CORS_SERVLET_DELETE {
            @Override
            HttpServlet getServlet() {
                return createCorsServlet();
            }

            @Override
            String getMethod() {
                return "DELETE";
            }
        };

        abstract HttpServlet getServlet();

        abstract String getMethod();
    }
}