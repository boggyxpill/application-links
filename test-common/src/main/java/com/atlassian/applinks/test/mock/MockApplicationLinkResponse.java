package com.atlassian.applinks.test.mock;

import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.Response.Status;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.Map;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.io.IOUtils.toByteArray;

/**
 * Mock implementation of {@link Response}.
 *
 * @since 4.3
 */
public class MockApplicationLinkResponse implements Response {
    private final Map<String, String> headers = Maps.newHashMap();

    private int statusCode = Status.OK.getStatusCode();

    private Object responseEntity = null;
    private byte[] responseBody = new byte[]{};
    private Charset responseEncoding = Charset.defaultCharset();

    private boolean credentialsRequired;
    private ResponseException responseException;
    private ResponseException readException;

    public String getHeader(String name) {
        return headers.get(name);
    }

    public int getStatusCode() {
        return statusCode;
    }

    @Nonnull
    public String getResponseBodyAsString() throws ResponseException {
        checkException();
        try {
            return IOUtils.toString(responseBody, responseEncoding.displayName());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Nonnull
    public InputStream getResponseBodyAsStream() throws ResponseException {
        checkException();
        return new ByteArrayInputStream(responseBody);
    }

    @Nonnull
    public <T> T getEntity(Class<T> entityClass) throws ResponseException {
        checkException();
        if (responseEntity == null) {
            throw new AssertionError("Entity was requested, but not provided");
        }
        if (!entityClass.isInstance(responseEntity)) {
            throw new ResponseException(String.format("Provided entity: %s, but requested %s",
                    responseEntity.getClass().getName(), entityClass.getName()));
        }

        return entityClass.cast(responseEntity);
    }

    @Nonnull
    public String getStatusText() {
        Status status = Status.fromStatusCode(statusCode);
        return status != null ? status.getReasonPhrase() : "";
    }

    public boolean isSuccessful() {
        return statusCode / 100 == 2;
    }

    @Nonnull
    public Map<String, String> getHeaders() {
        return headers;
    }

    public boolean isCredentialsRequired() {
        return credentialsRequired;
    }

    @Nullable
    public ResponseException getResponseException() {
        return responseException;
    }

    @Nonnull
    public MockApplicationLinkResponse setStatus(@Nonnull Status status) {
        requireNonNull(status, "status");
        this.statusCode = status.getStatusCode();
        return this;
    }

    @Nonnull
    public MockApplicationLinkResponse setStatus(int statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    @Nonnull
    public MockApplicationLinkResponse setResponseBody(@Nonnull String responseBody) {
        requireNonNull(responseBody, "responseBody");
        try {
            this.responseEncoding = Charset.defaultCharset();
            this.responseBody = toByteArray(new StringReader(responseBody), responseEncoding.displayName());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return this;
    }

    @Nonnull
    public MockApplicationLinkResponse setResponseBody(@Nonnull byte[] data, Charset encoding) {
        this.responseBody = requireNonNull(data, "responseBody");
        this.responseEncoding = requireNonNull(encoding, "encoding");
        return this;
    }

    @Nonnull
    public MockApplicationLinkResponse setEntity(@Nonnull Object entity) {
        this.responseEntity = requireNonNull(entity, "entity");
        return this;
    }

    @Nonnull
    public MockApplicationLinkResponse setHeader(@Nonnull String name, @Nonnull String value) {
        requireNonNull(name, "name");
        requireNonNull(value, "value");

        this.headers.put(name, value);
        return this;
    }

    public MockApplicationLinkResponse setCredentialsRequired(boolean credentialsRequired) {
        this.credentialsRequired = credentialsRequired;
        return this;
    }

    public MockApplicationLinkResponse setResponseException(ResponseException responseException) {
        this.responseException = responseException;
        return this;
    }

    public MockApplicationLinkResponse setReadException(ResponseException readException) {
        this.readException = readException;
        return this;
    }

    private void checkException() throws ResponseException {
        if (responseException != null) {
            throw responseException;
        }
        if (readException != null) {
            throw readException;
        }
    }
}
