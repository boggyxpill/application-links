package com.atlassian.applinks.test.matcher;

import com.atlassian.applinks.test.mock.MockApplicationLinkRequest;
import com.atlassian.sal.api.net.Request.MethodType;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.hamcrest.Matchers.is;

public final class MockRequestMatchers {
    private MockRequestMatchers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<MockApplicationLinkRequest> withMethodTypeThat(@Nonnull Matcher<MethodType> methodTypeMatcher) {
        return new FeatureMatcher<MockApplicationLinkRequest, MethodType>(methodTypeMatcher, "method type that",
                "methodType") {
            @Override
            protected MethodType featureValueOf(MockApplicationLinkRequest request) {
                return request.getMethodType();
            }
        };
    }

    @Nonnull
    public static Matcher<MockApplicationLinkRequest> withMethodType(@Nullable MethodType methodType) {
        return withMethodTypeThat(is(methodType));
    }

    @Nonnull
    public static Matcher<MockApplicationLinkRequest> withUrlThat(@Nonnull Matcher<String> urlMatcher) {
        return new FeatureMatcher<MockApplicationLinkRequest, String>(urlMatcher, "URL that", "url") {
            @Override
            protected String featureValueOf(MockApplicationLinkRequest request) {
                return request.getUrl();
            }
        };
    }

    @Nonnull
    public static <E> Matcher<MockApplicationLinkRequest> withEntity(@Nullable E expectedEntity) {
        return withEntityThat(is(expectedEntity));
    }

    @Nonnull
    public static <E> Matcher<MockApplicationLinkRequest> withEntityThat(@Nonnull Matcher<E> entityMatcher) {
        return new FeatureMatcher<MockApplicationLinkRequest, E>(entityMatcher, "entity that", "entity") {
            @Override
            @SuppressWarnings("unchecked")
            protected E featureValueOf(MockApplicationLinkRequest request) {
                return (E) request.getEntity();
            }
        };
    }

    @Nonnull
    public static Matcher<MockApplicationLinkRequest> withUrl(@Nullable String expectedUrl) {
        return withUrlThat(is(expectedUrl));
    }

    @Nonnull
    public static Matcher<MockApplicationLinkRequest> withConnectionTimeoutThat(@Nonnull Matcher<Integer> timeoutMatcher) {
        return new FeatureMatcher<MockApplicationLinkRequest, Integer>(timeoutMatcher, "connection timeout that",
                "connectionTimeout") {
            @Override
            protected Integer featureValueOf(MockApplicationLinkRequest request) {
                return request.getConnectionTimeout();
            }
        };
    }

    @Nonnull
    public static Matcher<MockApplicationLinkRequest> withConnectionTimeout(int expectedTimeout) {
        return withConnectionTimeoutThat(is(expectedTimeout));
    }

    @Nonnull
    public static Matcher<MockApplicationLinkRequest> withSocketTimeoutThat(@Nonnull Matcher<Integer> timeoutMatcher) {
        return new FeatureMatcher<MockApplicationLinkRequest, Integer>(timeoutMatcher, "socket timeout that",
                "soTimeout") {
            @Override
            protected Integer featureValueOf(MockApplicationLinkRequest request) {
                return request.getSoTimeout();
            }
        };
    }

    @Nonnull
    public static Matcher<MockApplicationLinkRequest> withSocketTimeout(int expectedTimeout) {
        return withSocketTimeoutThat(is(expectedTimeout));
    }
}
