package com.atlassian.applinks.test.mockito;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;


/**
 * @since 5.0
 */
public final class MockitoAnswers {
    private MockitoAnswers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static <T> Answer<T> withFirstArgument(final @Nonnull Class<T> argType) {
        return withArgument(0, argType);
    }

    @Nonnull
    public static <T> Answer<T> withArgument(final int argumentIndex, final @Nonnull Class<T> argType) {
        requireNonNull(argType, "argType");
        return new Answer<T>() {
            @Override
            public T answer(InvocationOnMock invocation) throws Throwable {
                return argType.cast(invocation.getArguments()[argumentIndex]);
            }
        };
    }
}
