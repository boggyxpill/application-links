const path = require('path');
const { PLUGIN_RESOURCES_FOLDER, P2_FRONTEND_TARGET_CLASSES_PATH } = require('./path.utils');

const ENTRYPOINTS = [
    {
        key: 'entitylink-page-init',
        path: 'applinks/internal/feature/entitylink/entitylinks-init.js',
        context: ['applinks.internal.entitylinks'],
    },
];

const PLUGIN_KEY = 'com.atlassian.applinks.applinks-plugin';

const XML_OUTPUT_PATH = path.join(
    P2_FRONTEND_TARGET_CLASSES_PATH,
    'META-INF',
    'plugin-descriptors'
);

const I18N_PROPERTY_FILES = [
    'com/atlassian/applinks/entitylinks.properties',
].map(file => path.join(PLUGIN_RESOURCES_FOLDER, file));

module.exports = {
    ENTRYPOINTS,
    PLUGIN_KEY,
    XML_OUTPUT_PATH,
    I18N_PROPERTY_FILES,
};
