const path = require('path');

// Frontend P2 plugin
const ROOT = path.resolve(path.join(__dirname, '..'));
const P2_FRONTEND_SRC_PATH = path.join(ROOT, 'src');
const PLUGIN_RESOURCES_FOLDER = path.join(P2_FRONTEND_SRC_PATH, 'main', 'resources');
const P2_FRONTEND_TARGET_PATH = path.join(ROOT, 'target');
const P2_FRONTEND_TARGET_CLASSES_PATH = path.join(P2_FRONTEND_TARGET_PATH, 'classes');

module.exports = {
    ROOT,
    P2_FRONTEND_SRC_PATH,
    PLUGIN_RESOURCES_FOLDER,
    P2_FRONTEND_TARGET_PATH,
    P2_FRONTEND_TARGET_CLASSES_PATH,
};