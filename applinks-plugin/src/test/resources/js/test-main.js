// bind polyfill for PhantomJS from MDN:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
//
// (Public Domain as per https://developer.mozilla.org/en-US/docs/MDN/About#Copyrights_and_licenses)
if (!Function.prototype.bind) {
    Function.prototype.bind = function (oThis) {
        if (typeof this !== "function") {
            // closest thing possible to the ECMAScript 5 internal IsCallable function
            throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            fNOP = function () {},
            fBound = function () {
                return fToBind.apply(this instanceof fNOP && oThis
                        ? this
                        : oThis,
                    aArgs.concat(Array.prototype.slice.call(arguments)));
            };

        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();

        return fBound;
    };
}


var TEST_REGEXP = /-test.js$/;
var tests = [];

var pathToModule = function(path) {
    if (/^\/absolute/.test(path)) {
        return path.replace('\\', '/');
    } else {
        return path.replace(/^\/base\//, '').replace(/\.js$/, '');
    }
};

for (var file in window.__karma__.files) {
    if (window.__karma__.files.hasOwnProperty(file)) {
        if (TEST_REGEXP.test(file)) {
            tests.push(pathToModule(file));
        }
    }
}

var requireConfig = {

    // Karma serves files under /base, which is the basePath from your config file
    baseUrl: '/base',
    // map file paths to module names
    paths: window.atlassianTestConfig.moduleMap
};

requirejs.config(requireConfig);

require(tests, function() {
    window.__karma__.start();
});

