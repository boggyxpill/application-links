define('applinks/test/hamjest-assert', [
    'applinks/lib/lodash',
    'hamjest'
], function(
    _,
    __
) {
    /**
     * Create a function that integrates Hamjest's `assertThat` with QUnit's test reporting API, returning
     * the `Assert` object with an extra `assertThat` method.
     *
     * @param assert {Object} QUnit's Assert object
     */
    function HamjestAssert(assert) {
        return {
            assertThat: function() {
                var hasMessage = arguments.length > 2;
                var message = hasMessage ? arguments[0] : 'assertThat succeeded';
                var actual = hasMessage ? arguments[1] : arguments[0];
                try {
                    __.assertThat.apply(null, arguments);
                } catch(error) {
                    // failed
                    assert.push(false, anyToString(actual), 'Hamjest matcher (see above)', error);
                }
                // succeeded
                assert.push(true, anyToString(actual), 'Hamjest matcher (see above)', message);
            },

            assertEquals: function(actual, expected) {
                this.assertThat(actual, __.equalTo(expected));
            },
            fail: function(message) {
                assert.ok(false, message);
            },
            // add other methods if required, but prefer assertThat

            // proxy all methods on Assert, can't do it using inheritance as we don't have access to Assert constructor
            async: _.bind(assert.async, assert),
            push: _.bind(assert.push, assert),
            assertThrows: _.bind(assert.throws, assert)
        };
    }

    function anyToString(obj) {
        if (_.isObject(obj)) {
            return objToString(obj);
        } else {
            return toString(obj);
        }
    }

    function objToString(obj) {
        if (isSinonProxy(obj)) {
            return '(Sinon stub): ' + obj.toString();
        }

        var stringFromObject = obj.toString();
        if (stringFromObject !== '[object Object]') {
            return stringFromObject;
        }

        var values = _.chain(obj).pairs().reduce(function(current, pair) {
            return current + '\n\t' + toString(pair[0]) + ': ' + toString(pair[1]);
        }, '\n{');
        return values + '\n}'

    }

    function toString(obj) {
        if (_.isUndefined(obj)) {
            return 'undefined';
        } else {
            return obj;
        }
    }

    function isSinonProxy(obj) {
        return obj.isSinonProxy;
    }

    return HamjestAssert;
});