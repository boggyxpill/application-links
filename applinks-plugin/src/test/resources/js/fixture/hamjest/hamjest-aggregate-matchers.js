define('applinks/test/hamjest-aggregate-matchers', [
    'applinks/lib/lodash',
    'hamjest'
], function(
    _,
    __
) {
    /**
     * Utility to patch some of the Hamjest aggregate matcher functions to detect an array of matchers passed as the
     * first argument.
     *
     * @param original original Hamjest function
     */
    function applyForMatcherArray(original) {
        return function() {
            // check for 1 argument that is an array of matchers
            if (arguments.length == 1 && _.isArray(arguments[0]) && _.every(arguments[0], __.isMatcher)) {
                // invoke using it as arguments
                return original.apply(this, arguments[0]);
            } else {
                // just invoke the original
                return original.apply(this, _.toArray(arguments));
            }
        }
    }

    return applyForMatcherArray;
});