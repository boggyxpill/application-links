/**
 * Rationale: aui-prototyping needs to create AJS global instead of AMD module.
 * When it's done we can revert "define.amd" removal so other libs can define their AMD.
 */
if (window.define && window.__defineamd) {
    window.define.amd = window.__defineamd;
}