define([
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration'
], function(
    _,
    QUnit,
    __,
    MockDeclaration
) {
    function applinksModulesConstants() {
        return {
            COMMON_EXPORTED: 'applinks-common-exported'
        };
    }

    var ENTITY_TYPE_MAP = {
        singular: {
            "jira.project": 'Jira Project',
            "stash.project": 'Bitbucket Server Project'
        },
        plural: {
            "jira.project": 'Jira Projects',
            "stash.project": 'Bitbucket Server Projects'
        }
    };

    var AUTH_TYPE_MAP = {
        "com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider": 'OAuth',
        "com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider": 'OAuth',
        "com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider": 'OAuth'
    };
    
    QUnit.module('applinks/common/i18n', {
        require: {
            main: 'applinks/common/i18n',
            jquery: 'applinks/lib/jquery'
        },
        mocks: {
            WRM: QUnit.moduleMock('applinks/lib/wrm', new MockDeclaration(['data.claim'])),
            AppliksModules: QUnit.moduleMock('applinks/common/modules',
                new MockDeclaration(['dataFqn'], applinksModulesConstants)),
            ApplinksProducts: QUnit.moduleMock('applinks/common/products', new MockDeclaration(['getTypeName'])),
            Preconditions: QUnit.moduleMock('applinks/common/preconditions', new MockDeclaration(['hasValue']))
        },
        beforeEach: function(asseert, ApplinksI18n, mocks) {
            // set up entity types in WRM
            mocks.AppliksModules.dataFqn.withArgs(mocks.AppliksModules.COMMON_EXPORTED, 'entity-types')
                .returns('entity-types-id');
            mocks.WRM.data.claim.withArgs('entity-types-id').returns(ENTITY_TYPE_MAP);
            mocks.Preconditions.hasValue.withArgs(ENTITY_TYPE_MAP, 'entity-types', 'Entity Types data not found')
                .returns(ENTITY_TYPE_MAP);

            // set up auth types in WRM
            mocks.AppliksModules.dataFqn.withArgs(mocks.AppliksModules.COMMON_EXPORTED, 'authentication-types')
                .returns('authentication-types-id');
            mocks.WRM.data.claim.withArgs('authentication-types-id').returns(AUTH_TYPE_MAP);
            mocks.Preconditions.hasValue.withArgs(AUTH_TYPE_MAP, 'authentication-types', 'Authentication Types data not found')
                .returns(AUTH_TYPE_MAP);
        }
    });

    QUnit.test('i18n getApplicationTypeName uses ApplinksProducts.getTypeName', function(assert, ApplinksI18n, mocks) {
        mocks.ApplinksProducts.getTypeName.withArgs('jira').returns('Jira');
        mocks.ApplinksProducts.getTypeName.withArgs('stash').returns('Bitbucket Server');
        mocks.ApplinksProducts.getTypeName.withArgs('foo').returns('foo');

        assert.assertThat(ApplinksI18n.getApplicationTypeName('jira'), __.is('Jira'));
        assert.assertThat(ApplinksI18n.getApplicationTypeName('stash'), __.is('Bitbucket Server'));
        assert.assertThat(ApplinksI18n.getApplicationTypeName('foo'), __.is('foo'));
    });

    QUnit.test('i18n getEntityTypeName', function(assert, ApplinksI18n) {
        assert.assertThat(ApplinksI18n.getEntityTypeName('jira.project'), __.is('Jira Project'));
        assert.assertThat(ApplinksI18n.getEntityTypeName('stash.project'), __.is('Bitbucket Server Project'));
        assert.assertThat(ApplinksI18n.getEntityTypeName('foo'), __.is('foo'));
    });

    QUnit.test('i18n getPluralizedEntityTypeName', function(assert, ApplinksI18n) {
        assert.assertThat(ApplinksI18n.getPluralizedEntityTypeName('jira.project'), __.is('Jira Projects'));
        assert.assertThat(ApplinksI18n.getPluralizedEntityTypeName('stash.project'), __.is('Bitbucket Server Projects'));
        assert.assertThat(ApplinksI18n.getPluralizedEntityTypeName('foo'), __.is('foo'));
    });

    QUnit.test('i18n getAuthenticationTypeName', function(assert, ApplinksI18n) {
        assert.assertThat(ApplinksI18n.getAuthenticationTypeName('com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider'),
            __.is('OAuth'));
        assert.assertThat(ApplinksI18n.getAuthenticationTypeName('com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider'),
            __.is('OAuth'));
        assert.assertThat(ApplinksI18n.getAuthenticationTypeName('com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider'),
            __.is('OAuth'));
        assert.assertThat(ApplinksI18n.getAuthenticationTypeName('foo'), __.is('foo'));
    });
});
