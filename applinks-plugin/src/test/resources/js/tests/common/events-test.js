define([
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-callbacks',
    'applinks/test/mock-declaration'
], function(
    QUnit,
    __,
    MockCallbacks,
    MockDeclaration
) {
    QUnit.module('applinks/common/events', {
        require: {
            main: 'applinks/common/events'
        },
        mocks: {
            jQuery: QUnit.moduleMock('applinks/lib/jquery', new MockDeclaration(['on', 'off', 'trigger']).withConstructor()),
            window: QUnit.moduleMock('applinks/lib/window', function() {return {document: 'mock document'}})
        }
    });

    QUnit.test('applinksEvent', function(assert, Events) {
        assert.assertThat(Events.applinksEvent('test'), __.equalTo('applinks.event.test'));
    });

    QUnit.test('applinksEvent with empty string', function(assert, Events) {
        var test = function() {
            Events.applinksEvent('');
        };
        assert.assertThat(test, __.throws(__.error('eventId: expected a non-empty string, was: <>')));
    });

    QUnit.test('applinksEvent with undefined', function(assert, Events) {
        var test = function() {
            Events.applinksEvent(undefined);
        };
        assert.assertThat(test, __.throws(__.error('eventId: expected a non-empty string, was: <undefined>')));
    });

    QUnit.test('on without custom context', function(assert, Events, mocks) {
        var test = this.sandbox.stub();
        Events.on('test', test);

        assert.assertThat(mocks.jQuery, __.calledOnceWith(mocks.window.document));
        assert.assertThat(mocks.jQuery.answer.on, __.calledOnceWith('test', test));
    });

    QUnit.test('on with custom context', function(assert, Events, mocks) {
        var testCallback = function() {
            this.called = true;
        };
        var testContext = {
            called: false
        };

        Events.on('test', testCallback, testContext);

        // $.on called with a wrapper
        assert.assertThat(mocks.jQuery, __.calledOnceWith(mocks.window.document));
        assert.assertThat(mocks.jQuery.answer.on, __.calledOnceWith('test', __.allOf(
            __.func(),
            __.not(__.equalTo(testCallback))
        )));

        MockCallbacks.runEventCallback(assert, mocks.jQuery.answer, 'test');
        assert.assertThat(testContext.called, __.truthy());
    });

    QUnit.test('off', function(assert, Events, mocks) {
        var test = this.sandbox.stub();
        Events.off('test', test);

        assert.assertThat(mocks.jQuery, __.calledOnceWith(mocks.window.document));
        assert.assertThat(mocks.jQuery.answer.off, __.calledOnceWith('test', test));
    });

    QUnit.test('off', function(assert, Events, mocks) {
        var data = {foo: 'foo', bar: 'bar'};
        Events.trigger('test', data);

        assert.assertThat(mocks.jQuery, __.calledOnceWith(mocks.window.document));
        assert.assertThat(mocks.jQuery.answer.trigger, __.calledOnceWith('test', data));
    });
});