define([
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration'
], function(
    QUnit,
    __,
    MockDeclaration
) {
    QUnit.module('applinks/common/rest-request', {
        require: {
            main: 'applinks/common/rest-request',
            jquery: 'applinks/lib/jquery',
            ResponseStatus: 'applinks/common/response-status'
        },
        mocks: {
            ResponseHandlers: QUnit.moduleMock('applinks/common/response-handlers', new MockDeclaration(['done', 'fail']))
        },
        beforeEach: function(assert, ApplinksRestRequest, mocks) {
            var ajax = this.sandbox.stub(this.jquery, 'ajax');
            ajax.responsePromise = new MockDeclaration(['done', 'fail']).createMock(this.sandbox);
            ajax.responsePromise.done.returns(ajax.responsePromise);
            ajax.responsePromise.fail.returns(ajax.responsePromise);
            ajax.returns(ajax.responsePromise);
            this.mocks.main = ajax;

            mocks.ResponseHandlers.doneCallback = this.sandbox.stub();
            mocks.ResponseHandlers.failCallback = this.sandbox.stub();
            mocks.ResponseHandlers.done.returns(mocks.ResponseHandlers.doneCallback);
            mocks.ResponseHandlers.fail.returns(mocks.ResponseHandlers.failCallback);
        }
    });

    QUnit.test('simple GET', function(assert, ApplinksRestRequest, mockAjax) {
        var result = new ApplinksRestRequest('/some/url').get();

        assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
        var ajaxSettings = mockAjax.firstCall.args[0];
        assert.assertThat(ajaxSettings, __.hasProperties({
            url: '/some/url',
            type: 'GET',
            contentType: 'application/json'
        }));
    });

    QUnit.test('simple DELETE', function(assert, ApplinksRestRequest, mockAjax) {
        var result = new ApplinksRestRequest('/some/url').del();

        assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
        var ajaxSettings = mockAjax.firstCall.args[0];
        assert.assertThat(ajaxSettings, __.hasProperties({
            url: '/some/url',
            type: 'DELETE',
            contentType: 'application/json'
        }));
        assert.assertThat(ajaxSettings, __.not(__.hasProperty('data')))
    });

    QUnit.test('simple POST without data', function(assert, ApplinksRestRequest, mockAjax) {
        var result = new ApplinksRestRequest('/some/url').post();

        assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
        var ajaxSettings = mockAjax.firstCall.args[0];
        assert.assertThat(ajaxSettings, __.hasProperties({
            url: '/some/url',
            type: 'POST',
            contentType: 'application/json'
        }));
        assert.assertThat(ajaxSettings, __.not(__.hasProperty('data')))
    });

    QUnit.test('simple POST with data', function(assert, ApplinksRestRequest, mockAjax) {
        var someData = {
            foo: 'foo',
            bar: 'bar'
        };
        var result = new ApplinksRestRequest('/some/url').post(someData);

        assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
        var ajaxSettings = mockAjax.firstCall.args[0];
        assert.assertThat(ajaxSettings, __.hasProperties({
            url: '/some/url',
            type: 'POST',
            contentType: 'application/json',
            data: __.equalTo(someData)
        }));
    });

    QUnit.test('simple PUT without data', function(assert, ApplinksRestRequest, mockAjax) {
        var result = new ApplinksRestRequest('/some/url').put();

        assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
        var ajaxSettings = mockAjax.firstCall.args[0];
        assert.assertThat(ajaxSettings, __.hasProperties({
            url: '/some/url',
            type: 'PUT',
            contentType: 'application/json'
        }));
        assert.assertThat(ajaxSettings, __.not(__.hasProperty('data')))
    });

    QUnit.test('simple PUT with data', function(assert, ApplinksRestRequest, mockAjax) {
        var someData = {
            foo: 'foo',
            bar: 'bar'
        };
        var result = new ApplinksRestRequest('/some/url').put(someData);

        assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
        var ajaxSettings = mockAjax.firstCall.args[0];
        assert.assertThat(ajaxSettings, __.hasProperties({
            url: '/some/url',
            type: 'PUT',
            contentType: 'application/json',
            data: __.equalTo(someData)
        }));
    });

    QUnit.test('GET with query parameters using queryParam', function(assert, ApplinksRestRequest, mockAjax) {
        var result = new ApplinksRestRequest('/some/url')
            .queryParam('key', 'value')
            .queryParam('key2', 'value2')
            .get();

        assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
        var ajaxSettings = mockAjax.firstCall.args[0];
        assertUrlWithQueryParams(assert, ajaxSettings.url, '/some/url', {
            key: 'value',
            key2: 'value2'
        });
        assert.assertThat(ajaxSettings, __.hasProperties({
            type: 'GET',
            contentType: 'application/json'
        }));
    });

    QUnit.test('POST with data and query parameters using queryParam', function(assert, ApplinksRestRequest, mockAjax) {
        var someData = {
            foo: 'foo',
            bar: 'bar'
        };
        var result = new ApplinksRestRequest('/some/url')
            .queryParam('key', 'value')
            .queryParam('key2', 'value2')
            .post(someData);

        assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
        var ajaxSettings = mockAjax.firstCall.args[0];
        assertUrlWithQueryParams(assert, ajaxSettings.url, '/some/url', {
            key: 'value',
            key2: 'value2'
        });
        assert.assertThat(ajaxSettings, __.hasProperties({
            type: 'POST',
            contentType: 'application/json',
            data: __.equalTo(someData)
        }));
    });

    QUnit.test('GET with multiple value query parameters using queryParam',
        function(assert, ApplinksRestRequest, mockAjax) {
            var result = new ApplinksRestRequest('/some/url')
                .queryParam('key', 'foo')
                .queryParam('key', 'bar')
                .queryParam('key2', '2foo')
                .queryParam('key2', '2bar')
                .get();

            assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
            var ajaxSettings = mockAjax.firstCall.args[0];
            assertUrlWithQueryParams(assert, ajaxSettings.url, '/some/url', {
                key: ['foo', 'bar'],
                key2: ['2foo', '2bar']
            });
            assert.assertThat(ajaxSettings, __.hasProperties({
                type: 'GET',
                contentType: 'application/json'
            }));
        });

    QUnit.test('GET with multiple value query parameters using queryParams',
        function(assert, ApplinksRestRequest, mockAjax) {
            var result = new ApplinksRestRequest('/some/url')
                .queryParam('key', 'foo')
                .queryParam('key2', '2foo')
                .queryParams({
                    key: 'bar',
                    key2: '2bar'
                })
                .get();

            assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
            var ajaxSettings = mockAjax.firstCall.args[0];
            assertUrlWithQueryParams(assert, ajaxSettings.url, '/some/url', {
                key: ['foo', 'bar'],
                key2: ['2foo', '2bar']
            });
            assert.assertThat(ajaxSettings, __.hasProperties({
                type: 'GET',
                contentType: 'application/json'
            }));
        });

    QUnit.test('GET with multiple value query parameters using queryParams array',
        function(assert, ApplinksRestRequest, mockAjax) {
            var result = new ApplinksRestRequest('/some/url')
                .queryParam('key', 'foo')
                .queryParam('key2', '2foo')
                .queryParams([{name: 'key', value: 'bar'}, {name: 'key2', value: '2bar'}])
                .get();

            assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
            var ajaxSettings = mockAjax.firstCall.args[0];
            assertUrlWithQueryParams(assert, ajaxSettings.url, '/some/url', {
                key: ['foo', 'bar'],
                key2: ['2foo', '2bar']
            });
            assert.assertThat(ajaxSettings, __.hasProperties({
                type: 'GET',
                contentType: 'application/json'
            }));
        });

    QUnit.test('GET with query parameters passed to the constructor as array',
        function(assert, ApplinksRestRequest, mockAjax) {
            var result = new ApplinksRestRequest('/some/url',
                [{name: 'key', value: 'foo'}, {name: 'key2', value: '2foo'}])
                .queryParam('key', 'bar')
                .queryParam('key2', '2bar')
                .queryParam('key3', '3foo')
                .get();

            assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
            var ajaxSettings = mockAjax.firstCall.args[0];
            assertUrlWithQueryParams(assert, ajaxSettings.url, '/some/url', {
                key: ['foo', 'bar'],
                key2: ['2foo', '2bar'],
                key3: '3foo'
            });
            assert.assertThat(ajaxSettings, __.hasProperties({
                type: 'GET',
                contentType: 'application/json'
            }));
        });

    QUnit.test('GET with query parameters passed to the constructor as object',
        function(assert, ApplinksRestRequest, mockAjax) {
            var result = new ApplinksRestRequest('/some/url', {key: 'foo', key2: '2foo'})
                .queryParam('key', 'bar')
                .queryParam('key2', '2bar')
                .queryParam('key3', '3foo')
                .get();

            assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
            var ajaxSettings = mockAjax.firstCall.args[0];
            assertUrlWithQueryParams(assert, ajaxSettings.url, '/some/url', {
                key: ['foo', 'bar'],
                key2: ['2foo', '2bar'],
                key3: '3foo'
            });
            assert.assertThat(ajaxSettings, __.hasProperties({
                type: 'GET',
                contentType: 'application/json'
            }));
        });

    QUnit.test('Invalid request no URL', function(assert, ApplinksRestRequest) {
        var invalidConstructor = function() {
            new ApplinksRestRequest();
        };
        assert.assertThat(invalidConstructor, __.throws(__.allOf(
            __.instanceOf(Error),
            __.hasProperty('message', 'url: expected a non-empty string, was: <undefined>')
        )));
    });

    QUnit.test('Invalid request empty URL', function(assert, ApplinksRestRequest) {
        var invalidConstructor = function() {
            new ApplinksRestRequest('');
        };
        assert.assertThat(invalidConstructor, __.throws(__.allOf(
            __.instanceOf(Error),
            __.hasProperty('message', 'url: expected a non-empty string, was: <>')
        )));
    });

    QUnit.test('Invalid request URL with #', function(assert, ApplinksRestRequest) {
        var invalidConstructor = function() {
            new ApplinksRestRequest('http://test.com#something');
        };
        assert.assertThat(invalidConstructor, __.throws(__.allOf(
            __.instanceOf(Error),
            __.hasProperty('message', 'url: "#" fragments not supported')
        )));
    });

    QUnit.test('GET with default expected status', function(assert, ApplinksRestRequest, mockAjax, mocks) {
        var result = new ApplinksRestRequest('/some/url').get();

        assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
        assert.assertThat(mockAjax.responsePromise.done, __.calledOnceWith(mocks.ResponseHandlers.doneCallback));
        assert.assertThat(mockAjax.responsePromise.fail, __.calledOnceWith(mocks.ResponseHandlers.failCallback));
        assert.assertThat(mocks.ResponseHandlers.done, __.calledOnceWith(__.contains(this.ResponseStatus.Family.SUCCESSFUL)));
        assert.assertThat(mocks.ResponseHandlers.fail, __.calledOnceWith(__.contains(this.ResponseStatus.Family.SUCCESSFUL)));
    });

    QUnit.test('GET with custom expected status', function(assert, ApplinksRestRequest, mockAjax, mocks) {
        var result = new ApplinksRestRequest('/some/url')
            .expectStatus(this.ResponseStatus.Family.REDIRECTION, this.ResponseStatus.SERVER_ERROR)
            .get();

        var expectedStatusMatcher = __.contains(this.ResponseStatus.Family.REDIRECTION,
            this.ResponseStatus.SERVER_ERROR);
        assert.assertThat(result, __.equalTo(mockAjax.responsePromise));
        assert.assertThat(mockAjax.responsePromise.done, __.calledOnceWith(mocks.ResponseHandlers.doneCallback));
        assert.assertThat(mockAjax.responsePromise.fail, __.calledOnceWith(mocks.ResponseHandlers.failCallback));
        assert.assertThat(mocks.ResponseHandlers.done, __.calledOnceWith(expectedStatusMatcher));
        assert.assertThat(mocks.ResponseHandlers.fail, __.calledOnceWith(expectedStatusMatcher));
    });


    function assertUrlWithQueryParams(assert, actual, expectedUrl, expectedParams) {
        assert.assertThat(actual, __.containsString('?'));
        var parts = actual.split('?');
        var queryParamMatchers = _.chain(expectedParams).map(function(value, key) {
            var values = _.isArray(value) ?  value : [value];
            return _.map(values, function(paramValue) {
                return __.containsString(key + '=' + paramValue)
            });
        }).flatten().value();

        assert.assertThat(parts[0], __.equalTo(expectedUrl));
        assert.assertThat(parts[1], __.allOf(queryParamMatchers));
    }
});