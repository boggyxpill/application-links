define([
    'applinks/lib/jquery',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/test/mock-callbacks'
], function(
    $,
    QUnit,
    __,
    MockDeclaration,
    MockCallbacks
) {

    QUnit.module('applinks/common/features', {
        require: {
            main: 'applinks/component/feature-discovery'
        },
        mocks: {
            ApplinksFeatures: QUnit.moduleMock('applinks/common/features', new MockDeclaration(['isDiscovered', 'discover'])),
            InlineDialog: QUnit.moduleMock('applinks/component/inline-dialog', new MockDeclaration(['realign', 'hide', 'isVisible']).withConstructor())
        },
        beforeEach: function(assert, Initializer, mocks) {
            mocks.ApplinksFeatures.isDiscovered.withArgs('BITBUCKET_REBRAND').returns(false);
            mocks.ApplinksFeatures.isDiscovered.withArgs('BITBUCKET_DISCOVERED').returns(true);

            mocks.InlineDialog.answer.isVisible.returns(true);
        }
    });

    QUnit.test('Constructing a FeatureDiscovery, feature discovered', function(assert, FeatureDiscovery, mocks) {
        var featureKey = 'BITBUCKET_DISCOVERED';
        var contents = 'test-contents';
        var anchor = $('<a>');

        var fd = new FeatureDiscovery(featureKey, contents, anchor);

        assert.assertThat(fd.featureId, __.is(featureKey));
        assert.assertThat(mocks.ApplinksFeatures.isDiscovered.calledWith(featureKey), __.is(true));
        assert.assertThat(mocks.ApplinksFeatures.discover.notCalled, __.is(true));

        assert.assertThat(fd.contents, __.is(contents));
        assert.assertThat(fd.anchor, __.is(anchor));
        assert.assertThat(fd.popupId, __.is('feature-discovery-' + featureKey));

        assert.assertThat(fd.dialog, __.is(__.undefined()));
    });

    QUnit.test('Constructing a FeatureDiscovery, feature not discovered', function(assert, FeatureDiscovery, mocks) {
        var featureKey = 'BITBUCKET_REBRAND';
        var contents = 'test-contents';
        var anchor = $('<a>');
        var ariaLabel = "Some aria label";

        var fd = new FeatureDiscovery(featureKey, contents, anchor, ariaLabel);

        assert.assertThat(fd.featureId, __.is(featureKey));
        assert.assertThat(mocks.ApplinksFeatures.isDiscovered.calledWith(featureKey), __.is(true));
        assert.assertThat(mocks.ApplinksFeatures.discover.calledWith(featureKey), __.is(true));

        assert.assertThat(fd.contents, __.is(contents));
        assert.assertThat(fd.anchor, __.is(anchor));
        assert.assertThat(fd.popupId, __.is('feature-discovery-' + featureKey));
        assert.assertThat(fd.ariaLabel, __.is(ariaLabel));

        assert.assertThat(mocks.InlineDialog.calledWith(fd.popupId, contents, {
            alignment: 'bottom center',
            open: true,
            persistent: true,
            ariaLabel: ariaLabel,
        }), __.is(true));

        assert.assertThat(mocks.InlineDialog.args[0][2], __.hasProperties({
            alignment: __.is(__.string()),
            open: __.is(__.bool()),
            persistent: __.is(__.bool()),
            ariaLabel: __.is(__.string()),
        }));

        assert.assertThat(fd.dialog, __.is(__.not(__.undefined())));
        assert.assertThat(fd.dialog.realign.calledOnce, __.is(true));
    });

    QUnit.test('Dismissing dialog, feature discovered', function(assert, FeatureDiscovery, mocks) {
        var featureKey = 'BITBUCKET_DISCOVERED';
        var contents = 'test-contents';
        var anchor = $('<a>');

        var fd = new FeatureDiscovery(featureKey, contents, anchor);
        fd.dismiss()

        // Since the dialog is undefined, it won't be possible to call the .isVisible() or .hide() methods.
        // This test ensures that the .dismiss() method includes checks to ensure it doesn't try to
        // invoke methods on undefined.
        assert.assertThat(fd.dialog, __.is(__.undefined()));
    });

    QUnit.test('Dismissing dialog, feature not discovered', function(assert, FeatureDiscovery, mocks) {
        var featureKey = 'BITBUCKET_REBRAND';
        var contents = 'test-contents';
        var anchor = $('<a>');

        var fd = new FeatureDiscovery(featureKey, contents, anchor);
        fd.dismiss()

        assert.assertThat(fd.dialog.isVisible.calledOnce, __.is(true));
        assert.assertThat(fd.dialog.hide.calledOnce, __.is(true));
    });
});
