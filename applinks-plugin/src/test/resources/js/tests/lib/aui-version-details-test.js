define([
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest'
], function(
    _,
    QUnit,
    __
) {
    QUnit.module('applinks/lib/aui-version-details', {
        require: {
            main: 'applinks/lib/aui-version-details',
            Version: 'applinks/lib/version'
        }
    });

    QUnit.test('AUI 5.7', function(assert, AuiVersionDetails) {
        var AJS  = {version: '5.7.15'};
        assert.assertThat(_.partial(AuiVersionDetails.addVersionDetails, AJS), __.throws(__.error(
            __.stringContainsInOrder('AUI version 5.7.15 is too low, you need to upgrade AUI ', 'for Applinks to work'))));
    });

    QUnit.test('AUI 5.8.14', function(assert, AuiVersionDetails) {
        var AJS  = {version: '5.8.14'};
        assert.assertThat(_.partial(AuiVersionDetails.addVersionDetails, AJS), __.throws(__.error(
            __.stringContainsInOrder('AUI version 5.8.14 is too low, you need to upgrade AUI ', 'for Applinks to work'))));
    });

    QUnit.test('AUI 5.9.12', function(assert, AuiVersionDetails) {
        var AJS  = {version: '5.9.12'};
        assert.assertThat(_.partial(AuiVersionDetails.addVersionDetails, AJS), __.throws(__.error(
            __.stringContainsInOrder('AUI version 5.9.12 is too low, you need to upgrade AUI ', 'for Applinks to work'))));
    });

    QUnit.test('AUI 5.9.0', function(assert, AuiVersionDetails) {
        var AJS  = {version: '5.9.0'};
        assert.assertThat(_.partial(AuiVersionDetails.addVersionDetails, AJS), __.throws(__.error(
            __.stringContainsInOrder('AUI version 5.9.0 is too low, you need to upgrade AUI ', 'for Applinks to work'))));
    });

    QUnit.test('AUI 5.8.15', function(assert, AuiVersionDetails) {
        var AJS  = {version: '5.8.15'};
        assert.assertThat(_.partial(AuiVersionDetails.addVersionDetails, AJS), __.throws(__.error(
            __.stringContainsInOrder('AUI version 5.8.15 is too low, you need to upgrade AUI ', 'for Applinks to work'))));
    });

    QUnit.test('AUI 5.9.13', function(assert, AuiVersionDetails) {
        var AJS  = {version: '5.9.13'};
        AuiVersionDetails.addVersionDetails(AJS);

        assert.assertThat(AJS.versionDetails, __.defined());
        assert.assertThat(AJS.versionDetails.equals(new this.Version(5, 9, 13)), __.truthy());
        assert.assertThat(AJS.versionDetails.is58, __.falsy());
        assert.assertThat(AJS.versionDetails.is59, __.truthy());
    });

    QUnit.test('AUI 6.0.0', function(assert, AuiVersionDetails) {
        var AJS  = {version: '6.0.0'};
        AuiVersionDetails.addVersionDetails(AJS);

        assert.assertThat(AJS.versionDetails, __.defined());
        assert.assertThat(AJS.versionDetails.equals(new this.Version(6, 0, 0)), __.truthy());
        assert.assertThat(AJS.versionDetails.is58, __.falsy());
        assert.assertThat(AJS.versionDetails.is59, __.falsy());
    });
});
