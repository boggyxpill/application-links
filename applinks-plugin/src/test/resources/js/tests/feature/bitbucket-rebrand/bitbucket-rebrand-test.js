define([
    'applinks/lib/jquery',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/test/mock-callbacks'
], function(
    $,
    QUnit,
    __,
    MockDeclaration,
    MockCallbacks
) {
    var FeatureConstants = {
        BITBUCKET_REBRAND: 'BITBUCKET_REBRAND'
    };
    var EventConstants = {
        APPLINKS_LOADED: 'applinks.event.loaded',
        APPLINKS_UPDATED: 'applinks.event.updated',
        V3_ONBOARDING_FINISHED: 'applinks.event.v3-onboarding-finished',

        Legacy: {
            MESSAGE_BOX_DISPLAYED: 'applinks.event.messagebox'
        }
    };

    QUnit.module('applinks/feature/bitbucket-rebrand', {
        require: {
            main: 'applinks/feature/bitbucket-rebrand'
        },

        mocks: {
            main: QUnit.moduleMock('applinks/common/features',
                new MockDeclaration(['isDiscovered', 'V3_UI', 'discover', 'isEnabled'], FeatureConstants)),
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['trigger', 'I18n.getText'])),
            ApplinksEvents: QUnit.moduleMock('applinks/common/events', new MockDeclaration(['on'], EventConstants)),
            ApplinksContext: QUnit.moduleMock('applinks/common/context', new MockDeclaration(['hostApplication'])),
            ApplinksProducts: QUnit.moduleMock('applinks/common/products', new MockDeclaration(['BITBUCKET'])),
            FeatureDiscovery: QUnit.moduleMock('applinks/component/feature-discovery',
                new MockDeclaration(['dismissTrigger', 'dialog.contents']).withConstructor()),
            ApplinksCreation: QUnit.moduleMock('applinks/feature/creation',
                new MockDeclaration(['isInProgress', 'getSubmittedStatusLog'])),
            V3Ui: QUnit.moduleMock('applinks/feature/v3/ui', new MockDeclaration(['isOn', 'isFinishedOnboarding'])),
            jQueryElement: QUnit.mock(new MockDeclaration(['focus', 'find']))
        },

        templates: {
            "applinks.feature.bitbucket.rebrand.featureDiscovery": function() {
                return '<div class="bb-rebrand-dialog"/>';
            }
        },

        beforeEach: function (assert, BitbucketRebrand, ApplinksFeatures, mocks) {
            mocks.ApplinksCreation.getSubmittedStatusLog.returns([]);
            mocks.ApplinksContext.hostApplication.returns({type: 'JIRA'});

            mocks.jQueryElement.find.returns(mocks.jQueryElement);
            mocks.FeatureDiscovery.answer.dialog.contents.returns(mocks.jQueryElement);

            mocks.AJS.I18n.getText.withArgs('applinks.inline_dialog.feature_discovery.stash_renamed.aria_label').returns('AriaLabelText');
        }

    });

    QUnit.test("Bitbucket Rebrand should fire in V3 UI with onboarding already finished",
        function(assert, BitbucketRebrand, ApplinksFeatures, mocks) {
            appendV3UiBbRow(this.fixture);

            ApplinksFeatures.isDiscovered.withArgs(ApplinksFeatures.BITBUCKET_REBRAND).returns(false);
            ApplinksFeatures.isEnabled.withArgs(ApplinksFeatures.BITBUCKET_REBRAND).returns(true);

            mocks.V3Ui.isOn.returns(true);
            mocks.V3Ui.isFinishedOnboarding.returns(true);

            BitbucketRebrand.init();

            // Onboarding finished -> BB rebrand should pop up straight on "Applinks Loaded"
            MockCallbacks.runEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.APPLINKS_LOADED);
            assert.assertThat(mocks.FeatureDiscovery, __.calledOnce());
            assert.assertThat(mocks.FeatureDiscovery.firstCall.args[3], __.is("AriaLabelText"));
            MockCallbacks.assertEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.Legacy.MESSAGE_BOX_DISPLAYED);
            MockCallbacks.assertEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.APPLINKS_UPDATED);
        });

    QUnit.test("Bitbucket Rebrand should fire when V3 UI, after onboarding is finished",
        function(assert, BitbucketRebrand, ApplinksFeatures, mocks) {
            appendV3UiBbRow(this.fixture);

            ApplinksFeatures.isDiscovered.withArgs(ApplinksFeatures.BITBUCKET_REBRAND).returns(false);
            ApplinksFeatures.isEnabled.withArgs(ApplinksFeatures.BITBUCKET_REBRAND).returns(true);

            mocks.V3Ui.isOn.returns(true);
            mocks.V3Ui.isFinishedOnboarding.returns(false);

            BitbucketRebrand.init();

            // Onboarding in progress -> BB rebrand should pop up on "Onboarding Finished" event
            MockCallbacks.runEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.APPLINKS_LOADED);
            assert.assertThat(mocks.FeatureDiscovery, __.notCalled());
            MockCallbacks.runEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.V3_ONBOARDING_FINISHED);
            assert.assertThat(mocks.FeatureDiscovery, __.calledOnce());
            assert.assertThat(mocks.FeatureDiscovery.firstCall.args[3], __.is("AriaLabelText"));
            MockCallbacks.assertEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.Legacy.MESSAGE_BOX_DISPLAYED);
            MockCallbacks.assertEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.APPLINKS_UPDATED);
        });

    QUnit.test("Bitbucket Rebrand should only initialize once in V3 UI on Applinks Loaded event",
        function(assert, BitbucketRebrand, ApplinksFeatures, mocks) {
            appendV3UiBbRow(this.fixture);

            ApplinksFeatures.isDiscovered.withArgs(ApplinksFeatures.BITBUCKET_REBRAND).returns(false);
            ApplinksFeatures.isEnabled.withArgs(ApplinksFeatures.BITBUCKET_REBRAND).returns(true);

            mocks.V3Ui.isOn.returns(true);
            mocks.V3Ui.isFinishedOnboarding.returns(true);

            BitbucketRebrand.init();

            // simulate "Applinks Loaded" raised twice - BB rebrand should only be initialized once
            MockCallbacks.runEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.APPLINKS_LOADED);
            MockCallbacks.runEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.APPLINKS_LOADED);
            assert.assertThat(mocks.FeatureDiscovery, __.calledOnce());
            assert.assertThat(mocks.FeatureDiscovery.firstCall.args[3], __.is("AriaLabelText"));
        });

    QUnit.test("Bitbucket Rebrand should only initialize once in V3 UI on Onboarding Finished event",
        function(assert, BitbucketRebrand, ApplinksFeatures, mocks) {
            appendV3UiBbRow(this.fixture);

            ApplinksFeatures.isDiscovered.withArgs(ApplinksFeatures.BITBUCKET_REBRAND).returns(false);
            ApplinksFeatures.isEnabled.withArgs(ApplinksFeatures.BITBUCKET_REBRAND).returns(true);

            mocks.V3Ui.isOn.returns(true);
            mocks.V3Ui.isFinishedOnboarding.returns(false);

            BitbucketRebrand.init();

            // simulate "Onboarding Finished" raised twice - BB rebrand should only be initialized once
            MockCallbacks.runEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.APPLINKS_LOADED);
            MockCallbacks.runEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.V3_ONBOARDING_FINISHED);
            MockCallbacks.runEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.V3_ONBOARDING_FINISHED);
            assert.assertThat(mocks.FeatureDiscovery, __.calledOnce());
            assert.assertThat(mocks.FeatureDiscovery.firstCall.args[3], __.is("AriaLabelText"));
        });


    QUnit.test("Bitbucket Rebrand should fire in V2 UI regardless of onboarding",
        function(assert, BitbucketRebrand, ApplinksFeatures, mocks) {
            appendV2UiBbRow(this.fixture);

            ApplinksFeatures.isDiscovered.withArgs(ApplinksFeatures.BITBUCKET_REBRAND).returns(false);
            ApplinksFeatures.isEnabled.withArgs(ApplinksFeatures.BITBUCKET_REBRAND).returns(true);

            // V2 UI, onboarding "not finished" because there's no onboarding in the V2 UI
            mocks.V3Ui.isOn.returns(false);
            mocks.V3Ui.isFinishedOnboarding.returns(false);

            BitbucketRebrand.init();

            // BB rebrand should pop up straight on "Applinks Loaded"
            MockCallbacks.runEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.APPLINKS_LOADED);
            assert.assertThat(mocks.FeatureDiscovery, __.calledOnce());
            assert.assertThat(mocks.FeatureDiscovery.firstCall.args[3], __.is("AriaLabelText"));
            MockCallbacks.assertEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.Legacy.MESSAGE_BOX_DISPLAYED);
            MockCallbacks.assertEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.APPLINKS_UPDATED);
        });

    QUnit.test("Bitbucket Rebrand should only initialize once in V2 UI",
        function(assert, BitbucketRebrand, ApplinksFeatures, mocks) {
            appendV2UiBbRow(this.fixture);

            ApplinksFeatures.isDiscovered.withArgs(ApplinksFeatures.BITBUCKET_REBRAND).returns(false);
            ApplinksFeatures.isEnabled.withArgs(ApplinksFeatures.BITBUCKET_REBRAND).returns(true);

            mocks.V3Ui.isOn.returns(false);
            mocks.V3Ui.isFinishedOnboarding.returns(false);

            BitbucketRebrand.init();

            // simulate "Applinks Loaded" raised twice - BB rebrand should only be initialized once
            MockCallbacks.runEventCallback(assert, mocks.ApplinksEvents, mocks.ApplinksEvents.APPLINKS_LOADED);
            assert.assertThat(mocks.FeatureDiscovery, __.calledOnce());
            assert.assertThat(mocks.FeatureDiscovery.firstCall.args[3], __.is("AriaLabelText"));
        });

    function appendV3UiBbRow(fixture) {
        var v3Table = $('<table id="agent-table">')
            .append($('<tr/>')
                .append('<span class="applinks-type">Bitbucket</span>'));
        fixture.append(v3Table);
    }

    function appendV2UiBbRow(fixture) {
        var v2Table = $('<table id="application-links-table">')
            .append($('<tr class="ual-row"/>')
                .append('<td class="application-type">Bitbucket</td>'));
        fixture.append(v2Table);
    }
});

