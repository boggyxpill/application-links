define([
    'applinks/lib/jquery',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/common/events'
], function(
    $,
    QUnit,
    __,
    MockDeclaration,
    ApplinksEvents
) {
    var onboardingFinished = false;
    var oldAjax;

    QUnit.module('applinks/feature/v3/onboarding', {
        require: {
            main: 'applinks/feature/v3/onboarding'
        },
        mocks: {
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['trigger'])),
            ApplinksFeatures: QUnit.moduleMock('applinks/common/features', new MockDeclaration(['isDiscovered', 'V3_UI', 'discover'])),
            ApplinksCreation: QUnit.moduleMock('applinks/feature/creation', new MockDeclaration(['isInProgress'])),
            V3Ui: QUnit.moduleMock( 'applinks/feature/v3/ui', new MockDeclaration(['isOn','isFinishedOnboarding']))

        },
        templates: {
            "applinks.feature.onboarding.v3.onboardingDialog": function() {
                return '<div class="onboarding-dialog"/>';
            }
        },

        beforeEach: function(assert, V3Onboarding, mocks) {
            onboardingFinished = false;
            oldAjax = $.ajax;
            $.ajax = function(settings) {
                settings.success();
            };

            mocks.AJS.dialog2 = function(divElement){
                return {show: function (){}, hide: function(){}}
            };
            mockDocument = $('<div id="mock-document"><id="v3-ui-feature-discovery-dialog"/><button id="v3-onboarding-dialog-close-button"></button></button></div></div>');
            this.fixture.append(mockDocument);

            $('body').removeAttr('data-v3-onboarding-dialog-finished');

            $(document).bind(ApplinksEvents.V3_ONBOARDING_FINISHED, function(){
                onboardingFinished = true;
            });
        },

        afterEach: function() {
            $(document).unbind(ApplinksEvents.V3_ONBOARDING_FINISHED);
            $(document).unbind(ApplinksEvents.READY);
            $.ajax = oldAjax;
        }
    });

    QUnit.test('Only initialises when Applinks is ready', function (assert, V3Onboarding, mocks) {
        mocks.ApplinksFeatures.isDiscovered.returns(false);
        mocks.ApplinksCreation.isInProgress.returns(false);
        mocks.V3Ui.isOn.returns(true);
        V3Onboarding.init();
        assert.assertThat($('body').attr('data-v3-onboarding-dialog-code-loaded'), __.is(undefined));
        $(document).trigger(ApplinksEvents.READY);
        assert.assertThat($('body').attr('data-v3-onboarding-dialog-code-loaded'), __.is(__.not(undefined)));
    });

    QUnit.test('When V3 UI already discovered Applinks Loaded event is fired', function(assert, V3Onboarding, mocks) {
        mocks.ApplinksFeatures.isDiscovered.returns(true);
        mocks.V3Ui.isOn.returns(true);
        V3Onboarding.init();
        $(document).trigger(ApplinksEvents.READY);
        assert.assertThat(onboardingFinished, __.is(true));
        assert.assertThat($('body').attr('data-v3-onboarding-dialog-finished'), __.is("finished"));
    });

    QUnit.test('When V3 UI onboarding dialog dismissed Onboarding finished event is fired', function(assert, V3Onboarding, mocks) {
        mocks.ApplinksFeatures.isDiscovered.returns(false);
        mocks.ApplinksCreation.isInProgress.returns(false);
        mocks.V3Ui.isOn.returns(true);
        V3Onboarding.init();
        $(document).trigger(ApplinksEvents.READY);
        $('#v3-onboarding-dialog-close-button').click();
        assert.assertThat(onboardingFinished, __.is(true));
        assert.assertThat($('body').attr('data-v3-onboarding-dialog-finished'), __.is("finished"));
        assert.assertThat($('body').attr('data-v3-onboarding-dialog-code-loaded'), __.is(__.not(undefined)));
    });

    QUnit.test('When V3 UI onboarding dialog is not dismissed Onboarding finished event is not fired', function(assert, V3Onboarding, mocks) {
        mocks.ApplinksFeatures.isDiscovered.returns(false);
        mocks.ApplinksCreation.isInProgress.returns(false);
        mocks.V3Ui.isOn.returns(true);
        V3Onboarding.init();
        $(document).trigger(ApplinksEvents.READY);
        assert.assertThat(onboardingFinished, __.is(false));
        assert.assertThat($('body').attr('data-v3-onboarding-dialog-finished'), __.falsey());
        assert.assertThat($('body').attr('data-v3-onboarding-dialog-code-loaded'), __.is(__.not(undefined)));
    });
});

