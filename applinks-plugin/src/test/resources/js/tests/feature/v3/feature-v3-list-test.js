define([
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-callbacks',
    'applinks/test/mock-declaration',
    'applinks/test/mock-common',
    'applinks/test/mock-model'
], function(
    $,
    _,
    QUnit,
    __,
    MockCallbacks,
    MockDeclaration,
    ApplinksCommonMocks,
    ApplinksModelMocks
) {
    QUnit.module('applinks/feature/v3/list', {
        require: {
            main: 'applinks/feature/v3/list',
            ResponseStatus: 'applinks/common/response-status',
            jQuery: 'applinks/lib/jquery',
            ApplinkModel: 'applinks/model/applink'
        },
        mocks: {
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['I18n.getText'])),
            ApplinksRest: ApplinksCommonMocks.mockRestModule(QUnit),
            ApplinkStatus: QUnit.moduleMock('applinks/feature/status', new MockDeclaration(['View'])),
            ApplinkStatusDetails: QUnit.moduleMock('applinks/feature/status/details',
                new MockDeclaration(['Dialog', 'removeDialog'])),
            Events: ApplinksCommonMocks.mockEventsModule(QUnit),
            LegacyApplinks: QUnit.moduleMock('applinks/shim/applinks',
                new MockDeclaration(['UI.hideInfoBox', 'editAppLink'])),
            OAuthDance: QUnit.moduleMock('applinks/feature/oauth-dance', new MockDeclaration(['initialize', 'onSuccess'],
                {AUTHENTICATE_LINK_SELECTOR: '.auth-link'}).withConstructor()),
            V3ListViews: QUnit.moduleMock('applinks/feature/v3/list-views', new MockDeclaration(['ConfirmDialog'])),
            window: QUnit.moduleMock('applinks/lib/window', new MockDeclaration(['open'])),
            // mocks for ApplinkModel
            ResponseHandlers: QUnit.moduleMock('applinks/common/response-handlers',
                new MockDeclaration(['done', 'fail']))
        },
        templates: {
            "applinks.feature.v3.list.templates.rowStatic": function(params) {
                return '<tr class="static"><td>@content</td></tr>'.replace(/@content/g, params.content);
            },
            "applinks.feature.v3.list.templates.table": function() {
                return '<table id="agent-table" hidden=""><tbody></tbody></table><div id="v3-no-applinks-notice" hidden=""></div>';
            }
        },

        beforeEach: function (assert, V3ApplinkList, mocks) {
            new ApplinksCommonMocks.Stubber(this).setUpRest(mocks.ApplinksRest);

            setUpStatusDetails(this);
            setUpOAuthDance(this);
            setUpRowView(mocks.V3ListViews, this.sandbox);

            // stub jQuery plugins, those will be removed when sandbox is destroyed
            this.jQuery.fn.tooltip = this.sandbox.stub();
            this.jQuery.fn.spin = this.sandbox.stub();

            // use ApplinkModel as main mock
            this.mocks.main = this.ApplinkModel;
        },

        afterEach: function() {
            // remove the Backbone view if created during the test
            this.testView && this.testView.remove();
        }
    });

    QUnit.test('Render applinks list with multiple applinks', function(assert, V3ApplinkList, ApplinkModel, mocks) {
        var applink1 = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '123'})),
            applink2 = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '456'})),
            applink3 = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '789'})),
            applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection());
        applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([applink1, applink2, applink3]);

        this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection});

        assert.assertThat(mocks.V3ListViews.Row, __.calledWithAt(0, __.hasProperty('model', applink1)));
        assert.assertThat(mocks.V3ListViews.Row, __.calledWithAt(1, __.hasProperty('model', applink2)));
        assert.assertThat(mocks.V3ListViews.Row, __.calledWithAt(2, __.hasProperty('model', applink3)));

        // assert stuff has been rendered
        assert.assertThat(this.fixture.find('#agent-table').length, __.equalTo(1));
        assert.assertThat(this.fixture.find('#agent-table').find('tr').length, __.equalTo(3));
    });

    QUnit.test('Render applinks list with no applinks', function(assert, V3ApplinkList, ApplinkModel, mocks) {
        var applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection());
        applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([]);

        this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection});

        // no rows to render
        assert.assertThat(mocks.V3ListViews.Row, __.notCalled());
        // should render one empty row
        var table = this.fixture.find('#agent-table');
        var noApplinksNotice = this.fixture.find('#v3-no-applinks-notice');

        assert.assertThat(table.length, __.equalTo(1));
        assert.assertThat(table.attr("hidden"), __.is(__.defined()));
        assert.assertThat(table.find('tbody').length, __.equalTo(1));
        assert.assertThat(table.find('tbody').children().length, __.equalTo(0));

        assert.assertThat(noApplinksNotice.length, __.equalTo(1));
        assert.assertThat(noApplinksNotice.attr("hidden"), __.is(__.undefined()));
    });

    QUnit.test('Render applinks fetch status, status loading', function(assert, V3ApplinkList, ApplinkModel, mocks) {
        var applink = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '123'})),
            applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection());
        applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([applink]);
        this.sandbox.stub(applink, 'fetchStatus');

        this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection});

        // fetchStatus should be called
        assert.assertThat(applink.fetchStatus, __.calledOnce());
        // status loading - ApplinkStatus should be called, but not ApplinkStatusDetails
        applink.set('statusLoaded', false);
        applink.set('status', null);
        assert.assertThat(mocks.ApplinkStatus.View, __.calledOnceWith(__.hasProperty('applink', __.hasProperties({
            statusLoaded: false, status: null
        }))));
        // dialog called regardless of statusLoaded
        assert.assertThat(mocks.ApplinkStatusDetails.Dialog, __.calledOnceWith(applink));
    });

    QUnit.test('Render applinks fetch status, status loaded', function(assert, V3ApplinkList, ApplinkModel, mocks) {
        var applink = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '123'})),
            applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection());
        applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([applink]);
        this.sandbox.stub(applink, 'fetchStatus');

        this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection});

        // fetchStatus should be called
        assert.assertThat(applink.fetchStatus, __.calledOnce());
        // status loaded - ApplinkStatus and ApplinkStatusDetails should be called
        applink.set('statusLoaded', true);
        applink.set('status', 'new status');
        assert.assertThat(mocks.ApplinkStatus.View, __.calledOnceWith(__.hasProperty('applink', __.hasProperties({
            statusLoaded: true, status: 'new status'
        }))));
        assert.assertThat(mocks.ApplinkStatusDetails.Dialog,  __.calledOnceWith(applink));
    });

    QUnit.test('Oprhaned upgrade event should re-fetch applinks', function(assert, V3ApplinkList, ApplinkModel, mocks) {
        var applink1 = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '123'})),
            applink2 = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '456'})),
            applink3 = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '789'})),
            applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection());

        // first only 2 applinks
        applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([applink1, applink2]);

        this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection});

        // 2 rows rendered initially
        assert.assertThat(mocks.V3ListViews.Row, __.calledTwice());

        // set up 3 applinks and trigger orphaned upgrade event
        applinksCollection.fetch.reset();
        mocks.V3ListViews.Row.reset();
        applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([applink1, applink2, applink3]);
        MockCallbacks.runEventCallback(assert, mocks.Events, mocks.Events.ORPHANED_UPGRADE);

        // 3 rows rendered, incl. applink3 as last row
        assert.assertThat(applinksCollection.fetch, __.calledOnce());
        assert.assertThat(mocks.V3ListViews.Row, __.calledThrice());
        assert.assertThat(mocks.V3ListViews.Row, __.calledWithAt(2, __.hasProperty('model', applink3)));
    });

    QUnit.test('Delete should expect status Successful or Not Found', function(assert, V3ApplinkList, ApplinkModel) {
        var applink = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '123'})),
            applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection([applink]));
        applink.destroy.response = ApplinksModelMocks.responseSuccess();

        this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection}).deleteApplink('123');

        assert.assertThat(applink.destroy, __.calledOnceWith(__.hasProperties({
            wait: true,
            expectedStatuses: [this.ResponseStatus.Family.SUCCESSFUL, this.ResponseStatus.NOT_FOUND]
        })));
    });

    QUnit.test('Model destroy should remove corresponding applink row',
        function(assert, V3ApplinkList, ApplinkModel, mocks) {
            var applink1 = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '123'})),
                applink2 = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '456'})),
                applink3 = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '789'})),
                applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection());
            applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([applink1, applink2, applink3]);

            this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection});
            applinksCollection.trigger('destroy', applink1);

            // applink row 123 should be removed (but other rows should remain)
            assert.assertThat(this.fixture.find('tr').length, __.equalTo(2));
            assert.assertThat(this.fixture.find('#123').length, __.equalTo(0));
            // status details dialog should also be removed
            assert.assertThat(mocks.ApplinkStatusDetails.removeDialog, __.calledOnceWith('123'));
        });

    QUnit.test('Edit for non-V3 editable applinks should trigger legacy edit',
        function(assert, V3ApplinkList, ApplinkModel, mocks) {
            var applink = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '123'})),
                applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection());
            applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([applink]);
            applink.set({type: 'jira', name: 'Test applink'});
            mocks.ApplinksRest.V1.applink.answer.getUrl.returns('v1 applink URL');

            // init V3 list, add legacy edit link and click it
            this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection});
            this.fixture.find('#123 .actions').append('<button class="legacy-edit-button"></button>');
            this.fixture.find('#123 .legacy-edit-button').click();

            // make sure legacy edit was invoked with correct params
            assert.assertThat(mocks.LegacyApplinks.editAppLink, __.calledOnceWith(__.hasProperties({
                link: __.contains(__.hasProperties({rel: 'self', href: 'v1 applink URL'})),
                typeId: 'jira',
                type: 'jira',
                name: 'Test applink',
                hasIncoming: true,
                hasOutgoing: true
            }), __.string(), __.falsy(), __.func(), __.func()));
        });

    QUnit.test('Render applinks list with system links', function(assert, V3ApplinkList, ApplinkModel, mocks) {
        var atlassianSystem1 = ApplinksModelMocks.setUpModel(this,
            new ApplinkModel.Applink({id: '0', system: true, data: {atlassian: true}}));
        var atlassianNonSystem1 = ApplinksModelMocks.setUpModel(this,
            new ApplinkModel.Applink({id: '1', system: false, data: {atlassian: true}}));
        var nonAtlassianNonSystem = ApplinksModelMocks.setUpModel(this,
            new ApplinkModel.Applink({id: '2', system: false, data: {atlassian: false}}));
        var nonAtlassianSystem1 = ApplinksModelMocks.setUpModel(this,
            new ApplinkModel.Applink({id: '3', system: true, data: {atlassian: false}}));
        var atlassianNonSystem2 = ApplinksModelMocks.setUpModel(this,
            new ApplinkModel.Applink({id: '4', system: false, data: {atlassian: true}}));
        var nonAtlassianSystem2 = ApplinksModelMocks.setUpModel(this,
            new ApplinkModel.Applink({id: '5', system: true, data: {atlassian: false}}));
        var atlassianSystem2 = ApplinksModelMocks.setUpModel(this,
            new ApplinkModel.Applink({id: '6', system: true, data: {atlassian: true}}));

        var applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection());
        applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([
            atlassianSystem1,
            atlassianNonSystem1,
            nonAtlassianNonSystem,
            nonAtlassianSystem1,
            atlassianNonSystem2,
            nonAtlassianSystem2,
            atlassianSystem2
        ]);

        this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection});

        // filter out non-Atlassian system links, Atlassian system links render last, otherwise order preserved
        assert.assertThat(mocks.V3ListViews.Row, __.calledWithAt(0, __.hasProperty('model', atlassianNonSystem1)));
        assert.assertThat(mocks.V3ListViews.Row, __.calledWithAt(1, __.hasProperty('model', nonAtlassianNonSystem)));
        assert.assertThat(mocks.V3ListViews.Row, __.calledWithAt(2, __.hasProperty('model', atlassianNonSystem2)));
        assert.assertThat(mocks.V3ListViews.Row, __.calledWithAt(3, __.hasProperty('model', atlassianSystem1)));
        assert.assertThat(mocks.V3ListViews.Row, __.calledWithAt(4, __.hasProperty('model', atlassianSystem2)));


        var table = this.fixture.find('#agent-table');
        var noApplinksNotice = this.fixture.find('#v3-no-applinks-notice');

        // assert stuff has been rendered
        assert.assertThat(table.length, __.equalTo(1));
        assert.assertThat(table.find('tr').length, __.equalTo(5));
        assert.assertThat(table.attr("hidden"), __.is(__.undefined()));

        assert.assertThat(noApplinksNotice.length, __.equalTo(1));
        assert.assertThat(noApplinksNotice.attr("hidden"), __.is(__.defined()));

    });

    QUnit.test('OAuth dance success should re-fetch status', function(assert, V3ApplinkList, ApplinkModel, mocks) {
        var applink = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '123'})),
            applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection());
        applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([applink]);
        this.sandbox.stub(applink, 'fetchStatus');

        this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection});
        // fetchStatus should be called initially
        assert.assertThat(applink.fetchStatus, __.calledOnce());
        applink.fetchStatus.reset();

        MockCallbacks.runCallback(assert, mocks.OAuthDance.answer.onSuccess, [this.fixture.find('.oauth-dance')]);
        // should re-fetch status for the corresponding applink
        assert.assertThat(applink.fetchStatus, __.calledOnce());
    });

    // see SIN-131
    QUnit.test('preventDefault is not called for custom applink actions',
        function(assert, V3ApplinkList, ApplinkModel) {
            var applink = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '123'})),
                applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection());
            applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([applink]);
            applink.set({type: 'jira', name: 'Test applink', data: {configUrl: 'http://test.com/config'}});

            // init V3 list, invoke custom action dropdown click
            this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection});
            var customAction = '<a href="#" data-action="custom">Custom Action</a>';
            var mockEvent = {
                target: customAction,
                preventDefault: this.sandbox.stub()
            };
            this.testView.onActionsDropdownClick(mockEvent);

            assert.assertThat(mockEvent.preventDefault, __.notCalled());
        });

    QUnit.test('Go to remote applink action', function(assert, V3ApplinkList, ApplinkModel, mocks) {
        var applink = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '123'})),
            applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection());
        applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([applink]);
        applink.set({type: 'jira', name: 'Test applink', data: {configUrl: 'http://test.com/config'}});

        // init V3 list, append dropdown and click on "Go to remote"
        this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection});
        var actions = '<div id="dropdown-button-123" data-id="123" class="aui-dropdown2 aui-style-default actions-dropdown-content v3-applink-actions-dropdown">' +
            '<div class="aui-dropdown2-section">' +
            '<ul class="aui-list-truncate">' +
            '<li><a href="#" data-action="remote" class="action-remote">Go to remote</a></li>' +
            '<li><a href="#" data-action="primary" class="action-primary">Make primary</a></li>' +
            '<li><a href="#" data-action="legacy-edit" class="action-legacy-edit">Legacy edit</a></li>' +
            '<li><a href="#" data-action="delete" class="action-delete">Delete</a></li>' +
            '</ul>' +
            '</div>' +
            '</div>';
        this.fixture.append(actions);
        this.fixture.find('#dropdown-button-123 .action-remote').click();

        assert.assertThat(mocks.window.open, __.calledOnceWith('http://test.com/config'));
    });

    // see CAPL-661
    QUnit.test('Applinks action callbacks only work inside applinks list',
        function(assert, V3ApplinkList, ApplinkModel, mocks) {
            var applink = ApplinksModelMocks.setUpModel(this, new ApplinkModel.Applink({id: '123'})),
                applinksCollection = ApplinksModelMocks.setUpCollection(this, new ApplinkModel.Collection());
            applinksCollection.fetch.response = ApplinksModelMocks.responseSuccess([applink]);
            applink.set({type: 'jira', name: 'Test applink', data: {configUrl: 'http://test.com/config'}});

            // init V3 list, add actions dropdown and another dropdown and click on its "Go to Remote" action
            this.testView = new V3ApplinkList({el: this.fixture, model: applinksCollection});
            var actions = '<div id="dropdown-button-123" data-id="123" class="aui-dropdown2 aui-style-default actions-dropdown-content v3-applink-actions-dropdown">' +
                '<div class="aui-dropdown2-section">' +
                '<ul class="aui-list-truncate">' +
                '<li><a href="#" data-action="remote" class="action-remote">Go to remote</a></li>' +
                '<li><a href="#" data-action="primary" class="action-primary">Make primary</a></li>' +
                '<li><a href="#" data-action="legacy-edit" class="action-legacy-edit">Legacy edit</a></li>' +
                '<li><a href="#" data-action="delete" class="action-delete">Delete</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>';
            var anotherDropdown = '<div id="outside-dropdown" data-id="123" class="aui-dropdown2 aui-style-default actions-dropdown-content">' +
                '<div class="aui-dropdown2-section">' +
                '<ul class="aui-list-truncate">' +
                '<li><a href="#" data-action="remote" class="action-remote">Go to remote</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>';
            this.fixture.append(actions);
            this.fixture.append(anotherDropdown);
            this.fixture.find('#outside-dropdown .action-remote').click();

            // Go to remote callback should not execute
            assert.assertThat(mocks.window.open, __.notCalled());
        });

    var setUpRowView = function (V3ListViews, sandbox) {
        V3ListViews.Row = function(params) {
            this.params = params;
        };
        V3ListViews.Row.prototype.render = function() {
            var row = '<tr id="@id">' +
                '<td class="status"><button class="oauth-dance"></button></td>' +
                '<td class="actions">' +
                '</td>' +
                '</tr>';
            return $(row.replace(/@id/g, this.params.model.id));
        };
        sandbox.spy(V3ListViews, 'Row');
    };

    var setUpStatusDetails = function (currentTest) {
        currentTest.mocks.ApplinkStatusDetails.Dialog.returns(currentTest.sandbox.stub());
    };

    var setUpOAuthDance = function (currentTest) {
        currentTest.mocks.OAuthDance.answer.onSuccess.returns(currentTest.mocks.OAuthDance.answer);
    };
});
