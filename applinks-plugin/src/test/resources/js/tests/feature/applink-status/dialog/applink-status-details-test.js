define([
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/test/mock-callbacks'
], function(
    $,
    _,
    QUnit,
    __,
    MockDeclaration,
    MockCallbacks
) {
    var MOCK_APPLINK_ID = 'abcdef';

    QUnit.module('applinks/feature/status/details', {
        require: {
            main: 'applinks/feature/status/details',
            ApplinkErrors: 'applinks/feature/status/errors',
            underscore: 'applinks/lib/lodash'
        },
        mocks: {
            main: QUnit.mock(function() {
                return {
                    link: {id: MOCK_APPLINK_ID},
                    localAuthentication: {},
                    remoteAuthentication: {}
                };
            }),
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['contextPath', 'format', 'trigger', 'I18n.getText'])),
            skate: QUnit.moduleMock('applinks/lib/skate', new MockDeclaration(['init'])),
            console: QUnit.moduleMock('applinks/lib/console', new MockDeclaration(['warn'])),
            ApplinksRest: QUnit.moduleMock('applinks/common/rest', new MockDeclaration(['OAuth.V1.consumerToken'])),
            ApplinksUrls: QUnit.moduleMock('applinks/common/urls', new MockDeclaration(['Local.edit'])),
            OAuthMatcher : QUnit.moduleMock('applinks/feature/status/oauth-matcher'),
            DeprecatedStatus: QUnit.moduleMock('applinks/feature/status/details/deprecated', new MockDeclaration(['onLoad'])),
            OAuthDance: QUnit.moduleMock('applinks/feature/oauth-dance', new MockDeclaration(['start']).withConstructor()),
            RestRequest: QUnit.mock(new MockDeclaration(['del'])),
            promise: QUnit.mock(new MockDeclaration(['done', 'fail']))
        },
        templates: {
            "applinks.inlineDialog.popup": function(params) {
                return '<aui-inline-dialog2 id="' + params.id + '" class="status-dialog" aria-label="'+params.ariaLabel+'">' + params.contents + '</aui-inline-dialog2>';
            },
            "applinks.status.details.notificationContainer": function(data) {
                return '<div class="applink-status-notification aui-message" data-applink-id="' + data.status.link.id  + '">'
                    + data.contents +
                    '</div>';
            },
            "applinks.status.details.helpLink": detailsTemplate(function() {
                return '<div class="help-link troubleshoot-link"/>';
            }),
            "applinks.status.details.connection_refused": detailsTemplate(function() {
                return '<div class="connection_refused"/>';
            }),
            "applinks.status.details.unknown_host": detailsTemplate(function() {
                return '<div class="unknown_host"/>';
            }),
            "applinks.status.details.oauth_problem": detailsTemplate(function() {
                return '<div class="oauth_problem"/>';
            }),
            "applinks.status.details.oauth_timestamp_refused": detailsTemplate(function() {
                return '<div class="oauth_timestamp_refused"/>';
            }),
            "applinks.status.details.unexpected_response": detailsTemplate(function() {
                return '<div class="unexpected_response" />';
            }),
            "applinks.status.details.auth_level_mismatch": detailsTemplate(function() {
                return '<div class="auth_level_mismatch"/>';
            }),
            "applinks.status.details.auth_level_mismatch_2loi": detailsTemplate(function() {
                return '<div class="auth_level_mismatch_2loi"/>';
            }),
            "applinks.status.details.unknown_error": detailsTemplate(function() {
                return '<div class="unknown"/>';
            }),
            "applinks.status.details.auth_level_unsupported": detailsTemplate(function() {
                return '<div class="auth_level_unsupported"/>';
            }),
            "applinks.status.details.ssl_untrusted": detailsTemplate(function() {
                return '<div class="ssl_untrusted"/>';
            }),
            "applinks.status.details.ssl_hostname_unmatched": detailsTemplate(function() {
                return '<div class="ssl_hostname_unmatched"/>';
            }),
            "applinks.status.details.ssl_unmatched": detailsTemplate(function() {
                return '<div class="ssl_unmatched"/>';
            }),
            "applinks.status.details.system_link": detailsTemplate(function() {
                return '<div class="system_link"/>';
            }),
            "applinks.status.details.generic_link": detailsTemplate(function() {
                return '<div class="generic_link"/>';
            }),
            "applinks.status.details.remote_version_incompatible": detailsTemplate(function() {
                return '<div class="remote_version_incompatible"/>';
            }),
            "applinks.status.details.non_atlassian": detailsTemplate(function() {
                return '<div class="non_atlassian"/>';
            }),
            "applinks.status.details.no_remote_applink": detailsTemplate(function() {
                return '<div class="no_remote_applink"/>';
            }),
            "applinks.status.details.no_outgoing_auth": detailsTemplate(function() {
                return '<div class="no_outgoing_auth"><button class="applinks-default-focus" tabindex="0">Close</button></div>';
            }),
            "applinks.status.details.insufficient_remote_permission": detailsTemplate(function() {
                return '<div class="insufficient_remote_permission"><button class="status-insufficient-remote-permission">Authorize</button></div>';
            }),
            "applinks.status.details.oauth_signature_invalid": detailsTemplate(function() {
                return '<div class="oauth_signature_invalid"/>';
            }),
            "applinks.status.details.legacy_removal": detailsTemplate(function() {
                return '<div class="legacy_removal"/>';
            }),
            "applinks.status.details.legacy_update": detailsTemplate(function() {
                return '<div class="legacy_update"/>';
            }),
            "applinks.status.details.manual_legacy_update": detailsTemplate(function() {
                return '<div class="manual_legacy_update"/>';
            }),
            "applinks.status.details.manual_legacy_removal": detailsTemplate(function() {
                return '<div class="manual_legacy_removal"/>';
            }),
            "applinks.status.details.manual_legacy_removal_with_old_edit": detailsTemplate(function() {
                return '<div class="manual_legacy_removal_with_old_edit"/>';
            })
        },
        beforeEach: function(assert, StatusDetails, mockStatus, mocks) {
            this.body = $('body');

            mocks.ApplinksRest.OAuth.V1.consumerToken.returns(mocks.RestRequest);
            mocks.RestRequest.del.returns(mocks.promise);
            mocks.promise.done.returns(mocks.promise);
            mocks.promise.fail.returns(mocks.promise);
            mocks.AJS.versionDetails = {is58: false, is59: false};

            mocks.ApplinksUrls.Local.edit.withArgs(MOCK_APPLINK_ID).returns('applink_edit_url');

            mocks.AJS.I18n.getText.withArgs('applinks.inline_dialog.link_status.aria_label').returns('someI18nText');
        },
        afterEach : function() {
            this.body.find('aui-inline-dialog2').remove();
        },

        /** helper function that runs a test callback against all errors except the excluded ones */
        runForEachError: function (testCallback) {
            var ApplinkErrors = this.ApplinkErrors;

            // some errors cannot be tested by the generic "for all statuses" tests because of non-standard rendering
            // those should have specific test cases
            var EXCLUDED_ERRORS = [
                    ApplinkErrors.UNEXPECTED_RESPONSE_STATUS,
                    // no status details for errors below
                    ApplinkErrors.REMOTE_AUTH_TOKEN_REQUIRED,
                    ApplinkErrors.LOCAL_AUTH_TOKEN_REQUIRED
                ];

            ApplinkErrors.eachError(function(error) {
                if ((_.includes ? _.includes : _.contains)(EXCLUDED_ERRORS, error)) {
                    return;
                }
                testCallback(error);
            });
        }
    });

    QUnit.test('Should render dialog for all errors', function(assert, StatusDetails, mockStatus, mocks) {
        mocks.OAuthMatcher.withArgs(mockStatus).returns(true);

        var body = this.body;
        this.runForEachError(function(error) {
            mockStatus.error = error;

            new StatusDetails.Dialog(mockApplink(mockStatus, 'jira'));

            var dialog = body.find('aui-inline-dialog2.status-dialog');
            assert.assertThat('Dialog not rendered for ' + error.type, dialog, __.hasSize(1));
            assert.assertThat('Incorrect status rendered for ' + error.type,
                dialog.find('div.' + error.type.toLowerCase()), __.hasSize(1));
            assertTemplateData(assert, mockStatus);
            assertInjectedDataForDialog(assert);
            dialog.remove();
        });
    });

    QUnit.test('Should render in notification mode for all errors', function(assert, StatusDetails, mockStatus, mocks) {
        mocks.OAuthMatcher.withArgs(mockStatus).returns(true);

        var fixture = this.fixture;
        this.runForEachError(function(error) {
            mockStatus.error = error;

            new StatusDetails.Notification({
                applink: mockApplink(mockStatus, 'jira'),
                container: fixture
            });

            var notification = fixture.find('.applink-status-notification');
            assert.assertThat('Status details notification not rendered for ' + error.type, notification, __.hasSize(1));
            assert.assertThat('Incorrect status rendered for ' + error.type,
                notification.find('div.' + error.type.toLowerCase()), __.hasSize(1));
            assertTemplateData(assert, mockStatus);
            assertInjectedDataForNotification(assert);
            notification.remove();
        });
    });

    QUnit.test('Should render help link for all errors', function(assert, StatusDetails, mockStatus, mocks) {
        mocks.OAuthMatcher.withArgs(mockStatus).returns(true);

        var fixture = this.fixture;
        this.runForEachError(function(error) {
            mockStatus.error = error;

            new StatusDetails.HelpLink({
                applink: mockApplink(mockStatus, 'jira'),
                container: fixture,
                extraClasses: 'foo bar'
            });

            var helpLink = fixture.find('.help-link');
            assert.assertThat('Status details help link not rendered for ' + error.type, helpLink, __.hasSize(1));
            assertHelpLinkTemplateData(assert, __.hasProperty('extraClasses', 'foo bar'));
            helpLink.remove();
        });
    });


    // ----------- Status Details Dialog tests

    QUnit.test('status is undefined', function(assert, StatusDetails) {
        new StatusDetails.Dialog(mockApplink(undefined));
        assert.assertThat($('body').find('aui-inline-dialog2'), __.hasSize(0));
    });

    QUnit.test('should remove existing dialog', function(assert, StatusDetails, mockStatus) {
        mockStatus.error = {
            "category" : "should not exist",
            "type" : "should not exist"
        };

        this.body.append('<aui-inline-dialog2 id="applinks-status-dialog-' + mockStatus.link.id + '"></aui-inline-dialog2>');

        new StatusDetails.Dialog(mockApplink(mockStatus));
        assert.assertThat(this.body.find('aui-inline-dialog2'), __.hasSize(0));
    });

    QUnit.test('non existent error', function(assert, StatusDetails, mockStatus) {
        mockStatus.error = {
            "category" : "should not exist",
            "type" : "should not exist"
        };
        new StatusDetails.Dialog(mockApplink(mockStatus));

        assert.assertThat(this.body.find('aui-inline-dialog2'), __.hasSize(0));
    });

    QUnit.test('undefined error', function(assert, StatusDetails, mockStatus) {
        mockStatus.error = undefined;

        new StatusDetails.Dialog(mockApplink(mockStatus));

        assert.assertThat(this.body.find('aui-inline-dialog2'), __.hasSize(0));
    });

    QUnit.test('null error', function(assert, StatusDetails, mockStatus) {
        mockStatus.error = null;
        new StatusDetails.Dialog(mockApplink(mockStatus));
        assert.assertThat(this.body.find('aui-inline-dialog2'), __.hasSize(0));
    });

    QUnit.test('No status for auth token errors', function(assert, StatusDetails, mockStatus) {
        mockStatus.error = this.ApplinkErrors.REMOTE_AUTH_TOKEN_REQUIRED;
        new StatusDetails.Dialog(mockApplink(mockStatus));
        assert.assertThat(this.body.find('aui-inline-dialog2'), __.hasSize(0));

        mockStatus.error = this.ApplinkErrors.LOCAL_AUTH_TOKEN_REQUIRED;
        new StatusDetails.Dialog(mockApplink(mockStatus));
        assert.assertThat(this.body.find('aui-inline-dialog2'), __.hasSize(0));
    });

    QUnit.test('should render dialog for UNEXPECTED_RESPONSE_STATUS including details',
        function(assert, StatusDetails, mockStatus) {
            mockStatus.error = this.ApplinkErrors.UNEXPECTED_RESPONSE_STATUS;

            new StatusDetails.Dialog(mockApplink(mockStatus));

            assert.assertThat(this.body.find('aui-inline-dialog2').find('.unexpected_response'), __.hasSize(1));
            assertTemplateData(assert, mockStatus, __.hasProperty('renderDetails', true));
            assertInjectedDataForDialog(assert);
        });

    QUnit.test('should render dialog for UNEXPECTED_RESPONSE without details',
        function(assert, StatusDetails, mockStatus) {
            mockStatus.error = this.ApplinkErrors.UNEXPECTED_RESPONSE;

            new StatusDetails.Dialog(mockApplink(mockStatus));

            assert.assertThat(this.body.find('aui-inline-dialog2').find('.unexpected_response'), __.hasSize(1));
            assert.assertThat(this.body.find('aui-inline-dialog2').attr('aria-label'), __.is('someI18nText'));
            assertTemplateData(assert, mockStatus, __.hasProperty('renderDetails', false));
            assertInjectedDataForDialog(assert);
        });

    QUnit.test('should render dialog for mismatching OAuth with existing remote auth',
        function(assert, StatusDetails, mockStatus, mocks) {
            mocks.OAuthMatcher.withArgs(mockStatus).returns('mismatch desc');
            mockStatus.error = this.ApplinkErrors.AUTH_LEVEL_MISMATCH;

            new StatusDetails.Dialog(mockApplink(mockStatus));

            assert.assertThat(this.body.find('aui-inline-dialog2').find('.auth_level_mismatch'), __.hasSize(1));
            assert.assertThat(this.body.find('aui-inline-dialog2').attr('aria-label'), __.is('someI18nText'));
            assertTemplateData(assert, mockStatus, __.hasProperties({
                mismatches: 'mismatch desc',
            }));
            assertInjectedDataForDialog(assert);
        });

    QUnit.test('should not render dialog for invalid or matching OAuth',
        function(assert, StatusDetails, mockStatus, mocks) {
            mockStatus.error = this.ApplinkErrors.AUTH_LEVEL_MISMATCH;
            mocks.OAuthMatcher.withArgs(mockStatus).returns(undefined);

            var invalidConstructorCall = function() {
                new StatusDetails.Dialog(mockApplink(mockStatus));
            };

            assert.assertThat(invalidConstructorCall, __.throws(__.error('Unable to find mismatch description')));
        });

    QUnit.test('should render dialog for remote version incompatible applink',
        function(assert, StatusDetails, mockStatus, mocks) {
            mockStatus.error = this.ApplinkErrors.REMOTE_VERSION_INCOMPATIBLE;

            new StatusDetails.Dialog(mockApplink(mockStatus, 'Bitbucket Server'));

            assert.assertThat(this.body.find('aui-inline-dialog2').find('.remote_version_incompatible'), __.hasSize(1));
            assert.assertThat(this.body.find('aui-inline-dialog2').attr('aria-label'), __.is('someI18nText'));
            assertTemplateData(assert, mockStatus, __.hasProperty('applinkType', 'Bitbucket Server'));
            assertInjectedDataForDialog(assert);
        });

    QUnit.test('should render dialog for INSUFFICIENT_REMOTE_PERMISSION and run OAuth dance on successful token delete',
        function(assert, StatusDetails, mockStatus, mocks) {
            mockStatus.error = this.ApplinkErrors.INSUFFICIENT_REMOTE_PERMISSION;
            // local auth non 2LOi
            mockStatus.localAuthentication.outgoing = {
                enabled: true
            };

            new StatusDetails.Dialog(mockApplink(mockStatus, 'stash'));

            assert.assertThat(this.body.find('aui-inline-dialog2').find('.insufficient_remote_permission'), __.hasSize(1));
            assert.assertThat(this.body.find('aui-inline-dialog2').attr('aria-label'), __.is('someI18nText'));
            assertTemplateData(assert, mockStatus);
            assertInjectedDataForDialog(assert);

            // simulate clicking the link
            this.body.find('.status-insufficient-remote-permission').click();
            assert.assertThat(mocks.RestRequest.del, __.calledOnce());

            // call the success callback and make sure the OAuth Dance is triggered
            MockCallbacks.runCallback(assert, mocks.promise.done);
            assert.assertThat(mocks.OAuthDance, __.calledWithNew());
            assert.assertThat(mocks.OAuthDance.answer.start, __.calledOnce());
        });

    QUnit.test('should render dialog for INSUFFICIENT_REMOTE_PERMISSION and run OAuth dance on failed token delete',
        function(assert, StatusDetails, mockStatus, mocks) {
            mocks.AJS.format.returns('formatted text');
            mockStatus.error = this.ApplinkErrors.INSUFFICIENT_REMOTE_PERMISSION;
            // local auth non 2LOi
            mockStatus.localAuthentication.outgoing = {
                enabled: true
            };

            new StatusDetails.Dialog(mockApplink(mockStatus, 'stash'));

            assert.assertThat(this.body.find('aui-inline-dialog2').find('.insufficient_remote_permission'),
                __.hasSize(1));
            assert.assertThat(this.body.find('aui-inline-dialog2').attr('aria-label'), __.is('someI18nText'));
            assertTemplateData(assert, mockStatus);
            assertInjectedDataForDialog(assert);

            // simulate clicking the link
            this.body.find('.status-insufficient-remote-permission').click();
            assert.assertThat(mocks.RestRequest.del, __.calledOnce());

            // call the error callback and make sure OAuth Dance is still triggered (as well as console WARN)
            MockCallbacks.runCallback(assert, mocks.promise.fail, [{status: 401, responseText: 'Unauthorized'}]);
            assert.assertThat(mocks.OAuthDance, __.calledWithNew());
            assert.assertThat(mocks.OAuthDance.answer.start, __.calledOnce());
            assert.assertThat(mocks.AJS.format, __.calledOnce());
            assert.assertThat(mocks.console.warn, __.calledOnceWith('formatted text'));
        });

    QUnit.test('should render dialog for CONNECTION_REFUSED including edit URL',
        function(assert, StatusDetails, mockStatus, mocks) {

            mockStatus.error = this.ApplinkErrors.CONNECTION_REFUSED;

            new StatusDetails.Dialog(mockApplink(mockStatus, 'stash'));

            assert.assertThat(this.body.find('aui-inline-dialog2').find('.connection_refused'),
                __.hasSize(1));
            assert.assertThat(this.body.find('aui-inline-dialog2').attr('aria-label'), __.is('someI18nText'));
            assertTemplateData(assert, mockStatus);
            assertInjectedDataForDialog(assert);
        });

    QUnit.test('should render dialog for UNKNOWN_HOST including edit URL',
        function(assert, StatusDetails, mockStatus, mocks) {

            mockStatus.error = this.ApplinkErrors.UNKNOWN_HOST;

            new StatusDetails.Dialog(mockApplink(mockStatus, 'stash'));

            assert.assertThat(this.body.find('aui-inline-dialog2').find('.unknown_host'),
                __.hasSize(1));
            assert.assertThat(this.body.find('aui-inline-dialog2').attr('aria-label'), __.is('someI18nText'));
            assertTemplateData(assert, mockStatus);
            assertInjectedDataForDialog(assert);
        });

    QUnit.test('should render dialog for UNKNOWN including edit URL',
        function(assert, StatusDetails, mockStatus, mocks) {

            mockStatus.error = this.ApplinkErrors.UNKNOWN;

            new StatusDetails.Dialog(mockApplink(mockStatus, 'stash'));

            assert.assertThat(this.body.find('aui-inline-dialog2').find('.unknown'),
                __.hasSize(1));
            assert.assertThat(this.body.find('aui-inline-dialog2').attr('aria-label'), __.is('someI18nText'));
            assertTemplateData(assert, mockStatus);
            assertInjectedDataForDialog(assert);
        });

    QUnit.test('should trigger analytics when opening the dialog', function (assert, StatusDetails, mockStatus, mocks) {
        mockStatus.error = this.ApplinkErrors.NO_OUTGOING_AUTH;

        var dialog = new StatusDetails.Dialog(mockApplink(mockStatus));
        dialog.onDialogShow();

        assert.assertThat(mocks.AJS.trigger, __.calledOnce());
        assert.assertThat(mocks.AJS.trigger.args[0].length, __.equalTo(2));
        assert.assertThat(mocks.AJS.trigger.args[0][0], __.equalTo('analyticsEvent'));

        var firedAnalyticEvent = mocks.AJS.trigger.args[0][1];
        assert.assertThat(firedAnalyticEvent.name, __.equalTo('applinks.status.dialog.opened'));
        assert.assertThat(firedAnalyticEvent.data.status, __.equalTo("no_outgoing_auth"));
        assert.assertThat(firedAnalyticEvent.data.category, __.equalTo("incompatible"));
    });

    // ----------- Status Details Notification tests

    QUnit.test('status details notification given missing applink', function(assert, StatusDetails) {
        var container = this.fixture;

        var invalidConstructor = function() {
            new StatusDetails.Notification({
                container: container
            });
        };

        assert.assertThat(invalidConstructor, __.throws(__.error('options.applink: expected a value')));
    });

    QUnit.test('status details notification given missing container', function(assert, StatusDetails, mockStatus) {
        mockStatus.error = this.ApplinkErrors.INSUFFICIENT_REMOTE_PERMISSION;

        var invalidConstructor = function() {
            new StatusDetails.Notification({
                applink: mockApplink(mockStatus, 'jira')
            });
        };

        assert.assertThat(invalidConstructor, __.throws(__.error('options.container: expected a value')));
    });

    QUnit.test('status details notification given status is undefined', function(assert, StatusDetails) {
        var container = this.fixture;
        new StatusDetails.Notification({
            applink: mockApplink(undefined),
            container: container
        });

        assert.assertThat(container.find('.applink-status-notification'), __.hasSize(0));
    });

    QUnit.test('status details notification given non-existent error', function(assert, StatusDetails, mockStatus) {
        var container = this.fixture;
        mockStatus.error = {
            "category" : "should not exist",
            "type" : "should not exist"
        };

        new StatusDetails.Notification({
            applink: mockApplink(mockStatus),
            container: container
        });

        assert.assertThat(container.find('.applink-status-notification'), __.hasSize(0));
    });

    QUnit.test('status details notification refresh option should true by default',
        function(assert, StatusDetails, mockStatus) {
            var container = this.fixture;
            mockStatus.error = this.ApplinkErrors.CONNECTION_REFUSED;
            // pre-existing status details notification
            container.append(applinks.status.details.notificationContainer({
                status: mockStatus,
                contents: '<div class="old-status-notification" />'
            }));

            new StatusDetails.Notification({
                applink: mockApplink(mockStatus),
                container: container
            });

            // should remove previous status details notification
            assert.assertThat(container.find('.old-status-notification'), __.hasSize(0));
            assert.assertThat(container.find('.applink-status-notification'), __.hasSize(1));
        });

    QUnit.test('status details notification given refresh false',
        function(assert, StatusDetails, mockStatus) {
            var container = this.fixture;
            mockStatus.error = this.ApplinkErrors.CONNECTION_REFUSED;
            // pre-existing status details notification
            container.append(applinks.status.details.notificationContainer({
                status: mockStatus,
                contents: '<div class="old-status-notification" />'
            }));

            new StatusDetails.Notification({
                applink: mockApplink(mockStatus),
                container: container,
                refresh: false
            });

            // should not remove previous status details notification
            assert.assertThat(container.find('.old-status-notification'), __.hasSize(1));
            assert.assertThat(container.find('.applink-status-notification'), __.hasSize(2));
        });

    QUnit.test('should render notification for UNEXPECTED_RESPONSE_STATUS including details',
        function(assert, StatusDetails, mockStatus) {
            var container = this.fixture;
            mockStatus.error = this.ApplinkErrors.UNEXPECTED_RESPONSE_STATUS;

            new StatusDetails.Notification({
                applink: mockApplink(mockStatus),
                container: container
            });

            assert.assertThat(container.find('.applink-status-notification').find('.unexpected_response'),
                __.hasSize(1));
            assertTemplateData(assert, mockStatus, __.hasProperty('renderDetails', true));
            assertInjectedDataForNotification(assert);
        });

    QUnit.test('should render notification for UNEXPECTED_RESPONSE without details',
        function(assert, StatusDetails, mockStatus) {
            var container = this.fixture;
            mockStatus.error = this.ApplinkErrors.UNEXPECTED_RESPONSE;

            new StatusDetails.Notification({
                applink: mockApplink(mockStatus),
                container: container
            });

            assert.assertThat(container.find('.applink-status-notification').find('.unexpected_response'),
                __.hasSize(1));
            assertTemplateData(assert, mockStatus, __.hasProperty('renderDetails', false));
            assertInjectedDataForNotification(assert);
        });

    QUnit.test('should render notification for INSUFFICIENT_REMOTE_PERMISSION and run OAuth dance on successful token delete',
        function(assert, StatusDetails, mockStatus, mocks) {
            var fixture = this.fixture;
            mockStatus.error = this.ApplinkErrors.INSUFFICIENT_REMOTE_PERMISSION;
            // local auth non 2LOi
            mockStatus.localAuthentication.outgoing = {
                enabled: true
            };

            new StatusDetails.Notification({
                applink: mockApplink(mockStatus, 'stash'),
                container: fixture
            });

            assert.assertThat(fixture.find('.applink-status-notification').find('.insufficient_remote_permission'),
                __.hasSize(1));
            assertTemplateData(assert, mockStatus);
            assertInjectedDataForNotification(assert);

            // simulate clicking the link
            fixture.find('.status-insufficient-remote-permission').click();
            assert.assertThat(mocks.RestRequest.del, __.calledOnce());

            // call the success callback and make sure the OAuth Dance is triggered
            MockCallbacks.runCallback(assert, mocks.promise.done);
            assert.assertThat(mocks.OAuthDance, __.calledWithNew());
            assert.assertThat(mocks.OAuthDance.answer.start, __.calledOnce());
        });

    QUnit.test('should run Migration onLoad for all MANUAL_LEGACY_REMOVAL',
        function(assert, StatusDetails, mockStatus, mocks) {
            mockStatus.error = this.ApplinkErrors.MANUAL_LEGACY_REMOVAL;
            var applink = mockApplink(mockStatus);
            var dialog = new StatusDetails.Dialog(applink);
            assert.assertThat(mocks.DeprecatedStatus.onLoad,  __.calledOnce(dialog, applink));
        });

    QUnit.test('should run Migration onLoad for all MANUAL_LEGACY_UPDATE',
        function(assert, StatusDetails, mockStatus, mocks) {
            mockStatus.error = this.ApplinkErrors.MANUAL_LEGACY_UPDATE;
            var applink = mockApplink(mockStatus);
            var dialog = new StatusDetails.Dialog(applink);
            assert.assertThat(mocks.DeprecatedStatus.onLoad,  __.calledOnce(dialog, applink));
        });

    QUnit.test('should run Migration onLoad for all LEGACY_UPDATE',
        function(assert, StatusDetails, mockStatus, mocks) {
            mockStatus.error = this.ApplinkErrors.LEGACY_UPDATE;
            var applink = mockApplink(mockStatus);
            var dialog = new StatusDetails.Dialog(applink);
            assert.assertThat(mocks.DeprecatedStatus.onLoad,  __.calledOnce(dialog, applink));
        });

    QUnit.test('should run Migration onLoad for all LEGACY_REMOVAL',
        function(assert, StatusDetails, mockStatus, mocks) {
            mockStatus.error = this.ApplinkErrors.LEGACY_REMOVAL;
            var applink = mockApplink(mockStatus);
            var dialog = new StatusDetails.Dialog(applink);
            assert.assertThat(mocks.DeprecatedStatus.onLoad,  __.calledOnce(dialog, applink));
        });

    // ----------- Help link

    QUnit.test('Status details help link given missing applink', function(assert, StatusDetails) {
        var container = this.fixture;

        var invalidConstructor = function() {
            new StatusDetails.HelpLink({
                container: container
            });
        };

        assert.assertThat(invalidConstructor, __.throws(__.error('options.applink: expected a value')));
    });

    QUnit.test('Status details help link given missing container', function(assert, StatusDetails, mockStatus) {
        mockStatus.error = this.ApplinkErrors.INSUFFICIENT_REMOTE_PERMISSION;

        var invalidConstructor = function() {
            new StatusDetails.HelpLink({
                applink: mockApplink(mockStatus, 'jira')
            });
        };

        assert.assertThat(invalidConstructor, __.throws(__.error('options.container: expected a value')));
    });

    QUnit.test('Status details help link given status is undefined', function(assert, StatusDetails) {
        var container = this.fixture;
        new StatusDetails.HelpLink({
            applink: mockApplink(undefined),
            container: container
        });

        assert.assertThat(findHelpLink(container), __.hasSize(0));
    });

    QUnit.test('Status details help link given non-existent error', function(assert, StatusDetails, mockStatus) {
        var container = this.fixture;
        mockStatus.error = {
            "category" : "should not exist",
            "type" : "should not exist"
        };

        new StatusDetails.HelpLink({
            applink: mockApplink(mockStatus),
            container: container
        });

        assert.assertThat(findHelpLink(container), __.hasSize(0));
    });

    QUnit.test('Status details help link refresh option should true by default',
        function(assert, StatusDetails, mockStatus) {
            var container = this.fixture;
            mockStatus.error = this.ApplinkErrors.CONNECTION_REFUSED;
            // pre-existing help link
            container.append('<div class="help-link troubleshoot-link old-help-link"></div>');

            new StatusDetails.HelpLink({
                applink: mockApplink(mockStatus),
                container: container
            });

            // should remove previous help link
            assert.assertThat(findHelpLink(container), __.hasSize(1));
            assert.assertThat(container.find('.old-help-link'), __.hasSize(0));
        });

    QUnit.test('Status details help link given refresh false',
        function(assert, StatusDetails, mockStatus) {
            var container = this.fixture;
            mockStatus.error = this.ApplinkErrors.CONNECTION_REFUSED;
            // pre-existing help link
            container.append('<div class="help-link troubleshoot-link old-help-link"></div>');

            new StatusDetails.HelpLink({
                applink: mockApplink(mockStatus),
                container: container,
                refresh: false
            });

            // should preserve previous help link
            assert.assertThat(findHelpLink(container), __.hasSize(2));
            assert.assertThat(container.find('.old-help-link'), __.hasSize(1));
        });

    QUnit.test('Status details help link extra classes',
        function(assert, StatusDetails, mockStatus) {
            var container = this.fixture;
            mockStatus.error = this.ApplinkErrors.CONNECTION_REFUSED;

            new StatusDetails.HelpLink({
                applink: mockApplink(mockStatus),
                container: container,
                extraClasses: 'foo bar'
            });

            assert.assertThat(findHelpLink(container), __.hasSize(1));
            assertHelpLinkTemplateData(assert, __.hasProperty('extraClasses', 'foo bar'));
        });

    
    // ----------- Utils

    function mockApplink(status, typeName) {
        return {
            id: MOCK_APPLINK_ID,
            get: function(key) {
                if (key === 'status') {
                    return status;
                }
                return null;
            },
            getAdminUrl: function() {
                return 'admin_url';
            },
            getTypeName: function() {
                return typeName;
            }
        }
    }

    function assertTemplateData(assert, mockStatus, extraDataMatcher) {
        var data = parseDataFromHtml(assert, $('#template-data').text());
        assert.assertThat(data, __.hasProperties({
            status: __.hasProperties({
                error: __.object(),
                link: __.equalTo(mockStatus.link),
                localAuthentication: __.equalTo(mockStatus.localAuthentication),
                remoteAuthentication: __.equalTo(mockStatus.remoteAuthentication)
            }),
            displayUrl: 'admin_url',
            editUrl: 'applink_edit_url'
        }));
        assert.assertThat(mockStatus.error.matches(data.status), __.truthy());
        if (extraDataMatcher) {
            assert.assertThat(data, extraDataMatcher);
        }
    }

    function assertHelpLinkTemplateData(assert, extraMatcher) {
        var data = parseDataFromHtml(assert, $('#template-data').text());
        assert.assertThat(data, __.allOf(
            __.hasProperty('linkKey'), __.hasProperty('sectionKey'),
            __.hasProperty('style'), __.hasProperty('textMode')));
        if (extraMatcher) {
            assert.assertThat(data, extraMatcher);
        }
    }

    function assertInjectedDataForDialog(assert) {
        assertInjectedData(assert, 'dialog');
    }

    function assertInjectedDataForNotification(assert) {
        assertInjectedData(assert, 'notification');
    }

    function assertInjectedData(assert, mode) {
        var data = parseDataFromHtml(assert, $('#template-ij-data').text());
        assert.assertThat(data, __.hasProperties({mode: mode, isDialog: mode == 'dialog'}));
    }

    function parseDataFromHtml(assert, dataText) {
        try {
            return JSON.parse(dataText);
        } catch(error) {
            assert.fail('Failed to parse JSON text "' + dataText + '": ' + error);
        }
    }

    function detailsTemplate(templateGenerator) {
        return function(data, out, injectedData) {
            return templateGenerator(data, out, injectedData) + renderTemplateData(data, out, injectedData);
        }
    }

    function renderTemplateData(data, out, injectedData) {
        return '<div id="template-data">' + JSON.stringify(data) + '</div>' +
            '<div id="template-ij-data">' + JSON.stringify(injectedData) + '</div>';
    }

    function findHelpLink(container) {
        return container.find('.help-link');
    }
});
