package com.atlassian.applinks.internal.feature;

import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NotAuthenticatedException;
import com.atlassian.applinks.internal.common.exception.PermissionException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.exception.SystemFeatureException;
import com.atlassian.applinks.internal.common.test.mock.SimpleServiceExceptionFactory;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.test.mock.PermissionValidationMocks;
import com.atlassian.applinks.test.rule.SystemPropertyRule;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.applinks.internal.feature.ApplinksFeatures.V3_UI;
import static com.atlassian.applinks.internal.feature.ApplinksFeatures.TEST_NON_SYSTEM;
import static com.atlassian.sal.api.features.DarkFeatureManager.ATLASSIAN_DARKFEATURE_PREFIX;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultApplinksFeatureServiceTest {
    @Mock
    private DarkFeatureManager darkFeatureManager;
    @Mock
    private PermissionValidationService permissionValidationService;
    @Spy // for @InjectMocks
    private SimpleServiceExceptionFactory serviceExceptionFactory = new SimpleServiceExceptionFactory();
    @Mock
    private UserManager userManager;

    @InjectMocks
    private PermissionValidationMocks permissionValidationMocks;
    @InjectMocks
    private DefaultApplinksFeatureService applinksFeatureService;

    @Rule
    public final SystemPropertyRule systemPropertyRule = new SystemPropertyRule();

    @Test
    public void testIsEnabledNoUserContext() throws NoAccessException {
        stubNotAuthenticated();
        stubNonSystemFeature(true);

        assertFalse(applinksFeatureService.isEnabled(TEST_NON_SYSTEM));
    }

    @Test
    public void testIsEnabledForDisabledFeature() throws NoAccessException {
        stubAuthenticated();
        stubNonSystemFeature(false);

        assertFalse(applinksFeatureService.isEnabled(TEST_NON_SYSTEM));
    }

    @Test
    public void testIsEnabledForEnabledFeature() throws NoAccessException {
        stubAuthenticated();
        stubNonSystemFeature(true);

        assertTrue(applinksFeatureService.isEnabled(TEST_NON_SYSTEM));
    }

    @Test
    public void testIsEnabledForSystemFeatureSystemProperty() throws NoAccessException {
        stubAuthenticated();

        systemPropertyRule.setProperty(ATLASSIAN_DARKFEATURE_PREFIX + V3_UI.featureKey, Boolean.FALSE.toString());
        assertFalse(applinksFeatureService.isEnabled(V3_UI));

        systemPropertyRule.setProperty(ATLASSIAN_DARKFEATURE_PREFIX + V3_UI.featureKey, Boolean.TRUE.toString());
        assertTrue(applinksFeatureService.isEnabled(V3_UI));
    }

    @Test
    public void testIsEnabledForSystemFeatureFallsBackToDefaultValue() throws NoAccessException {
        stubAuthenticated();

        assertEquals(V3_UI.getDefaultValue(), applinksFeatureService.isEnabled(V3_UI));
    }

    @Test(expected = NotAuthenticatedException.class)
    public void testEnableNoUserContext() throws ServiceException {
        permissionValidationMocks.failedValidateAuthenticated();

        applinksFeatureService.enable(TEST_NON_SYSTEM);
    }

    @Test(expected = PermissionException.class)
    public void testEnableNonAdminUser() throws ServiceException {
        permissionValidationMocks.failedValidateAdmin();

        applinksFeatureService.enable(TEST_NON_SYSTEM);
    }

    @Test(expected = PermissionException.class)
    public void testEnableAdminOnlyUser() throws ServiceException {
        permissionValidationMocks.failedValidateSysadmin();

        applinksFeatureService.enable(TEST_NON_SYSTEM);
    }

    @Test
    public void testEnableSysadminUser() throws ServiceException {
        applinksFeatureService.enable(TEST_NON_SYSTEM);

        permissionValidationMocks.verifyValidateSysadmin();
        verify(darkFeatureManager).enableFeatureForAllUsers(TEST_NON_SYSTEM.featureKey);
        verifyNoMoreInteractions(darkFeatureManager);
    }

    @Test(expected = SystemFeatureException.class)
    public void testEnableSystemFeature() throws ServiceException {
        applinksFeatureService.enable(V3_UI);
    }

    @Test
    public void testEnableMoreThanOneFeature() throws ServiceException {
        applinksFeatureService.enable(TEST_NON_SYSTEM, TEST_NON_SYSTEM, TEST_NON_SYSTEM);

        permissionValidationMocks.verifyValidateSysadmin();
        verify(darkFeatureManager, times(3)).enableFeatureForAllUsers(TEST_NON_SYSTEM.featureKey);
        verifyNoMoreInteractions(darkFeatureManager);
    }

    @Test(expected = NotAuthenticatedException.class)
    public void testDisableNoUserContext() throws ServiceException {
        permissionValidationMocks.failedValidateAuthenticated();

        applinksFeatureService.disable(TEST_NON_SYSTEM);
    }

    @Test(expected = PermissionException.class)
    public void testDisableNonAdminUser() throws ServiceException {
        permissionValidationMocks.failedValidateAdmin();

        applinksFeatureService.disable(TEST_NON_SYSTEM);
    }

    @Test(expected = PermissionException.class)
    public void testDisableAdminOnlyUser() throws ServiceException {
        permissionValidationMocks.failedValidateSysadmin();

        applinksFeatureService.disable(TEST_NON_SYSTEM);
    }

    @Test
    public void testDisableSysadminUser() throws ServiceException {
        applinksFeatureService.disable(TEST_NON_SYSTEM);

        permissionValidationMocks.verifyValidateSysadmin();
        verify(darkFeatureManager).disableFeatureForAllUsers(TEST_NON_SYSTEM.featureKey);
        verifyNoMoreInteractions(darkFeatureManager);
    }

    @Test(expected = SystemFeatureException.class)
    public void testDisableSystemFeature() throws ServiceException {
        applinksFeatureService.disable(V3_UI);
    }

    @Test
    public void testDisableMoreThanOneFeature() throws ServiceException {
        applinksFeatureService.disable(TEST_NON_SYSTEM, TEST_NON_SYSTEM, TEST_NON_SYSTEM);

        permissionValidationMocks.verifyValidateSysadmin();
        verify(darkFeatureManager, times(3)).disableFeatureForAllUsers(TEST_NON_SYSTEM.featureKey);
        verifyNoMoreInteractions(darkFeatureManager);
    }

    private void stubNonSystemFeature(boolean enabled) {
        when(darkFeatureManager.isFeatureEnabledForCurrentUser(TEST_NON_SYSTEM.featureKey)).thenReturn(enabled);
    }

    private void stubNotAuthenticated() {
        when(userManager.getRemoteUserKey()).thenReturn(null);
    }

    private void stubAuthenticated() {
        when(userManager.getRemoteUserKey()).thenReturn(new UserKey("test"));
    }
}
