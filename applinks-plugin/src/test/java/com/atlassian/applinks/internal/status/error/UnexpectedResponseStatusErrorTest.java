package com.atlassian.applinks.internal.status.error;

import com.atlassian.applinks.test.mock.MockApplicationLinkResponse;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UnexpectedResponseStatusErrorTest {
    @Test
    public void responseApplinkErrorGivenInvalidStatusCode() {
        ResponseApplinkError error = new UnexpectedResponseStatusError(new MockApplicationLinkResponse().setStatus(600));

        assertEquals(600, error.getStatusCode());
        assertEquals("600", error.getDetails());
    }

    @Test
    public void responseApplinkErrorGivenValidStatusCode() {
        ResponseApplinkError error = new UnexpectedResponseStatusError(new MockApplicationLinkResponse().setStatus(409));

        assertEquals(409, error.getStatusCode());
        assertEquals("409: Conflict", error.getDetails());
    }

}
