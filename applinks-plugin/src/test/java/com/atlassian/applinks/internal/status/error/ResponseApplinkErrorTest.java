package com.atlassian.applinks.internal.status.error;

import com.atlassian.applinks.test.mock.MockApplicationLinkResponse;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;
import org.hamcrest.Matchers;
import org.junit.Test;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response.Status;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static com.atlassian.applinks.test.matcher.StringMatchers.containsInOrder;
import static com.atlassian.applinks.test.matchers.status.ApplinkErrorMatchers.responseErrorWith;
import static com.atlassian.applinks.test.matchers.status.ApplinkErrorMatchers.responseErrorWithNoBody;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static org.apache.commons.lang3.StringUtils.repeat;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class ResponseApplinkErrorTest {
    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void responseApplinkErrorGivenNullResponse() {
        new TestResponseApplinkError(null);
    }

    @Test
    public void responseApplinkErrorGivenNoBody() {
        Response response = new MockApplicationLinkResponse().setStatus(Status.BAD_REQUEST);

        ResponseApplinkError error = new TestResponseApplinkError(response);

        assertEquals(HttpStatus.SC_BAD_REQUEST, error.getStatusCode());
        assertNull(error.getBody());
        assertNull(error.getContentType());
    }

    @Test
    public void responseApplinkErrorGivenShortBodyWithoutContentType() {
        Response response = new MockApplicationLinkResponse()
                .setStatus(Status.BAD_REQUEST)
                .setResponseBody(new byte[]{(byte) 0xE2, (byte) 0x82, (byte) 0xAC}, StandardCharsets.UTF_8); // € sign

        ResponseApplinkError error = new TestResponseApplinkError(response);

        assertEquals(HttpStatus.SC_BAD_REQUEST, error.getStatusCode());
        assertEquals("€", error.getBody()); // should use UTF-8 as a fallback and therefore decode correctly
        assertEquals(TEXT_PLAIN, error.getContentType()); // default content type
    }

    @Test
    public void responseApplinkErrorGivenShortBodyWithContentTypeButNoEncoding() {
        Response response = new MockApplicationLinkResponse()
                .setStatus(Status.BAD_REQUEST)
                .setHeader(HttpHeaders.CONTENT_TYPE, TEXT_PLAIN)
                .setResponseBody(new byte[]{(byte) 0xE2, (byte) 0x82, (byte) 0xAC}, StandardCharsets.UTF_8); // € sign

        ResponseApplinkError error = new TestResponseApplinkError(response);

        assertEquals(HttpStatus.SC_BAD_REQUEST, error.getStatusCode());
        assertEquals("€", error.getBody()); // should use UTF-8 as a fallback and therefore decode correctly
        assertEquals(TEXT_PLAIN, error.getContentType()); // default content type
    }

    @Test
    public void responseApplinkErrorGivenShortBodyWithContentTypeAndEncoding() {
        Response response = new MockApplicationLinkResponse()
                .setStatus(Status.BAD_REQUEST)
                .setHeader(HttpHeaders.CONTENT_TYPE, contentTypeFor(TEXT_PLAIN, StandardCharsets.ISO_8859_1))
                .setResponseBody(new byte[]{(byte) 0xF2}, StandardCharsets.ISO_8859_1);

        ResponseApplinkError error = new TestResponseApplinkError(response);

        assertEquals(HttpStatus.SC_BAD_REQUEST, error.getStatusCode());
        assertEquals("ò", error.getBody()); // should use ISO_8859_1 (from the header) and therefore read the contents correctly
        assertEquals(TEXT_PLAIN, error.getContentType());
    }

    @Test
    public void responseApplinkErrorGivenJsonResponse() {
        Response response = new MockApplicationLinkResponse()
                .setStatus(Status.BAD_REQUEST)
                .setHeader(HttpHeaders.CONTENT_TYPE, contentTypeFor(APPLICATION_JSON, null))
                .setResponseBody("{\"key\": \"value\"}");

        ResponseApplinkError error = new TestResponseApplinkError(response);

        assertEquals(HttpStatus.SC_BAD_REQUEST, error.getStatusCode());
        assertEquals("{\"key\": \"value\"}", error.getBody());
        assertEquals(APPLICATION_JSON, error.getContentType());
    }

    @Test
    public void responseApplinkErrorGivenIOExceptionWhenGettingResponseStream() {
        Response response = new MockApplicationLinkResponse()
                .setStatus(Status.BAD_REQUEST)
                .setResponseException(new ResponseException("Fail"));

        ResponseApplinkError error = new TestResponseApplinkError(response);

        assertEquals(HttpStatus.SC_BAD_REQUEST, error.getStatusCode());
        assertEquals("<Could not retrieve response body>", error.getBody());
        assertEquals(TEXT_PLAIN, error.getContentType());
    }

    @Test
    public void responseApplinkErrorGivenLargeResponseContents() {
        Response response = new MockApplicationLinkResponse()
                .setStatus(Status.BAD_REQUEST)
                .setResponseBody(createLargeBody());

        ResponseApplinkError error = new TestResponseApplinkError(response);

        assertEquals(HttpStatus.SC_BAD_REQUEST, error.getStatusCode());
        assertEquals(TEXT_PLAIN, error.getContentType());
        Iterable<String> body = toLines(error.getBody());
        assertThat(body, Matchers.<String>iterableWithSize(500));
        assertThat(Iterables.get(body, 0), containsInOrder(repeat("a", 494), " (...)"));
        assertEquals(Iterables.get(body, 499), "(101 more lines ...)");
    }

    @Test
    public void unexpectedResponseStatusError() {
        Response response = new MockApplicationLinkResponse()
                .setStatus(Status.BAD_REQUEST)
                .setResponseBody("Some body");

        ResponseApplinkError error = new UnexpectedResponseStatusError(response);

        assertEquals(ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS, error.getType());
        assertEquals("400: Bad Request", error.getDetails());
        assertThat(error, responseErrorWith(Status.BAD_REQUEST, "Some body", TEXT_PLAIN));
    }

    @Test
    public void unexpectedResponseErrorGivenNullBody() {
        Response response = new MockApplicationLinkResponse().setStatus(Status.BAD_REQUEST);

        ResponseApplinkError error = new UnexpectedResponseError(response);

        assertEquals(ApplinkErrorType.UNEXPECTED_RESPONSE, error.getType());
        assertNull(error.getDetails());
        assertThat(error, responseErrorWithNoBody(Status.BAD_REQUEST));
    }

    @Test
    public void unexpectedResponseErrorGivenNonEmptyBody() {
        Response response = new MockApplicationLinkResponse()
                .setStatus(Status.BAD_REQUEST)
                .setResponseBody("Some body\nsome more\nand more");

        ResponseApplinkError error = new UnexpectedResponseError(response);

        assertEquals(ApplinkErrorType.UNEXPECTED_RESPONSE, error.getType());
        assertThat(error.getDetails(), nullValue());
        assertThat(error, responseErrorWith(Status.BAD_REQUEST, "Some body\nsome more\nand more", TEXT_PLAIN));
    }

    private static String contentTypeFor(String mediaType, Charset charset) {
        return ContentType.create(mediaType, charset).toString();
    }

    private static String createLargeBody() {
        StringBuilder builder = new StringBuilder(360000);
        for (int i = 0; i < 600; i++) {
            builder.append(repeat("a", 600)).append(System.lineSeparator());
        }
        return builder.toString();
    }

    private static Iterable<String> toLines(String details) {
        return Splitter.on(System.lineSeparator()).split(details);
    }

    static class TestResponseApplinkError extends AbstractResponseApplinkError {
        public TestResponseApplinkError(Response response) {
            super(response);
        }

        @Nonnull
        @Override
        public ApplinkErrorType getType() {
            return ApplinkErrorType.UNEXPECTED_RESPONSE_STATUS;
        }

        @Nullable
        @Override
        public String getDetails() {
            return null;
        }
    }
}
