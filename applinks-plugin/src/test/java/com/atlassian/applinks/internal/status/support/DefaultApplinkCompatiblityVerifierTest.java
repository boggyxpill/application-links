package com.atlassian.applinks.internal.status.support;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.application.bamboo.BambooApplicationTypeImpl;
import com.atlassian.applinks.application.generic.GenericApplicationTypeImpl;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.ui.SampleApplicationType;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultApplinkCompatiblityVerifierTest {
    @Mock
    private InternalHostApplication hostApplication;
    @Mock
    private RequestFactory requestFactory;
    @Mock
    private UserManager userManager;

    @Mock
    ApplicationLink applicationLink;

    private final DefaultApplinkCompatibilityVerifier compatibilityVerifier = new DefaultApplinkCompatibilityVerifier();

    @Test
    public void bambooApplicationTypeIsCompatible() {
        when(applicationLink.getType()).thenReturn(mock(BambooApplicationTypeImpl.class));

        assertNull(compatibilityVerifier.verifyLocalCompatibility(applicationLink));
    }

    @Test
    public void genericApplicationTypeIsIncompatible() {
        when(applicationLink.getType()).thenReturn(mock(GenericApplicationTypeImpl.class));

        assertEquals(ApplinkErrorType.GENERIC_LINK, compatibilityVerifier.verifyLocalCompatibility(applicationLink));
    }

    @Test
    public void nonBuiltInApplicationTypeIsIncompatible() {
        when(applicationLink.getType()).thenReturn(mock(SampleApplicationType.class));

        assertEquals(ApplinkErrorType.NON_ATLASSIAN, compatibilityVerifier.verifyLocalCompatibility(applicationLink));
    }

    @Test
    public void systemLinkIsIncompatible() {
        when(applicationLink.isSystem()).thenReturn(true);

        assertEquals(ApplinkErrorType.SYSTEM_LINK, compatibilityVerifier.verifyLocalCompatibility(applicationLink));
    }
}
