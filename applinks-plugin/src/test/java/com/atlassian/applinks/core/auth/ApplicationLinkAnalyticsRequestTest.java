package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.analytics.ApplinksRequestExecutionEvent;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.core.auth.ApplicationLinkAnalyticsRequest.AnalyticsResponseHandler;
import com.atlassian.applinks.core.auth.ApplicationLinkAnalyticsRequest.ApplicationLinkAnalyticsResponseHandler;
import com.atlassian.applinks.core.auth.ApplicationLinkAnalyticsRequest.ReturningAnalyticsResponseHandler;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.net.RequestFilePart;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class ApplicationLinkAnalyticsRequestTest {
    private static final String REMOTE_ID_STRING = UUID.fromString("44444444-4444-4444-4444-444444444444").toString();

    @Mock
    private ApplicationLinkRequest wrappedRequest;
    @Mock
    private ApplicationLink remoteLink;
    @Mock
    private ApplicationId remoteLinkId;
    @Mock
    private EventPublisher publisher;
    @InjectMocks
    private ApplicationLinkAnalyticsRequest request;

    @Before
    public void setup() {
        initMocks(this);
    }

    @Test
    public void setConnectionTimeout() {
        request.setConnectionTimeout(343);
        verify(wrappedRequest, times(1))
                .setConnectionTimeout(343);
    }

    @Test
    public void setSoTimeout() {
        request.setSoTimeout(343);
        verify(wrappedRequest, times(1))
                .setSoTimeout(343);
    }

    @Test
    public void setUrl() {
        String atlassianCom = "https://www.atlassian.com";
        request.setUrl(atlassianCom);
        verify(wrappedRequest, times(1))
                .setUrl(atlassianCom);
    }

    @Test
    public void setRequestBody() {
        String body = "Hey it's me";
        request.setRequestBody(body);
        verify(wrappedRequest, times(1))
                .setRequestBody(body);
    }

    @Test
    public void setFiles() {
        List<RequestFilePart> files = Arrays.asList(mockFile(123),
                mockFile(343));
        request.setFiles(files);
        verify(wrappedRequest, times(1))
                .setFiles(files);
    }

    @Test
    public void setEntity() {
        Object entity = mock(Object.class);
        request.setEntity(entity);
        verify(wrappedRequest, times(1))
                .setEntity(entity);
    }

    @Test
    public void addRequestParameters() {
        String[] parameters = new String[] {"name1", "val1", "name2", "val2"};
        request.addRequestParameters(parameters);
        verify(wrappedRequest, times(1))
                .addRequestParameters(parameters);
    }

    @Test
    public void addBasicAuthentication() {
        request.addBasicAuthentication("hostname", "username", "password");
        verify(wrappedRequest, times(1))
                .addBasicAuthentication("hostname", "username", "password");
    }

    @Test
    public void addHeader() {
        request.addHeader("name", "value");
        verify(wrappedRequest, times(1))
                .addHeader("name", "value");
    }

    @Test
    public void setHeader() {
        request.setHeader("name", "value");
        verify(wrappedRequest, times(1))
                .setHeader("name", "value");
    }

    @Test
    public void setFollowRedirectsTrue() {
        request.setFollowRedirects(true);
        verify(wrappedRequest, times(1))
                .setFollowRedirects(true);
        verify(wrappedRequest, never())
                .setFollowRedirects(false);
    }

    @Test
    public void setFollowRedirectsFalse() {
        request.setFollowRedirects(false);
        verify(wrappedRequest, times(1))
                .setFollowRedirects(false);
        verify(wrappedRequest, never())
                .setFollowRedirects(true);
    }

    @Test
    public void getHeaders() {
        Map<String, List<String>> headers = new HashMap<>();
        headers.put("test", new ArrayList<>()); // Ensure not empty
        doReturn(headers).when(wrappedRequest).getHeaders();
        assertEquals(headers, request.getHeaders());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void applinkRequestResponseHandlerExecutePublishesEvent() throws ResponseException {
        setupAnalytics();
        request.execute(mock(ApplicationLinkResponseHandler.class));
        verify(publisher, times(1))
                .publish(any(ApplinksRequestExecutionEvent.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void applinkRequestResponseHandlerExecuteCallsWrapper() throws ResponseException {
        setupAnalytics();
        ApplicationLinkResponseHandler<Object> handler = mock(ApplicationLinkResponseHandler.class);
        request.execute(handler);
        verify(wrappedRequest, times(1))
                .execute(any(ApplicationLinkAnalyticsResponseHandler.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void requestResponseHandlerExecutePublishesEvent() throws ResponseException {
        setupAnalytics();
        request.execute(mock(ResponseHandler.class));
        verify(publisher, times(1))
                .publish(any(ApplinksRequestExecutionEvent.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void requestResponseHandlerExecuteCallsWrapper() throws ResponseException {
        setupAnalytics();
        ResponseHandler<Response> handler = mock(ResponseHandler.class);
        request.execute(handler);
        verify(wrappedRequest, times(1))
                .execute(any(AnalyticsResponseHandler.class));
    }

    @Test
    public void requestExecutePublishesEvent() throws ResponseException {
        setupAnalytics();
        request.execute();
        verify(publisher, times(1))
                .publish(any(ApplinksRequestExecutionEvent.class));
    }

    @Test
    public void requestExecuteCallsWrapper() throws ResponseException {
        setupAnalytics();
        request.execute();
        verify(wrappedRequest, times(1))
                .execute();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void requestResponseHandlerExecuteAndReturnPublishesEvent() throws ResponseException {
        setupAnalytics();
        request.executeAndReturn(mock(ReturningResponseHandler.class));
        verify(publisher, times(1))
                .publish(any(ApplinksRequestExecutionEvent.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void requestResponseHandlerExecuteAndReturnCallsWrapper() throws ResponseException {
        setupAnalytics();
        ReturningResponseHandler<Response, String> handler = mock(ReturningResponseHandler.class);
        request.executeAndReturn(handler);
        verify(wrappedRequest, times(1))
                .executeAndReturn(any(ReturningAnalyticsResponseHandler.class));
    }

    @Test
    public void stringBodyRequestSize() throws ResponseException {
        setupAnalytics();
        String body = "hey it's me";
        doReturn(wrappedRequest).when(wrappedRequest).setRequestBody(anyString());
        request.setRequestBody(body);
        request.execute();
        ArgumentCaptor<ApplinksRequestExecutionEvent> captor
                = ArgumentCaptor.forClass(ApplinksRequestExecutionEvent.class);
        verify(publisher).publish(captor.capture());
        assertEquals(body.getBytes().length, captor.getValue().getApproxRequestSize().longValue());
    }

    @Test
    public void stringBodyRequestSizeWithHeaders() throws ResponseException {
        setupAnalytics();
        String body = "hey it's me";
        doReturn(wrappedRequest).when(wrappedRequest).setRequestBody(anyString());
        request.setRequestBody(body);
        doReturn(mockSingleHeader()).when(wrappedRequest).getHeaders();
        request.execute();
        ArgumentCaptor<ApplinksRequestExecutionEvent> captor
                = ArgumentCaptor.forClass(ApplinksRequestExecutionEvent.class);
        verify(publisher).publish(captor.capture());
        assertEquals(body.getBytes().length + mockSingleHeaderLength(),
                captor.getValue().getApproxRequestSize().longValue());
    }

    @Test
    public void setEntityRequestSizeNullWithHeaders() throws ResponseException {
        setupAnalytics();
        Object body = mock(Object.class);
        doReturn(wrappedRequest).when(wrappedRequest).setEntity(any());
        request.setEntity(body);
        doReturn(mockSingleHeader()).when(wrappedRequest).getHeaders();
        request.execute();
        ArgumentCaptor<ApplinksRequestExecutionEvent> captor
                = ArgumentCaptor.forClass(ApplinksRequestExecutionEvent.class);
        verify(publisher).publish(captor.capture());
        assertNull(captor.getValue().getApproxRequestSize());
    }

    private RequestFilePart mockFile(long length) {
        RequestFilePart requestfilePart = mock(RequestFilePart.class);
        File file = mock(File.class);
        doReturn(length).when(file).length();
        doReturn(file).when(requestfilePart).getFile();
        return requestfilePart;
    }

    private Map<String, List<String>> mockSingleHeader() {
        Map<String, List<String>> headers = new HashMap<>();
        headers.put("name", new ArrayList<>());
        headers.get("name").add("value");
        return headers;
    }

    private long mockSingleHeaderLength() {
        String name = "name";
        String value = "value";
        return name.getBytes().length + value.getBytes().length;
    }

    private void setupAnalytics() {
        doReturn(REMOTE_ID_STRING).when(remoteLinkId).get();
        doReturn(remoteLinkId).when(remoteLink).getId();
    }
}
