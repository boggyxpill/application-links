package com.atlassian.applinks.core.v1.rest.ui;

import com.atlassian.applinks.analytics.ApplinksCreatedEventFactory;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.auth.OrphanedTrustDetector;
import com.atlassian.applinks.core.rest.RestResourceTestUtils;
import com.atlassian.applinks.core.rest.model.ApplicationLinkEntity;
import com.atlassian.applinks.core.rest.model.ConfigurationFormValuesEntity;
import com.atlassian.applinks.core.rest.model.CreateApplicationLinkRequestEntity;
import com.atlassian.applinks.core.rest.model.ErrorListEntity;
import com.atlassian.applinks.core.rest.ui.CreateApplicationLinkUIResource;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.UUID;

import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CreateApplicationLinkUIResourceTest {
    @Mock
    MutatingApplicationLinkService applicationLinkService;
    @Mock
    RequestFactory requestFactory;
    @Mock
    InternalHostApplication internalHostApplication;
    @Mock
    I18nResolver i18nResolver;
    @Mock
    InternalTypeAccessor typeAccessor;
    @Mock
    ManifestRetriever manifestRetriever;
    @Mock
    RestUrlBuilder restUrlBuilder;
    @Mock
    OrphanedTrustDetector orphanedTrustDetector;
    @Mock
    PluginAccessor pluginAccessor;
    @Mock
    UserManager userManager;
    @Mock
    MutableApplicationLink applicationLink;
    @Mock
    private ApplinksCreatedEventFactory applinksCreatedEventFactory;
    @Mock
    private EventPublisher eventPublisher;

    CreateApplicationLinkUIResource resource;
    ApplicationType dummyType;

    @Before
    public void createService() throws Exception {
        URI uri = URI.create("http://localhost");
        resource = new CreateApplicationLinkUIResource(applicationLinkService, requestFactory, internalHostApplication,
                i18nResolver, typeAccessor, manifestRetriever, restUrlBuilder,
                orphanedTrustDetector, pluginAccessor, userManager, applinksCreatedEventFactory, eventPublisher);

        dummyType = RestResourceTestUtils.getDummyApplicationType();

        when(applicationLink.getId()).thenReturn(ApplicationIdUtil.generate(uri));
        when(applicationLink.getType()).thenReturn(dummyType);
        when(applicationLink.getName()).thenReturn("name");
        when(applicationLink.getDisplayUrl()).thenReturn(uri);
        when(applicationLink.getRpcUrl()).thenReturn(uri);

        when(typeAccessor.loadApplicationType(any(TypeId.class))).thenReturn(dummyType);
        when(applicationLinkService.getApplicationLinks()).thenReturn(emptyList());
        when(applicationLinkService.createApplicationLink(any(), any())).thenReturn(applicationLink);
        when(applicationLinkService.createSelfLinkFor(any(ApplicationId.class))).thenReturn(uri);
        when(i18nResolver.getText("applinks.error.only.sysadmin.operation")).thenReturn("error text");

        when(userManager.getRemoteUsername()).thenReturn("username");
        when(userManager.isSystemAdmin("username")).thenReturn(false);
    }

    @Test
    public void verifyNonSysadminCannotCreateTrustedAppLink() {
        Response result = resource.createApplicationLink(createRequestEntity(true));
        assertNotNull(result);

        ErrorListEntity error = ((ErrorListEntity) result.getEntity());

        assertEquals(HttpServletResponse.SC_BAD_REQUEST, result.getStatus());
        assertEquals("error text", error.getErrors().get(0));
        assertEquals("same-userbase", error.getFields().get(0));
    }

    @Test
    public void verifySysadminCanCreateTrustedAppLink() {
        when(userManager.isSystemAdmin("username")).thenReturn(true);
        Response result = resource.createApplicationLink(createRequestEntity(true));
        assertNotNull(result);
        assertEquals(HttpServletResponse.SC_OK, result.getStatus());
    }

    @Test
    public void verifyAdminCanCreateOAuthAppLink() {
        when(userManager.isSystemAdmin("username")).thenReturn(false);
        Response result = resource.createApplicationLink(createRequestEntity(false));
        assertNotNull(result);
        assertEquals(HttpServletResponse.SC_OK, result.getStatus());
    }

    private CreateApplicationLinkRequestEntity createRequestEntity(boolean shareUserBase) {
        URI uri = URI.create("http://localhost");
        ApplicationLinkEntity linkEntity = new ApplicationLinkEntity(new ApplicationId(UUID.randomUUID().toString()),
                new TypeId("type"), "name", uri, uri, uri, uri, true, false, null);
        ConfigurationFormValuesEntity configEntity = new ConfigurationFormValuesEntity(true, shareUserBase);
        return new CreateApplicationLinkRequestEntity(linkEntity, "admin", "admin", false, uri, true, configEntity);
    }
}