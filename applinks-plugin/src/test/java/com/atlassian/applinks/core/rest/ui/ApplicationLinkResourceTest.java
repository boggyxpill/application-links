package com.atlassian.applinks.core.rest.ui;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.spi.link.MutatingEntityLinkService;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.message.I18nResolver;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

public class ApplicationLinkResourceTest {
    @Rule public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock private PluginAccessor pluginAccessor;
    @Mock private ApplicationLinkService applicationLinkService;
    @Mock private I18nResolver i18nResolver;
    @Mock private MutatingEntityLinkService mutatingEntityLinkService;
    @Mock private InternalHostApplication internalHostApplication;
    @Mock private TypeAccessor typeAccessor;

    @InjectMocks private ApplicationLinkInfoResource resource;

    @Test
    public void returnsNotFoundWhenTypeNotInstalled() throws TypeNotInstalledException {
        ApplicationId mockAppId = mock(ApplicationId.class);
        doThrow(new TypeNotInstalledException("testType", "testName", null))
                .when(applicationLinkService)
                .getApplicationLink(mockAppId);
        doReturn("notInstalled")
                .when(i18nResolver)
                .getText(anyString(), any());

        Response response = resource.getConfiguredAuthenticationTypesAndEntityLinksForApplicationLink(mockAppId);
        assertEquals(NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void returnsNotFoundWhenApplinkNull() throws TypeNotInstalledException {
        ApplicationId mockAppId = mock(ApplicationId.class);
        doReturn(null)
                .when(applicationLinkService)
                .getApplicationLink(mockAppId);

        Response response = resource.getConfiguredAuthenticationTypesAndEntityLinksForApplicationLink(mockAppId);
        assertEquals(NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void returnsOkOtherwise() throws TypeNotInstalledException {
        ApplicationId mockAppId = mock(ApplicationId.class);
        doReturn(mock(ApplicationLink.class))
                .when(applicationLinkService)
                .getApplicationLink(mockAppId);

        Response response = resource.getConfiguredAuthenticationTypesAndEntityLinksForApplicationLink(mockAppId);
        assertEquals(OK.getStatusCode(), response.getStatus());
    }
}
