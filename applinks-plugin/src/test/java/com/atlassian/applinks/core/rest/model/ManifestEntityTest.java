package com.atlassian.applinks.core.rest.model;

import java.net.URI;

import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.core.rest.RestResourceTestUtils;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.sal.api.ApplicationProperties;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ManifestEntityTest {
    private static final URI dummyIconUrl = URI.create("http://localhost");
    @Mock
    InternalHostApplication internalHostApp;
    @Mock
    ApplicationProperties applicationProperties;
    @Mock
    AppLinkPluginUtil pluginUtil;

    @Before
    public void setup() {
        initMocks(this);
        when(internalHostApp.getType()).thenReturn(RestResourceTestUtils.getDummyApplicationType());
        when(internalHostApp.getSupportedInboundAuthenticationTypes()).thenReturn(Lists.<Class<? extends AuthenticationProvider>>newArrayList());
    }

    @Test
    public void getIconUrl() throws Exception {
        ManifestEntity entity = new ManifestEntity(internalHostApp, applicationProperties, pluginUtil);
        assertEquals(dummyIconUrl, entity.getIconUrl());
    }
}
