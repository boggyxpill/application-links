package com.atlassian.applinks.core.util;

import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for the redirect helper class.
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class RedirectHelperTest {

    private static final String BASE_URL = "http://localhost:8080/";
    private static final String REQUEST_URL = BASE_URL + "servlet";
    @Mock
    private Response response;

    @Test
    public void willGetNextRedirectLocation() {
        when(response.getHeader("location")).thenReturn("http://anotherhost/redirected");
        RedirectHelper helper = new RedirectHelper(REQUEST_URL);
        String result = helper.getNextRedirectLocation(response);

        assertEquals("http://anotherhost/redirected", result);
        verify(response).getHeader("location");
    }

    @Test
    public void willGetNextRedirectRelativeLocationAsAbsoluteUrl() {
        when(response.getHeader("location")).thenReturn("/redirected");
        RedirectHelper helper = new RedirectHelper(REQUEST_URL);
        String result = helper.getNextRedirectLocation(response);

        assertEquals(BASE_URL + "redirected", result);
        verify(response).getHeader("location");
    }

    @Test
    public void willVerifyResponseShouldRedirect() {
        when(response.getStatusCode()).thenReturn(302);
        when(response.getHeader("location")).thenReturn("redirected");
        RedirectHelper helper = new RedirectHelper(REQUEST_URL);

        assertTrue(helper.responseShouldRedirect(response));
    }

    @Test
    public void willVerifyResponseWillNotRedirectAsExceedsMaximum() {
        when(response.getStatusCode()).thenReturn(302);
        when(response.getHeader("location")).thenReturn("redirected");

        RedirectHelper helper = new RedirectHelper(REQUEST_URL);

        assertTrue(helper.responseShouldRedirect(response));
        helper.getNextRedirectLocation(response);
        assertTrue(helper.responseShouldRedirect(response));
        helper.getNextRedirectLocation(response);
        assertTrue(helper.responseShouldRedirect(response));
        helper.getNextRedirectLocation(response);
        assertFalse(helper.responseShouldRedirect(response));
    }

    @Test
    public void willVerifyResponseWillNotRedirectAsNullLocation() {
        when(response.getStatusCode()).thenReturn(302);
        when(response.getHeader("location")).thenReturn(null);
        RedirectHelper helper = new RedirectHelper(REQUEST_URL);

        assertFalse(helper.responseShouldRedirect(response));
    }

    @Test
    public void willVerifyResponseWillNotRedirectAsEmptyLocation() {
        when(response.getStatusCode()).thenReturn(302);
        when(response.getHeader("location")).thenReturn("");
        RedirectHelper helper = new RedirectHelper(REQUEST_URL);

        assertFalse(helper.responseShouldRedirect(response));
    }

    @Test
    public void willVerifyResponseWillNotRedirectAsBlankSpaceLocation() {
        when(response.getStatusCode()).thenReturn(302);
        when(response.getHeader("location")).thenReturn(" ");
        RedirectHelper helper = new RedirectHelper(REQUEST_URL);

        assertFalse(helper.responseShouldRedirect(response));
    }

    @Test
    public void willVerifyResponseWillNotRedirectAsIncorrectStatusCode() {
        when(response.getStatusCode()).thenReturn(299);
        when(response.getHeader("location")).thenReturn("redirected");
        RedirectHelper helper = new RedirectHelper(REQUEST_URL);

        assertFalse(helper.responseShouldRedirect(response));
    }

    @Test
    public void willGetNextRedirectLocationWithNoPortInOriginalUrl() {
        when(response.getHeader("location")).thenReturn("/redirected");
        RedirectHelper helper = new RedirectHelper("https://localhost/servlet");
        String result = helper.getNextRedirectLocation(response);

        assertEquals("https://localhost/redirected", result);
        verify(response).getHeader("location");
    }
}
