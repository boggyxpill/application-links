package com.atlassian.applinks.core.auth;

import java.net.URI;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.auth.Anonymous;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.ImpersonatingAuthenticationProvider;
import com.atlassian.applinks.api.auth.NonImpersonatingAuthenticationProvider;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ApplicationLinkRequestFactoryFactoryImplTest {
    private static final String USERNAME = "username";

    @Mock
    private RequestFactory<Request<?, ?>> delegateFactory;
    @Mock
    private UserManager userManager;
    @Mock
    private AuthenticatorAccessor authenticatorAccessor;
    @Mock
    private ApplicationLink link;
    @Mock
    private ImpersonatingAuthenticationProvider impersonatingAuthenticationProvider;
    @Mock
    private ApplicationLinkRequestFactory impersonatingRequestFactory;
    @Mock
    private NonImpersonatingAuthenticationProvider nonImpersonatingAuthenticationProvider;
    @Mock
    private ApplicationLinkRequestFactory nonImpersonatingRequestFactory;
    @Mock
    private EventPublisher publisher;
    private ApplicationLinkRequestFactoryFactoryImpl factoryFactory;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        when(impersonatingAuthenticationProvider.getRequestFactory(USERNAME)).thenReturn(impersonatingRequestFactory);
        when(impersonatingAuthenticationProvider.getRequestFactory(null)).thenReturn(impersonatingRequestFactory);
        when(nonImpersonatingAuthenticationProvider.getRequestFactory()).thenReturn(nonImpersonatingRequestFactory);

        // over-ride toString() to make error messages a bit nicer
        when(delegateFactory.toString()).thenReturn("Anonymous");
        when(impersonatingRequestFactory.toString()).thenReturn("Impersonating");
        when(nonImpersonatingRequestFactory.toString()).thenReturn("Non-Impersonating");

        factoryFactory = new ApplicationLinkRequestFactoryFactoryImpl(delegateFactory, userManager, authenticatorAccessor, publisher);
    }

    @Test
    public void testCreatingAnonymousRequestFactory() throws Exception {
        assertNonWrappedFactoryReturnedForGetAnonymousRequestFactory();
    }

    @Test
    public void testCreatingImpersonatingRequestFactory() throws Exception {
        when(authenticatorAccessor.getAuthenticationProvider(link, ImpersonatingAuthenticationProvider.class))
                .thenReturn(impersonatingAuthenticationProvider);
        when(impersonatingAuthenticationProvider.getRequestFactory(USERNAME)).thenReturn(impersonatingRequestFactory);
        when(userManager.getRemoteUsername()).thenReturn(USERNAME);

        assertWrappedFactoryReturnedForSpecifiedClass(impersonatingRequestFactory, ImpersonatingAuthenticationProvider.class);

        verify(authenticatorAccessor).getAuthenticationProvider(same(link), same(ImpersonatingAuthenticationProvider.class));
        verify(userManager).getRemoteUsername();
        verifyNoMoreInteractions(authenticatorAccessor);
    }

    @Test
    public void testCreatingImpersonatingRequestFactoryWhenConfiguredButNoLoggedInUser() throws Exception {
        assertNull(factoryFactory.getApplicationLinkRequestFactory(link, ImpersonatingAuthenticationProvider.class));

        verify(userManager).getRemoteUsername();
        verifyZeroInteractions(authenticatorAccessor);
        verifyZeroInteractions(impersonatingAuthenticationProvider);
    }

    @Test
    public void testCreatingNonImpersonatingRequestFactory() throws Exception {
        when(authenticatorAccessor.getAuthenticationProvider(link, NonImpersonatingAuthenticationProvider.class))
                .thenReturn(nonImpersonatingAuthenticationProvider);
        when(nonImpersonatingAuthenticationProvider.getRequestFactory()).thenReturn(nonImpersonatingRequestFactory);

        assertWrappedFactoryReturnedForSpecifiedClass(nonImpersonatingRequestFactory, NonImpersonatingAuthenticationProvider.class);

        // non-impersonating request factories, _when requested directly_, should not care if there is a
        // context user or not; otherwise, it prevents 2LO from being used by background processes
        verify(authenticatorAccessor).getAuthenticationProvider(same(link), same(NonImpersonatingAuthenticationProvider.class));
        verifyNoMoreInteractions(authenticatorAccessor);
        verifyZeroInteractions(userManager); // should not check for a context user
    }

    @Test
    public void testOverridingUnconfiguredProvider() throws Exception {
        when(authenticatorAccessor.getAuthenticationProvider(link, ImpersonatingAuthenticationProvider.class))
                .thenReturn(impersonatingAuthenticationProvider);

        assertNull(factoryFactory.getApplicationLinkRequestFactory(link, NonImpersonatingAuthenticationProvider.class));
    }

    @Test
    public void testDefaultToImpersonatingAuthenticatorWhenLoggedInUser() throws Exception {
        when(authenticatorAccessor.getAuthenticationProvider(link, NonImpersonatingAuthenticationProvider.class))
                .thenReturn(nonImpersonatingAuthenticationProvider); //shouldn't be called
        when(authenticatorAccessor.getAuthenticationProvider(link, ImpersonatingAuthenticationProvider.class))
                .thenReturn(impersonatingAuthenticationProvider);
        when(userManager.getRemoteUsername()).thenReturn(USERNAME);

        assertWrappedFactoryReturned(impersonatingRequestFactory);

        verify(authenticatorAccessor, never()).getAuthenticationProvider(same(link), same(NonImpersonatingAuthenticationProvider.class));
        verify(userManager).getRemoteUsername();
    }

    @Test
    public void testDefaultToNonImpersonatingAuthenticatorWhenLoggedInUserAndImpersonatingAuthenticatorNotAvailable() {
        when(authenticatorAccessor.getAuthenticationProvider(link, NonImpersonatingAuthenticationProvider.class))
                .thenReturn(nonImpersonatingAuthenticationProvider);
        when(userManager.getRemoteUsername()).thenReturn(USERNAME);

        assertWrappedFactoryReturned(nonImpersonatingRequestFactory);

        verify(authenticatorAccessor).getAuthenticationProvider(same(link), same(ImpersonatingAuthenticationProvider.class));
        verify(authenticatorAccessor).getAuthenticationProvider(same(link), same(NonImpersonatingAuthenticationProvider.class));
        verify(userManager).getRemoteUsername();
    }

    @Test
    public void testDefaultToAnonymousAuthenticatorWhenLoggedInUserAndNoAuthenticationProvidersAreAvailable() {
        when(userManager.getRemoteUsername()).thenReturn(USERNAME);

        assertNonWrappedFactoryReturned();

        verify(authenticatorAccessor).getAuthenticationProvider(same(link), same(ImpersonatingAuthenticationProvider.class));
        verify(authenticatorAccessor).getAuthenticationProvider(same(link), same(NonImpersonatingAuthenticationProvider.class));
        verify(userManager).getRemoteUsername();
    }

    @Test
    public void testDefaultToAnonymousWhenNoLoggedInUserAlthoughAuthenticationProvidersAreAvailable() throws Exception {
        when(authenticatorAccessor.getAuthenticationProvider(link, NonImpersonatingAuthenticationProvider.class))
                .thenReturn(nonImpersonatingAuthenticationProvider); // shouldn't be called
        when(authenticatorAccessor.getAuthenticationProvider(link, ImpersonatingAuthenticationProvider.class))
                .thenReturn(impersonatingAuthenticationProvider);    // shouldn't be called

        assertNonWrappedFactoryReturned();

        verify(userManager).getRemoteUsername();
        verifyZeroInteractions(authenticatorAccessor);
    }

    @Test
    public void testDefaultToAnonymousWhenNoAuthenticatorsConfigured() throws Exception {
        when(userManager.getRemoteUsername()).thenReturn(USERNAME);

        assertNonWrappedFactoryReturnedForGetAnonymousRequestFactory();

        when(userManager.getRemoteUsername()).thenReturn(null); // behaviour here should be consistent regardless of context user

        assertNonWrappedFactoryReturned();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnsupportedAuthenticationProvider() {
        try {
            factoryFactory.getApplicationLinkRequestFactory(link, UnsupportedAuthenticationProvider.class);
        } finally {
            verifyZeroInteractions(authenticatorAccessor);
            verifyZeroInteractions(delegateFactory);
            verifyZeroInteractions(userManager);
        }
    }

    @Test
    public void testValidAbsoluteUris() throws Exception {
        when(link.getRpcUrl()).thenReturn(new URI("http://absolute.net"));
        assertUri("http://absolute.net", "http://absolute.net");

        when(link.getRpcUrl()).thenReturn(new URI("http://localhost:5990"));
        assertUri("http://localhost:5990/refapp", "http://localhost:5990/refapp");
    }


    @Test
    public void testInvalidAbsoluteUri() throws Exception {
        when(link.getRpcUrl()).thenReturn(new URI("http://rpc-host.net"));
        try {
            assertUri("http://non-rpc-host.net", "http://non-rpc-host.net");
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Request url 'http://non-rpc-host.net' isn't in the same origin as the rpc url 'http://rpc-host.net'", e.getMessage());
        }
    }

    @Test
    public void testInvalidAbsoluteUriWithAuth() throws Exception {
        when(link.getRpcUrl()).thenReturn(new URI("http://rpc-host.net"));
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Request url 'http://rpc-host.net@non-rpc-host.net' isn't in the same origin as the rpc url 'http://rpc-host.net'");
        assertUri("http://rpc-host.net@non-rpc-host.net", "http://rpc-host.net@non-rpc-host.net");
    }

    @Test
    public void testRelativeUri() throws Exception {
        final URI rpcUrl = new URI("http://localhost:5990/refapp");
        when(link.getRpcUrl()).thenReturn(rpcUrl);
        assertUri(rpcUrl.toString() + "/com/atlassian/applinks/core/rest/applinks/manifest", "/com/atlassian/applinks/core/rest/applinks/manifest");
        assertUri(rpcUrl.toString() + "/com/atlassian/applinks/core/rest/applinks/manifest", "com/atlassian/applinks/core/rest/applinks/manifest"); // no leading slash
    }

    private void assertUri(final String expected, final String uri) throws Exception {
        final ApplicationLinkRequestFactory delegateFactory = mock(ApplicationLinkRequestFactory.class);
        new ApplicationLinkRequestFactoryFactoryImpl.AbsoluteURLRequestFactory(delegateFactory, link).createRequest(Request.MethodType.GET, uri);
        verify(delegateFactory).createRequest(Request.MethodType.GET, expected);
    }

    private ApplicationLinkRequestFactory unwrapAnalyticsFactory(final ApplicationLinkRequestFactory factory) {
        return ((ApplicationLinkAnalyticsRequestFactory) factory).wrappedFactory;
    }

    private void assertNonWrappedFactoryReturnedForGetAnonymousRequestFactory() {
        assertEquals(ApplicationLinkRequestFactoryFactoryImpl.SalRequestFactoryAdapter.class,
                ((ApplicationLinkRequestFactoryFactoryImpl.AbsoluteURLRequestFactory) unwrapAnalyticsFactory(factoryFactory
                        .getApplicationLinkRequestFactory(link, Anonymous.class))).requestFactory.getClass());
    }

    private void assertNonWrappedFactoryReturned() {
        assertEquals(ApplicationLinkRequestFactoryFactoryImpl.SalRequestFactoryAdapter.class,
                ((ApplicationLinkRequestFactoryFactoryImpl.AbsoluteURLRequestFactory) unwrapAnalyticsFactory(factoryFactory
                        .getApplicationLinkRequestFactory(link))).requestFactory.getClass());
    }

    private void assertWrappedFactoryReturned(final ApplicationLinkRequestFactory factory) {
        assertEquals(factory, ((ApplicationLinkRequestFactoryFactoryImpl.AbsoluteURLRequestFactory) unwrapAnalyticsFactory(factoryFactory.getApplicationLinkRequestFactory(link))).requestFactory);
    }

    private void assertWrappedFactoryReturnedForSpecifiedClass(final ApplicationLinkRequestFactory factory, final Class<? extends AuthenticationProvider> clazz) {
        assertEquals(factory, ((ApplicationLinkRequestFactoryFactoryImpl.AbsoluteURLRequestFactory) unwrapAnalyticsFactory(factoryFactory.getApplicationLinkRequestFactory(link, clazz))).requestFactory);
    }

    private interface UnsupportedAuthenticationProvider extends AuthenticationProvider {
        // this interface is empty; it is only used to verify how unexpected AuthenticationProvider classes are handled
    }
}
