package com.atlassian.applinks.test.matchers.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;

public final class ApplicationVersionMatchers {
    private ApplicationVersionMatchers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<ApplicationVersion> hasVersionNumber(int major, int minor, int bugfix) {
        return allOf(withMajor(major), withMinor(minor), withBugfix(bugfix), withNoSuffix());
    }

    @Nonnull
    public static Matcher<ApplicationVersion> withMajorThat(@Nonnull Matcher<Integer> majorMatcher) {
        return new FeatureMatcher<ApplicationVersion, Integer>(majorMatcher, "major component that", "major") {
            @Override
            protected Integer featureValueOf(ApplicationVersion actual) {
                return actual.getMajor();
            }
        };
    }

    @Nonnull
    public static Matcher<ApplicationVersion> withMajor(int expectedMajor) {
        return withMajorThat(is(expectedMajor));
    }

    @Nonnull
    public static Matcher<ApplicationVersion> withMinorThat(@Nonnull Matcher<Integer> minorMatcher) {
        return new FeatureMatcher<ApplicationVersion, Integer>(minorMatcher, "minor component that", "minor") {
            @Override
            protected Integer featureValueOf(ApplicationVersion actual) {
                return actual.getMinor();
            }
        };
    }

    @Nonnull
    public static Matcher<ApplicationVersion> withMinor(int expectedMinor) {
        return withMinorThat(is(expectedMinor));
    }

    @Nonnull
    public static Matcher<ApplicationVersion> withBugfixThat(@Nonnull Matcher<Integer> bugfixMatcher) {
        return new FeatureMatcher<ApplicationVersion, Integer>(bugfixMatcher, "bugfix component that", "bugfix") {
            @Override
            protected Integer featureValueOf(ApplicationVersion actual) {
                return actual.getBugfix();
            }
        };
    }

    @Nonnull
    public static Matcher<ApplicationVersion> withBugfix(int expectedBugfix) {
        return withBugfixThat(is(expectedBugfix));
    }

    @Nonnull
    public static Matcher<ApplicationVersion> withSuffixThat(@Nonnull Matcher<String> suffixMatcher) {
        return new FeatureMatcher<ApplicationVersion, String>(suffixMatcher, "suffix component that", "suffix") {
            @Override
            protected String featureValueOf(ApplicationVersion actual) {
                return actual.getSuffix();
            }
        };
    }

    @Nonnull
    public static Matcher<ApplicationVersion> withNoSuffix() {
        return withSuffixThat(isEmptyString());
    }
}
