package com.atlassian.applinks.analytics;

import com.atlassian.applinks.test.rule.MockApplinksRule;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobRunnerKey;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static java.util.concurrent.TimeUnit.DAYS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ApplinkStatusJobTest {

    @Mock
    private SchedulerService schedulerService;
    @Mock
    private ApplinkStatusPublisher applinkStatusPublisher;
    @Mock
    private JobRunnerRequest jobRunnerRequest;

    @InjectMocks
    private ApplinkStatusJob job;

    @Captor
    private ArgumentCaptor<JobConfig> registerCaptor;
    @Captor
    private ArgumentCaptor<JobRunnerKey> unregisterCaptor;

    @Rule
    public final MockApplinksRule mockApplinksRule = new MockApplinksRule(this);

    @Test
    public void shouldRegisterAJobRunnerAndScheduleAJob() throws SchedulerServiceException {
        // invoke
        job.onStart();

        // check
        verify(schedulerService, times(1)).registerJobRunner(anyObject(), eq(job));
        verify(schedulerService, times(1)).scheduleJob(anyObject(), registerCaptor.capture());
        assertThat(
                registerCaptor.getValue().getSchedule().getIntervalScheduleInfo().getIntervalInMillis(),
                is(DAYS.toMillis(1))
        );
    }

    @Test
    public void shouldUnregisterTheJobOnDestroy() {
        // invoke
        job.onStop();

        // check
        verify(schedulerService, times(1)).unregisterJobRunner(unregisterCaptor.capture());
        assertThat(unregisterCaptor.getValue().toString(), is("com.atlassian.applinks.analytics.ApplinkStatusJob"));
    }

    @Test
    public void shouldRunJob() {
        // invoke
        final JobRunnerResponse jobRunnerResponse = job.runJob(jobRunnerRequest);

        // check
        verify(applinkStatusPublisher, times(1)).publishApplinkStatus();
        assertThat(jobRunnerResponse, is(JobRunnerResponse.success()));
    }
}