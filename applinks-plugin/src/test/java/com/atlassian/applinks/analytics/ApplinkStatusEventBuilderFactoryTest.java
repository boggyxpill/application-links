package com.atlassian.applinks.analytics;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.application.bamboo.BambooApplicationTypeImpl;
import com.atlassian.applinks.application.jira.JiraApplicationTypeImpl;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.core.ApplinkStatus;
import com.atlassian.applinks.core.ElevatedPermissionsService;
import com.atlassian.applinks.core.auth.AuthenticatorAccessor;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.sal.api.ApplicationProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.Version;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.EMPTY_LIST;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ApplinkStatusEventBuilderFactoryTest {

    private static final String BUILD_NUMBER = "build-foo";
    private static final String PRODUCT_NAME = "name-foo";
    private static final String PRODUCT_VERSION = "version-foo";

    @Mock
    private ApplicationId applicationId;
    @Mock
    private ApplicationId applinkId1;
    @Mock
    private ApplicationId applinkId2;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private AppLinkPluginUtil appLinkPluginUtil;
    @Mock
    private ApplinkStatus applinkStatus;
    @Mock
    private AuthenticatorAccessor authenticatorAccessor;
    @Mock
    private BambooApplicationTypeImpl bambooType;
    @Mock
    private ElevatedPermissionsService elevatedPermissions;
    @Mock
    private InternalHostApplication internalHostApplication;
    @Mock
    private JiraApplicationTypeImpl jiraType;
    @Mock
    private ReadOnlyApplicationLink applicationLink1;
    @Mock
    private ReadOnlyApplicationLink applicationLink2;
    @Mock
    private Version pluginVersion;

    private Map<String, Integer> expectedOccurrence = new HashMap<>();
    private Map<String, Boolean> expectedAuth = new HashMap<>();

    @InjectMocks
    private ApplinkStatusEventBuilderFactory builderFactory;

    @Before
    public void setUp() {
        when(applicationProperties.getBuildNumber()).thenReturn(BUILD_NUMBER);
        when(applicationProperties.getVersion()).thenReturn(PRODUCT_VERSION);
        when(applicationProperties.getPlatformId()).thenReturn(PRODUCT_NAME);
        when(internalHostApplication.getId()).thenReturn(applicationId);
        when(appLinkPluginUtil.getVersion()).thenReturn(pluginVersion);
        when(applicationId.get()).thenReturn("application-id-foo");
        when(authenticatorAccessor.getAllAuthenticationProviderPluginModules()).thenReturn(EMPTY_LIST);

        expectedOccurrence.put("bamboo", 0);
        expectedOccurrence.put("confluence", 0);
        expectedOccurrence.put("crowd", 0);
        expectedOccurrence.put("refApp", 0);
        expectedOccurrence.put("fecru", 0);
        expectedOccurrence.put("bitbucket", 0);
        expectedOccurrence.put("generic", 0);
        expectedOccurrence.put("jira", 0);
        expectedOccurrence.put("unknown", 0);

        expectedAuth.put("threeLo", false);
        expectedAuth.put("other", false);
        expectedAuth.put("cors", false);
        expectedAuth.put("trusted", false);
        expectedAuth.put("twoLo", false);
        expectedAuth.put("twoLoi", false);
        expectedAuth.put("basic", false);
    }


    @Test
    public void shouldCreateAnEventContainingDataAboutThePlugin() {
        // setup
        final ApplinkStatusEventBuilderFactory.Builder builder = builderFactory.createBuilder();

        // invoke
        final ApplinkStatusEventBuilderFactory.ApplinkStatusMainEvent applinkStatusMainEvent = builder.buildMainEvent();

        // check
        assertThat(applinkStatusMainEvent.getApplicationId(), is(applicationId.get()));
        assertThat(applinkStatusMainEvent.getBuildNumber(), is(BUILD_NUMBER));
        assertThat(applinkStatusMainEvent.getFailing(), is(0));
        assertThat(applinkStatusMainEvent.getOccurrence(), is(expectedOccurrence));
        assertThat(applinkStatusMainEvent.getPluginVersion(), is(pluginVersion.toString()));
        assertThat(applinkStatusMainEvent.getProduct(), is(PRODUCT_NAME));
        assertThat(applinkStatusMainEvent.getProductVersion(), is(PRODUCT_VERSION));
        assertThat(applinkStatusMainEvent.getTotal(), is(0));
        assertThat(applinkStatusMainEvent.getWorking(), is(0));
    }

    @Test
    public void shouldAccountForgivenApplinksInACorrectManner() throws Exception {
        // setup
        final int[] callCount = {0};
        when(applinkStatus.isWorking()).thenAnswer(invocation -> (callCount[0]++)%3 == 0);
        when(applicationLink1.getType()).thenReturn(bambooType);
        when(applicationLink2.getType()).thenReturn(jiraType);
        when(applicationLink1.getId()).thenReturn(applinkId1);
        when(applicationLink2.getId()).thenReturn(applinkId2);
        when(elevatedPermissions.executeAs(anyObject(), anyObject())).thenReturn(applinkStatus);
        final ApplinkStatusEventBuilderFactory.Builder builder = builderFactory.createBuilder();
        final HashMap<String, Integer> occurrence = new HashMap<>();
        occurrence.putAll(expectedOccurrence);
        occurrence.put("bamboo", 1);
        occurrence.put("jira", 1);


        // invoke
        builder.addApplink(applicationLink1);
        builder.addApplink(applicationLink2);
        final ApplinkStatusEventBuilderFactory.ApplinkStatusMainEvent applinkStatusMainEvent = builder.buildMainEvent();

        // check
        assertThat(applinkStatusMainEvent.getApplicationId(), is(applicationId.get()));
        assertThat(applinkStatusMainEvent.getBuildNumber(), is(BUILD_NUMBER));
        assertThat(applinkStatusMainEvent.getFailing(), is(1));
        assertThat(applinkStatusMainEvent.getOccurrence(), is(occurrence));
        assertThat(applinkStatusMainEvent.getPluginVersion(), is(pluginVersion.toString()));
        assertThat(applinkStatusMainEvent.getProduct(), is(PRODUCT_NAME));
        assertThat(applinkStatusMainEvent.getProductVersion(), is(PRODUCT_VERSION));
        assertThat(applinkStatusMainEvent.getTotal(), is(2));
        assertThat(applinkStatusMainEvent.getWorking(), is(1));
    }

    @Test
    public void shouldProvideDetailedEventsForEachApplink() throws Exception {
        // setup
        final int[] callCount = {0};
        when(applinkStatus.isWorking()).thenAnswer(invocation -> (callCount[0]++)%3 == 0);
        when(applicationLink1.getType()).thenReturn(bambooType);
        when(applicationLink2.getType()).thenReturn(jiraType);
        when(applinkId1.get()).thenReturn("id-bamboo-foo");
        when(applinkId2.get()).thenReturn("id-jira-foo");
        when(applicationLink1.getId()).thenReturn(applinkId1);
        when(applicationLink2.getId()).thenReturn(applinkId2);
        when(elevatedPermissions.executeAs(anyObject(), anyObject())).thenReturn(applinkStatus);
        final ApplinkStatusEventBuilderFactory.Builder builder = builderFactory.createBuilder();

        // invoke
        builder.addApplink(applicationLink1);
        builder.addApplink(applicationLink2);
        final List<ApplinkStatusEventBuilderFactory.ApplinkStatusApplinkEvent> applinkStatusApplinkEvents = builder.buildApplinkEvents();
        final ApplinkStatusEventBuilderFactory.ApplinkStatusApplinkEvent event1 = applinkStatusApplinkEvents.get(0);
        final ApplinkStatusEventBuilderFactory.ApplinkStatusApplinkEvent event2 = applinkStatusApplinkEvents.get(1);

        // check
        assertThat(event1.getProduct(), is(PRODUCT_NAME));
        assertThat(event1.getRemoteProduct(), is("bamboo"));
        assertThat(event1.getRemoteApplicationId(), is(applinkId1.get()));
        assertThat(event1.getApplicationId(), is(applicationId.get()));
        assertThat(event1.getIsWorking(), is(false));
        assertThat(event1.getAuth(), is(expectedAuth));
        assertThat(event1.getErrorCategory(), is(""));
        assertThat(event1.getErrorDetail(), is(""));

        assertThat(event2.getProduct(), is(PRODUCT_NAME));
        assertThat(event2.getRemoteProduct(), is("jira"));
        assertThat(event2.getRemoteApplicationId(), is(applinkId2.get()));
        assertThat(event2.getApplicationId(), is(applicationId.get()));
        assertThat(event2.getIsWorking(), is(true));
        assertThat(event2.getAuth(), is(expectedAuth));
        assertThat(event2.getErrorCategory(), is(""));
        assertThat(event2.getErrorDetail(), is(""));
    }
}