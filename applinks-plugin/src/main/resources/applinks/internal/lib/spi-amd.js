/**
 * Define the Applinks SPI functions as AMD module to facilitate unit testing of some modules
 */
define('applinks/lib/spi', function() {
    return {get:
        function (){
            //required since code that depends on this global is run before the global is defined,
            // can be eliminated once this module is made properly AMD
            return AppLinks.SPI;
    }}
});