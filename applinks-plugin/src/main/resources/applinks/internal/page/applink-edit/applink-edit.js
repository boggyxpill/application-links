define('applinks/page/applink-edit', [
    'aui/form-notification',
    'aui/form-validation',
    'applinks/lib/aui',
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/lib/window',
    'applinks/common/initializer',
    'applinks/common/browser',
    'applinks/common/products',
    'applinks/common/response-handlers',
    'applinks/common/response-status',
    'applinks/common/rest',
    'applinks/common/urls',
    'applinks/component/inline-dialog',
    'applinks/model/applink',
    'applinks/feature/help-link/analytics',
    'applinks/feature/oauth-dance',
    'applinks/feature/oauth-picker',
    'applinks/feature/oauth-picker-model',
    'applinks/feature/status',
    'applinks/feature/status/errors',
    'applinks/feature/status/details'
], function(
    auiFormNotification,
    auiFormValidation,
    AJS,
    $,
    _,
    window,
    Initializer,
    Browser,
    ApplinksProducts,
    ResponseHandlers,
    ResponseStatus,
    ApplinkRest,
    ApplinksUrls,
    InlineDialog,
    ApplinkModel,
    HelpLinkAnalytics,
    OAuthDance,
    OAuthPicker,
    OAuthPickerModel,
    ApplinkStatus,
    ApplinkStatusErrors,
    ApplinkStatusDetails
) {
    // extra data keys required to render the applinks config screen
    var DATA_KEYS = [
        ApplinkModel.DataKeys.CONFIG_URL,
        ApplinkModel.DataKeys.ICON_URI
    ];

    var ArrowDirection = {
        RIGHT: 'right',
        LEFT: 'left'
    };

    function setApplinkLoaded() {
        $('#applink-edit-header').attr('data-applink-loaded', true);
    }

    function isAui59OrLater() {
        return !AJS.versionDetails.is58;
    }

    return {
        init: function() {
            Initializer.init(HelpLinkAnalytics);

            this.dialogs = [];
            this.outgoingOAuthFirstSelection = true;
            this.incomingOAuthFirstSelection = true;

            this.form = $('form[data-applink-id]');
            this.cancelButton = this.form.find('#edit-cancel');

            var applinkId = this.form.attr('data-applink-id');

            this.startSpinners();
            this.initializeTable();

            var applink = this.model = new ApplinkModel.Applink({id: applinkId}, {dataKeys: DATA_KEYS});
            applink.on('change', this.update, this);
            applink.on('change:status', this.onStatusChange, this);
            applink.fetch()
                .always(this.stopSpinners.bind(this))
                .done(setApplinkLoaded, _.bind(this.onStatusChange, this))
                .fail(ResponseHandlers.fail());

            applink.fetchStatus();
            this.onStatusChange();

            this.form.on('submit', this.saveChanges.bind(this));
            this.cancelButton.attr('href', ApplinksUrls.Local.admin());
            this.cancelButton.on('click', this.cancel.bind(this));
            // wire up OAuth dance for Authenticate links
            new OAuthDance(this.form, ApplinkStatus.AUTHENTICATE_LINK_SELECTOR)
                .onSuccess(_.bind(applink.fetchStatus, applink))
                .initialize();
        },

        initializeTable: function() {
            this.form.find('.local-auth').addClass( isAui59OrLater() ? 'local-auth-59' : 'local-auth-58');
            this.form.find('.arrow-indicator').addClass( isAui59OrLater() ? 'arrow-indicator-59' : 'arrow-indicator-58');
        },

        update: function (model) {
            $('#application-name')[0].value = model.attributes.name || '';
            $('#application-url')[0].value = model.attributes.rpcUrl || '' ;
            $('#display-url')[0].value = model.attributes.displayUrl || '';
        },

        cancel: function () {
            this.update(this.model);
            this.resetValidationState();
        },

        onStatusChange: function () {
            var applinkModel = this.model;
            var applinkObject = applinkModel.toJSON();

            this.renderStatusLozenge(applinkObject);
            this.renderStatusDetails(applinkObject, applinkModel);
            this.updateLocalAuth(applinkObject);
            this.updateRemoteAuth(applinkObject, applinkModel);
        },

        renderStatusLozenge: function(applinkObject) {
            var header = $('#applink-edit-header');

            // remove any previous status lozenge
            header.find('.status-lozenge').remove();
            new ApplinkStatus.View({
                applink: applinkObject,
                container: header,
                clickable: false
            });
        },

        renderStatusDetails: function(applinkObject, applinkModel) {
            var statusDetailsContainer = $('#applink-status-details-container');
            statusDetailsContainer.empty();

            if (applinkObject.statusLoaded) {
                new ApplinkStatusDetails.Notification({
                    applink: applinkModel,
                    container: statusDetailsContainer
                });
            }
        },

        updateLocalAuth: function(applinkObject) {
            if (!applinkObject.statusLoaded) {
                return;
            }
            // reset local auth state
            $('#applink-outgoing-local-auth-section').empty();
            $('#applink-incoming-local-auth-section').empty();

            var status = applinkObject.status;

            this.outgoingOAuthPicker = new OAuthPicker.View('#applink-outgoing-local-auth-section',
                'applink-outgoing-local-auth', OAuthPickerModel.create(status.localAuthentication.outgoing));

            this.incomingOAuthPicker = new OAuthPicker.View('#applink-incoming-local-auth-section',
                'applink-incoming-local-auth', OAuthPickerModel.create(status.localAuthentication.incoming));

            this.outgoingOAuthPicker.onChangeHandler(this.updateOutgoingOAuthArrow.bind(this), applinkObject);
            this.incomingOAuthPicker.onChangeHandler(this.updateIncomingOAuthArrow.bind(this), applinkObject);

            this.initializeArrows(applinkObject);
        },

        /**
         * In AUI 5.8, the single select component triggers the change event when rendered initially with some selected 
         * value.
         * In AUI 5.9, the change event is no longer triggered in that scenario, except for IE/Edge, where the single 
         * select change event is raised as in AUI 5.8.
         *
         * @param applinkObject
         */
        initializeArrows: function(applinkObject) {
            if (isAui59OrLater() && !Browser.isMicrosoft()) {
                this.updateOutgoingOAuthArrow({
                    data: applinkObject
                });
                this.updateIncomingOAuthArrow({
                    data: applinkObject
                });
            }
        },

        updateRemoteAuth: function(applinkObject, applinkModel) {
            if (!applinkObject.statusLoaded || !applinkObject.name || !applinkObject.type) {
                return;
            }
            // reset remote auth state
            var remoteOutgoing = $('#applink-outgoing-remote-auth-section').empty(),
                remoteIncoming = $('#applink-incoming-remote-auth-section').empty();

            var authConfig = applinkObject.status.remoteAuthentication;

            this.renderRemoteOAuthState(remoteOutgoing, applinkObject, applinkModel,
                authConfig ? OAuthPickerModel.create(authConfig.outgoing) : null);

            this.renderRemoteOAuthState(remoteIncoming, applinkObject, applinkModel,
                authConfig ? OAuthPickerModel.create(authConfig.incoming) : null);
        },

        renderRemoteOAuthState: function (container, applinkObject, applinkModel, authConfig) {
            var html = applinks.page.applink.edit.status({
                applink: applinkObject,
                applicationType: ApplinksProducts.getTypeName(applinkObject.type),
                authLevelName: authConfig ? authConfig.getName() : ''
            });
            container.append(html);

            // add authenticate link if auth token is missing
            if (ApplinkStatusErrors.isAccessTokenError(applinkObject.status)) {
                new ApplinkStatus.View({
                    applink: applinkObject,
                    container: container,
                    extraClasses: 'remote-auth-row-item'
                })
            }
            // troubleshooting link
            new ApplinkStatusDetails.HelpLink({
                applink: applinkModel,
                container: container,
                extraClasses: 'remote-auth-row-item'
            })
        },

        saveChanges: function(e) {
            e.preventDefault();
            this.startSpinners();

            var urlConfig = {
                name: $('#application-name')[0].value,
                rpcUrl: $('#application-url')[0].value,
                displayUrl: $('#display-url')[0].value
            };

            var urlOptions = {
                wait: true,
                expectedStatuses: [ResponseStatus.Family.SUCCESSFUL, ResponseStatus.BAD_REQUEST]
            };

            this.resetValidationState();

            this.model.save(urlConfig, urlOptions)
                .fail(this.handleSaveError.bind(this))
                .done(this.handleSaveSuccess.bind(this));

            return false;
        },

        resetValidationState: function() {
            $('#application-name, #application-url, #display-url').removeAttr('data-aui-notification-error');
            $('#aui-message-bar').html('');
        },

        handleSaveSuccess: function () {
            var oauthConfig = JSON.stringify({
                incoming: this.incomingOAuthPicker.getModel(),
                outgoing: this.outgoingOAuthPicker.getModel()
            });

            // Expecting: OK, 403 (non-sysadmin setting up OAuth impersonation) or 409 (unable to enable OAuth because remote cannot be accessed)
            return ApplinkRest.V3.oAuthStatus(this.model.get('id'))
                .expectStatus(ResponseStatus.Family.SUCCESSFUL, ResponseStatus.FORBIDDEN, ResponseStatus.CONFLICT)
                .put(oauthConfig)
                .always(this.stopSpinners.bind(this))
                .done(this.handleOAuthSaveSuccess.bind(this))
                .fail(this.handleOAuthSaveError.bind(this));
        },

        handleSaveError: function (response) {
            this.stopSpinners();
            if (ResponseStatus.BAD_REQUEST.matches(response)) {
                var errors = JSON.parse(response.responseText).errors;
                var contextMap = {
                    name: '#application-name',
                    rpcUrl: '#application-url',
                    displayUrl: '#display-url'
                };

                _.each(errors, function (error) {
                    if (error.context && _.has(contextMap, error.context)) {
                        var input = $(contextMap[error.context]);
                        input.attr('data-aui-notification-error', AJS.escapeHtml(error.summary));
                    } else {
                        AJS.messages.error({
                            body: AJS.escapeHtml(error.summary)
                        });
                    }
                });
            }
        },

        handleOAuthSaveSuccess: function () {
            window.location.href = ApplinksUrls.Local.admin();
        },

        handleOAuthSaveError: function (response) {
            if (ResponseStatus.FORBIDDEN.matches(response) || ResponseStatus.CONFLICT.matches(response)) {
                var errors = JSON.parse(response.responseText).errors;

                _.each(errors, function (error) {
                    AJS.messages.error({
                        body: AJS.escapeHtml(error.summary)
                    });
                });
            }
        },

        startSpinners: function() {
            this.form.find('.button-spinner').spin({
                left: '260px'
            });
        },

        stopSpinners: function() {
            this.form.find('.button-spinner').spinStop();
        },

        updateOutgoingOAuthArrow: function(event) {
            var applinkObject = event.data;

            if (applinkObject.statusLoaded) {
                var status = applinkObject.status;
                var localOutgoingOAuth = this.outgoingOAuthPicker.getModel();
                var remoteIncomingOAuth = this._isValidRemoteAuthentication(status) ? OAuthPickerModel.create(status.remoteAuthentication.incoming) : null;

                this._renderArrow('#applink-outgoing-auth-arrow', ArrowDirection.RIGHT, localOutgoingOAuth,
                        remoteIncomingOAuth, ApplinkStatusErrors.isAccessTokenError(status), this.outgoingOAuthFirstSelection);

                this.outgoingOAuthFirstSelection = false;
            }
        },

        updateIncomingOAuthArrow: function(event) {
            var applinkObject = event.data;

            if (applinkObject.statusLoaded) {
                var status = applinkObject.status;
                var localIncomingOAuth = this.incomingOAuthPicker.getModel();
                var remoteOutgoingOAuth = this._isValidRemoteAuthentication(status) ? OAuthPickerModel.create(status.remoteAuthentication.outgoing) : null;

                this._renderArrow('#applink-incoming-auth-arrow', ArrowDirection.LEFT, localIncomingOAuth,
                        remoteOutgoingOAuth, ApplinkStatusErrors.isAccessTokenError(status), this.incomingOAuthFirstSelection);

                this.incomingOAuthFirstSelection = false;
            }
        },

        _isValidRemoteAuthentication: function(status) {
            // Some network errors will have a valid remote oauth such as OAUTH_TIMESTAMP_REFUSED
            // and in that case we want to show the mismatch arrows, not connected arrows
            return status.remoteAuthentication && !ApplinkStatusErrors.hasErrorCategory(status, 'NETWORK_ERROR');
         },

        _renderArrow: function(container, direction, localOAuth, remoteOAuth, authTokenMissing, firstSelection) {
            var $container = $(container);
            $container.empty();
            this._removeInlineDialog(direction);

            var arrowStatus = this._getArrowStatus(localOAuth, remoteOAuth, authTokenMissing);
            var hasInlineDialog = arrowStatus === 'mismatch' && remoteOAuth;

            var dialogId = 'oauth-mismatch-' + direction + '-dialog';
            var arrowHtml = applinks.page.applink.edit.connectivityArrow({direction: direction, status: arrowStatus,
                                                                    dialogId: dialogId, clickable: hasInlineDialog});
            $container.append(arrowHtml);
            if (hasInlineDialog) {
                $container.find('.arrow-' + direction +'-trigger img').css('cursor', 'pointer');
                this._configureInlineDialogForArrow(direction, dialogId, firstSelection);
            }
        },

        _getArrowStatus: function(localOAuth, remoteOAuth, authTokenMissing) {
            if (remoteOAuth !== null) {
                if (localOAuth.isOAuthDisabled() && remoteOAuth.isOAuthDisabled()) {
                    return 'disabled';
                } else if (localOAuth.equals(remoteOAuth)) {
                    return 'match';
                }
            } else if (authTokenMissing) {
                return 'disabled';
            }
            return 'mismatch';
        },

        _configureInlineDialogForArrow: function(direction, dialogId, firstSelection) {
            var buttonId = 'oauth-mismatch-' + direction + '-btn';
            var contents = applinks.page.applink.edit.oauthMismatchInlineDialogContents({id: buttonId, remoteAppUrl: this.model.getAdminUrl()});

            var dialog = new InlineDialog(dialogId, contents, {
                alignment: (direction === ArrowDirection.RIGHT ? 'top center' : 'bottom center'),
                persistent: true,
                ariaLabel: AJS.I18n.getText('applinks.inline_dialog.action_required_in_remote_app.aria_label'),
            });

            // Use this approach $(element).on(event, selector, handler) rather than $(element).find(selector).on(event, handler) to work in IE11
            dialog.contents().on('click','#' + buttonId, function() {
                dialog.hide();
            });

            var self = this;
            dialog.onShow(function() {
                // close the other direction dialog if open
                self._closeDialog(direction == ArrowDirection.RIGHT ? ArrowDirection.LEFT : ArrowDirection.RIGHT);
                _.defer(function() {
                    dialog.contents().find('#' + buttonId).focus();
                });
                self._removePersistentFlag(dialogId);
            });
            // Accessibility improvement: user can hit space/enter in the arrow to see the inline dialog and then enter key to return
            // the focus to the arrow in FF/Chrome/IE (no luck in Safari)
            dialog.onHide(function() {
                self.form.find('.arrow-' + direction +'-trigger').focus();
            });

            this.dialogs[direction] = dialog;

            !firstSelection && dialog.show();
        },

        _removePersistentFlag: function(dialogId) {
            // When opened via change handler from OAuth picker, the dialog intercepts the click into a picker and closes immediately
            // if persistent flag is set to false - this happens sometimes, not all the times. To prevent this, we open
            // the dialog with persistent set to true and change to false after a short amount of time.
            setTimeout(function() {
                $('#' + dialogId).removeAttr("persistent");
            }, 200);
        },

        _removeInlineDialog: function(direction) {
            if (this.dialogs[direction]) {
                this._closeDialog(direction);
                this.dialogs[direction].contents().remove();
                this.dialogs[direction] = null;
            }
        },

        _closeDialog: function(direction) {
            this.dialogs[direction] && this.dialogs[direction].isVisible() && this.dialogs[direction].hide();
        }
    }
});
