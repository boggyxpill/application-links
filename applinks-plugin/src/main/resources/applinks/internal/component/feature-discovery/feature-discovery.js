define('applinks/component/feature-discovery', [
    'applinks/lib/aui',
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/common/features',
    'applinks/component/inline-dialog'
], function(
    AJS,
    $,
    _,
    ApplinksFeatures,
    InlineDialog
) {
    /**
     * Generic feature discovery component for Applinks.
     *
     * @param featureKey {string} ID of the feature
     * @param contents {string} contents of the feature discovery dialog
     * @param anchor {HTMLElement} DOM element to attach the feature discovery to
     */
    function FeatureDiscovery(featureKey, contents, anchor, ariaLabel) {
        this.featureId = featureKey;
        this.contents = contents;
        this.anchor = anchor;
        this.ariaLabel = ariaLabel;
        this.popupId = 'feature-discovery-' + this.featureId;

        if (!ApplinksFeatures.isDiscovered(featureKey)) {
            this.init();
            ApplinksFeatures.discover(featureKey);
        }
    }

    FeatureDiscovery.prototype.init = function() {
        $(this.anchor).attr('aria-controls', this.popupId);

        this.dialog = new InlineDialog(this.popupId, this.contents, {
            alignment: 'bottom center',
            open: true,
            persistent: true,
            ariaLabel: this.ariaLabel,
        });
        this.dialog.realign();
    };

    /**
     * Dismiss the feature discovery popup if it's being displayed to the user.
     */
    FeatureDiscovery.prototype.dismiss = function() {
        if (this._isDisplayed()) {
            this.dialog.hide();
        }
    };

    /**
     * Attach event hadlers to the feature discovery popup, if it has been displayed. Use the same arguments as for
     * `jQuery.on`
     * @see {@link http://api.jquery.com/on/}
     */
    FeatureDiscovery.prototype.on = function() {
        if (this._isDisplayed()) {
            var $popup = this.dialog.$popup;
            $popup.on.apply($popup, arguments);
        }
    };

    /**
     * Provide selector for an element inside the popup that should dismiss it.
     *
     * @param triggerSelector selector for an element that will dismiss this feature discovery popup
     * @return {Object} this feature discovery
     */
    FeatureDiscovery.prototype.dismissTrigger = function(triggerSelector) {
        this.on('click', triggerSelector, _.bind(this.dismiss, this));
        return this;
    };

    /**
     * Refresh the feature discovery popup (if already showing) and optionally attach to a new `anchor`. This is
     * required in some cases where DOM is modified after the popup has been displayed already, or the anchor is
     * re-rendered or changed.
     *
     * Note: if a new anchor is provided, the entire popup has to be re-rendered, so all event handlers attached to it
     * will have to be re-added to.
     *
     * @param anchor {HTMLElement} optional new anchor for the popup
     * @return {Object} this feature discovery
     */
    FeatureDiscovery.prototype.refresh = function(anchor) {
        if (this._isDisplayed()) {
            if (anchor) {
                // new anchor - need to re-render the entire dialog
                this.anchor = anchor;
                this.dialog.$popup.remove();
                this.init();
            } else {
                // re-align the inline dialog
                this.dialog.realign();
            }
        }

        return this;
    };

    FeatureDiscovery.prototype._isDisplayed = function() {
        return this.dialog && this.dialog.isVisible();
    };

    return FeatureDiscovery;
});