define('applinks/component/inline-dialog', [
    'applinks/lib/lodash',
    'applinks/lib/jquery',
    'applinks/lib/aui',
    'applinks/lib/skate',
    'applinks/common/dialog-focus'
], function(
    _,
    $,
    AJS,
    skate,
    DialogFocus
) {
    function isAuiBefore59() {
        return AJS.versionDetails.is58;
    }

    function InlineDialog(id, contents, options) {
        this.id = id;
        var popupHtml = applinks.inlineDialog.popup(_.extend({
            id: id,
            contents: contents,
            auiVersion: AJS.versionDetails
        }, options));

        this.$popup = $(popupHtml);
        this.popup = this.$popup[0];

        $('body').append(this.$popup);
        if (isAuiBefore59()) {
            // skate will be undefined from AUI 5.9 and later.
            // Note that "applinks/lib/skate" is a wrapper around window.skate and skate is not exposed as a global
            // from AUI 5.9.
            skate.init(this.popup);
        }
        this.onShow(_.partial(DialogFocus.defaultFocus, this.$popup));
    }

    InlineDialog.prototype.hide = function() {
        if (isAuiBefore59()) {
            this.popup.hide();
        } else {
            this.popup.open = false;
        }
    };

    InlineDialog.prototype.show = function() {
        if (isAuiBefore59()) {
            this.popup.show();
        } else {
            this.popup.setAttribute('open', '');
        }
    };

    InlineDialog.prototype.isVisible = function() {
        if (isAuiBefore59()) {
            return this.popup.isVisible();
        } else {
            return this.popup.hasAttribute("open");
        }
    };

    InlineDialog.prototype.realign = function() {
        if (isAuiBefore59()) {
            this.popup.show();
        }
        return this;
    };

    InlineDialog.prototype.on = function() {
        this.$popup.on.apply(this.$popup, arguments);
        return this;
    };

    InlineDialog.prototype.onShow = function(callback) {
        if (isAuiBefore59()) {
            this.$popup.on('aui-layer-show', callback);
        } else {
            this.$popup.on('aui-show', callback)
        }
        return this;
    };

    InlineDialog.prototype.onHide = function(callback) {
        if (isAuiBefore59()) {
            this.$popup.on('aui-layer-hide', callback);
        } else {
            this.$popup.on('aui-hide', callback)
        }
        return this;
    };

    /**
     * @returns {HTMLElement} the DOM element representing this dialog's contents
     */
    InlineDialog.prototype.html = function() {
        return this.popup;
    };

    /**
     * @returns {jQuery} jQuery element representing this dialog's contents
     */
    InlineDialog.prototype.contents = function() {
        return this.$popup;
    };

    return InlineDialog;
});
