define('applinks/common/rest-oauth', [
    'applinks/lib/aui',
    'applinks/common/rest-request',
    'applinks/common/preconditions'
], function(
    AJS,
    ApplinksRestRequest,
    Preconditions
) {
    function ApplinksOAuthRestModule(version) {
        Preconditions.hasValue(version, 'version');
        this.baseUrl =  AJS.contextPath() + '/rest/applinks-oauth/' + version + '/';
    }

    ApplinksOAuthRestModule.prototype._withPath = function(path) {
        return this.baseUrl + path;
    };

    function ApplinksOAuthV1RestModule() {
        this.module = new ApplinksOAuthRestModule('1.0');
    }

    ApplinksOAuthV1RestModule.prototype.consumerToken = function(applinkId) {
        Preconditions.hasValue(applinkId, 'applinkId');
        return new ApplinksRestRequest(this.module._withPath('consumer-token/' + applinkId));
    };

    return {
        V1: new ApplinksOAuthV1RestModule()
    }
});
