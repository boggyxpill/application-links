define('applinks/common/features', [
    'applinks/lib/wrm',
    'applinks/lib/lodash',
    'applinks/common/modules',
    'applinks/common/context',
    'applinks/common/rest'
], function(
    WRM,
    _,
    ApplinksModules,
    ApplinksContext,
    ApplinksRest
) {
    var enabledFeatures, discoveredFeatures;

    function _getEnabledFeatures() {
        if (_.isUndefined(enabledFeatures)) {
            enabledFeatures = WRM.data.claim(ApplinksModules.dataFqn('applinks-common', 'applinks-features'));
        }
        return enabledFeatures;
    }

    function _getDiscoveredFeatures() {
        if (_.isUndefined(discoveredFeatures)) {
            discoveredFeatures = WRM.data.claim(ApplinksModules.dataFqn('applinks-common', 'applinks-discovered-features'));
        }
        return discoveredFeatures;
    }

    function isDiscovered(featureKey) {
        return (_.includes ? _.includes : _.contains)(_getDiscoveredFeatures(), featureKey.toLowerCase());
    }

    function addEnabledFeatures(features) {
        _.extend(_getEnabledFeatures(), features);
    }

    function addDiscoveredFeatures(featureKeys) {
        featureKeys.forEach(function(featureKey){
            if (!isDiscovered(featureKey)) {
                _getDiscoveredFeatures().push(featureKey.toLowerCase());
            }
        });
    }

    /**
     * Allows to query for, enable and discover Applinks features.
     */
    return {

        BITBUCKET_REBRAND: 'BITBUCKET_REBRAND',
        ONE_WAY_APPLINKS: 'ONE_WAY_APPLINKS',
        V3_UI_OPT_IN: 'V3_UI_OPT_IN',
        V3_UI: 'V3_UI',

        /**
         * @returns {Object.<string, Object>} all enabled features mapped by key
         */
        getEnabledFeatures: function() {
            return _getEnabledFeatures();
        },

        isEnabled: function(featureName) {
            return this.getEnabledFeatures()[featureName];
        },

        /**
         * Disable `featureName`, requires user context and admin permissions.
         *
         * @param featureName {string} feature name to enable
         * @return the request promise to hook callbacks to
         */
        disable: function(featureName) {
            ApplinksContext.validateCurrentUser();
            return ApplinksRest.V3.features(featureName).del()
                .done(function() {
                    _getEnabledFeatures()[featureName] = false;
                });
        },

        /**
         * Enable `featureName`, requires user context and admin permissions.
         *
         * @param featureName {string} feature name to enable
         * @return the request promise to hook callbacks to
         */
        enable: function(featureName) {
            ApplinksContext.validateCurrentUser();
            return ApplinksRest.V3.features(featureName).put()
                .done(function(feature) {
                    addEnabledFeatures(feature);
                });
        },

        /**
         * @returns {string[]} an array of discovered feature keys
         */
        getDiscoveredFeatures: function() {
            return _getDiscoveredFeatures();
        },

        isDiscovered: isDiscovered,

        /**
         * Discover `featureKey`, requires user context.
         *
         * @param featureKey {string} key to discover
         */
        discover: function(featureKey) {
            ApplinksContext.validateCurrentUser();
            ApplinksRest.V3.featureDiscovery(featureKey).put()
                .fail(function(req) {
                    throw 'Feature discovery request failed: ' + req.status + ': ' + req.responseText;
                }).done(function(featureKey) {
                    addDiscoveredFeatures(featureKey);
                });
        }
    }
});
