define('applinks/feature/status/errors', [
    'applinks/lib/lodash',
    'applinks/common/preconditions'
], function(
    _,
    Preconditions
) {

    function ApplinkError(type, category, linkKey, sectionKey) {
        this.type = type;
        this.category = category;
        this.linkKey = linkKey;
        this.sectionKey = sectionKey;
    }

    ApplinkError.prototype.equals = function(error) {
        return isErrorObject(error) && 
            this.type === error.type && 
            this.category === error.category;
    };

    /**
     * @param statusOrError status or error object
     * @returns {boolean} `true`, if this error is equal to the error represented by the `status` object
     */
    ApplinkError.prototype.matches = function(statusOrError) {
        if (isStatusObject(statusOrError)) {
            return this.equals(statusOrError.error);
        } else if (isErrorObject(statusOrError)) {
            return this.equals(statusOrError);
        } else {
            return false;
        }
    };

    function isStatusObject(object) {
        return object && object.error && isErrorObject(object.error);
    }

    function isErrorObject(object) {
        return object && object.type && object.category;
    }

    function isApplinkError(object) {
        return object instanceof ApplinkError;
    }

    /**
     * NOTE: the keys and errors below must match `ApplinkErrorType`. Make sure to run ApplinkErrorTypeContractTest
     * after updating this file.
     */
    var Errors =  {
        "CONNECTION_REFUSED": new ApplinkError('CONNECTION_REFUSED', 'NETWORK_ERROR', 'applinks.docs.diagnostics.troubleshoot.connectionrefused', 'connectionrefused'),
        "UNKNOWN_HOST": new ApplinkError('UNKNOWN_HOST', 'NETWORK_ERROR', 'applinks.docs.diagnostics.troubleshoot.unknownhost', 'unknownhost'),
        "SSL_UNTRUSTED": new ApplinkError('SSL_UNTRUSTED', 'NETWORK_ERROR', 'applinks.docs.diagnostics.troubleshoot.ssluntrusted', 'SSLnottrusted'),
        "SSL_HOSTNAME_UNMATCHED": new ApplinkError('SSL_HOSTNAME_UNMATCHED', 'NETWORK_ERROR', 'applinks.docs.diagnostics.troubleshoot.sslunmatched', 'SSLnotmatching'),
        "SSL_UNMATCHED": new ApplinkError('SSL_UNMATCHED', 'NETWORK_ERROR', 'applinks.docs.diagnostics.troubleshoot.sslunmatched', 'SSLnotmatching'),
        "OAUTH_PROBLEM": new ApplinkError('OAUTH_PROBLEM', 'NETWORK_ERROR', 'applinks.docs.diagnostics.troubleshoot.oauthproblem', 'commonerrors'),
        "OAUTH_TIMESTAMP_REFUSED": new ApplinkError('OAUTH_TIMESTAMP_REFUSED', 'NETWORK_ERROR', 'applinks.docs.diagnostics.troubleshoot.oauthtimestamprefused', 'OAuthtimestamp'),
        "OAUTH_SIGNATURE_INVALID": new ApplinkError('OAUTH_SIGNATURE_INVALID', 'NETWORK_ERROR', 'applinks.docs.diagnostics.troubleshoot.oauthsignatureinvalid', 'OAuthinvalidsig'),
        "UNEXPECTED_RESPONSE": new ApplinkError('UNEXPECTED_RESPONSE', 'NETWORK_ERROR', 'applinks.docs.diagnostics.troubleshoot.unexpectedresponse', 'unexpectedresponse'),
        "UNEXPECTED_RESPONSE_STATUS": new ApplinkError('UNEXPECTED_RESPONSE_STATUS', 'NETWORK_ERROR', 'applinks.docs.diagnostics.troubleshoot.unexpectedresponse', 'unexpectedresponse'),

        "AUTH_LEVEL_MISMATCH": new ApplinkError('AUTH_LEVEL_MISMATCH', 'CONFIG_ERROR', 'applinks.docs.diagnostics.troubleshoot.authlevelmismatch', 'OAuthmatch'),
        "NO_REMOTE_APPLINK" : new ApplinkError('NO_REMOTE_APPLINK', 'CONFIG_ERROR'),

        "LOCAL_AUTH_TOKEN_REQUIRED": new ApplinkError('LOCAL_AUTH_TOKEN_REQUIRED', 'ACCESS_ERROR'),
        "REMOTE_AUTH_TOKEN_REQUIRED": new ApplinkError('REMOTE_AUTH_TOKEN_REQUIRED', 'ACCESS_ERROR'),
        "INSUFFICIENT_REMOTE_PERMISSION" : new ApplinkError('INSUFFICIENT_REMOTE_PERMISSION', 'ACCESS_ERROR', 'applinks.docs.configuring.auth.oauth', 'accessdenied_oauth'),

        "REMOTE_VERSION_INCOMPATIBLE" : new ApplinkError('REMOTE_VERSION_INCOMPATIBLE', 'INCOMPATIBLE'),
        "NO_OUTGOING_AUTH" : new ApplinkError('NO_OUTGOING_AUTH', 'INCOMPATIBLE', 'applinks.docs.configuring.auth.oauth'),

        "GENERIC_LINK" : new ApplinkError('GENERIC_LINK', 'NON_ATLASSIAN'),
        "NON_ATLASSIAN" : new ApplinkError('NON_ATLASSIAN', 'NON_ATLASSIAN'),

        "SYSTEM_LINK" : new ApplinkError('SYSTEM_LINK', 'SYSTEM'),

        "AUTH_LEVEL_UNSUPPORTED" : new ApplinkError('AUTH_LEVEL_UNSUPPORTED', 'DEPRECATED', 'applinks.docs.diagnostics.troubleshoot.authlevelunsupported', 'OAuthunsupported'),
        "LEGACY_UPDATE" : new ApplinkError('LEGACY_UPDATE', 'DEPRECATED', 'applinks.docs.diagnostics.troubleshoot.migration', 'autoUpdate'),
        "LEGACY_REMOVAL" : new ApplinkError('LEGACY_REMOVAL', 'DEPRECATED', 'applinks.docs.diagnostics.troubleshoot.migration', 'autoRemove'),
        "MANUAL_LEGACY_UPDATE" : new ApplinkError('MANUAL_LEGACY_UPDATE', 'DEPRECATED', 'applinks.docs.diagnostics.troubleshoot.migration', 'manualUpdate'),
        "MANUAL_LEGACY_REMOVAL" : new ApplinkError('MANUAL_LEGACY_REMOVAL', 'DEPRECATED', 'applinks.docs.diagnostics.troubleshoot.migration', 'manualRemove'),
        "MANUAL_LEGACY_REMOVAL_WITH_OLD_EDIT" : new ApplinkError('MANUAL_LEGACY_REMOVAL_WITH_OLD_EDIT', 'DEPRECATED', 'applinks.docs.diagnostics.troubleshoot.migration', 'manualOldEditRemove'),

        "UNKNOWN" : new ApplinkError('UNKNOWN', 'UNKNOWN', 'applinks.docs.diagnostics.troubleshoot.unknownerror'),

        eachError: function(callback) {
            _.chain(Errors).values().filter(isApplinkError).each(callback);
        },

        /**
         * @param statusOrError status or status error to match by
         * @returns {object} matching status error, or undefined if not found
         */
        find: function(statusOrError) {
            if (isErrorObject(statusOrError)) {
                return Errors[statusOrError.type]
            } else if (isStatusObject(statusOrError)) {
                return Errors[statusOrError.error.type];
            } else {
                return undefined;
            }
        },

        /**
         * @param statusOrError status or status error to match
         * @returns {boolean} `true` if `status` represents an "access token missing" error, `false` otherwise
         */
        isAccessTokenError: function(statusOrError) {
            return Errors.REMOTE_AUTH_TOKEN_REQUIRED.matches(statusOrError) ||
                Errors.LOCAL_AUTH_TOKEN_REQUIRED.matches(statusOrError);
        },

        hasErrorCategory: function(statusOrError, targetCategory) {
            Preconditions.hasValue(targetCategory, 'targetCategory');

            var category = isErrorObject(statusOrError) ? statusOrError.category :
                            (isStatusObject(statusOrError) ? statusOrError.error.category : undefined);
            return category === targetCategory;
        }
    };

    return Errors;
});
