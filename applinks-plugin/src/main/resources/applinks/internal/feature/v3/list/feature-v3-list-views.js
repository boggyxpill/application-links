define('applinks/feature/v3/list-views', [
    'applinks/lib/lodash',
    'applinks/lib/backbone',
    'applinks/lib/aui',
    'applinks/lib/jquery',
    'applinks/lib/window',
    'applinks/common/products',
    'applinks/common/urls'
], function(
    _,
    Backbone,
    AJS,
    $,
    window,
    Products,
    Urls
) {

    // include legacy edit action if "magic" query parameter is present; lazy-load to facilitate unit testing
    function checkLegacyEdit() {
        return window.location.search.indexOf('legacyEdit=true') > 0;  
    }
    var includeLegacyEdit = _.memoize(checkLegacyEdit);
    
    /**
     * Backbone view for table Row.
     */
     var Row = Backbone.View.extend({
        template: applinks.feature.v3.list.templates,

        initialize: function (options) {
            var applink = options.model.toJSON();
            this.$el.html(this.template.row({
                applink: applink,
                typeName: Products.getTypeName(applink.type),
                editUrl: Urls.Local.edit(applink.id),
                includeLegacyEdit: includeLegacyEdit()
            }));
            this.setFirstLinkInDropdownAsActive();
        },

        render: function () {
            return this.$el.find('tr');
        },

        setFirstLinkInDropdownAsActive: function() {
            this.$el.find(".actions-dropdown-content").on({
                "aui-dropdown2-show": function() {
                    $(this).find('a:first').addClass('active');
                },
                "aui-dropdown2-hide": function() {
                    $(this).find('a').removeClass('active');
                }
            });
        }
    });

    /*
     * Dialog confirmation on delete.
     */
    var ConfirmationDialog = Backbone.View.extend({

        template: applinks.feature.v3.list.templates,

        initialize: function (options) {
            // extend with options
            _.extend(this, options);

            this.$dialog = new AJS.Dialog({
                id: "delete-applink-dialog",
                closeOnOutsideClick: true,
                height: 'auto',
                width: 620
            });

            // dialog content
            this.$dialog.addHeader(AJS.I18n.getText('applinks.agent.dialog.confirmation.header'));
            this.$dialog.addPanel(null, this.template.confirmationDialog());

            // confirm button
            this.$dialog.addButton(
                AJS.I18n.getText('applinks.v3.config.action.confirm'),
                _.bind(this.onConfirm, this),
                    "delete-confirm-button aui-button aui-button-primary"
            );

            // cancel link
            this.$dialog.addButton(
                AJS.I18n.getText('applinks.v3.config.action.cancel'),
                _.bind(this.onCancel, this)
            );
        },

        /**
         * Trigger confirm event and also call cancel logic.
         */
        onConfirm: function (dialog) {
            this.trigger('confirm', this.id);
            this.onCancel(dialog);
        },

        /**
         * On cancel hide dialog and remove element from DOM.
         */
        onCancel: function (dialog) {
            dialog.hide().remove();
        },

        /**
         * Show dialog.
         */
        show: function () {
            this.$dialog.show();
        }
    });

    return {
        Row: Row,
        ConfirmDialog : ConfirmationDialog
    }
});