define('applinks/feature/bitbucket-rebrand', [
    'applinks/lib/aui',
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/common/context',
    'applinks/common/events',
    'applinks/common/features',
    'applinks/common/products',
    'applinks/component/feature-discovery',
    'applinks/feature/creation',
    'applinks/feature/v3/ui'
], function(
    AJS,
    $,
    _,
    ApplinksContext,
    ApplinksEvents,
    ApplinksFeatures,
    ApplinksProducts,
    FeatureDiscovery,
    ApplinksCreation,
    V3Ui
) {

    var DISMISS_BUTTON_SELECTOR = '#bb-rebrand-dismiss';

    return {
        // use BB rebrand feature name as feature key for discovery
        FEATURE_KEY: ApplinksFeatures.BITBUCKET_REBRAND,

        _featureDiscovery: undefined,
        _featureDiscoveryApplinkId: undefined,

        initOldUi: function() {
            if (this._isInitialized()) {
                return;
            }

            // old UI
            var bbAppLegacyRows = $('#application-links-table').find('tr.ual-row').filter(function(index, elem) {
                return $(elem).find('td.application-type').text().indexOf('Bitbucket') > -1;
            });
            if (bbAppLegacyRows.length) {
                this._discoverBitbucketRebrand(bbAppLegacyRows.find('.application-icon').get(0),
                    bbAppLegacyRows.first().attr('application-id'));
            }
        },

        initNewUi: function() {
            if (this._isInitialized()) {
                return;
            }

            // new UI
            var bbAppNewRows = $('#agent-table').find('tr').filter(function(index, elem) {
                return $(elem).find('.applinks-type').text().indexOf('Bitbucket') > -1;
            });
            if (bbAppNewRows.length) {
                this._discoverBitbucketRebrand(bbAppNewRows.find('.applinks-type').get(0),
                    bbAppNewRows.first().attr('id'));
            }
        },

        init: function() {
            // only initiate discovery if: not in Bitbucket/Stash already, rebrand is enabled, but not discovered yet
            if (!this._isBitbucket() && this._isRebrandEnabled() && !this._isRebrandDiscovered()) {
                ApplinksEvents.on(ApplinksEvents.APPLINKS_LOADED, _.bind(function() {
                    if (this._isInitialized()) {
                        return;
                    }

                    if (V3Ui.isOn()) {
                        if(V3Ui.isFinishedOnboarding()) {
                            this.initNewUi();
                        } else {
                            ApplinksEvents.on(ApplinksEvents.V3_ONBOARDING_FINISHED, _.bind(function(){
                               this.initNewUi();
                            }, this));
                        }
                    } else {
                        this.initOldUi();
                    }
                }, this));
            }
        },

        _isBitbucket: function() {
            return ApplinksProducts.BITBUCKET === ApplinksContext.hostApplication().type;
        },

        _isRebrandEnabled: function() {
            return ApplinksFeatures.isEnabled(this.FEATURE_KEY);
        },

        _isRebrandDiscovered: function() {
            return ApplinksFeatures.isDiscovered(this.FEATURE_KEY);
        },

        _isInitialized: function() {
            return !!this._featureDiscovery;
        },

        _discoverBitbucketRebrand: function(anchor, applinkId) {
            if (this._isCreationInProgress()) {
                // don't show feature discovery if there's a link being created - if we got here, it means there were
                // no existing Stash links on the page before the creation started, so just discover the feature
                ApplinksFeatures.discover(this.FEATURE_KEY);
            } else {
                this._featureDiscoveryApplinkId = applinkId;
                this._featureDiscovery = new FeatureDiscovery(
                    this.FEATURE_KEY,
                    applinks.feature.bitbucket.rebrand.featureDiscovery(),
                    anchor,
                    AJS.I18n.getText('applinks.inline_dialog.feature_discovery.stash_renamed.aria_label')
                );
                this._featureDiscovery.dismissTrigger(DISMISS_BUTTON_SELECTOR);
                this._bindEvents();
                this._fixFocus();
            }
        },

        _isCreationInProgress: function() {
            // if creation is in progress, the creation log array will be available
            var creationLog = ApplinksCreation.getSubmittedStatusLog();
            return creationLog && creationLog.length;
        },

        // listen to UI events to re-align the popup
        _bindEvents: function() {
            // only bind if we have a popup showing
            if (this._isInitialized()) {
                ApplinksEvents.on(ApplinksEvents.Legacy.MESSAGE_BOX_DISPLAYED, _.bind(function() {
                    if (this._featureDiscovery) {
                        this._featureDiscovery.refresh();
                    }
                }, this));
                ApplinksEvents.on(ApplinksEvents.APPLINKS_UPDATED, _.bind(function() {
                    var element;

                    if (this._featureDiscovery && this._featureDiscoveryApplinkId) {
                        // applinks table refreshed, need to find the new anchor
                        if (V3Ui.isOn()) {
                            element = $('#' + this._featureDiscoveryApplinkId + ' .applinks-type')[0];
                        } else {
                            element = $('[application-id=' + this._featureDiscoveryApplinkId + '] .application-icon')[0]
                        }

                        this._featureDiscovery.refresh(element);
                        this._featureDiscovery.dismissTrigger(DISMISS_BUTTON_SELECTOR);
                        this._fixFocus();
                    }
                }, this));
            }
        },

        _fixFocus: function() {
            if (this._featureDiscovery) {
                this._featureDiscovery.dialog.contents().find(DISMISS_BUTTON_SELECTOR).focus();
            }
        }
    };
});
