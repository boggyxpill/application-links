define('applinks/feature/help-link/analytics', [
    'applinks/lib/aui',
    'applinks/lib/jquery'
], function(
    AJS,
    $
) {
    /**
     * Initializable component that raises Analytics events for all help links clicks.
     */
    return {
        init: function() {
            $('body').on('click', '.help-link', function () {
                AJS.trigger('analyticsEvent', {
                    name: 'applinks.view.documentation',
                    data: {linkKey: $(this).attr('data-help-link-key')}
                });
            });
        }
    };
});
