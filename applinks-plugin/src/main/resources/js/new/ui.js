(function($, _, applinksUtil, window, applinksSPI, applinksCreation, ApplinksEvents){
    /**
     * Init UI bindings.
     */
    function init() {
        applinksCreation.get().userDefinedUrlField = $('#applinks-url-entered');
        applinksCreation.get().createAppLinkButton = $('#applinks-create-button');

        applinksCreation.get().Dialogs.bindDialogBehaviors();

            applinksCreation.get().userDefinedUrlField.on('input', function() {
                applinksCreation.get().Dialogs.setSubmitStatus();
            });


        applinksCreation.get().Dialogs.resetProcess();

        if (applinksCreation.get().getCreationState().isNonInitiatingSide()) {

            var url = applinksCreation.get().getSubmittedRemoteUrl();
            applinksCreation.get().getCreationState().setCorrectedUrl(url);

            if (!applinksCreation.get().userDefinedUrlField.length) {
                //something is wrong
                AJS.log('Something is wrong. New workflow resources provided but UI elements not found.');
                applinksCreation.get().Dialogs.showApplinkCreationWorkflowNotAvailableDialog(AJS.I18n.getText('applinks.create.fail.process.unavailable.reciprocal', applinksCreation.get().getCreationState().getCorrectedUrl()), applinksCreation.get().globalCreationState);
                return;
            }
            // in App2 after a redirect from App1
            // start the creation process
            // ignore errors
            // optionally respect redirects
            applinksCreation.get().Process.fetchManifests(false, applinksCreation.get().getCreationState());
        } else {

            // App 1
            $('#createApplicationLink').submit(function(e) {
                //Not always called via an event.
                e && e.preventDefault();

                if(applinksCreation.get().createAppLinkButton.prop('disabled')
                    && applinksCreation.get().getCreationState().getInitiatorTargetUrl() == null) {
                    return;
                }

                AppLinks.UI.hideErrorBox();
                AppLinks.UI.hideInfoBox();

                if (!applinksCreation.get().userDefinedUrlField.length) {
                    //something is wrong
                    AJS.log('Something is wrong. New workflow resources provided but UI elements not found.');
                    applinksCreation.get().Dialogs.showApplinkCreationWorkflowNotAvailableDialog(null, applinksCreation.get().getCreationState());
                    return;
                }

                function createGenericLinkViaDescriptor() {
                    var applinkDescriptor = JSON.parse(applinksCreation.get().userDefinedUrlField.val());

                    if(!applinkDescriptor["consumerKey"]){
                        applinkDescriptor["consumerKey"] = applinkDescriptor["name"];

                    }
                    if(! applinkDescriptor["consumerName"]){
                        applinkDescriptor["consumerName"] = applinkDescriptor["name"];
                    }

                    var applinkCreated = null;
                    applinksSPI.get().createApplicationLink(
                        null,
                        applinkDescriptor["name"],
                        applinkDescriptor["url"],
                        applinkDescriptor["url"],
                        "generic", function () {
                        },
                        applinksCreation.get().Dialogs.handleRestErrors
                    ).success(_.bind(function (data) {
                        applinkCreated = data["resources-created"][0]["href"];
                        var slicedId = applinkCreated.split("/");
                        var id = slicedId[slicedId.length - 1];

                        applinksSPI.get().createConsumer(id,
                            applinkDescriptor["consumerKey"],
                            applinkDescriptor["consumerName"],
                            null,
                            null,
                            applinkDescriptor["publicKey"],
                            false,
                            null,
                            null,
                            false,
                            function () {
                                window.location.reload();
                            },

                            _.bind(function (data) {
                                AppLinks.SPI.deleteLink(id, false, function () {
                                }, function () {
                                });
                                applinksCreation.get().Dialogs.handleRestErrors(data);

                            }), this)
                    }), this);
                }

                //indicates that the user has submitted something that looks like JSON, we'll assume it's an applinkDescriptor
                if( applinksCreation.get().userDefinedUrlField.val()[0] === "{"){
                    createGenericLinkViaDescriptor.call(this);
                    return
                }


                // started a new creation process so explicitly create a new creationState
                applinksCreation.get().initCreationState();

                // start the status log
                applinksCreation.get().getCreationState().updateStatusLog(applinksCreation.get().STATUS_CREATION_INPROGRESS);

                // must be run after as it tweaks the creationState to reflect the external initiation if there is one.
                AppLinks.ExternalInitiation.init(applinksCreation.get().getCreationState());

                // make sure the entered URL has a protocol before going any further.
                var url = AppLinks.UI.addProtocolToURL(applinksCreation.get().userDefinedUrlField.val());
                applinksCreation.get().getCreationState().setUserDefinedUrl(url);
                applinksCreation.get().getCreationState().setCorrectedUrl(applinksCreation.get().getCreationState().getUserDefinedUrl());

                // will disable the form submission button and add a spinner.
                applinksCreation.get().Dialogs.resetProcess();

                // start the creation process
                // don't ignore errors
                // respect redirects
                applinksCreation.get().Process.fetchManifests(false, applinksCreation.get().getCreationState());
            });

            if (applinksCreation.get().getCreationState().reciprocalCreationCompleted()) {
                // The following would support creation of the link and configure incoming authentication in the
                // local app, before redirecting to the remote app to create a link and configure incoming and outgoing authentication
                // before redirecting to the local app to configure the outgoing authentication.
                var ualLinkCreation = applinksCreation.get().Process.retrieveUalLinkAndConfigureOutgoing(applinksCreation.get().getCreationState());
                ualLinkCreation.progress(function () {
                    //
                });
                ualLinkCreation.done(function () {
                    applinksUtil.waitUntilDone(function() {
                        return applinksCreation.get().Process.successNoRedirect(applinksCreation.get().getCreationState());
                    })();
                });
                ualLinkCreation.fail(function () {
                    //@todo: handle promise rejection, likely an ajax call failure.
                });
            }
        }
    }

    $(document).bind(ApplinksEvents.READY, function() {
        applinksSPI.get().setApiVersion("3.0");

        applinksCreation.get().getAndDisplayOutstandingMessages();

        init();

        // must be run after init() as it needs everything it sets up.
        AppLinks.ExternalInitiation.init(applinksCreation.get().globalCreationState);

        // if the the process was initiated externally and a starting url was provided, shortcut the first stage of the process
        if(applinksCreation.get().globalCreationState.initiatorTargetUrl !== null) {
            $('#createApplicationLink').submit();
        }

        // if a new applink has just been created - fire the event
        applinksCreation.get().fireEventIfApplinkCreated();
    });
})(require('applinks/lib/jquery'), require('applinks/lib/lodash'), require('applinks/js/util'), require('applinks/lib/window'), require('applinks/lib/spi'), require('applinks/lib/creation'), require('applinks/common/events'));
