/**
 * AppLinks.Creation.Process
 * Used to hold the functionality used to create application links.
 */
(function($, applinksUtil) {
    AppLinks.Creation.Process = {
    debug: false,
    /**
     * Create a collection of NonUAL types compatible with aui.form.selectField
     * @return {Array}
     */
    createNonUalTypes: function() {
        var typeOptions = [];
        for (var i = 0; i < nonAppLinksApplicationTypes.length; i++) {
            var option;
            if(nonAppLinksApplicationTypes[i].typeId === 'generic') {
                option = { text : nonAppLinksApplicationTypes[i].label, value : nonAppLinksApplicationTypes[i].typeId, selected : true };

            } else {
                option = { text : nonAppLinksApplicationTypes[i].label, value : nonAppLinksApplicationTypes[i].typeId };
            }
            typeOptions.push(option)
        }
        return typeOptions;
    },
    /**
     * Create an application link.
     * Create an autoconfigured Atlassian 2 Atlassian link if given a manifest file by the remote application.
     * Create a generic link if not given a manifest file.
     * @param  creationState
     */
    createApplicationLink: function (creationState) {
        var typeOptions = AppLinks.Creation.Process.createNonUalTypes();

        if (typeof creationState.getRemoteApplication().typeId !== "undefined") {

            if (creationState.getRemoteApplication().url !== creationState.getCorrectedUrl()) {

                // confirm discrepancy between (entered) RPC URL and the base url return by the manifest
                AppLinks.Creation.Dialogs.showConfirmUrlsDialog(creationState);
            } else {

                //Create UAL link
                AppLinks.Creation.Dialogs.showCreateApplinkDialog( creationState);
            }
        } else {
            // Can't get Manifest so create a 3rd Party link.
            var remoteApplication = {
                                        url: creationState.getCorrectedUrl(),
                                        name: null,
                                        product: null,
                                        logoUrl: null,
                                        typeId: "generic"
                                    };
            creationState.setRemoteApplication(remoteApplication);
            AppLinks.Creation.Dialogs.showCreateNonUalLinkDialog(typeOptions,  creationState);
        }
    },
    /**
     * Take the local and remote manifest's check for errors, if there are non start the create link process.
     * @param remoteManifest
     * @param ignoreErrors
     * @param creationState
     * @param localManifest
     */
    processManifests: function (remoteManifest, ignoreErrors, creationState, localManifest)
    {
        var manifestErrors = AppLinks.Creation.Process.fetchManifestErrorHandler(remoteManifest, ignoreErrors, creationState);

        // if there are no errors in the manifests continue the creation process.
        if (!manifestErrors)
        {
            creationState.setLocalApplication(applinksUtil.manifestToApplication(localManifest));
            creationState.setRemoteApplication(applinksUtil.manifestToApplication(remoteManifest));

            // TODO APLDEV-3 currently these decisions are hardcoded to depend on OAuth support, in principle they
            // should be something more generic.
            creationState.setConfigureIncomingUserAuthentication(applinksUtil.applicationSupportsIncomingThreeLeggedOAuth(localManifest) &&
                applinksUtil.applicationSupportsOutgoingThreeLeggedOAuth(remoteManifest));
            creationState.setConfigureIncomingServerAuthentication(applinksUtil.applicationSupportsIncomingTwoLeggedOAuth(localManifest) &&
                applinksUtil.applicationSupportsOutgoingTwoLeggedOAuth(remoteManifest));
            creationState.setConfigureIncomingPreAuthentication(applinksUtil.applicationSupportsIncomingTwoLeggedOAuthWithImpersonation(localManifest) &&
                applinksUtil.applicationSupportsOutgoingTwoLeggedOAuthWithImpersonation(remoteManifest));

            creationState.setConfigureOutgoingUserAuthentication(applinksUtil.applicationSupportsOutgoingThreeLeggedOAuth(localManifest) &&
                applinksUtil.applicationSupportsIncomingThreeLeggedOAuth(remoteManifest));
            creationState.setConfigureOutgoingServerAuthentication(applinksUtil.applicationSupportsOutgoingTwoLeggedOAuth(localManifest) &&
                applinksUtil.applicationSupportsIncomingTwoLeggedOAuth(remoteManifest));
            creationState.setConfigureOutgoingPreAuthentication(applinksUtil.applicationSupportsOutgoingTwoLeggedOAuthWithImpersonation(localManifest) &&
                applinksUtil.applicationSupportsIncomingTwoLeggedOAuthWithImpersonation(remoteManifest));

            AppLinks.Creation.Process.createApplicationLink(creationState);
        }
    },
    /**
     * Attempt to fetch the local and remote manifests.
     * @param ignoreErrors
     * @param  creationState
     */
    fetchManifests: function(ignoreErrors,  creationState) {
        var remoteManifestRequest;
        if(creationState.getUseCors()) {
            remoteManifestRequest = AppLinks.SPI.getRemoteManifest(creationState.getCorrectedUrl(),
                function(data) {
                    AppLinks.Creation.Dialogs.handleFetchManifestRestSuccess(data,  creationState);
                },
                function(data) {
                    AppLinks.Creation.Dialogs.handleFetchManifestRestFailure(data,  creationState);
                });
        } else {
            remoteManifestRequest = AppLinks.SPI.tryToFetchManifest(creationState.getCorrectedUrl(),
                function(data) {
                    AppLinks.Creation.Dialogs.handleFetchManifestRestSuccess(data,  creationState);
                },
                function(data) {
                    AppLinks.Creation.Dialogs.handleFetchManifestRestFailure(data,  creationState);
                });
        }
        var localManifestRequest = AppLinks.SPI.getLocalManifest(
            function(data) {
                AppLinks.Creation.Dialogs.handleFetchManifestRestSuccess(data,  creationState);
            },
            function(data) {
                AppLinks.Creation.Dialogs.handleFetchManifestRestFailure(data,  creationState);
            });

        var manifestFetch = $.when(remoteManifestRequest, localManifestRequest);

        manifestFetch.done(function (remoteData, localData) {
            var localManifest = localData[0];
            var remoteManifest = remoteData[0];
            AppLinks.Creation.Process.processManifests(remoteManifest, ignoreErrors, creationState, localManifest);
        });

        // Handle CORS failures
        manifestFetch.fail(function (remoteData) {
            // Failed CORS data is not an array
            var remoteManifest = remoteData;

            // We still need to have a handle on the localmanifets so go get it again!
            // @todo we should be able to get the local manifest once and store it instead.
            AppLinks.SPI.getLocalManifest(null, function(data) {
                    AppLinks.Creation.Dialogs.handleUrlErrors(data,  creationState);
                }).done(function(localManifest) {
                        AppLinks.Creation.Process.processManifests(remoteManifest, ignoreErrors, creationState, localManifest);
                });
        });
        return manifestFetch;
    },
    /**
     * Handle errors from the manifest fetching for create applinks workflow 2.0
     *
     * @param manifest
     * @param respectRedirects
     * @param ignoreErrors
     * @param  creationState
     * @param remoteUrl
     */
    fetchManifestErrorHandler: function(manifest, ignoreErrors,  creationState) {
        var removeWarnings = function(){
                forceWhenHostIsOffline = false;
                AppLinks.UI.hideErrorBox();
            },
            forceWhenHostIsOffline;

        //reset warnings when user changes input.
        AppLinks.Creation.userDefinedUrlField.on('change', removeWarnings);

        var handleError = function(manifest){

            if(ignoreErrors) {
                // explicitly decided to ignore errors, e.g. a 3rd party url that does not support manifests.
                removeWarnings();
                return false;
            }

            if (typeof manifest.code === 'undefined') {
                // no code = no errors
                return false;
            }

            // redirection
            if (manifest.code == 'applinks.warning.redirected.host') {
                creationState.incrementRedirectCount();

                // warn user about redirection
                if (manifest.params && manifest.params.redirectedUrl) {
                    if(manifest.params.redirectedUrl.indexOf('rest/applinks/') > 0) {
                        var url = manifest.params.redirectedUrl.substring(0, manifest.params.redirectedUrl.indexOf('rest/applinks/'));
                        creationState.setCorrectedUrl(url)
                    }
                    else {
                        var url = manifest.params.redirectedUrl;
                        creationState.setCorrectedUrl(url)
                    }
                }

                var message;
                if( creationState.getRedirectCount() == 1) {
                    message = AJS.I18n.getText('applinks.warning.redirected.host.count.once',  creationState.getRedirectCount())
                } else {
                    message = AJS.I18n.getText('applinks.warning.redirected.host.count',  creationState.getRedirectCount())
                }

                AppLinks.Creation.Dialogs.showConfigureUrlDialog(message, ignoreErrors,  creationState, true);
            }

            // non existent host, e.g. uncontactable url
            if (manifest.code == 'applinks.warning.unknown.host') {
                if (creationState.isNonInitiatingSide()) {
                    // if in the process of creating a reciprocal link, then stop the process
                    AppLinks.Creation.Dialogs.showReciprocalManifestUnobtainableDialog( creationState);
                } else {
                    // The host doesn't ping. Stay on the same page, ask for a confirmation.
                    // if the user confirms then we can ignore any subsequent errors.
                    AppLinks.Creation.Dialogs.showConfigureUrlDialog(manifest.warning, true,  creationState, false);
                }
            }

            // incompatible applinks versions.
            if(manifest.code == 'applinks.warning.incompatible.applinks.version') {
                creationState.setRemoteApplication(applinksUtil.manifestToApplication(manifest));
                AppLinks.Creation.Dialogs.showIncompatibleApplinksVersionDialog(creationState);
            }

            // 3rd party url
            if(manifest.code == 'applinks.warning.host.does-not-support-manifest') {
                if(creationState.getUseCors()) {
                    if(creationState.isNonInitiatingSide()) {
                        console.log('Unable to retrieve remote Manifest using CORS');
                        // if already tried via cors than bail out.
                        AppLinks.Creation.Dialogs.showReciprocalManifestUnobtainableDialog(creationState);
                    }
                } else {
                    // try using cors
                    console.log('Unable to retrieve remote Manifest. Try again using CORS');
                    creationState.setUseCors(true);
                    AppLinks.Creation.Process.fetchManifests(false, creationState);
                }
            }

            // applink already exists
            if(manifest.code == 'applinks.warning.host.duplicated') {
                if (creationState.isNonInitiatingSide()) {
                    AppLinks.Creation.Dialogs.showReciprocalDuplicateDialog(creationState.getCorrectedUrl(), manifest.warning, creationState);
                } else {
                    AppLinks.Creation.Dialogs.showLocalDuplicateDialog(manifest.warning, creationState);
                }
            }

            // invalid url entered
            if(manifest.code == 'applinks.error.rpcurl') {
                AppLinks.Creation.Dialogs.showConfigureUrlDialog(manifest.warning, true, creationState, false);
            }

            return true;
        };

        return handleError(manifest);
    },
    /**
     * Create an OAuth Consumer, created by retrieving the OAuth Consumer info from the remote application,
     * either by server to server communication , or if we've already had to fall back on CORS use that instead.
     * @param creationState
     * @param deferred
     * @returns {*}
     */
    createConsumer: function(creationState, deferred) {
        if(creationState.getUseCors()) {
            return AppLinks.Creation.Process.createConsumerConfiguredFromRemoteConsumerInfo( creationState, deferred);
        } else {
            return AppLinks.Creation.Process.createConsumerAutoConfigured( creationState, deferred);
        }
    },
    /**
     * Create a consumer,
     * i.e. a representation of a remote application with permission to access resources on this application.
     * TODO can be done in Parallel after creation of an application link.
     * @param applicationLink
     */
    createConsumerAutoConfigured: function( creationState, deferred) {
        // always allow a form of 2LO
        var twoLOAllowed = true,
            // by default allow 2LO anonymous
            executingTwoLOUser = null,
            twoLOWithImpersonationAllowed =  creationState.getSharedUserbase();

        //5. add consumer
        AppLinks.SPI.createConsumerAutoConfigure( creationState.getCreatedApplicationLinkId(), twoLOAllowed, executingTwoLOUser, twoLOWithImpersonationAllowed, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleRestErrors)
            .done(function (data) {
                console.log(data, 'Consumer successfully created and autoconfigured.');
                deferred.notify();
                deferred.resolve();
            })
            .fail(function (data) {
                creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "autoConfigureConsumerFailed");
                console.log(data, 'Consumer creation and autoconfigure failed.');
                deferred.reject();
            });
    },
    /**
     * Create a consumer, based on consumer information retrieved via CORS
     * i.e. a representation of a remote application with permission to access resources on this application.
     * @param applicationLink
     */
    createConsumerConfiguredFromRemoteConsumerInfo: function( creationState, deferred) {
        // always allow a form of 2LO
        var twoLOAllowed = true,
        // by default allow 2LO anonymous
            executingTwoLOUser = null,
            twoLOWithImpersonationAllowed =  creationState.getSharedUserbase();

        //5a. get the remote OAuth consumer information
        AppLinks.SPI.getRemoteOAuthConsumerInfo(creationState.getCorrectedUrl(), AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleRestErrors)
            .done(function (data) {
                console.log(data, 'Retrieving remote Consumer Info succeeded.');
                var consumerName = $(data).find("name").text(),
                    consumerKey = $(data).find("key").text(),
                    publicKey = $(data).find("publicKey").text();

                AppLinks.SPI.createConsumer(creationState.getCreatedApplicationLinkId(), consumerKey, consumerName, null, null, publicKey, twoLOAllowed, executingTwoLOUser, twoLOWithImpersonationAllowed, false, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleConfigureIncomingNonUalLinkErrors)
                    .done(function(data){
                        creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_SUCCESS);
                        console.log(data, 'Consumer successfully created.');
                    })
                    .fail(function(data){
                        creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "createConsumerFailed");
                        console.log(data, 'Consumer creation failed.');
                        //@todo handle failure.
                    });
                deferred.notify();
                deferred.resolve();
            })
            .fail(function (data) {
                console.log(data, 'Retrieving remote Consumer Info failed.');
                deferred.reject();
            });
    },
     /**
     * Step 4a register a 2LOi Authentication provider
     * This step is optional, depending on the shareduserbase option.
     * TODO can be done in Parallel after step 2.
     * @param applicationLink
     * @param config
     */
    register2LOiProvider: function(config,  creationState, deferred) {
         var applicationId = creationState.getCreatedApplicationLinkId() == null ? creationState.getOriginalCreatedApplicationLinkId() : creationState.getCreatedApplicationLinkId();
        AppLinks.SPI.registerProvider( applicationId, "com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider", config, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleRestErrors)
            .done(function (data) {
                console.log(data, '2LOi Provider registered.');
                deferred.notify();
                deferred.resolve();
            })
            .fail(function () {
                creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "register2LOiProviderFailed");
                console.log('OAuth 2LOi AuthenticationProvider registration failed.');
                deferred.reject();
            });
    },
    /**
     * Step 4 register 2LO Provider.
     * TODO can be done in Parallel after step 2.
     * @param applicationLink
     * @param config
     */
    register2LOProvider: function( creationState, deferred) {
        var config = {};
        var applicationId = creationState.getCreatedApplicationLinkId() == null ? creationState.getOriginalCreatedApplicationLinkId() : creationState.getCreatedApplicationLinkId();
        AppLinks.SPI.registerProvider(applicationId, "com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider", config, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleRestErrors)
            .done(function (data) {
                console.log(data, '2LO Provider registered.');
                if (creationState.getConfigureOutgoingPreAuthentication()) {
                    if (creationState.getSharedUserbase() == true && applinksUtil.isSysAdmin()) {
                        creationState.updateStatusLog(AppLinks.Creation.STATUS_REGISTERPROVIDER_2LOI_FAILURE, 'register2LOiProviderNotSysAdminFailed')
                        AppLinks.Creation.Process.register2LOiProvider(config, creationState, deferred);
                    } else {
                        deferred.notify();
                        deferred.resolve();
                    }
                } else {
                    console.log('Skipped registering outgoing 2LOi as one host does not support it.');
                    deferred.notify();
                    deferred.resolve();
                }
            })
            .fail(function (data) {
                creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "register2LOProviderFailed");
                console.log(data, 'OAuth 2LO AuthenticationProvider registration failed.');
                deferred.reject();
            });
    },
    /**
     * Step 3 Register a 3LO Authentication provider.
     * TODO can be done in Parallel after step 2.
     * @param applicationLink
     */
    register3LOProvider: function( creationState, deferred) {
        //3. register OAuth provider
        var config = {};
        AppLinks.SPI.registerProvider( creationState.getCreatedApplicationLinkId(), "com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider", config, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleRestErrors)
            .done(function () {
                if (creationState.getConfigureOutgoingServerAuthentication()) {
                    AppLinks.Creation.Process.register2LOProvider(creationState, deferred);
                } else {
                    console.log('Skipped registering outgoing 2LO as one host does not support it.');
                    deferred.notify();
                    deferred.resolve();
                }
            })
            .fail(function () {
                creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "register3LOProviderFailed");
                console.log('OAuth AuthenticationProvider registration failed.');
                deferred.reject();
            });
    },
    /**
     * Step 2 retrieve the newly created Application Link, since we need the ID.
     * @param data
     */
    retrieveApplicationLink: function(data,  creationState, deferred, processLink) {
        var applicationLinkUrl = data['resources-created'][0].href;
        AppLinks.get(applicationLinkUrl, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleRestErrors)
            .done(function (data) {
                console.log(data, 'Application link read.');
                // grab hold of the new applink id
                creationState.setCreatedApplicationLinkId(data.id);
                if(! creationState.isNonInitiatingSide()) {
                     creationState.setOriginalCreatedApplicationLinkId(data.id);
                }

                if(typeof processLink != 'undefined') {
                    processLink( creationState, deferred);
                }
            })
            .fail(function () {
                creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "retrieveApplicationLinkFailed");
                console.log('Application link read failed.');
                deferred.reject();
            });
    },
    createUalLinkAndConfigureIncoming: function ( creationState){
        var deferred = new $.Deferred();

        // if not creating a reciporcal link respect the user's choice about shared user base,
        // otherwise just go with the setting sent as part of reciprocal creation.
        if(! creationState.isNonInitiatingSide()) {
             creationState.setSharedUserbase($('#userbase').is(':checked'));
        }

        /**
         * Step 1 create an Application link.
         */
            // TODO APLDEV-3 determine if the remote actually supports OAuth first, check the manifest, before creating consumer and other OAuth
            // constructs. extract OAuth creation code into OAuth specific js files in the Oauth plugin.
        AppLinks.SPI.createApplicationLink( creationState.getRemoteApplication().id,  creationState.getRemoteApplication().name,  creationState.getRemoteApplication().rpcUrl,  creationState.getRemoteApplication().url,  creationState.getRemoteApplication().typeId, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleRestErrors)
            .done(function(data){
                if(creationState.getConfigureIncomingAuthentication()) {
                    AppLinks.Creation.Process.retrieveApplicationLink(data,
                        creationState,
                        deferred,
                        AppLinks.Creation.Process.createConsumer);
                } else if (creationState.isOneWayApplink()) {
                    AppLinks.Creation.Process.retrieveApplicationLink(data,
                        creationState,
                        deferred,
                        function() {
                            deferred.notify();
                            deferred.resolve();
                        });
                } else {
                    console.log(data, 'Skipping Consumer creation as local instance does not support 3LO.');
                    deferred.notify();
                    deferred.resolve();
                }
            })
            .fail(function(){
                creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "createApplicationLinkFailed");
                console.log('Application link creation failed.');
                deferred.reject();
            });
        return deferred.promise();

    },
    /**
     * Create a URL only link
     * @param creationState
     * @returns {*}
     */
    createUrlLink: function (creationState){
        var deferred = new $.Deferred();

        /**
         * Step 1 create an Application link.
         */
        AppLinks.SPI.createApplicationLink(creationState.getRemoteApplication().id,  creationState.getRemoteApplication().name,  creationState.getRemoteApplication().rpcUrl,  creationState.getRemoteApplication().url,  creationState.getRemoteApplication().typeId, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleRestErrors)
            .done(function(data){
                console.log('Application link created.');
                AppLinks.Creation.Process.retrieveApplicationLink(data,  creationState, deferred, function() {
                    deferred.notify();
                    deferred.resolve();
                });
            })
            .fail(function(){
                console.log('Application link creation failed.');
                deferred.reject();
            });
        return deferred.promise();

    },
    /**
     * Retrieve a previously created Application Link and configure the outgoing authentication.
     * NB. depends on  creationState.getCreatedApplicationLinkId() and  creationState.getSharedUserbase() being correctly defined.
     * @param  creationState the current creation state.
     */
    retrieveUalLinkAndConfigureOutgoing: function ( creationState){
        var deferred = new $.Deferred();
        var config = {};
        if (creationState.getConfigureOutgoingUserAuthentication()) {
            AppLinks.SPI.registerProvider( creationState.getOriginalCreatedApplicationLinkId(), "com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider", config, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleRestErrors)
                .done(function (data) {
                    if (creationState.getConfigureOutgoingServerAuthentication()) {
                        AppLinks.Creation.Process.register2LOProvider(creationState, deferred);
                    } else {
                        console.log('Skipped registering outgoing 2LO as one host does not support it.');
                        deferred.notify();
                        deferred.resolve();
                    }
                })
                .fail(function (data) {
                    creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "register3LOProviderFailed");
                    console.log(data, 'OAuth AuthenticationProvider registration failed.');
                    deferred.reject();
                });
        } else {
            console.log('Skipped registering outgoing 3LO as one host does not support it.');
            deferred.notify();
            deferred.resolve();
        }

        return deferred.promise();

    },
    /**
     * Create a new UAL link and complete incoming and outgoing OAuth configuration.
     * Depends on the following being correctly defined:
     *  -  creationState.getRemoteApplication()
     *  -  creationState.getSharedUserbase()
     * @param  creationState the current creation status
     */
    createUalLinkAndConfigureIncomingAndOutgoing: function ( creationState){
        var deferred = new $.Deferred();

        // if not creating a reciprocal link respect the user's choice about shared user base,
        // otherwise just go with the setting sent as part of reciprocal creation.
        if(! creationState.isNonInitiatingSide()) {
             creationState.setSharedUserbase($('#userbase').is(':checked'));
        }

        // Step 1 create an Application link.
        AppLinks.SPI.createApplicationLink( creationState.getRemoteApplication().id,
                                             creationState.getRemoteApplication().name,
                                             creationState.getRemoteApplication().rpcUrl,
                                             creationState.getRemoteApplication().url,
                                             creationState.getRemoteApplication().typeId,
                                            AppLinks.Creation.Dialogs.handleRestSuccess,
                                            AppLinks.Creation.Dialogs.handleRestErrors)
            .done(function(data){
                AppLinks.Creation.Process.retrieveApplicationLink(data,
                                                                     creationState,
                                                                    deferred,
                                                                    function( creationState, deferred) {
// TODO APLDEV-3 determine if the remote actually supports OAuth first, check the manifest
// extract OAuth creation code into OAuth specific js files in the Oauth plugin.
                                                                        var doneNothing = false;
                                                                        if(creationState.getConfigureOutgoingUserAuthentication()) {
                                                                            // step 2a configure outgoing authentication
                                                                            AppLinks.Creation.Process.register3LOProvider(creationState, deferred);
                                                                        } else {
                                                                            console.log(data, 'Skipped configuring outgoing User Authentication.');
                                                                            doneNothing = true;
                                                                        }

                                                                        if(creationState.getConfigureIncomingAuthentication()) {
                                                                            // step 2b configure incoming authentication
                                                                            AppLinks.Creation.Process.createConsumer(creationState, deferred);
                                                                        } else {
                                                                            console.log(data, 'Skipped configuring incoming Authentication.');
                                                                            doneNothing = true;
                                                                        }

                                                                        if( doneNothing) {
                                                                            console.log(data, 'Link retrieved successfully.');
                                                                            deferred.notify();
                                                                            deferred.resolve();
                                                                        }

                                                                    });
            })
            .fail(function(){
                creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "createApplicationLinkFailed");
                console.log('Application link creation failed.');
                deferred.reject();
            });

        return deferred.promise();

    },
        /**
         * Configure the incoming authentication for an application link.
         * @param consumerName
         * @param consumerKey
         * @param publicKey
         * @param creationState
         * @returns {*}
         */
    configureIncomingNonUalLink: function(consumerName, consumerKey, publicKey, creationState) {
        creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_INPROGRESS);
        return AppLinks.SPI.createConsumer(creationState.getCreatedApplicationLinkId(), consumerKey, consumerName, null, null, publicKey, false, null, null, false, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleConfigureIncomingNonUalLinkErrors)
            .done(function(data){
                creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_SUCCESS);
                console.log(data, 'Consumer successfully created.');
            })
            .fail(function(data){
                creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "createConsumerFailed");
                console.log(data, 'Consumer creation failed.');
                //@todo handle failure.
            });
    },
    /**
     * Create a default NON UAL link.
     * and optionally configure Outgoing.
     * @return {*}
     */
    createOutgoingNonUalLink: function(
        name,
        typeId,
        rpcUrl,
        accessTokenUrl,
        requestTokenUrl,
        consumerKey,
        authorizeUrl,
        sharedSecret,
        serviceProvider,
        createIncoming,
         creationState){
        // "global" to hold the newly created applink.
        var deferred = new $.Deferred(),
            applicationLink;

        /**
         * read the newly created applicationlink and if successful register a provider
         * @param data
         */
        function readApplicationLinkAndRegisterProvider(data){

            // creating the applinks goives us backa url to the new resource so go fetch it to get the new applink.
            AppLinks.get(data['resources-created'][0].href, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleCreateNonUalLinkErrors)
                .done(function(data) {
                    // grab hold of the new applink id
                     creationState.setCreatedApplicationLinkId(data.id);
                    if(! creationState.isNonInitiatingSide()) {
                         creationState.setOriginalCreatedApplicationLinkId(data.id)
                    }

                    if( accessTokenUrl == ""
                        && requestTokenUrl == ""
                        && consumerKey == ""
                        && authorizeUrl == ""
                        && sharedSecret == ""
                        && serviceProvider == "")
                    {
                        // SUCCESS completed NON-UAL basic application link.
                        creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_SUCCESS);
                        if(createIncoming != true)
                        {
                            deferred.notify();
                            deferred.resolve();
                        } else {
                            // if nothing was set don't bother trying to configure the outgoing link.
                            // potentially just go and configure the incoming one.
                            runIncomingNonUalLinkDialog();
                        }
                    } else {
                        // if anything was set then do try and configure the link
                        register3rdPartyOauthProviderAndAddConsumer();
                    }
                })
                .fail(function(){
                    creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "retrieveApplicationLinkFailed");
                    console.log('Unable to read ApplicationLink.');
                    deferred.reject();
                });
        }

        /**
         * Register a provider for the specified ApplicationLink and if successful add a consumer
         */
        function register3rdPartyOauthProviderAndAddConsumer() {
            var config = {
                'serviceProvider.accessTokenUrl': accessTokenUrl,
                'serviceProvider.requestTokenUrl': requestTokenUrl,
                'consumerKey.outbound': consumerKey,
                'serviceProvider.authorizeUrl': authorizeUrl
            };

            AppLinks.SPI.registerProvider( creationState.getCreatedApplicationLinkId(), "com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider", config, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleCreateNonUalLinkErrors)
                .done(addConsumerAndConfigureIncoming)
                .fail(function(){
                    creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "register3LOProviderFailed");
                    console.log('AuthenticationProvider registration failed.');
                    deferred.reject();
                });
        }

        /**
         * Add a Consumer and if successful, and if requested by the user then run the incoming dialog.
         * @param data
         */
        function addConsumerAndConfigureIncoming(data){
            console.log(data, 'AuthenticationProvider successfully registered.');
            AppLinks.SPI.createConsumer( creationState.getCreatedApplicationLinkId(), consumerKey, serviceProvider, null, sharedSecret, null, true, null, null, true, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleCreateNonUalLinkErrors)
                .done(runIncomingNonUalLinkDialog)
                .fail(function(data){
                    creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "createConsumerFailed");
                    console.log(data, 'Consumer creation failed.');
                    deferred.reject();
                });
        }

        /**
         * Run the dialog to configure incoming connections for current applicationlink
         */
        function runIncomingNonUalLinkDialog(){
            deferred.notify();
            console.log('Consumer successfully created.');

            if(!createIncoming) {
                creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_SUCCESS);
                deferred.resolve();
                return;
            }

            var dialog = AppLinks.Creation.Dialogs.showConfigureIncomingNonUalLinkDialog(creationState, deferred);
        }

        // Attempt to submit
        AppLinks.SPI.createApplicationLink(null, name, rpcUrl, rpcUrl, typeId, AppLinks.Creation.Dialogs.handleRestSuccess, AppLinks.Creation.Dialogs.handleCreateNonUalLinkErrors)
            .done(readApplicationLinkAndRegisterProvider)
            .fail(function(){
                creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "createApplicationLinkFailed");
                console.log('Application link creation failed.');
                deferred.reject();
            });
        return deferred.promise();
    },
    /**
     * AppLink successfully Created now redirect to Application 2 to create the reciprocal link.
     * @return {{wait: Function, done: Function}}
     */
    successRedirectToCreateReciprocal: function( creationState) {
         creationState.updateStatusLog(AppLinks.Creation.STATUS_LOCAL_SUCCESS);
         creationState.updateStatusLog(AppLinks.Creation.STATUS_RECIPROCAL_INPROGRESS);
        return {
            'wait': function() {
                AppLinks.Creation.Dialogs.showRedirectPauseDialog(true,  creationState.getRemoteApplication().name)
            },
            'done': function(){
                var redirectUrl = AppLinks.Creation.generateApplinksCreationRedirectionUrl(true,  creationState);
                window.location = redirectUrl;
            }};
    },
    /**
     * AppLink successfully created, optionally display dialog offering to redirect user to the remote application.
     * @return {{wait: Function, done: Function}}
     */
    successIncompatibleApplinksVersion: function ( creationState) {
        creationState.updateStatusLog(AppLinks.Creation.STATUS_LOCAL_SUCCESS);
        return {
            'wait': function(){},
            'done': function(){
                if (creationState.doInitiatorRedirect()) {
                    AppLinks.Creation.Process.showFinalStatus(creationState);
                } else {
                    AppLinks.Creation.Dialogs.showIncompatibleApplinksVersionCompletionDialog(creationState);
                }
            }
        }
    },
    /**
     * AppLink successfully created, display manual reciprocal link to user,and refresh the AppLinks table.
     * @return {{wait: Function, done: Function}}
     */
    successManualRedirect: function ( creationState) {
        creationState.updateStatusLog(AppLinks.Creation.STATUS_LOCAL_SUCCESS);
        return {
            'wait': function(){},
            'done': function(){
                AppLinks.Creation.Dialogs.showManuallyCreateReciprocalLinkDialog( creationState.getRemoteApplication().name,  creationState.getRemoteApplication().url,  creationState.getLocalApplication().url, false,  creationState);
            }
        }
    },
    /**
     * AppLink successfully Created now redirect back to Application 1.
     * @return {{wait: Function, done: Function}}
     */
    successRedirectToReturn: function( creationState) {
         creationState.updateStatusLog(AppLinks.Creation.STATUS_RECIPROCAL_SUCCESS);
        return {
            'wait': function() {
                AppLinks.Creation.Dialogs.showRedirectPauseDialog(false,  creationState.getRemoteApplication().name);
            },
            'done': function(){
                var redirectUrl = AppLinks.Creation.generateApplinksCreationRedirectionUrl(false,  creationState);
                window.location = redirectUrl;
            }};
    },
    /**
     * AppLink successfully created, now need to redirect, so refresh the AppLinks table.
     * @return {{wait: Function, done: Function}}
     */
    successNoRedirect: function( creationState) {
        return {
            'wait': function(){
                AppLinks.Creation.Dialogs.showProcessingPauseDialog(creationState);
            },
            'done': function(){
                AppLinks.Creation.Process.showFinalStatus( creationState);
            }
        }
    },
        /**
         * Display the final status of the creation process to the user.
         */
         showFinalStatus: function( creationState)
        {
            var message, status;
            var createdApplicationLinkId;
            if(creationState.isNonInitiatingSide()) {
                createdApplicationLinkId = creationState.getCreatedApplicationLinkId();
            } else {
                createdApplicationLinkId = creationState.getOriginalCreatedApplicationLinkId();
            }

            if(createdApplicationLinkId === null) {
                // not created anything so no messages to show.
                // just resets the process.
                AppLinks.Creation.Process.doFinalDisplay(status, message,  creationState);
                return;
            }

            AppLinks.SPI.getApplicationLink(createdApplicationLinkId)
                .done(function (applicationLink)
                {
                    // in App 1 after reciprocal process is completed, so log we have completed and tell the user.
                    if ( creationState.localCreationSucceeded() &&  creationState.reciprocalCreationSucceeded()) {
                        status = AppLinks.Creation.CREATION_RESULT_INFO_KEY;
                        message = AJS.I18n.getText('applinks.create.success', applicationLink.name);
                    } else if ( creationState.localCreationSucceeded() && ! creationState.getRespectRedirects()) {
                        status = AppLinks.Creation.CREATION_RESULT_WARN_KEY;
                        message = AJS.I18n.getText('applinks.create.fail.local.success.reciprocal.notattempted', applicationLink.name);
                        creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE);
                    } else if ( creationState.localCreationSucceeded() && ! creationState.reciprocalCreationSucceeded()) {
                        status = AppLinks.Creation.CREATION_RESULT_WARN_KEY;
                        message = AJS.I18n.getText('applinks.create.fail.local.success.reciprocal.fail', applicationLink.name);
                        if(creationState.doInitiatorRedirect()){
                            // for remote initiation we are expecting the remote end to have been created.
                            creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE, "reciprocalCreationNotAttempted");
                        } else {
                            creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE);
                        }
                    } else if (!creationState.localCreationSucceeded() &&  creationState.reciprocalCreationSucceeded()) {
                        // Not sure this can ever happen.
                        status = AppLinks.Creation.CREATION_RESULT_WARN_KEY;
                        message = AJS.I18n.getText('applinks.create.fail.local.fail.reciprocal.success', applicationLink.name);
                        creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE);
                    } else if ( creationState.creationHasCompletedSuccessfully()) {
                        status = AppLinks.Creation.CREATION_RESULT_INFO_KEY;
                        message = AJS.I18n.getText('applinks.create.success.local.success', applicationLink.name);
                    } else {
                        // Not sure this can ever happen.
                        status = AppLinks.Creation.CREATION_RESULT_WARN_KEY;
                        message = AJS.I18n.getText('applinks.create.unable.to.read.status', applicationLink.name);
                        creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE);
                    }

                    AppLinks.Creation.Process.doFinalDisplay(status, message, creationState, applicationLink);
                })
                .fail(function (data)
                {
                    status = AppLinks.Creation.CREATION_RESULT_WARN_KEY;
                    message = AJS.I18n.getText('applinks.create.unable.to.read.link', createdApplicationLinkId);
                     creationState.updateStatusLog(AppLinks.Creation.STATUS_CREATION_FAILURE);

                    AppLinks.Creation.Process.doFinalDisplay(status, message,  creationState);
                });
        },

        /**
         * Display the final status messages to the user.
         * Possibly redirects back to the applinks admin page to remove any querystring params that trigger functionality.
         */
        doFinalDisplay: function (status, message, creationState, applicationLink) {
            var messages = [];
            messages.push(message);

            if( creationState.isNonInitiatingSide() &&  creationState.getRespectRedirects() && creationState.creationInProgress()) {
                // if in the middle of creating a reciprocal link then just display messages as we're about to redirect away
                AppLinks.Creation.displayOutstandingMessages(status, messages);
                AppLinks.Creation.Dialogs.resetProcess();
            } else if( creationState.doInitiatorRedirect()) {
                // if completing an externally initiated link then just display messages as we're about the redirect away
                AppLinks.Creation.displayOutstandingMessages(status, messages);
                // if the the process was initiated externally and the creation process has completed respect the redirect
                AppLinks.ExternalInitiation.processInitiatorRedirect( creationState);
            } else if(window.location.search === null) {
                // if there is no querystring then there is nothing to tidy up, and we're not going to redirect, so just refresh the
                // current page.
                AppLinks.Creation.displayOutstandingMessages(status, messages);
                AppLinks.UI.listApplicationLinks();
                AppLinks.Creation.Dialogs.resetProcess();
            } else {
                // if there is a query string then tidy it up by redirecting back to the admin page, with just the messages.
                var parametersMap = {};

                applinksUtil.putParameter(status, messages, parametersMap);

                // put successfully created applink data into query string for further processing
                if (applicationLink && creationState.localCreationSucceeded()) {
                    applinksUtil.putParameter(AppLinks.Creation.CREATION_EVENT_NEW_APPLINK_ID, applicationLink.id, parametersMap);
                    applinksUtil.putParameter(AppLinks.Creation.CREATION_EVENT_NEW_APPLINK_TYPE, applicationLink.typeId, parametersMap);
                }

                // redirect back to the current page
                window.location = AppLinks.Creation.generateUrl(window.location.pathname, parametersMap);
            }

            AppLinks.Creation.initCreationState();
        }
    }
})(AJS.$, require('applinks/js/util'));
