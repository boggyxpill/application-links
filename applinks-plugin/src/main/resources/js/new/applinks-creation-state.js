(function(applinksUtil) {
    /**
     * define the object used to hold state during the creation process.
     * @constructor
     */
    function CreationState() {

        // TODO tidy this up.
        var startingUrlQuery = AppLinks.Creation.CREATION_REMOTE_URL,
            ignoreRedirectsQuery = AppLinks.Creation.CREATION_IGNORE_REDIRECTS,
            startingUrlIndex = (window.location.search.indexOf(startingUrlQuery) + 1),
            ignoreRedirectsIndex = (window.location.search.indexOf(ignoreRedirectsQuery) + 1),
            ignoreRedirects = !!ignoreRedirectsIndex;
        var respectRedirects = !ignoreRedirects;
        var nonInitiatingSide = !!startingUrlIndex;

        this.sharedUserbase = AppLinks.Creation.getSubmittedSharedUserbase();
        this.nonInitiatingSide = nonInitiatingSide;

        this.remoteType = applinksUtil.getParameterValue(AppLinks.Creation.CREATION_REMOTE_TYPE);
        this.remoteUrl = applinksUtil.getParameterValue(AppLinks.Creation.CREATION_REMOTE_URL);

        // status log will be empty on creation of new CreationState at the start of the process, or prepopulated if created after a redirect.
        this.statusLog = AppLinks.Creation.getSubmittedStatusLog();

        // the id of the applicationId created on the current host application.
        this.createdApplicationLinkId = null;
        // the id of the applicationId created on the originating host application.
        this.originalCreatedApplicationLinkId = AppLinks.Creation.getSubmittedOriginalCreatedId();

        // the original url entered by the user.
        this.userDefinedUrl = null;

        // the ulr after correction, e.g. pre-pending scheme, handling redirects etc.
        this.correctedUrl = null;

        // parameter values
        this.initiatorRedirect = "",
        this.initiatorRedirectName = "";
        this.initiatorTargetUrl = "";
        this.initiatorSharedUserbase = false;
        this.initiatorIsRemoteAdmin = true;

        this.redirectCount = 0;
        this.respectRedirects = respectRedirects;

        // representation of the local application.
        this.localApplication = null;
        // representation of the remote application.
        this.remoteApplication = null;

        // a flag indicating if CORS has been used to retrieve remote information.
        this.useCors = false;

        // flag indicating whether the link is to a version of AppLinks < 4.0.0
        this.incompatibleApplinksVersion = false;

        // flag indicating if a URL and only the URL should be configured.
        this.createUnconfiguredLink = false;

        // flag indicating if a link is being created behind the firewall
        this.oneWayApplink = !!applinksUtil.getParameterValue(AppLinks.Creation.CREATION_ONE_WAY_APPLINK);

        // for backwards compatibility default to true for this
        this.configureOutgoingUserAuthentication
            = applinksUtil.getParameterValue(AppLinks.Creation.CREATION_CONFIGURE_OUTGOING_USER_AUTH) == null
                ? true : applinksUtil.getParameterValue(AppLinks.Creation.CREATION_CONFIGURE_OUTGOING_USER_AUTH);

        // for backwards compatibility default to true for this
        this.configureOutgoingServerAuthentication
            = applinksUtil.getParameterValue(AppLinks.Creation.CREATION_CONFIGURE_OUTGOING_SERVER_AUTH) == null
                ? true : applinksUtil.getParameterValue(AppLinks.Creation.CREATION_CONFIGURE_OUTGOING_SERVER_AUTH);

        this.configureOutgoingPreAuthentication
         = applinksUtil.getParameterValue(AppLinks.Creation.CREATION_CONFIGURE_OUTGOING_PRE_AUTH) == null
                ? false : applinksUtil.getParameterValue(AppLinks.Creation.CREATION_CONFIGURE_OUTGOING_PRE_AUTH);

        // for backwards compatibility default to true for this
        this.configureIncomingUserAuthentication
            = applinksUtil.getParameterValue(AppLinks.Creation.CREATION_CONFIGURE_INCOMING_USER_AUTH) == null
                ? true : applinksUtil.getParameterValue(AppLinks.Creation.CREATION_CONFIGURE_INCOMING_USER_AUTH);

        // for backwards compatibility default to true for this
        this.configureIncomingServerAuthentication
            = applinksUtil.getParameterValue(AppLinks.Creation.CREATION_CONFIGURE_INCOMING_SERVER_AUTH) == null
                ? true : applinksUtil.getParameterValue(AppLinks.Creation.CREATION_CONFIGURE_INCOMING_SERVER_AUTH);

        this.configureIncomingPreAuthentication
            = applinksUtil.getParameterValue(AppLinks.Creation.CREATION_CONFIGURE_INCOMING_PRE_AUTH) == null
                ? false : applinksUtil.getParameterValue(AppLinks.Creation.CREATION_CONFIGURE_INCOMING_PRE_AUTH);
    };

    CreationState.prototype.getSharedUserbase =  function() {
        return this.sharedUserbase;
    };

    CreationState.prototype.setSharedUserbase =  function(sharedUserbase) {
        this.sharedUserbase = sharedUserbase;
    };

    /**
     * Returns true if on non-initiating side of Application Link creation.
     * @returns {boolean}
     */
    CreationState.prototype.isNonInitiatingSide = function() {
        return this.nonInitiatingSide;
    };

    CreationState.prototype.getStatusLog = function() {
        if(typeof this.statusLog == 'undefined') {
            this.statusLog = [];
        }
        return this.statusLog;
    };

    CreationState.prototype.getOriginalCreatedApplicationLinkId = function() {
        return this.originalCreatedApplicationLinkId;
    };

    CreationState.prototype.setOriginalCreatedApplicationLinkId = function(id) {
        this.originalCreatedApplicationLinkId = id;
    };

    CreationState.prototype.getCreatedApplicationLinkId = function() {
        return this.createdApplicationLinkId;
    };

    CreationState.prototype.setCreatedApplicationLinkId = function(id) {
        this.createdApplicationLinkId = id;
    };

    CreationState.prototype.getInitiatorRedirect = function() {
        return this.initiatorRedirect;
    };

    CreationState.prototype.setInitiatorRedirect = function(redirectUrl) {
        this.initiatorRedirect = redirectUrl;
    };

    CreationState.prototype.getInitiatorRedirectName = function() {
        return this.initiatorRedirectName;
    };

    CreationState.prototype.setInitiatorRedirectName = function(name) {
        this.initiatorRedirectName = name;
    };

    CreationState.prototype.getInitiatorTargetUrl = function() {
        return this.initiatorTargetUrl;
    };

    CreationState.prototype.setInitiatorTargetUrl = function(url) {
        this.initiatorTargetUrl = url;
    };

    CreationState.prototype.getInitiatorSharedUserbase = function() {
        return this.initiatorSharedUserbase;
    };

    CreationState.prototype.setInitiatorSharedUserbase = function(sharedUserbase) {
        this.initiatorSharedUserbase = sharedUserbase;
    };

    CreationState.prototype.getInitiatorIsRemoteAdmin = function() {
        return this.initiatorIsRemoteAdmin;
    };

    CreationState.prototype.setInitiatorIsRemoteAdmin = function(isRemoteAdmin) {
        this.initiatorIsRemoteAdmin = isRemoteAdmin;
    };

    CreationState.prototype.doInitiatorRedirect = function() {
        return this.getInitiatorRedirect() !== null
            && this.getInitiatorRedirect() !== ''
            && !this.isNonInitiatingSide();
    };

    CreationState.prototype.getRedirectCount = function() {
        return this.redirectCount;
    };

    CreationState.prototype.incrementRedirectCount = function() {
        this.redirectCount++;
    };

    CreationState.prototype.getRespectRedirects = function() {
        return this.respectRedirects;
    };

    /**
     * Get the original URL entered by the user
     * @returns {*}
     */
    CreationState.prototype.getUserDefinedUrl =  function() {
        return this.userDefinedUrl;
    };

    /**
     * Record the original URL entered by the user.
     * @param url
     */
    CreationState.prototype.setUserDefinedUrl =  function(url) {
        this.userDefinedUrl = url;
    };

    /**
     * Get the corrected URL
     * @returns {*}
     */
    CreationState.prototype.getCorrectedUrl =  function() {
        return this.correctedUrl;
    };

    /**
     * Get the remote Application Name.
     * Uses the details retrieved from the Manifest
     * if that is not available uses teh current Url
     * @returns {*}
     */
    CreationState.prototype.getRemoteApplicationUrl =  function() {
        return this.getRemoteApplication() !== null ? this.getRemoteApplication().url : this.correctedUrl;
    };

    /**
     * Get the remote Application TypeId.
     * Uses the details retrieved from the Manifest
     * if unavailable will fallback on information sent in the query.
     * @returns {*}
     */
    CreationState.prototype.getRemoteApplicationType =  function() {
        return this.getRemoteApplication() !== null ? this.getRemoteApplication().typeId : this.remoteType;
    };

    /**
     * Get the remote Application Name.
     * Uses the details retrieved from the Manifest. Name if is available, url otherwise.
     * if neither are available uses the current Url
     * @returns {*}
     */
    CreationState.prototype.getRemoteApplicationName =  function() {
        return this.getRemoteApplication() !== null ?
            this.getRemoteApplication().name !== null ? this.getRemoteApplication().name : this.getRemoteApplication().url
            : this.correctedUrl;
    };

    /**
     * Record the corrected URL.
     * @param url
     */
    CreationState.prototype.setCorrectedUrl =  function(url) {
        this.correctedUrl = url;
    };

    /**
     * Update the current status
     * @param newStatus
     */
    CreationState.prototype.updateStatusLog = function(status, detail) {
        this.getStatusLog().push( { 'status' : status, 'detail' : detail } );
    };

    CreationState.prototype.getCurrentStatus = function() {
        if(this.getStatusLog().length > 0) {
            return this.getStatusLog()[this.getStatusLog().length - 1].status;
        } else {
            return null;
        }

    };

    CreationState.prototype.getCurrentStatusDetail = function() {
        if(this.getStatusLog().length > 0) {
            return this.getStatusLog()[this.getStatusLog().length - 1].detail;
        } else {
            return null;
        }
    };

    CreationState.prototype.getFirstFailureStatus = function() {
        var failureRegExp = new RegExp(AppLinks.Creation.STATUS_CREATION_FAILURE, "i")
        return this.getStatus(failureRegExp);
    };

    CreationState.prototype.getFirstFailureStatusStatus = function() {
        return this.getFirstFailureStatus().status;
    };

    CreationState.prototype.getFirstFailureStatusDetail = function() {
        return this.getFirstFailureStatus().detail;
    };

    CreationState.prototype.getStatus = function(regexp) {
        for (var i = 0; i < this.getStatusLog().length; i++) {
            if (regexp.test(this.getStatusLog()[i].status)) {
                return this.getStatusLog()[i];
            }
        }
        return null;
    };

    /**
     * Check if the reciprocal creation process has been completed, successfully or not.
     * @returns {boolean}
     */
    CreationState.prototype.reciprocalCreationCompleted = function () {
        return this.reciprocalCreationSucceeded() || this.reciprocalCreationFailed();
    };

    /**
     * Check if the reciprocal creation process has succeeded.
     * @returns {boolean}
     */
    CreationState.prototype.reciprocalCreationSucceeded = function () {
        var successRegExp = new RegExp(AppLinks.Creation.STATUS_RECIPROCAL_SUCCESS, "i")
        return this.getStatus(successRegExp) != null;
    };

    /**
     * Check if the reciprocal creation process has failed.
     * @returns {boolean}
     */
    CreationState.prototype.reciprocalCreationFailed = function () {
        var failureRegExp = new RegExp(AppLinks.Creation.STATUS_RECIPROCAL_FAILURE, "i")
        return this.getStatus(failureRegExp) != null;
    };

    /**
     * Check if the local creation process has succeeded.
     * @returns {boolean}
     */
    CreationState.prototype.localCreationSucceeded = function () {
        var successRegExp = new RegExp(AppLinks.Creation.STATUS_LOCAL_SUCCESS, "i")
        return this.getStatus(successRegExp) != null;
    };

    /**
     * Check if the local creation process has failed.
     * @returns {boolean}
     */
    CreationState.prototype.localCreationFailed = function () {
        var failureRegExp = new RegExp(AppLinks.Creation.STATUS_LOCAL_FAILURE, "i")
        return this.getStatus(failureRegExp) != null;
    };

    /**
     * Check if the local creation process has been completed, successfully or not.
     * @returns {boolean}
     */
    CreationState.prototype.localCreationIsComplete = function () {
        var successRegExp = new RegExp(AppLinks.Creation.STATUS_LOCAL_SUCCESS, "i")
        var failureRegExp = new RegExp(AppLinks.Creation.STATUS_LOCAL_FAILURE, "i")
        return this.getStatus(successRegExp) != null || this.getStatus(failureRegExp) != null;
    };

    /**
     * Check if the creation process has completed successfully.
     * @returns {boolean}
     */
    CreationState.prototype.creationHasCompletedSuccessfully = function () {
        var status = this.getCurrentStatus()
        var re = new RegExp(AppLinks.Creation.STATUS_CREATION_SUCCESS, "i")
        return status !== null && re.test(status);
    };

    /**
     * Check if the creation process has completed successfully.
     * @returns {boolean}
     */
    CreationState.prototype.creationHasCompleted = function () {
        var status = this.getCurrentStatus()
        var successRegExp = new RegExp(AppLinks.Creation.STATUS_CREATION_SUCCESS, "i")
        var failureRegExp = new RegExp(AppLinks.Creation.STATUS_CREATION_FAILURE, "i")
        var inProgressRegExp = new RegExp(AppLinks.Creation.STATUS_CREATION_INPROGRESS, "i")
        return status !== null && (successRegExp.test(status) || failureRegExp.test(status))
            && !inProgressRegExp.test(status);
    };

    /**
     * Check if the creation process IS "IN PROGRESS".
     * @returns {boolean}
     */
    CreationState.prototype.creationInProgress = function () {
        return this.getStatusLog().length > 0 && !this.creationHasCompleted();
    };

    /**
     * Get the representation of the local application.
     */
    CreationState.prototype.getLocalApplication =  function() {
        return this.localApplication;
    };

    /**
     * Gets the representation of the local application.
     */
    CreationState.prototype.setLocalApplication =  function(application) {
        this.localApplication = application;
    };

    /**
     * Get the representation of the remote application.
     */
    CreationState.prototype.getRemoteApplication =  function() {
        return this.remoteApplication;
    };

    /**
     * Sets the representation of the remote application.
     */
    CreationState.prototype.setRemoteApplication =  function(application) {
        this.remoteApplication = application;
    };

    /**
     * Gets a flag indicating that the process is trying to create a link with a version of AppLinks < 4.0.0.
     */
    CreationState.prototype.getIncompatibleApplinksVersion =  function() {
        return this.incompatibleApplinksVersion;
    };

    /**
     * Sets a flag indicating that the process is trying to create a link with a version of AppLinks < 4.0.0.
     */
    CreationState.prototype.setIncompatibleApplinksVersion =  function(incompatibleApplinksVersion) {
        this.incompatibleApplinksVersion = incompatibleApplinksVersion;
    };

    /**
     * Sets a flag indicating if CORS should be used to get remote data.
     */
    CreationState.prototype.setUseCors =  function(useCors) {
        this.useCors = useCors;
    };

    /**
     * Gets a flag indicating if CORS should be used to get remote data.
     */
    CreationState.prototype.getUseCors =  function() {
        return this.useCors;
    };

    CreationState.prototype.getCreateUnconfiguredLink =  function() {
        return this.createUnconfiguredLink;
    };

    CreationState.prototype.setCreateUnconfiguredLink =  function(createUnconfiguredLink) {
        this.createUnconfiguredLink = createUnconfiguredLink;
    };

    /**
     * Returns true if the user has specified that the initiating side is behind
     * a firewall.
     * @returns {boolean}
     */
    CreationState.prototype.isOneWayApplink =  function() {
        return this.oneWayApplink;
    };

    CreationState.prototype.setOneWayApplink =  function(oneWayApplink) {
        this.oneWayApplink = oneWayApplink;
    };

    CreationState.prototype.getConfigureIncomingUserAuthentication =  function() {
        return this.configureIncomingUserAuthentication;
    };

    CreationState.prototype.setConfigureIncomingUserAuthentication =  function(configureUserAuthentication) {
        this.configureIncomingUserAuthentication = configureUserAuthentication;
    };

    CreationState.prototype.getConfigureIncomingServerAuthentication =  function() {
        return this.configureIncomingServerAuthentication;
    };

    CreationState.prototype.setConfigureIncomingServerAuthentication =  function(configureServerAuthentication) {
        this.configureIncomingServerAuthentication = configureServerAuthentication;
    };

    CreationState.prototype.getConfigureIncomingPreAuthentication =  function() {
        return this.configureIncomingPreAuthentication;
    };

    CreationState.prototype.setConfigureIncomingPreAuthentication =  function(configurePreAuthentication) {
        this.configureIncomingPreAuthentication = configurePreAuthentication;
    };

    CreationState.prototype.getConfigureOutgoingUserAuthentication =  function() {
        return this.configureOutgoingUserAuthentication;
    };

    CreationState.prototype.setConfigureOutgoingUserAuthentication =  function(configureUserAuthentication) {
        this.configureOutgoingUserAuthentication = configureUserAuthentication;
    };

    CreationState.prototype.getConfigureOutgoingServerAuthentication =  function() {
        return this.configureOutgoingServerAuthentication;
    };

    CreationState.prototype.setConfigureOutgoingServerAuthentication =  function(configureServerAuthentication) {
        this.configureOutgoingServerAuthentication = configureServerAuthentication;
    };

    CreationState.prototype.getConfigureOutgoingPreAuthentication =  function() {
        return this.configureOutgoingPreAuthentication;
    };

    CreationState.prototype.setConfigureOutgoingPreAuthentication =  function(configurePreAuthentication) {
        this.configureOutgoingPreAuthentication = configurePreAuthentication;
    };

    CreationState.prototype.getConfigureIncomingAuthentication = function() {
        return (this.getConfigureIncomingUserAuthentication()
            || this.getConfigureIncomingServerAuthentication()
            || this.getConfigureIncomingPreAuthentication());
    }
    window.CreationState = CreationState;
}(require('applinks/js/util')));
