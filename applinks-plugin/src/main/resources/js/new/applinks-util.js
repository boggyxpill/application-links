define('applinks/js/util', [
    'applinks/lib/aui',
    'applinks/lib/jquery',
    'applinks/lib/window',
    'applinks/common/features'
], function(
    AJS,
    jQuery,
    window,
    ApplinksFeatures
) {
    window.AppLinks = window.AppLinks || {};

    // Maintain backwards compatibility with pre-AMD conversion by assigning to the global
    window.AppLinks.Util = {
        /**
         * In the (possible) absence of an AUI way to do this check if the browser is IE8
         * @returns {*|boolean}
         */
        isPreIE9: function() {
            return jQuery.browser.msie && jQuery.browser.version <= 8;
        },
        manifestToApplication: function(manifest) {
            return {
                url: manifest.url,
                rpcUrl: manifest.url,
                name: manifest.name,
                product: manifest.typeId ? AppLinks.I18n.getApplicationTypeName(manifest.typeId) : undefined,
                logoUrl: manifest.iconUri ? manifest.iconUri : manifest.iconUrl,
                typeId: manifest.typeId,
                id: manifest.id,
                applinksVersion: manifest.applinksVersion,
                publicSignUp: manifest.publicSignup,
                inboundAuthenticationTypes: manifest.inboundAuthenticationTypes,
                outboundAuthenticationTypes: manifest.outboundAuthenticationTypes
            }
        },
        applicationSupportsIncomingThreeLeggedOAuth: function(application) {
            return this.applicationSupportsIncomingAuth(application, "com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider");
        },
        applicationSupportsIncomingTwoLeggedOAuth: function(application) {
            return this.applicationSupportsIncomingAuth(application, "com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider");
        },
        applicationSupportsIncomingTwoLeggedOAuthWithImpersonation: function(application) {
            return this.applicationSupportsIncomingAuth(application, "com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider");
        },
        applicationSupportsOutgoingThreeLeggedOAuth: function(application) {
            return this.applicationSupportsOutgoingAuth(application, "com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider");
        },
        applicationSupportsOutgoingTwoLeggedOAuth: function(application) {
            return this.applicationSupportsOutgoingAuth(application, "com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider");
        },
        applicationSupportsOutgoingTwoLeggedOAuthWithImpersonation: function(application) {
            return this.applicationSupportsOutgoingAuth(application, "com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider");
        },
        applicationSupportsIncomingAuth: function(application, authProvider) {
            return this.applicationSupportsAuth(application.inboundAuthenticationTypes, authProvider);
        },
        applicationSupportsOutgoingAuth: function(application, authProvider) {
            return this.applicationSupportsAuth(application.outboundAuthenticationTypes, authProvider);
        },
        applicationSupportsAuth: function(authProviders, authProvider) {
            return AJS.$.inArray(authProvider,authProviders) >= 0 ? true : false;
        },
        waitUntilDone: function(callback){
            var callbacks = callback(),
                wait = callbacks.wait,
                done = callbacks.done;

            var deferred = new AJS.$.Deferred();
            wait();
            setTimeout(function(){
                deferred.resolve();
            }, 2000);

            return function(){
                deferred.done(done);
            };
        },
        getFieldValueFromQueryString: function (queryString, fieldName) {
            var fieldIndex = window.location.search.indexOf(fieldName);
            if (fieldIndex<0) {
                return null;
            }
            var fieldValueString;
            var nextParamIndex = queryString.slice(fieldIndex).indexOf("&");
            if (nextParamIndex > 0) {
                fieldValueString = queryString.slice(fieldIndex + fieldName.length + "=".length, fieldIndex + nextParamIndex);
            } else {
                fieldValueString = queryString.slice(fieldIndex + fieldName.length + "=".length);
            }

            var decodedFieldValue = decodeURIComponent(fieldValueString);
            try {
                return JSON.parse(decodedFieldValue);
            } catch(e) {
                console.log('Unable to JSON parse: [' + decodedFieldValue + "], using as is");
                return decodedFieldValue;
            }
        },
        /**
         * Find the value for the named parameter.
         * Checks first in sessionStorage and then in the queryString.
         * @param fieldName the name of the parameter.
         * @param clean flag indicating if the value should be read and then cleared for sessionStorage.
         */
        getParameterValue: function (fieldName, clean)
        {
            // check sessionstorage first
            if(typeof(Storage)!=="undefined") {
                var value = window.sessionStorage.getItem(fieldName);
                if(typeof value !== 'undefined'
                    && value != null ) {
                    if(clean === true) {
                        window.sessionStorage.removeItem(fieldName);
                    }
                    return JSON.parse(value);
                }
            }

            // if not there try querystring
            return this.getFieldValueFromQueryString(window.location.search, fieldName);
        },

        /**
         * Put a pair (fieldName, fieldValue) into sessionStorage (if it exists)
         * or in the given parametersMap
         */
        putParameter: function (fieldName, fieldValue, parametersMap) {
            if(typeof(Storage)!=="undefined") {
                window.sessionStorage.setItem(fieldName, JSON.stringify(fieldValue));
            } else {
                parametersMap[fieldName] = fieldValue;
            }
        },

        /**
         * Check the data-sysadmin-flag from list_application_links vm.
         * @returns {*|jQuery}
         */
        isSysAdmin: function(){
            return AJS.$('#ual.list-links').data("sysadminFlag")
        },
        /**
         * Remove the disabled attribute and any sibling spinner from the button.
         * @param button
         */
        enableButton: function(button) {
            if (!button || !button.length) {
                return;
            }

            button.prop("disabled", false);
            button[0].idle();
        },
        /**
         * Mark the button as disabled.
         * When `busy` param is a truthy value, a spinner will be visible.
         * @param button
         * @param busy
         */
        disableButton: function(button, busy) {
            if (!button || !button.length) {
                return;
            }

            if (busy) {
                button[0].busy();
            }
            button.prop("disabled", true);
        },
        /**
         * set the specified button to be enabled or disabled.
         * A disabled button can shown with a spinner if it is busy.
         * @param button
         * @param enabled
         * @param busy
         */
        setButtonEnabledState: function(button, enabled, busy) {
            if (enabled) {
                this.enableButton(button);
            } else {
                this.disableButton(button, busy);
            }
        },
        /**
         * check the receivedVersion string implies a version equal to or greater than v4.0.0
         * expects a string in the form '([0-9]\.[0-9]\.[0-9])(\.*[0-9A-Za-z-]*)'
         * @param receivedVersion
         * @returns {boolean} true if 4.0.0 or higher, false otherwise.
         */
        isVersion4OrHigher: function(receivedVersion) {
            var receivedVersionParts = receivedVersion.match('([0-9]\.[0-9]\.[0-9])(\.*[0-9A-Za-z-]*)');

            if(receivedVersionParts == null) {
                console.log('Received version is in an unrecognized format ' + receivedVersion );
                return false;
            }

            var majorMinorPatchString = receivedVersionParts[1];
            var prereleaseString = receivedVersionParts[2];
            var majorMinorPatchParts = majorMinorPatchString.split('.');

            // check major version is at least 4
            if(parseFloat(majorMinorPatchParts[0]) < 4) {
                return false;
            }

            if(parseFloat(majorMinorPatchParts[0]) > 4) {
                return true;
            }

            // we have a v4.#.#

            // check minor version is greater than 0
            if(parseFloat(majorMinorPatchParts[1]) > 0) {
                return true;
            }

            // check patch version is greater than 0
            if(parseFloat(majorMinorPatchParts[2]) > 0) {
                return true;
            }

            // we have a 4.0.0 version is it prerelease?
            if(prereleaseString == null
                || prereleaseString === ''
                || prereleaseString === '.SNAPSHOT') {
                // not a prerelease so run with it.
                return true;
            }

            // if in doubt not compatible.
            return false;
        },

        isOneWayEnabled: function() {
            return ApplinksFeatures.isEnabled(ApplinksFeatures.ONE_WAY_APPLINKS);
        }
    };

    return window.AppLinks.Util;
});
