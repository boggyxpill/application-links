(function($, applinksUrls, applinksUtil) {
    AppLinks.Creation = {
        // parameter key names
        CREATION_REMOTE_URL : "applinkStartingUrl",
        CREATION_REMOTE_TYPE : "applinkStartingType",
        ORIGINAL_APPLINK_ID_KEY : "applinkOriginalCreatedId",
        CREATION_STATUS_KEY : "applinkCreationStatus",
        CREATION_STATUS_LOG_KEY : "applinkCreationStatusLog",
        CREATION_STATUS_REASON_KEY : "applinkCreationStatusReason",
        CREATION_SHAREDUSERBASE_KEY : "sharedUserbase",
        CREATION_RESULT_INFO_KEY : "applinkCreationResultInfo",
        CREATION_RESULT_WARN_KEY : "applinkCreationResultWarn",
        CREATION_RESULT_ERROR_KEY : "applinkCreationResultError",
        CREATION_IGNORE_REDIRECTS : "applinkIgnoreRedirects",
        CREATION_CONFIGURE_INCOMING_USER_AUTH : "applinkConfigInUserAuth",
        CREATION_CONFIGURE_INCOMING_SERVER_AUTH : "applinkConfigInServerAuth",
        CREATION_CONFIGURE_INCOMING_PRE_AUTH : "applinkConfigInPreAuth",
        CREATION_CONFIGURE_OUTGOING_USER_AUTH : "applinkConfigOutUserAuth",
        CREATION_CONFIGURE_OUTGOING_SERVER_AUTH : "applinkConfigOutServerAuth",
        CREATION_CONFIGURE_OUTGOING_PRE_AUTH : "applinkConfigOutPreAuth",
        CREATION_EVENT_NEW_APPLINK_TYPE: "applinkCreatedEventNewType",
        CREATION_EVENT_NEW_APPLINK_ID: "applinkCreatedEventNewId",
        CREATION_ONE_WAY_APPLINK: "oneWayApplink",

        // statuses
        STATUS_CREATION_SUCCESS : "completed.creation",
        STATUS_CREATION_FAILURE : "failed.creation",
        STATUS_CREATION_INPROGRESS : "inprogress.creation",
        STATUS_LOCAL_SUCCESS : "completed.creation.local",
        STATUS_RECIPROCAL_SUCCESS : "completed.creation.reciprocal",
        STATUS_RECIPROCAL_INPROGRESS : "inprogress.creation.reciprocal",
        STATUS_LOCAL_FAILURE : "failed.creation.local",
        STATUS_RECIPROCAL_FAILURE : "failed.creation.reciprocal",
        STATUS_REGISTERPROVIDER_2LOI_FAILURE: "failed.registerprovider.2loi",

        // UI features
        userDefinedUrlField: {},
        createAppLinkButton: {},
        // a global holder of the current creation state
        // only used during initiation and "out of process".
        // "in process" the creation state is passed as parameters.
        globalCreationState: null,
        /**
         * Generate a url from the base and parameters.
         * @param baseUrl the baseUrl
         * @param parametersMap a map of parameters to add, in the form of { parameterName :  parameterValue }f
         * @return {*}
         */
        generateUrl: function(baseUrl, parametersMap) {
            return applinksUrls.generateUrl(baseUrl, parametersMap);
        },
        /**
         * Generate a URL to the Applinks Admin screen.
         * @param remoteBaseUrl
         * @param parametersMap
         * @returns {*}
         */
        generateApplinksAdminUrl : function(remoteBaseUrl, remoteTypeId, parametersMap) {
            return applinksUrls.Remote.admin(remoteBaseUrl, remoteTypeId, parametersMap);
        },
        /**
         * Generate the url to use to redirect the user between application during applink creation.
         * @return {*}
         */
        generateApplinksCreationRedirectionUrl: function (allowRedirectionBack, creationState) {
            if(creationState.getIncompatibleApplinksVersion()) {
                return AppLinks.Creation.generateApplinksAdminUrl(creationState.getRemoteApplicationUrl(), creationState.getRemoteApplicationType());
            } else {
                var parametersMap = {};

                if(creationState.getStatusLog().length > 0) {
                    parametersMap[AppLinks.Creation.CREATION_STATUS_LOG_KEY] = JSON.stringify(creationState.getStatusLog());
                }

                // track the ID of the new application link created on the local (application 1)
                if(creationState.getOriginalCreatedApplicationLinkId() !== null) {
                    parametersMap[AppLinks.Creation.ORIGINAL_APPLINK_ID_KEY] = creationState.getOriginalCreatedApplicationLinkId();
                }

                // if the process was started externally make sure we carry the initiatorRedirect around with us.
                if(creationState.getInitiatorRedirect() !== null) {
                    parametersMap[AppLinks.ExternalInitiation.INITIATOR_REDIRECT_KEY] =creationState.getInitiatorRedirect();
                }

                parametersMap[AppLinks.Creation.CREATION_SHAREDUSERBASE_KEY] = creationState.getSharedUserbase();

                parametersMap[AppLinks.Creation.CREATION_CONFIGURE_INCOMING_USER_AUTH] = creationState.getConfigureIncomingUserAuthentication();
                parametersMap[AppLinks.Creation.CREATION_CONFIGURE_INCOMING_SERVER_AUTH] = creationState.getConfigureIncomingServerAuthentication();
                parametersMap[AppLinks.Creation.CREATION_CONFIGURE_INCOMING_PRE_AUTH] = creationState.getConfigureIncomingPreAuthentication();

                parametersMap[AppLinks.Creation.CREATION_CONFIGURE_OUTGOING_USER_AUTH] = creationState.getConfigureOutgoingUserAuthentication();
                parametersMap[AppLinks.Creation.CREATION_CONFIGURE_OUTGOING_SERVER_AUTH] = creationState.getConfigureOutgoingServerAuthentication();
                parametersMap[AppLinks.Creation.CREATION_CONFIGURE_OUTGOING_PRE_AUTH] = creationState.getConfigureOutgoingPreAuthentication();

                // The initiating side is outbound only
                parametersMap[AppLinks.Creation.CREATION_ONE_WAY_APPLINK] = creationState.isOneWayApplink();

                if (creationState.isNonInitiatingSide()) {
                    if (creationState.isOneWayApplink()) {
                        parametersMap[AppLinks.Creation.CREATION_CONFIGURE_OUTGOING_USER_AUTH] = true;
                        parametersMap[AppLinks.Creation.CREATION_CONFIGURE_OUTGOING_SERVER_AUTH] = true;
                        parametersMap[AppLinks.Creation.CREATION_CONFIGURE_OUTGOING_PRE_AUTH] = true;
                    }
                    return AppLinks.Creation.generateApplinksAdminUrl(creationState.getRemoteApplicationUrl(), creationState.getRemoteApplicationType(), parametersMap);
                }

                if(creationState.getLocalApplication() !== null) {
                    parametersMap[AppLinks.Creation.CREATION_REMOTE_URL] = creationState.getLocalApplication().url;
                    parametersMap[AppLinks.Creation.CREATION_REMOTE_TYPE] = creationState.getLocalApplication().typeId;
                }

                if(allowRedirectionBack !== true) {
                    parametersMap[AppLinks.Creation.CREATION_IGNORE_REDIRECTS] = true;
                }

                return AppLinks.Creation.generateApplinksAdminUrl(creationState.getRemoteApplicationUrl(), creationState.getRemoteApplicationType(), parametersMap);
            }
        },
        /**
         * Redirect back to Application, display informational dialog to the user.
         * @return {{wait: Function, done: Function}}
         */
        redirectUserWithDialog: function(allowRedirectionBack, creationState) {
            return {
                'wait': function() {
                    // TODO why is this fixed to be false?
                    AppLinks.Creation.Dialogs.showRedirectPauseDialog(false, creationState.getRemoteApplicationName());
                },
                'done': function(){
                    var redirectUrl = AppLinks.Creation.generateApplinksCreationRedirectionUrl(allowRedirectionBack, creationState);
                    window.location = redirectUrl;
                }};
        },
        /**
         * get the remoteUrl passed to the page
         * @returns {string}
         */
        getSubmittedRemoteUrl: function () {
            return applinksUtil.getParameterValue(AppLinks.Creation.CREATION_REMOTE_URL);
        },
        /**
         * get the statusLog passed to the page
         * @returns {string}
         */
        getSubmittedStatusLog: function () {
            var log = JSON.parse(applinksUtil.getParameterValue(AppLinks.Creation.CREATION_STATUS_LOG_KEY));
            if( log == null) {
                return [];
            }
            return log;
        },
        /**
         * get the sharedUserbase passed to the page
         * @returns {string}
         */
        getSubmittedSharedUserbase: function () {
            var sharedUserbase = applinksUtil.getParameterValue(AppLinks.Creation.CREATION_SHAREDUSERBASE_KEY)
            return sharedUserbase == null ? false : sharedUserbase;
        },
        /**
         * get the original created id passed to the page
         * @returns {string}
         */
        getSubmittedOriginalCreatedId: function () {
            return applinksUtil.getParameterValue(AppLinks.Creation.ORIGINAL_APPLINK_ID_KEY);
        },
        /**
         * Retrieve and Display any outstanding messages
         */
        getAndDisplayOutstandingMessages: function() {
            AppLinks.Creation.displayOutstandingMessages(
                AppLinks.Creation.CREATION_RESULT_INFO_KEY,
                AppLinks.Creation.getOutstandingMessages(AppLinks.Creation.CREATION_RESULT_INFO_KEY));
            AppLinks.Creation.displayOutstandingMessages(
                AppLinks.Creation.CREATION_RESULT_WARN_KEY,
                AppLinks.Creation.getOutstandingMessages(AppLinks.Creation.CREATION_RESULT_WARN_KEY));
            AppLinks.Creation.displayOutstandingMessages(
                AppLinks.Creation.CREATION_RESULT_ERROR_KEY,
                AppLinks.Creation.getOutstandingMessages(AppLinks.Creation.CREATION_RESULT_ERROR_KEY));
        },
        /**
         * Retrieve any outstanding messages
         */
        displayOutstandingMessages: function(key, messages) {
            AJS.$.each(messages, function(index, message) {
                if(key === AppLinks.Creation.CREATION_RESULT_INFO_KEY) {
                    AppLinks.UI.showInfoBox(AppLinks.UI.sanitiseHTML(message));
                }

                if(key === AppLinks.Creation.CREATION_RESULT_WARN_KEY) {
                    AppLinks.UI.showWarningBox(AppLinks.UI.sanitiseHTML(message));
                }

                if(key === AppLinks.Creation.CREATION_RESULT_ERROR_KEY) {
                    AppLinks.UI.showErrorBox(AppLinks.UI.sanitiseHTML(message));
                }
            });

        },
        /**
         * Get any messages for the specified key to display on screen.
         * @param key
         **/
        getOutstandingMessages: function(key) {
            var messageString = applinksUtil.getParameterValue(key, true);
            if (AJS.$.isArray(messageString)) {
                return messageString;
            }
            try {
                var messages = JSON.parse(messageString);
                if(messages != null) {
                return messages;
                }
            }catch(e){
                console.log("Message format invalid:", messageString);
            }
            return [];
        },
        /**
         * Initialize the creation state for the process, regardless of whether there was a creation state before.
         */
        initCreationState: function() {
            AppLinks.Creation.globalCreationState = new CreationState();
        },
        /**
         * get the current global creation state, will initialize it if necessary.
         * @returns {null}
         */
        getCreationState: function() {
            if(AppLinks.Creation.globalCreationState == null) {
                AppLinks.Creation.globalCreationState = new CreationState();
            }
            return AppLinks.Creation.globalCreationState;
        },
        /**
         * Check if processing means the browser will be redirected away form the current page.
         * @returns {boolean}
         */
        aboutToRedirect: function() {
            // no querystring so no processing, so no redirect
            if(window.location.search === "") {
                return false;
            }

            var localCreationState = AppLinks.Creation.getCreationState();

            if(localCreationState.doInitiatorRedirect()) {
                // externally initiated and about to redirect to external target.
                return true;
            } else if(!localCreationState.isNonInitiatingSide() && localCreationState.creationHasCompleted()) {
                // processing has completed so about to redirect to clean the querystring.
                return true;
            } else if(localCreationState.creationInProgress()) {
                // don't load the list if we are mid process as we might be leaving shortly and so might kill the rest call.
                // the completion of the process will cause another re-listing.
                return true;
            } else {
                return false;
            }
        },

        /**
         * Check request parameters and if the specified keys exist fire the event
         */
        fireEventIfApplinkCreated: function() {
            var applinkType = applinksUtil.getParameterValue(AppLinks.Creation.CREATION_EVENT_NEW_APPLINK_TYPE, true);
            var applinkId = applinksUtil.getParameterValue(AppLinks.Creation.CREATION_EVENT_NEW_APPLINK_ID, true);

            if (applinkType && applinkId) {

                var applinkTypeName = AppLinks.I18n.getApplicationTypeName(applinkType);

                /**
                 * devstatus is listening for "applink creation" event, that is fired here.
                 * We have a race condition: sometimes "applinks" fires the event before devstatus is able to register its listener.
                 * The proposed solution is that applinks attaches the required data as body attribute on firing the event,
                 * so in case devstatus finds these attributes it knows the event was already fired and this data can be used.
                 * On the other hand if devstatus could not find the attached attributes
                 * this means that the event is still not fired so it can register the listener
                 */
                var $body = $('body');

                $body.attr("data-created-applink-id", applinkId);
                $body.attr("data-created-applink-type", applinkType);
                $body.attr("data-created-applink-type-name", applinkTypeName);

                $(document).trigger(AppLinks.Event.NEW_APPLINK_CREATED, {
                    id: applinkId,
                    type: applinkType,
                    typeName: applinkTypeName
                });
            }
        }
    };
})(AJS.$, require('applinks/common/urls'), require('applinks/js/util'));
