package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.DependsOn;
import com.atlassian.applinks.api.event.ApplicationLinkAuthConfigChangedEvent;
import com.atlassian.applinks.core.property.ApplicationLinkProperties;
import com.atlassian.applinks.core.property.PropertyService;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.event.api.EventPublisher;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * @since 3.0
 */
@Component
public class AuthenticationConfigurationManagerImpl implements AuthenticationConfigurationManager {
    private static final Logger log = LoggerFactory.getLogger(AuthenticationConfigurationManagerImpl.class);

    private final ApplicationLinkService applicationLinkService;
    private final AuthenticatorAccessor authenticatorAccessor;
    private final PropertyService propertyService;
    private final EventPublisher eventPublisher;

    @Autowired
    public AuthenticationConfigurationManagerImpl(final ApplicationLinkService applicationLinkService, final AuthenticatorAccessor authenticatorAccessor, final PropertyService propertyService, final EventPublisher eventPublisher) {
        this.applicationLinkService = applicationLinkService;
        this.authenticatorAccessor = authenticatorAccessor;
        this.propertyService = propertyService;
        this.eventPublisher = eventPublisher;
    }

    public Map<String, String> getConfiguration(final ApplicationId id, final Class<? extends AuthenticationProvider> provider) {
        assertApplicationLinkPresence(id);
        return propertyService.getApplicationLinkProperties(id).getProviderConfig(getPrefixForProvider(provider));
    }

    public boolean isConfigured(final ApplicationId id, final Class<? extends AuthenticationProvider> provider) {
        return propertyService.getApplicationLinkProperties(id).authProviderIsConfigured(getPrefixForProvider(provider));
    }

    public void registerProvider(final ApplicationId id, final Class<? extends AuthenticationProvider> provider, final Map<String, String> config) {
        assertApplicationLinkPresence(id);
        final ApplicationLinkProperties props = propertyService.getApplicationLinkProperties(id);
        props.setProviderConfig(getPrefixForProvider(provider), config);
        publishChangeEvent(id);
    }

    public void unregisterProvider(final ApplicationId id, final Class<? extends AuthenticationProvider> provider) {
        final ApplicationLink applicationLink = assertApplicationLinkPresence(id);
        final ApplicationLinkProperties props = propertyService.getApplicationLinkProperties(id);
        props.removeProviderConfig(getPrefixForProvider(provider));
        for (final Class<? extends AuthenticationProvider> dependentProviderClass : findDependentProviders(provider, applicationLink)) {
            // if the provider class depends on the current provider class that we wants to delete, delete it as well
            props.removeProviderConfig(getPrefixForProvider(dependentProviderClass));
        }
        publishChangeEvent(id);
    }

    /**
     * Find the provider classes that indicate that it depends on the given {@code providerClass} by marking with the {@link com.atlassian.applinks.api.auth.DependsOn}.
     * Only provider classes that are configured on the given {@code applicationLink} will be considered.
     * <br>
     * NOTE: The dependency check works recursively.
     *
     * @param providerClass   the provider class that other providers will depend on
     * @param applicationLink the application link that all relevant providers are associated with
     */
    private Set<Class<? extends AuthenticationProvider>> findDependentProviders(final Class<? extends AuthenticationProvider> providerClass, final ApplicationLink applicationLink) {
        final Iterable<AuthenticationProviderPluginModule> allAuthenticationProviderPluginModules = authenticatorAccessor.getAllAuthenticationProviderPluginModules();
        final Set<Class<? extends AuthenticationProvider>> allDependentProviders = Sets.newHashSet();
        return findDependentProviders(ImmutableList.<Class<? extends AuthenticationProvider>>of(providerClass), applicationLink, allAuthenticationProviderPluginModules, allDependentProviders);
    }

    private Set<Class<? extends AuthenticationProvider>> findDependentProviders(final Collection<Class<? extends AuthenticationProvider>> providerClasses,
                                                                                final ApplicationLink applicationLink,
                                                                                final Iterable<AuthenticationProviderPluginModule> allAuthenticationProviderPluginModules,
                                                                                final Set<Class<? extends AuthenticationProvider>> allDependentProviders) {
        final Set<Class<? extends AuthenticationProvider>> newDependentProviders = Sets.newHashSet();
        for (final AuthenticationProviderPluginModule module : allAuthenticationProviderPluginModules) {
            final AuthenticationProvider authenticationProvider = module.getAuthenticationProvider(applicationLink);
            if (authenticationProvider != null) {
                final Class<? extends AuthenticationProvider> dependentProviderClass = module.getAuthenticationProviderClass();
                try {
                    final DependsOn dependsOn = dependentProviderClass.getAnnotation(DependsOn.class);
                    if (dependsOn != null) {
                        for (Class<? extends AuthenticationProvider> dependedProviderClass : dependsOn.value()) {
                            if (providerClasses.contains(dependedProviderClass) && !allDependentProviders.contains(dependentProviderClass)) {
                                // if the provider class depends on the current batch of provider classes being checked
                                //  it means that it depends indirectly on the original batch of provider classes
                                //  add it if it was not previously found
                                newDependentProviders.add(dependentProviderClass);
                            }
                        }
                    }
                } catch (NoClassDefFoundError ncdfe) {
                    log.error("Unable to determine authentication provider dependents", ncdfe);
                }
            }
        }
        if (newDependentProviders.isEmpty()) {
            return allDependentProviders;
        } else {
            // recursively find new providers that might indirectly depend on the original providers
            allDependentProviders.addAll(newDependentProviders);
            return findDependentProviders(newDependentProviders, applicationLink, allAuthenticationProviderPluginModules, allDependentProviders);
        }
    }

    private ApplicationLink assertApplicationLinkPresence(final ApplicationId id) {
        final ApplicationLink applicationLink;
        applicationLink = getApplicationLink(id);

        if (applicationLink == null) {
            throw new IllegalArgumentException(String.format(
                    "Application Link \"%s\" not found.", id));
        }

        return applicationLink;
    }

    private ApplicationLink getApplicationLink(final ApplicationId id) {
        final ApplicationLink applicationLink;
        try {
            applicationLink = applicationLinkService.getApplicationLink(id);
        } catch (TypeNotInstalledException e) {
            throw new IllegalStateException(String.format(
                    "Failed to load application link %s as type %s is no longer installed.",
                    id, e.getType()));
        }
        return applicationLink;
    }

    private String getPrefixForProvider(final Class<? extends AuthenticationProvider> provider) {
        return requireNonNull(provider, "AuthenticationProvider").getName();
    }

    private void publishChangeEvent(final ApplicationId id) {
        final ApplicationLink link = getApplicationLink(id);
        if (link != null) {
            eventPublisher.publish(new ApplicationLinkAuthConfigChangedEvent(link));
        }
    }
}
