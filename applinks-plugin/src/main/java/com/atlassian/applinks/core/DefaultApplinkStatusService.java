package com.atlassian.applinks.core;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.internal.common.exception.InvalidArgumentException;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.internal.status.error.ApplinkError;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkErrorVisitor;
import com.atlassian.applinks.internal.status.error.AuthorisationUriAwareApplinkError;
import com.atlassian.applinks.internal.status.error.ResponseApplinkError;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.oauth.OAuthStatusService;
import com.atlassian.applinks.internal.status.oauth.remote.OAuthConnectionVerifier;
import com.atlassian.applinks.internal.status.oauth.remote.RemoteOAuthStatusService;
import com.atlassian.applinks.internal.status.support.ApplinkStatusValidationService;
import com.atlassian.event.api.EventPublisher;
import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.EnumSet;
import java.util.concurrent.TimeUnit;

import static com.atlassian.applinks.core.DefaultApplinkStatus.working;
import static com.atlassian.applinks.internal.common.net.HttpUtils.toStatusString;
import static java.util.Objects.requireNonNull;

/**
 * Default implementation of {@link ApplinkStatusService}.
 *
 * @since 4.3
 */
@Component
public class DefaultApplinkStatusService implements ApplinkStatusService {
    private static final Logger log = LoggerFactory.getLogger(DefaultApplinkStatusService.class);

    // applink errors that are related to missing/invalid token and suggest authentication is required to retrieve
    // status. For those we want to make an extra OAuth check to make sure authentication is even possible
    private static final EnumSet<ApplinkErrorType> TOKEN_PROBLEMS = EnumSet.of(
            ApplinkErrorType.LOCAL_AUTH_TOKEN_REQUIRED,
            ApplinkErrorType.REMOTE_AUTH_TOKEN_REQUIRED
    );

    private final ApplinkHelper applinkHelper;
    private final ApplinkStatusValidationService statusValidationService;
    private final OAuthConnectionVerifier oAuthConnectionVerifier;
    private final OAuthStatusService oAuthStatusService;
    private final PermissionValidationService permissionValidationService;
    private final RemoteCapabilitiesService remoteCapabilitiesService;
    private final RemoteOAuthStatusService remoteOAuthStatusService;
    private final EventPublisher eventPublisher;

    @Autowired
    public DefaultApplinkStatusService(ApplinkHelper applinkHelper,
                                       ApplinkStatusValidationService statusValidationService,
                                       OAuthConnectionVerifier oAuthConnectionVerifier,
                                       OAuthStatusService oAuthStatusService,
                                       PermissionValidationService permissionValidationService,
                                       RemoteCapabilitiesService remoteCapabilitiesService,
                                       RemoteOAuthStatusService remoteOAuthStatusService,
                                       EventPublisher eventPublisher) {
        this.applinkHelper = applinkHelper;
        this.statusValidationService = statusValidationService;
        this.oAuthConnectionVerifier = oAuthConnectionVerifier;
        this.oAuthStatusService = oAuthStatusService;
        this.permissionValidationService = permissionValidationService;
        this.remoteCapabilitiesService = remoteCapabilitiesService;
        this.remoteOAuthStatusService = remoteOAuthStatusService;
        this.eventPublisher = eventPublisher;
    }

    @Nonnull
    @Override
    public ApplinkStatus getApplinkStatus(@Nonnull ApplicationId id) throws NoSuchApplinkException, NoAccessException {
        requireNonNull(id, "id");
        permissionValidationService.validateAdmin();

        ApplicationLink link = applinkHelper.getApplicationLink(id);
        DefaultApplinkStatus applinkStatus;
        ApplinkOAuthStatus localStatus = null;
        ApplinkOAuthStatus remoteStatus = null;

        try {
            localStatus = oAuthStatusService.getOAuthStatus(link);
            statusValidationService.checkLocalCompatibility(link);
            statusValidationService.checkVersionCompatibility(link);

            remoteStatus = remoteOAuthStatusService.fetchOAuthStatus(link);
            statusValidationService.checkOAuthSupportedCompatibility(localStatus);
            statusValidationService.checkOAuthSupportedCompatibility(remoteStatus);
            statusValidationService.checkOAuthMismatch(localStatus, remoteStatus);
            statusValidationService.checkLegacyAuthentication(link, localStatus, remoteStatus);
            statusValidationService.checkDisabled(localStatus, remoteStatus);
            verifyOAuthOnSuccessfulStatus(link);

            applinkStatus = working(link, localStatus, remoteStatus);
        } catch (Exception error) {
            // should not happen within the try-catch, but we should nevertheless propagate those rather than turn
            // them into a status
            Throwables.propagateIfInstanceOf(error, NoSuchApplinkException.class);
            Throwables.propagateIfInstanceOf(error, NoAccessException.class);

            applinkStatus = createStatus(id, link, localStatus, remoteStatus, checkForNetworkError(error, link));
        }

        return applinkStatus;
    }

    private static boolean isTokenProblem(Exception error) {
        return error instanceof ApplinkError && TOKEN_PROBLEMS.contains(ApplinkError.class.cast(error).getType());
    }

    private void verifyOAuthOnSuccessfulStatus(ApplicationLink link) throws NoAccessException {
        // Given successful status retrieval, we only need to verify OAuth if the status was obtained anonymously
        // without involving OAuth connection - using the status API; So do the check only if Status API is on
        if (hasStatusApi(link)) {
            oAuthConnectionVerifier.verifyOAuthConnection(link);
        }
    }

    private boolean hasStatusApi(ApplicationLink link) throws NoAccessException {
        try {
            return remoteCapabilitiesService.getCapabilities(link, 1, TimeUnit.HOURS).getCapabilities()
                    .contains(ApplinksCapabilities.STATUS_API);
        } catch (InvalidArgumentException e) {
            throw new AssertionError("Unexpected InvalidArgumentException when getting capabilities", e);
        }
    }

    private Exception checkForNetworkError(Exception originalError, ApplicationLink link) {
        // some applink errors indicate local/token problems, which means either no actual request have been made to the
        // remote app, or the OAuth framework has indicated that authentication is required - in which case any
        // potential existing network errors that would prevent any authentication attempt could go unnoticed.
        // To prevent this, use the OAuth verifier to force any network issues to surface
        if (isTokenProblem(originalError)) {
            try {
                oAuthConnectionVerifier.verifyOAuthConnection(link);
            } catch (Exception e) {
                return e;
            }
        }
        // otherwise go with the original error
        return originalError;
    }

    private DefaultApplinkStatus createStatus(ApplicationId id, ApplicationLink link, ApplinkOAuthStatus localStatus,
                                              ApplinkOAuthStatus remoteStatus, Exception error) {
        if (error instanceof ApplinkError) {
            final ApplinkError applinkError = (ApplinkError) error;
            switch (applinkError.getType().getCategory()) {
                case DISABLED:
                    return DefaultApplinkStatus.disabled(link, applinkError);
                case CONFIG_ERROR:
                    return DefaultApplinkStatus.configError(link, localStatus, remoteStatus, applinkError);
                case NETWORK_ERROR:
                    logApplinkError("Network", error, id);
                default:
                    return DefaultApplinkStatus.error(link, localStatus, remoteStatus, applinkError);
            }
        } else {
            logApplinkError("Unrecognized", error, id);
            return DefaultApplinkStatus.unknown(link, localStatus, error);
        }
    }

    private void logApplinkError(String errorType, Exception error, ApplicationId id) {
        log.warn("{} error while attempting to retrieve status of Application Link '{}'", errorType, id);
        // log applink error specifics
        if (error instanceof ApplinkError) {
            ApplinkError.class.cast(error).accept(new ApplinkErrorLogger(id));
        }
        log.debug("{} error trace for '{}'", errorType, id, error);
    }

    private static final class ApplinkErrorLogger implements ApplinkErrorVisitor<Void> {
        private final ApplicationId id;

        private ApplinkErrorLogger(ApplicationId id) {
            this.id = id;
        }

        @Nullable
        @Override
        public Void visit(@Nonnull ApplinkError error) {
            log.debug("'{}' error type: {}", id, error.getType());
            return null;
        }

        @Nullable
        @Override
        public Void visit(@Nonnull AuthorisationUriAwareApplinkError error) {
            visit((ApplinkError) error);
            // no specific logging for AuthorisationUriAwareApplinkError
            return null;
        }

        @Nullable
        @Override
        public Void visit(@Nonnull ResponseApplinkError responseError) {
            visit((ApplinkError) responseError);

            log.debug("'{}' response status: {}", id, toStatusString(responseError.getStatusCode()));
            if (responseError.getBody() != null) {
                log.debug("'{}' response content type: {}", id, responseError.getContentType());
                log.debug("'{}' response contents\n\n{}\n\n", id, responseError.getBody());
            } else {
                log.debug("'{}' response has no body", id);
            }

            return null;
        }
    }
}
