package com.atlassian.applinks.core.link;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.ImpersonatingAuthenticationProvider;
import com.atlassian.applinks.api.auth.NonImpersonatingAuthenticationProvider;
import com.atlassian.applinks.api.event.ApplicationLinkDetailsChangedEvent;
import com.atlassian.applinks.core.ImmutableApplicationLink;
import com.atlassian.applinks.core.auth.ApplicationLinkRequestFactoryFactory;
import com.atlassian.applinks.core.property.ApplicationLinkProperties;
import com.atlassian.applinks.spi.application.StaticUrlApplicationType;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.event.api.EventPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

import static java.util.Objects.requireNonNull;

public class DefaultApplicationLink implements InternalApplicationLink {
    private final ApplicationId id;
    private final ApplicationType type;
    private final ApplicationLinkProperties applicationLinkProperties;

    private final ApplicationLinkRequestFactoryFactory requestFactoryFactory;
    private final EventPublisher eventPublisher;
    private static final Logger LOG = LoggerFactory.getLogger(DefaultApplicationLink.class.getName());

    public DefaultApplicationLink(
            final ApplicationId serverId,
            final ApplicationType type,
            final ApplicationLinkProperties applicationLinkProperties,
            final ApplicationLinkRequestFactoryFactory requestFactoryFactory,
            final EventPublisher eventPublisher) {
        this.id = requireNonNull(serverId,"serverId can't be null");
        this.applicationLinkProperties = requireNonNull(applicationLinkProperties,"applicationLinkProperties can't be null");
        this.requestFactoryFactory = requireNonNull(requestFactoryFactory,"requestFactoryFactory can't be null");
        this.type = requireNonNull(type,"type can't be null");
        this.eventPublisher = requireNonNull(eventPublisher,"eventPublisher can't be null");
    }

    public void update(final ApplicationLinkDetails details) {
        ImmutableApplicationLink originalApplink = new ImmutableApplicationLink(this, requestFactoryFactory);
        applicationLinkProperties.setName(details.getName());
        applicationLinkProperties.setDisplayUrl(details.getDisplayUrl());
        //TODO: Do we support to change the RPC URL at all?
        applicationLinkProperties.setRpcUrl(details.getRpcUrl());

        eventPublisher.publish(new ApplicationLinkDetailsChangedEvent(this, originalApplink));
    }

    public void setPrimaryFlag(final boolean isPrimary) {
        applicationLinkProperties.setIsPrimary(isPrimary);
    }

    public void setSystem(final boolean isSystem) {
        applicationLinkProperties.setSystem(isSystem);
    }

    public ApplicationId getId() {
        return id;
    }

    public ApplicationType getType() {
        return type;
    }

    public String getName() {
        return applicationLinkProperties.getName();
    }

    public URI getDisplayUrl() {
        if (type instanceof StaticUrlApplicationType) {
            return ((StaticUrlApplicationType) type).getStaticUrl();
        } else {
            return applicationLinkProperties.getDisplayUrl();
        }
    }

    public URI getRpcUrl() {
        if (type instanceof StaticUrlApplicationType) {
            return ((StaticUrlApplicationType) type).getStaticUrl();
        } else {
            return applicationLinkProperties.getRpcUrl();
        }
    }

    public boolean isPrimary() {
        return applicationLinkProperties.isPrimary();
    }

    public boolean isSystem() {
        return applicationLinkProperties.isSystem();
    }

    public ApplicationLinkRequestFactory createAuthenticatedRequestFactory() {
        return requestFactoryFactory.getApplicationLinkRequestFactory(this);
    }

    public ApplicationLinkRequestFactory createAuthenticatedRequestFactory(final Class<? extends AuthenticationProvider> providerClass) {
        return requestFactoryFactory.getApplicationLinkRequestFactory(this, providerClass);
    }

    public ApplicationLinkRequestFactory createImpersonatingAuthenticatedRequestFactory() {
        return requestFactoryFactory.getApplicationLinkRequestFactory(this, ImpersonatingAuthenticationProvider.class);
    }

    public ApplicationLinkRequestFactory createNonImpersonatingAuthenticatedRequestFactory() {
        return requestFactoryFactory.getApplicationLinkRequestFactory(this, NonImpersonatingAuthenticationProvider.class);
    }

    public Object getProperty(final String key) {
        return applicationLinkProperties.getProperty(key);
    }

    public Object putProperty(final String key, final Object value) {
        if (LOG.isDebugEnabled()) {
            String message = String.format("Putting property [%s] as [%s] for application link [%s/%s]",
                    key,
                    value,
                    this.getId() != null ? this.getId().get() : null,
                    this.getRpcUrl() != null ? this.getRpcUrl().toString() : null);
            LOG.debug(message);
        }

        return applicationLinkProperties.putProperty(key, value);
    }

    public Object removeProperty(final String key) {
        if (LOG.isDebugEnabled()) {
            String message = String.format("Removing property [%s] was [%s] for application link [%s/%s]",
                    key,
                    applicationLinkProperties.getProperty(key),
                    this.getId() != null ? this.getId().get() : null,
                    this.getRpcUrl() != null ? this.getRpcUrl().toString() : null);
            LOG.debug(message);
        }

        return applicationLinkProperties.removeProperty(key);
    }

    @Override
    public String toString() {
        return String.format("%s (%s) %s %s", getName(), id, getRpcUrl(), getType());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final DefaultApplicationLink that = (DefaultApplicationLink) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
