package com.atlassian.applinks.core;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.status.error.ApplinkError;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.SimpleApplinkError;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.internal.status.error.ApplinkErrorCategory.CONFIG_ERROR;
import static com.atlassian.applinks.internal.status.error.ApplinkErrorCategory.DISABLED;
import static com.atlassian.applinks.internal.status.error.ApplinkErrorCategory.UNKNOWN;
import static com.atlassian.applinks.internal.status.error.ApplinkErrors.toDetails;
import static java.util.Objects.requireNonNull;

public final class DefaultApplinkStatus implements ApplinkStatus {
    private final ApplicationLink link;
    private final ApplinkError errorDetails;

    private final ApplinkOAuthStatus localAuthentication;
    private final ApplinkOAuthStatus remoteAuthentication;

    private DefaultApplinkStatus(@Nonnull ApplicationLink link, @Nonnull ApplinkOAuthStatus localAuthentication,
                                 @Nullable ApplinkOAuthStatus remoteAuthentication, @Nullable ApplinkError error) {
        this.link = requireNonNull(link, "link");
        this.errorDetails = error;

        this.localAuthentication = requireNonNull(localAuthentication, "localAuthentication");
        this.remoteAuthentication = remoteAuthentication;
    }

    /**
     * Create a {@link ApplinkStatus#isWorking()}) working status with matching local and remote {@code authentication}.
     *
     * @param link                 applink
     * @param localAuthentication  authentication configuration set on both local server
     * @param remoteAuthentication authentication configuration set on both remote server
     * @return working status with {@code authentication}
     */
    @Nonnull
    public static DefaultApplinkStatus working(@Nonnull ApplicationLink link,
                                               @Nonnull ApplinkOAuthStatus localAuthentication,
                                               @Nonnull ApplinkOAuthStatus remoteAuthentication) {
        return new DefaultApplinkStatus(link, localAuthentication, remoteAuthentication, null);
    }

    /**
     * Create a disabled status where both local and remote are disabled.
     *
     * @param link  applink
     * @param error the error with category
     *              {@link com.atlassian.applinks.internal.status.error.ApplinkErrorCategory#DISABLED DISABLED}
     * @return disabled status.
     */
    @Nonnull
    public static DefaultApplinkStatus disabled(@Nonnull ApplicationLink link,
                                                @Nonnull ApplinkError error) {
        requireNonNull(error, "error");
        Validate.isTrue(DISABLED.equals(error.getType().getCategory()), "error");
        return new DefaultApplinkStatus(link, ApplinkOAuthStatus.OFF, ApplinkOAuthStatus.OFF, error);
    }

    /**
     * Create a config error status where both local and remote authentication are available but user configuration error
     * has occurred such as level mismatch
     *
     * @param link                 applink
     * @param localAuthentication  authentication configuration set on both local server
     * @param remoteAuthentication authentication configuration set on both remote server
     * @param error                the error with category {@link com.atlassian.applinks.internal.status.error.ApplinkErrorCategory#CONFIG_ERROR CONFIG_ERROR}
     * @return config error status
     */
    @Nonnull
    public static DefaultApplinkStatus configError(@Nonnull ApplicationLink link,
                                                   @Nonnull ApplinkOAuthStatus localAuthentication,
                                                   @Nonnull ApplinkOAuthStatus remoteAuthentication,
                                                   @Nonnull ApplinkError error) {
        requireNonNull(error, "error");
        Validate.isTrue(CONFIG_ERROR.equals(error.getType().getCategory()));
        return new DefaultApplinkStatus(link, localAuthentication, remoteAuthentication, error);
    }

    /**
     * Create error status for an arbitrary error status, with local authentication being available and optional remote
     * authentication.
     *
     * @param link                 applink
     * @param localAuthentication  authentication configuration set on the local server
     * @param remoteAuthentication authentication configuration set on the remote server
     * @param error                the Applink error that occurred
     * @return error status
     */
    @Nonnull
    public static DefaultApplinkStatus error(@Nonnull ApplicationLink link,
                                             @Nonnull ApplinkOAuthStatus localAuthentication,
                                             @Nullable ApplinkOAuthStatus remoteAuthentication,
                                             @Nonnull ApplinkError error) {
        requireNonNull(error, "error");
        return new DefaultApplinkStatus(link, localAuthentication, remoteAuthentication, error);
    }

    /**
     * Create error status for an arbitrary error status, with local authentication being available and no remote
     * authentication.
     *
     * @param link                applink
     * @param localAuthentication authentication configuration set on the local server
     * @param error               the Applink error that occurred
     * @return error status
     */
    @Nonnull
    public static DefaultApplinkStatus error(@Nonnull ApplicationLink link,
                                             @Nonnull ApplinkOAuthStatus localAuthentication,
                                             @Nonnull ApplinkError error) {
        requireNonNull(error, "error");
        return new DefaultApplinkStatus(link, localAuthentication, null, error);
    }

    /**
     * Create an unknown error status where an unknown AppLinkError has occurred outside our knowledge base.
     * This happens when there is {@link com.atlassian.applinks.internal.status.remote.RemoteStatusUnknownException remoteStatusUnknownException}.
     *
     * @param link                applink
     * @param localAuthentication authentication configuration set on both local server
     * @param error               any exception raised by the system that we cannot identify wrapped by us.
     * @return unknown exception status
     */
    @Nonnull
    public static DefaultApplinkStatus unknown(@Nonnull ApplicationLink link,
                                               @Nonnull ApplinkOAuthStatus localAuthentication,
                                               @Nonnull ApplinkError error) {
        requireNonNull(error, "error");
        Validate.isTrue(UNKNOWN.equals(error.getType().getCategory()));
        return new DefaultApplinkStatus(link, localAuthentication, null, error);
    }

    /**
     * Create an unknown error status where an exception may have occurred before localStatus has been retrieved.
     *
     * @param link      applink
     * @param exception any exception raised by the system that we cannot identify
     * @return unknown exception status
     */
    @Nonnull
    public static DefaultApplinkStatus unknown(@Nonnull ApplicationLink link,
                                               @Nullable ApplinkOAuthStatus localAuthentication,
                                               @Nonnull Exception exception) {
        final ApplinkOAuthStatus localStatus = localAuthentication == null ? ApplinkOAuthStatus.OFF : localAuthentication;
        return unknown(link, localStatus, createUnknownError(exception));
    }

    @Nonnull
    @Override
    public ApplicationLink getLink() {
        return link;
    }

    @Nullable
    @Override
    public ApplinkError getError() {
        return errorDetails;
    }

    @Override
    public boolean isWorking() {
        return errorDetails == null;
    }

    @Nonnull
    @Override
    public ApplinkOAuthStatus getLocalAuthentication() {
        return localAuthentication;
    }

    @Nullable
    @Override
    public ApplinkOAuthStatus getRemoteAuthentication() {
        return remoteAuthentication;
    }

    private static ApplinkError createUnknownError(Exception error) {
        return new SimpleApplinkError(ApplinkErrorType.UNKNOWN, toDetails(error));
    }
}
