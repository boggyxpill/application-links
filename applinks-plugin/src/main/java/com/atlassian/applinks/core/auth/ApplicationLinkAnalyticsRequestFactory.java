package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.net.Request;

import java.net.URI;

import static java.util.Objects.requireNonNull;

class ApplicationLinkAnalyticsRequestFactory implements ApplicationLinkRequestFactory {
    protected final ApplicationLinkRequestFactory wrappedFactory; // Protected for unit tests
    private final ApplicationLink remoteApplicationLink;
    private final EventPublisher publisher;

    ApplicationLinkAnalyticsRequestFactory(final ApplicationLinkRequestFactory wrappedFactory,
                                           final ApplicationLink remoteApplicationLink,
                                           final EventPublisher publisher) {
        this.wrappedFactory = requireNonNull(wrappedFactory);
        this.remoteApplicationLink = requireNonNull(remoteApplicationLink);
        this.publisher = requireNonNull(publisher);
    }

    @Override
    public ApplicationLinkRequest createRequest(final Request.MethodType methodType,
                                                final String url) throws CredentialsRequiredException {
        return new ApplicationLinkAnalyticsRequest(wrappedFactory.createRequest(methodType, url), remoteApplicationLink, publisher);
    }

    @Override
    public URI getAuthorisationURI(final URI callback) {
        return wrappedFactory.getAuthorisationURI(callback);
    }

    @Override
    public URI getAuthorisationURI() {
        return wrappedFactory.getAuthorisationURI();
    }
}
