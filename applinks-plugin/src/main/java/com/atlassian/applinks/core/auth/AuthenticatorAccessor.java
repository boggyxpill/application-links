package com.atlassian.applinks.core.auth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.core.plugin.AuthenticationProviderModuleDescriptor;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

import static com.atlassian.applinks.core.plugin.AuthenticationProviderModuleDescriptor.BY_WEIGHT;
import static java.util.Objects.requireNonNull;

/**
 * @since v3.0
 */
@Component
public class AuthenticatorAccessor {
    private final PluginAccessor pluginAccessor;
    private static final Logger log = LoggerFactory.getLogger(AuthenticatorAccessor.class);

    @Autowired
    public AuthenticatorAccessor(final PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    /**
     * Gets  an {@link com.atlassian.applinks.api.auth.AuthenticationProvider} instance that implements the
     * specified type and is configured for use with the specified {@link com.atlassian.applinks.api.ApplicationLink}.
     *
     * When more than one {@link com.atlassian.applinks.api.auth.AuthenticationProvider} satisfies these criteria,
     * the one with the highest priority (having the lowest weight) is returned.
     *
     * @return authentication provider, null if none satisfies the criteria
     */
    @SuppressWarnings("unchecked")
    public <T extends AuthenticationProvider> T getAuthenticationProvider(final ApplicationLink applicationLink, final Class<T> providerClass) {
        requireNonNull(applicationLink, "applicationLink can't be null");
        requireNonNull(providerClass, "providerClass can't be null");
        log.debug("Looking for {} for application link {}", providerClass, applicationLink.getName());

        for (final AuthenticationProviderPluginModule module : getAllAuthenticationProviderPluginModules()) {
            final AuthenticationProvider provider = module.getAuthenticationProvider(applicationLink);
            if (provider != null && providerClass.isAssignableFrom(provider.getClass())) {
                return (T) provider; // the cast is fine
            }
        }
        return null;
    }

    /**
     * Gets all the available {@link AuthenticationProviderPluginModule} in the system.
     *
     * @return all the available {@link AuthenticationProviderPluginModule} in the system ordered by weight ascendingly
     */
    public Iterable<AuthenticationProviderPluginModule> getAllAuthenticationProviderPluginModules() {
        final List<AuthenticationProviderModuleDescriptor> descriptors = Lists.newArrayList(pluginAccessor.getEnabledModuleDescriptorsByClass(AuthenticationProviderModuleDescriptor.class));
        Collections.sort(descriptors, BY_WEIGHT);

        return Iterables.transform(descriptors, new Function<AuthenticationProviderModuleDescriptor, AuthenticationProviderPluginModule>() {
            public AuthenticationProviderPluginModule apply(final AuthenticationProviderModuleDescriptor from) {
                return from.getModule();
            }
        });
    }
}
