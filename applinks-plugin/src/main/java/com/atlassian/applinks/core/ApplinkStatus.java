package com.atlassian.applinks.core;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.status.error.ApplinkError;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Represents status of an application link. Application links can either be {@link #isWorking() working},
 * or subject to various errors, as documented in
 * {@link com.atlassian.applinks.internal.status.error.ApplinkErrorType}. In case of an the details can be queried
 * using {@link #getError()}.
 *
 * @see ApplinkError
 * @since 4.3
 */
public interface ApplinkStatus {
    /**
     * @return the corresponding Application Link
     */
    @Nonnull
    ApplicationLink getLink();

    /**
     * @return optional representation of the error, should the link not be {@link #isWorking() working}, {@code null}
     * for working Application Links
     */
    @Nullable
    ApplinkError getError();

    /**
     * The basic status of this Application Link. If {@code false}, further details can be obtained via
     * {@link #getError()}.
     *
     * @return flag indicating whether this link is operational
     */
    boolean isWorking();

    /**
     * @return local OAuth configuration
     */
    @Nonnull
    ApplinkOAuthStatus getLocalAuthentication();

    /**
     * Remote OAuth status, if available. Certain {@link #getError() applink errors} (e.g.
     * {@link com.atlassian.applinks.internal.status.error.ApplinkErrorCategory#NETWORK_ERROR network errors}) will
     * prevent the system from obtaining the remote OAuth configuration.
     * <p>
     * In particular a {@link #isWorking() working} status implies that this property be non-{@code null} and
     * {@link com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus#matches(ApplinkOAuthStatus) matches} the
     * {@link #getLocalAuthentication() local authentication config}.
     * </p>
     *
     * @return remote configuration if available, otherwise {@code null}
     */
    @Nullable
    ApplinkOAuthStatus getRemoteAuthentication();
}
