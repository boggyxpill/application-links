package com.atlassian.applinks.core;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.applinks.api.EntityType;
import com.atlassian.applinks.api.PropertySet;
import com.atlassian.applinks.api.SubvertedEntityLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.event.ApplicationLinkAddedEvent;
import com.atlassian.applinks.api.event.ApplicationLinkDeletedEvent;
import com.atlassian.applinks.api.event.ApplicationLinkMadePrimaryEvent;
import com.atlassian.applinks.api.event.EntityLinkAddedEvent;
import com.atlassian.applinks.api.event.EntityLinkDeletedEvent;
import com.atlassian.applinks.core.auth.ApplicationLinkRequestFactoryFactory;
import com.atlassian.applinks.core.auth.AuthenticationConfigurator;
import com.atlassian.applinks.core.event.BeforeApplicationLinkDeletedEvent;
import com.atlassian.applinks.core.link.DefaultApplicationLink;
import com.atlassian.applinks.core.link.InternalApplicationLink;
import com.atlassian.applinks.core.link.InternalEntityLinkService;
import com.atlassian.applinks.core.property.ApplicationLinkProperties;
import com.atlassian.applinks.core.property.EntityLinkProperties;
import com.atlassian.applinks.core.property.PropertyService;
import com.atlassian.applinks.core.rest.client.ApplicationLinkClient;
import com.atlassian.applinks.core.rest.client.EntityLinkClient;
import com.atlassian.applinks.core.rest.context.CurrentContext;
import com.atlassian.applinks.core.rest.model.ApplicationLinkEntity;
import com.atlassian.applinks.core.rest.model.ErrorListEntity;
import com.atlassian.applinks.core.rest.ui.AuthenticationResource;
import com.atlassian.applinks.core.rest.util.RestUtil;
import com.atlassian.applinks.core.v1.rest.ApplicationLinkResource;
import com.atlassian.applinks.host.spi.EntityReference;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.net.BasicHttpAuthRequestFactory;
import com.atlassian.applinks.internal.common.net.Uris;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.applinks.spi.auth.AuthenticationScenario;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.spi.link.AuthenticationResponseException;
import com.atlassian.applinks.spi.link.EntityLinkBuilderFactory;
import com.atlassian.applinks.spi.link.LinkCreationResponseException;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.link.NotAdministratorException;
import com.atlassian.applinks.spi.link.ReciprocalActionException;
import com.atlassian.applinks.spi.link.RemoteErrorListException;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.NotificationException;
import com.atlassian.plugin.util.ChainingClassLoader;
import com.atlassian.plugin.util.ClassLoaderUtils;
import com.atlassian.plugins.rest.common.Link;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.atlassian.applinks.internal.application.IconUriResolver.resolveIconUri;
import static com.atlassian.applinks.spi.application.TypeId.getTypeId;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

@Component
public class DefaultApplicationLinkService implements InitializingBean, MutatingApplicationLinkService,
        SubvertedEntityLinkService, InternalEntityLinkService {
    @VisibleForTesting
    static final String APPLICATION_IDS = "application.ids";

    private static final Logger LOG = LoggerFactory.getLogger(DefaultApplicationLinkService.class);
    private static final int CREATE_APPLICATION_LINK_SOCKET_TIMEOUT = 60000;

    private final ApplicationLinkRequestFactoryFactory requestFactoryFactory;
    private final PropertyService propertyService;
    private final InternalTypeAccessor typeAccessor;
    private final ApplicationLinkClient applicationLinkClient;
    private final EventPublisher eventPublisher;
    private final InternalHostApplication internalHostApplication;
    private final RequestFactory<Request<Request<?, Response>, Response>> requestFactory;
    private final RestUrlBuilder restUrlBuilder;
    private final ManifestRetriever manifestRetriever;
    private final AuthenticationConfigurator authenticationConfigurator;
    private final EntityLinkServiceApi entityLinkService;

    private final Lock applicationIdsLock = new ReentrantLock();

    @Autowired
    @SuppressWarnings("squid:S00107") // Reducing the constructor parameters would need a significant refactor
    public DefaultApplicationLinkService(final PropertyService propertyService,
                                         final ApplicationLinkRequestFactoryFactory requestFactoryFactory,
                                         final InternalTypeAccessor typeAccessor,
                                         final ApplicationLinkClient applicationLinkClient,
                                         final EventPublisher eventPublisher,
                                         final InternalHostApplication internalHostApplication,
                                         final RequestFactory<Request<Request<?, Response>, Response>> requestFactory,
                                         final RestUrlBuilder restUrlBuilder,
                                         final ManifestRetriever manifestRetriever,
                                         final AuthenticationConfigurator authenticationConfigurator,
                                         final EntityLinkBuilderFactory entityLinkBuilderFactory,
                                         final EntityLinkClient entityLinkClient) {
        this(propertyService, requestFactoryFactory, typeAccessor, applicationLinkClient, eventPublisher,
                internalHostApplication, requestFactory, restUrlBuilder, manifestRetriever, authenticationConfigurator,
                new DefaultEntityLinkService(propertyService, entityLinkBuilderFactory,
                        internalHostApplication, typeAccessor, entityLinkClient, eventPublisher));
    }


    @VisibleForTesting
    @SuppressWarnings("squid:S00107") // Reducing the constructor parameters would need a significant refactor
    DefaultApplicationLinkService(PropertyService propertyService,
                                  ApplicationLinkRequestFactoryFactory requestFactoryFactory,
                                  InternalTypeAccessor typeAccessor,
                                  ApplicationLinkClient applicationLinkClient,
                                  EventPublisher eventPublisher,
                                  InternalHostApplication internalHostApplication,
                                  RequestFactory<Request<Request<?, Response>, Response>> requestFactory,
                                  RestUrlBuilder restUrlBuilder,
                                  ManifestRetriever manifestRetriever,
                                  AuthenticationConfigurator authenticationConfigurator,
                                  EntityLinkServiceApi entityLinkService)

    {
        this.requestFactoryFactory = requestFactoryFactory;
        this.propertyService = propertyService;
        this.typeAccessor = typeAccessor;
        this.applicationLinkClient = applicationLinkClient;
        this.eventPublisher = eventPublisher;
        this.internalHostApplication = internalHostApplication;
        this.requestFactory = requestFactory;
        this.restUrlBuilder = restUrlBuilder;
        this.manifestRetriever = manifestRetriever;
        this.authenticationConfigurator = authenticationConfigurator;
        this.entityLinkService = entityLinkService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (entityLinkService instanceof DefaultEntityLinkService) {
            DefaultEntityLinkService.class.cast(entityLinkService).setApplicationLinkService(this);
        }
    }

    public InternalApplicationLink getApplicationLink(final ApplicationId id) throws TypeNotInstalledException {
        if (!getApplicationIds().contains(id)) {
            return null;
        }

        return retrieveApplicationLink(id);
    }

    public void changeApplicationId(final ApplicationId oldId, final ApplicationId newId) throws TypeNotInstalledException {
        applicationIdsLock.lock();

        if (LOG.isDebugEnabled()) {
            LOG.debug("Changing application link id from [{}] to [{}]", oldId, newId);
        }

        try {
            final List<ApplicationId> applicationIds = getApplicationIds();

            if (!applicationIds.contains(requireNonNull(oldId))) {
                throw new IllegalArgumentException("Application with server ID " +
                        oldId.toString() + " does not exist.");
            } else {
                final ApplicationLinkProperties oldProperties = propertyService.getApplicationLinkProperties(oldId);
                final ApplicationLinkProperties newProperties = propertyService.getApplicationLinkProperties(requireNonNull(newId));

                // copy app properties over:
                newProperties.setProperties(oldProperties);
                if (!applicationIds.contains(newId)) {
                    applicationIds.add(newId);
                } else {
                    LOG.warn("There is already an Application Link registered with the ID '{}'. We" +
                            " are merging the upgraded NON-UAL Application Link with this existing" +
                            " Application Link.", newId);
                }

                setApplicationIds(applicationIds);

                final InternalApplicationLink from = retrieveApplicationLink(oldId);
                final InternalApplicationLink to = retrieveApplicationLink(newId);

                entityLinkService.migrateEntityLinks(from, to);

                // remove the old properties
                oldProperties.remove();
                applicationIds.remove(oldId);
                setApplicationIds(applicationIds);
            }
        } finally {
            applicationIdsLock.unlock();
        }
    }

    public void makePrimary(final ApplicationId id) throws TypeNotInstalledException {
        applicationIdsLock.lock();

        if (LOG.isDebugEnabled()) {
            LOG.debug("Making application link id [{}] primary", id.get());
        }

        try {
            final InternalApplicationLink internalApplicationLink = getApplicationLink(id);
            if (internalApplicationLink == null) {
                throw new IllegalArgumentException("No application link with ID=" + id);
            }
            final Iterable<InternalApplicationLink> applicationLinksOfType = getInternalApplicationLinks(internalApplicationLink.getType().getClass());
            for (final InternalApplicationLink link : applicationLinksOfType) {
                link.setPrimaryFlag(link.getId().equals(id));
            }

            eventPublisher.publish(new ApplicationLinkMadePrimaryEvent(internalApplicationLink));
        } finally {
            applicationIdsLock.unlock();
        }
    }

    public void setSystem(ApplicationId id, boolean isSystem) throws TypeNotInstalledException {
        final InternalApplicationLink internalApplicationLink = getApplicationLink(id);
        internalApplicationLink.setSystem(isSystem);
    }

    public InternalApplicationLink addApplicationLink(final ApplicationId id, final ApplicationType type,
                                                      final ApplicationLinkDetails details) {
        //Note that reciprocation of create is currently done in ApplicationLinkClientResource
        try {
            applicationIdsLock.lock();

            if (LOG.isDebugEnabled()) {
                LOG.debug("Adding application link id [{}]", id.get());
            }

            final List<ApplicationId> applicationIds = getApplicationIds();

            if (applicationIds.contains(id)) {
                throw new IllegalArgumentException("Application with server ID " + id + " is already configured");
            }

            final boolean onlyLinkOfItsType = Iterables.isEmpty(getApplicationLinks(type.getClass()));
            //Ensure an application link of this type exists.
            final ApplicationLinkProperties applicationLinkProperties = propertyService.getApplicationLinkProperties(id);

            applicationLinkProperties.setType(getTypeId(type));
            applicationLinkProperties.setName(findSuitableName(details.getName()));
            applicationLinkProperties.setDisplayUrl(details.getDisplayUrl());
            applicationLinkProperties.setRpcUrl(details.getRpcUrl());

            // The following two lines add the application link to the PluginSettings
            applicationIds.add(id);
            setApplicationIds(applicationIds);

            final InternalApplicationLink addedAppLink =
                    new DefaultApplicationLink(id,
                            type,
                            applicationLinkProperties,
                            requestFactoryFactory,
                            eventPublisher);

            if (details.isPrimary() || onlyLinkOfItsType) {
                try {
                    makePrimary(id);
                } catch (TypeNotInstalledException e) {
                    //This is actually impossible, because we pass in the application type class.
                    LOG.warn("Failed to make new application link the primary application link", e);
                }
            }
            eventPublisher.publish(new ApplicationLinkAddedEvent(addedAppLink));
            return addedAppLink;
        } finally {
            applicationIdsLock.unlock();
        }
    }

    /**
     * Checks whether 'name' is already the name of an application link. If yes, appends
     * " - 2" to the name and increments the figure
     *
     * @param name
     * @return
     */
    private String findSuitableName(final String name) {
        Iterable<ApplicationLink> allApplicationLinks = getApplicationLinks();
        if (!isNameInUse(name, null, allApplicationLinks)) {
            return name;
        }

        // If the string is in the form "Refapp - 2", then remove " - 2"
        String root = name.replace(" - [0-9]+$", "");
        // Proposes names using the counter i
        String proposedName;
        int i = 2;
        do {
            proposedName = String.format("%s - %d", root, i);
            i++;
        }
        while (isNameInUse(proposedName, null, allApplicationLinks));
        return proposedName;
    }

    /**
     * Checks whether an Applink already exists with this name
     *
     * @param name                the name of the applink
     * @param id                  An application link to exclude (can be null if no applink has to be excluded)
     * @param allApplicationLinks The applinks to search into
     * @return true if an Applinks with 'name' and not 'id' was found among 'allApplicationLinks'
     */
    private boolean isNameInUse(final String name, final ApplicationId id, final Iterable<? extends ApplicationLink> allApplicationLinks) {
        try {
            Iterables.find(allApplicationLinks, new Predicate<ApplicationLink>() {
                public boolean apply(ApplicationLink appLink) {
                    return appLink.getName().equals(name) && !appLink.getId().equals(id);
                }
            });
        } catch (NoSuchElementException nsee) {
            return false;
        }
        return true;
    }

    /**
     * Checks whether an application link already exists with this name
     *
     * @param name Name of the application link
     * @param id   Applink to be excluded from the result. If null, it means no link will be excluded.
     * @return true if an Applink already exist with this name and another 'id'.
     */
    public boolean isNameInUse(final String name, final ApplicationId id) {
        Iterable<InternalApplicationLink> allApplicationLinks = getInternalApplicationLinks();
        return isNameInUse(name, id, allApplicationLinks);
    }

    public void deleteReciprocatedApplicationLink(final ApplicationLink link) throws ReciprocalActionException,
            CredentialsRequiredException {
        applicationLinkClient.deleteReciprocalLinkFrom(link);
        deleteApplicationLink(link);
    }

    public void deleteApplicationLink(final ApplicationLink link) {
        ImmutableApplicationLink originalLink = new ImmutableApplicationLink(link, requestFactoryFactory);
        try {
            applicationIdsLock.lock();

            if (LOG.isDebugEnabled()) {
                LOG.debug("Deleting application link id [{}]", link.getId());
            }

            final List<ApplicationId> applicationIds = getApplicationIds();

            // cascade delete of entities related to this application link
            entityLinkService.deleteEntityLinksFor(link);

            if (applicationIds.remove(link.getId())) {
                final ApplicationLinkProperties appLinkProperties = propertyService.getApplicationLinkProperties(link.getId());
                TypeId typeId = appLinkProperties.getType();
                boolean wasPrimary = link.isPrimary();
                try {
                    eventPublisher.publish(new BeforeApplicationLinkDeletedEvent(link));
                } catch (NotificationException e) {
                    LOG.error("An error occurred when broadcasting event {} for application link with id '{}' and name '{}'",
                            BeforeApplicationLinkDeletedEvent.class.getName(), link.getId(), link.getName());
                }

                setApplicationIds(applicationIds);

                appLinkProperties.remove();

                if (wasPrimary) {
                    if (typeId == null) {
                        LOG.warn("Failed to make new application link the primary application link to replace link with id '{}' and name '{}': Could not find type", link.getId(), link.getName());
                    } else {
                        final ApplicationType deletedType = typeAccessor.loadApplicationType(typeId);
                        final Iterator<InternalApplicationLink> linkIterator = getInternalApplicationLinks(deletedType.getClass()).iterator();
                        if (linkIterator.hasNext()) {
                            final ApplicationLink newPrimaryApplicationLink = linkIterator.next();
                            try {
                                makePrimary(newPrimaryApplicationLink.getId());
                            } catch (TypeNotInstalledException ex) {
                                //This is actually impossible, because we pass in the application type class.
                                LOG.warn("Failed to make new application link the primary application link", ex);
                            }
                        }
                    }
                }
                eventPublisher.publish(new ApplicationLinkDeletedEvent(originalLink));
            }
        } finally {
            applicationIdsLock.unlock();
        }
    }

    private InternalApplicationLink retrieveApplicationLink(final ApplicationId id) throws TypeNotInstalledException {
        final ApplicationLinkProperties properties = propertyService.getApplicationLinkProperties(requireNonNull(id));

        TypeId typeId = properties.getType();
        if (typeId == null) {
            LOG.warn("Couldn't find type id for application link with id {}. Link is corrupted", id.get());
            throw new TypeNotInstalledException("unknown", properties.getName(), properties.getRpcUrl());
        }

        final ApplicationType type = typeAccessor.loadApplicationType(typeId);

        if (type == null) {
            LOG.debug("Couldn't load type {} for application link with id {}, name {}, rpc.url {} . The type may not be installed.",
                    typeId, id.get(), properties.getName(), properties.getRpcUrl());
            throw new TypeNotInstalledException(typeId.get(), properties.getName(), properties.getRpcUrl());
        }

        return new DefaultApplicationLink(id, type, properties,
                requestFactoryFactory, eventPublisher);
    }

    public Iterable<ApplicationLink> getApplicationLinks() {
        return Iterables.filter(getInternalApplicationLinks(), ApplicationLink.class);
    }

    public Iterable<InternalApplicationLink> getInternalApplicationLinks() {
        Collection<InternalApplicationLink> links = new ArrayList<>();

        for (ApplicationId id : getApplicationIds()) {
            try {
                links.add(retrieveApplicationLink(id));
            } catch (TypeNotInstalledException e) {
                // ignore -- try to load next link
            }
        }

        return links;
    }

    public Iterable<ApplicationLink> getApplicationLinks(final Class<? extends ApplicationType> type) {
        return stream(getInternalApplicationLinks(type).spliterator(), false)
                .filter(Objects::nonNull)
                .sorted((applicationLink, applicationLink1) -> {
                    if (applicationLink.isPrimary() == applicationLink1.isPrimary()) {
                        return 0;
                    } else if (applicationLink.isPrimary()) {
                        return -1;
                    }
                    return 1;
                }).collect(toList());
    }

    public Iterable<InternalApplicationLink> getInternalApplicationLinks(final Class<? extends ApplicationType> type) {
        requireNonNull(type);
        return stream(this.getInternalApplicationLinks().spliterator(), false)
                .filter(i -> type.isAssignableFrom(i.getType().getClass()))
                .collect(toList());
    }

    public ApplicationLink getPrimaryApplicationLink(final Class<? extends ApplicationType> type) {
        final Iterator<ApplicationLink> iterator = getApplicationLinks(type).iterator();

        if (!iterator.hasNext()) {
            // no applications of this type configured
            return null;
        }

        while (iterator.hasNext()) {
            final ApplicationLink application = iterator.next();
            if (application.isPrimary()) {
                return application;
            }
        }

        throw new IllegalStateException("There are application links of type " + type + " configured, but none are " +
                "marked as primary");
    }

    @SuppressWarnings("unchecked")
    private List<ApplicationId> getApplicationIds() {
        List<String> list = (List<String>) propertyService.getGlobalAdminProperties().getProperty(APPLICATION_IDS);
        if (list == null) {
            return new ArrayList<>();
        }
        return list.stream().map(ApplicationId::new).collect(toList());
    }

    private void setApplicationIds(final List<ApplicationId> applicationIds) {
        if (LOG.isDebugEnabled()) {
            String message = String.format("Setting application link ids [%s]", applicationIds);
            LOG.debug(message);
        }

        //NB: we have to copy the transformed list from Lists.transform(), otherwise the persistence fails in Confluence which uses Bandana and xStreams to persists plugin settings.
        propertyService.getGlobalAdminProperties().putProperty(APPLICATION_IDS,
                new ArrayList<>(applicationIds.stream().map(ApplicationId::get).collect(toList())));
    }

    @SuppressWarnings("squid:CallToDeprecatedMethod")
    public void createReciprocalLink(final URI remoteRpcUrl, final URI customLocalRpcUrl, final String username, final String password) throws ReciprocalActionException {
        final URI localRpcUrl;
        if (customLocalRpcUrl != null) {
            localRpcUrl = customLocalRpcUrl;
        } else {
            localRpcUrl = internalHostApplication.getBaseUrl();
        }
        try {
            final boolean adminUser = isAdminUserInRemoteApplication(remoteRpcUrl, username, password);
            if (!adminUser) {
                throw new NotAdministratorException();
            }
        } catch (ResponseException ex) {
            throw new AuthenticationResponseException();
        }
        final ApplicationLinkEntity linkBackToMyself = new ApplicationLinkEntity(
                internalHostApplication.getId(),
                getTypeId(internalHostApplication.getType()),
                internalHostApplication.getName(),
                internalHostApplication.getBaseUrl(),
                internalHostApplication.getType().getIconUrl(),
                resolveIconUri(internalHostApplication.getType()),
                localRpcUrl,
                false,
                false,
                Link.self(createSelfLinkFor(internalHostApplication.getId())));


        final String url;
        try {
            ApplicationLinkResource resource = restUrlBuilder.getUrlFor(RestUtil.getBaseRestUri(remoteRpcUrl), ApplicationLinkResource.class);
            url = resource.updateApplicationLink(internalHostApplication.getId().toString(), null).toString();
        } catch (TypeNotInstalledException e) {
            throw new AssertionError(RestUrlBuilder.class.getName() + " must never throw " +
                    TypeNotInstalledException.class.getName());
        }

        final Request<Request<?, Response>, Response> request =
                new BasicHttpAuthRequestFactory<>(
                        requestFactory,
                        username,
                        password)
                        .createRequest(Request.MethodType.PUT, url);

        // bump up the socket timeout, as application link creation requests can take a *long* time, as this will
        // trigger a request from the remote application back to this one, and also trigger an ApplicationLinkAddedEvent
        // in registered listeners in the remote application, synchronously! The FishEye plugin is one such example
        // of this - see FECRU-534
        request.setSoTimeout(CREATE_APPLICATION_LINK_SOCKET_TIMEOUT);

        ErrorListEntity errorListEntity;
        final ClassLoader currentContextClassloader = Thread.currentThread().getContextClassLoader();
        final ChainingClassLoader chainingClassLoader = new ChainingClassLoader(currentContextClassloader,
                ClassLoaderUtils.class.getClassLoader(), ClassLoader.getSystemClassLoader());
        Thread.currentThread().setContextClassLoader(chainingClassLoader); // APL-837
        try {
            // 201 means we created a new application link.
            // 200 means there already is an application link and we just updated this one.
            errorListEntity = request
                    .setEntity(linkBackToMyself)
                    .executeAndReturn(response -> !response.isSuccessful() ? response.getEntity(ErrorListEntity.class) : null);
        } catch (ResponseException ex) {
            final String message = "After creating the 2-Way link an error occurred when reading the response from the remote application. {}";
            throw new LinkCreationResponseException(message, ex);
        } catch (RuntimeException ex) {
            final String message = "An error occurred when trying to create the application link in the remote application.";
            throw new ReciprocalActionException(message, ex);
        } finally {
            Thread.currentThread().setContextClassLoader(currentContextClassloader);
        }

        if (errorListEntity != null) {
            throw new RemoteErrorListException(errorListEntity.getErrors());
        }
    }

    public boolean isAdminUserInRemoteApplication(final URI url, final String username, final String password)
            throws ResponseException {
        final URI uri = Uris.uncheckedConcatenate(url, RestUtil.REST_APPLINKS_URL);
        final AuthenticationResource restUrl = restUrlBuilder.getUrlFor(uri, AuthenticationResource.class);
        return requestFactory
                .createRequest(Request.MethodType.GET,
                        restUrl.getIsAdminUser().toString())
                .addBasicAuthentication(url.getHost(), username, password)
                .executeAndReturn(Response::isSuccessful);
    }

    public URI createSelfLinkFor(final ApplicationId id) {
        try {
            URI baseUri = Optional.ofNullable(CurrentContext.getContext())
                    .map(context -> context.getUriInfo().getBaseUri())
                    .orElseGet(internalHostApplication::getBaseUrl);
            final ApplicationLinkResource applicationLinkResource = restUrlBuilder.getUrlFor(
                    baseUri,
                    ApplicationLinkResource.class);
            final String idString = id.get();
            final javax.ws.rs.core.Response applicationLink = applicationLinkResource.getApplicationLink(idString);
            return restUrlBuilder.getURI(applicationLink);
        } catch (TypeNotInstalledException e) {
            // this should _never_ happen, as com.atlassian.plugins.rest.common.util.RestUrlBuilder.getUrlFor() is just returning a proxy stub
            throw new IllegalStateException(String.format("Failed to load application %s as the %s type is not installed",
                    id.get(), e.getType()));
        }
    }

    public ApplicationLink createApplicationLink(final ApplicationType type, final ApplicationLinkDetails linkDetails) throws ManifestNotFoundException {
        final Manifest manifest = manifestRetriever.getManifest(linkDetails.getRpcUrl(), type);
        return addApplicationLink(manifest.getId(), type, linkDetails);
    }

    public void configureAuthenticationForApplicationLink(final ApplicationLink applicationLink,
                                                          final AuthenticationScenario authenticationScenario,
                                                          final String username, final String password)
            throws AuthenticationConfigurationException {
        authenticationConfigurator.configureAuthenticationForApplicationLink(
                applicationLink,
                authenticationScenario,
                new BasicHttpAuthRequestFactory<>(
                        requestFactory,
                        username,
                        password)
        );
    }

    // EntityLinkService API, see APLDEV-337

    @Override
    public EntityLink addEntityLink(String localKey, Class<? extends EntityType> localType, EntityLink entityLink) {
        return entityLinkService.addEntityLink(localKey, localType, entityLink);
    }

    @Override
    public EntityLink addReciprocatedEntityLink(String localKey, Class<? extends EntityType> localType,
                                                EntityLink entityLink)
            throws ReciprocalActionException, CredentialsRequiredException {
        return entityLinkService.addReciprocatedEntityLink(localKey, localType, entityLink);
    }

    @Override
    public boolean deleteEntityLink(String localKey, Class<? extends EntityType> localType, EntityLink entityLink) {
        return entityLinkService.deleteEntityLink(localKey, localType, entityLink);
    }

    @Override
    public boolean deleteReciprocatedEntityLink(String localKey, Class<? extends EntityType> localType,
                                                EntityLink entityLink) throws ReciprocalActionException, CredentialsRequiredException {
        return entityLinkService.deleteReciprocatedEntityLink(localKey, localType, entityLink);
    }

    @Override
    public void deleteEntityLinksFor(ApplicationLink link) {
        entityLinkService.deleteEntityLinksFor(link);
    }

    @Override
    public EntityLink makePrimary(String localKey, Class<? extends EntityType> localType, EntityLink entityLink) {
        return entityLinkService.makePrimary(localKey, localType, entityLink);
    }

    @Override
    public EntityLink getEntityLink(String localKey, Class<? extends EntityType> localType, String remoteKey,
                                    Class<? extends EntityType> remoteType, ApplicationId applicationId) {
        return entityLinkService.getEntityLink(localKey, localType, remoteKey, remoteType, applicationId);
    }

    @Override
    public Iterable<EntityLink> getEntityLinksForApplicationLink(ApplicationLink applicationLink)
            throws TypeNotInstalledException {
        return entityLinkService.getEntityLinksForApplicationLink(applicationLink);
    }

    @Override
    public Iterable<EntityLink> getEntityLinksForKey(String localKey, Class<? extends EntityType> localType,
                                                     Class<? extends EntityType> type) {
        return entityLinkService.getEntityLinksForKey(localKey, localType, type);
    }

    @Override
    public Iterable<EntityLink> getEntityLinksForKey(String localKey, Class<? extends EntityType> localType) {
        return entityLinkService.getEntityLinksForKey(localKey, localType);
    }

    @Override
    public EntityLink getPrimaryEntityLinkForKey(String localKey, Class<? extends EntityType> localType,
                                                 Class<? extends EntityType> type) {
        return entityLinkService.getPrimaryEntityLinkForKey(localKey, localType, type);
    }

    @Override
    public EntityLinkBuilderFactory getEntityLinkBuilderFactory() {
        return entityLinkService.getEntityLinkBuilderFactory();
    }

    @Override
    public Iterable<EntityLink> getEntityLinksNoPermissionCheck(Object entity, Class<? extends EntityType> type) {
        return entityLinkService.getEntityLinksNoPermissionCheck(entity, type);
    }

    @Override
    public Iterable<EntityLink> getEntityLinksNoPermissionCheck(Object entity) {
        return entityLinkService.getEntityLinksNoPermissionCheck(entity);
    }

    @Override
    public Iterable<EntityLink> getEntityLinks(Object entity, Class<? extends EntityType> type) {
        return entityLinkService.getEntityLinks(entity, type);
    }

    @Override
    public Iterable<EntityLink> getEntityLinks(Object entity) {
        return entityLinkService.getEntityLinks(entity);
    }

    @Override
    public EntityLink getPrimaryEntityLink(Object entity, Class<? extends EntityType> type) {
        return entityLinkService.getPrimaryEntityLink(entity, type);
    }

    @Override
    public void migrateEntityLinks(ApplicationLink from, ApplicationLink to) {
        entityLinkService.migrateEntityLinks(from, to);
    }

    // aggregate of all EntityLinkService interfaces to facilitate testing
    @VisibleForTesting
    interface EntityLinkServiceApi extends InternalEntityLinkService, SubvertedEntityLinkService {
    }

    /**
     * Beacuse of circular dependency with DefaultEntityLinkService, this class has to be kept internal until the
     * dependency is broken down. See APLDEV-337.
     */
    @VisibleForTesting
    static class DefaultEntityLinkService implements EntityLinkServiceApi, InternalEntityLinkService,
            SubvertedEntityLinkService {
        private static final Logger LOG = LoggerFactory.getLogger(DefaultEntityLinkService.class.getName());

        private static final String LINKED_ENTITIES = "linked.entities";
        private static final String PRIMARY_FMT = "primary.%s";

        private static final String TYPE = "type";
        private static final String TYPE_I18N = "typeI18n";
        private static final String APPLICATION_ID = "applicationId";
        private static final String KEY = "key";
        private static final String NAME = "name";

        private final PropertyService propertyService;
        private final EntityLinkBuilderFactory entityLinkBuilderFactory;
        private final InternalHostApplication internalHostApplication;
        private final InternalTypeAccessor typeAccessor;
        private final EntityLinkClient entityLinkClient;
        private final EventPublisher eventPublisher;

        private ApplicationLinkService applicationLinkService;

        public DefaultEntityLinkService(PropertyService propertyService,
                                        EntityLinkBuilderFactory entityLinkBuilderFactory,
                                        InternalHostApplication internalHostApplication,
                                        InternalTypeAccessor typeAccessor,
                                        EntityLinkClient entityLinkClient,
                                        EventPublisher eventPublisher) {
            this.propertyService = propertyService;
            this.entityLinkBuilderFactory = entityLinkBuilderFactory;
            this.internalHostApplication = internalHostApplication;
            this.typeAccessor = typeAccessor;
            this.entityLinkClient = entityLinkClient;
            this.eventPublisher = eventPublisher;
        }

        void setApplicationLinkService(ApplicationLinkService applicationLinkService) {
            this.applicationLinkService = applicationLinkService;
        }

        public EntityLinkBuilderFactory getEntityLinkBuilderFactory() {
            return entityLinkBuilderFactory;
        }

        public EntityLink addReciprocatedEntityLink(final String localKey, final Class<? extends EntityType> localTypeClass,
                                                    final EntityLink entityLink)
                throws ReciprocalActionException, CredentialsRequiredException {
            entityLinkClient.createEntityLinkFrom(entityLink, loadTypeFromClass(localTypeClass), localKey);
            return addEntityLink(localKey, localTypeClass, entityLink);
        }

        private EntityType loadTypeFromClass(final Class<? extends EntityType> localTypeClass) {
            return requireNonNull(typeAccessor.getEntityType(localTypeClass),
                    String.format("%s class available, but type not installed?", localTypeClass));
        }

        public void migrateEntityLinks(final ApplicationLink from, final ApplicationLink to) {
            if (LOG.isDebugEnabled()) {
                String message = String.format("Migrating Entity Links from Application Link [%s] to [%s]", from.getId().get(), to.getId().get());
                LOG.debug(message);
            }

            for (final EntityReference localEntity : internalHostApplication.getLocalEntities()) {
                final List<? extends EntityLink> entityLinks = Lists.newArrayList(
                        getStoredEntityLinks(localEntity.getKey(), localEntity.getType().getClass()).stream().map(oldEntityLink -> {
                            if (oldEntityLink.getApplicationLink().getId().equals(from.getId())) {
                                final EntityLink newEntityLink = entityLinkBuilderFactory.builder()
                                        .applicationLink(to)
                                        .type(oldEntityLink.getType())
                                        .key(oldEntityLink.getKey())
                                        .name(oldEntityLink.getName())
                                        .primary(oldEntityLink.isPrimary())
                                        .build();

                                final EntityLinkProperties oldLinkProperties = propertyService.getProperties(oldEntityLink);
                                final EntityLinkProperties newLinkProperties = propertyService.getProperties(newEntityLink);
                                newLinkProperties.setProperties(oldLinkProperties); // copy over all user properties
                                oldLinkProperties.removeAll();  // delete old user properties

                                // update potential references in the local entity props
                                // (if this entityLink happens to be the primary for this local entity)
                                final String primaryPropertyKey = primaryPropertyKey(getTypeId(newEntityLink.getType()));
                                final PropertySet props = propertyService.getLocalEntityProperties(localEntity.getKey(), getTypeId(localEntity.getType()));
                                final Object value = props.getProperty(primaryPropertyKey);
                                if (value != null) {
                                    final Properties primary = (Properties) value;
                                    if (from.getId().get().equals(primary.get(APPLICATION_ID))) {
                                        primary.put(APPLICATION_ID, to.getId().get());
                                        props.putProperty(primaryPropertyKey, primary);
                                    }
                                }

                                return newEntityLink;
                            } else {
                                return oldEntityLink;
                            }
                        }).collect(toList()));
                setStoredEntityLinks(localEntity.getKey(), localEntity.getType().getClass(), entityLinks);
            }
        }

        /**
         * Add or update an entity link
         */
        public EntityLink addEntityLink(
                final String localKey, final Class<? extends EntityType> localType, final EntityLink entityLink) {
            final List<EntityLink> entities = this.getStoredEntityLinks(localKey, localType);

            boolean isUpdate = false;
            for (final Iterator<EntityLink> iterator = entities.iterator(); iterator.hasNext(); ) {
                final EntityLink storedEntity = iterator.next();
                if (equivalent(storedEntity, entityLink)) {
                    // we're performing an update, remove old entity record
                    iterator.remove();
                    isUpdate = true;
                    break;
                }
            }

            if (LOG.isDebugEnabled()) {
                String message;
                if (isUpdate) {
                    message = String.format("Updating Entity Link for [%s] [%s] as [%s]", localType, localKey, entityLink);
                } else {
                    message = String.format("Adding Entity Link for [%s] [%s] as [%s]", localType, localKey, entityLink);
                }
                LOG.debug(message);
            }

            entities.add(entityLink);
            setStoredEntityLinks(localKey, localType, entities);

            EntityLink newLink = entityLink;

            // set the link as primary if the isPrimary flag is explicitly set, or there is no existing primary entity
            // link of it's type already already associated with the local entity
            if (entityLink.isPrimary() || getPrimaryRef(localKey, lookUpTypeId(localType), getTypeId(entityLink.getType())) == null) {
                newLink = makePrimaryImpl(localKey, localType, entityLink);
            }

            eventPublisher.publish(new EntityLinkAddedEvent(newLink, localKey, localType));
            return newLink;
        }

        /**
         * @param localType the {@link Class} of the {@link EntityType} to resolve an {@link TypeId} for
         * @return the {@link TypeId} of the specified {@link EntityType}
         * @throws IllegalStateException if the supplied class does not have an enabled implementation registered via the
         *                               {@code applinks-entity-link} module descriptor
         */
        private TypeId lookUpTypeId(final Class<? extends EntityType> localType) {
            final EntityType type = typeAccessor.getEntityType(localType);
            if (type == null) {
                throw new IllegalStateException("Couldn't load " + localType.getName() + ", type not installed?");
            }
            return TypeId.getTypeId(type);
        }

        public boolean deleteReciprocatedEntityLink(final String localKey, final Class<? extends EntityType> localType,
                                                    final EntityLink entityToDelete)
                throws ReciprocalActionException, CredentialsRequiredException {
            if (LOG.isDebugEnabled()) {
                String message = String.format("Deleting Reciprocated Entity Link for [%s] [%s] was [%s]", loadTypeFromClass(localType), localKey, entityToDelete);
                LOG.debug(message);
            }

            entityLinkClient.deleteEntityLinkFrom(entityToDelete, loadTypeFromClass(localType), localKey);
            return deleteEntityLink(localKey, localType, entityToDelete);
        }

        public boolean deleteEntityLink(final String localKey, final Class<? extends EntityType> localType,
                                        final EntityLink entityToDelete) {
            final List<EntityLink> entities = getStoredEntityLinks(localKey, localType);
            boolean deleted = false;
            for (final Iterator<EntityLink> iterator = entities.iterator(); iterator.hasNext(); ) {
                final EntityLink entity = iterator.next();
                if (equivalent(entity, entityToDelete)) {
                    iterator.remove();
                    deleted = true;
                    break;
                }
            }

            if (deleted) {
                if (LOG.isDebugEnabled()) {
                    String message = String.format("Deleting Entity Link for [%s] [%s] was [%s]", loadTypeFromClass(localType), localKey, entityToDelete);
                    LOG.debug(message);
                }

                //if that link was the primary and there's still links of that type in existence, we'll need to assign a new one
                final PrimaryRef primary = getPrimaryRef(localKey, lookUpTypeId(localType), getTypeId(entityToDelete.getType()));
                if (primary == null || primary.refersTo(entityToDelete)) {
                    selectNewPrimary(localKey, localType, entityToDelete.getType().getClass(), entities);
                }

                propertyService.getProperties(entityToDelete).removeAll();

                setStoredEntityLinks(localKey, localType, entities);

                eventPublisher.publish(new EntityLinkDeletedEvent(entityToDelete, localKey, localType));
            }

            return deleted;
        }

        private void selectNewPrimary(final String localKey, final Class<? extends EntityType> localType,
                                      final Class<? extends EntityType> type,
                                      final Iterable<? extends EntityLink> entities) {
            final Iterator<? extends EntityLink> it = entities.iterator();
            if (!it.hasNext()) {
                // no more of this type - remove the primary reference
                final String primaryPropertyKey = primaryPropertyKey(lookUpTypeId(type));
                propertyService.getLocalEntityProperties(localKey, lookUpTypeId(localType)).removeProperty(primaryPropertyKey);
            } else {
                // still some left, just choose the next one
                makePrimaryImpl(localKey, localType, it.next());
            }
        }

        public void deleteEntityLinksFor(final ApplicationLink link) {
            requireNonNull(link);

            if (LOG.isDebugEnabled()) {
                String message = String.format("Deleting Entity Links for Application Link [%s]", link.getId().get());
                LOG.debug(message);
            }


            for (final EntityReference localEntity : internalHostApplication.getLocalEntities()) {
                final Set<Class<? extends EntityType>> typesForWhichToReassignPrimaries = new HashSet<>();
                final Set<EntityLink> removedEntityLinks = new HashSet<>();

                // remove entities that are children of this application link --
                // n.b. MUST exhaust the result of Iterables.filter() for the predicate to populate our type set
                final List<? extends EntityLink> updatedLinks = getStoredEntityLinks(localEntity.getKey(), localEntity.getType().getClass()).stream().filter(input -> {
                    if (link.getId().equals(input.getApplicationLink().getId())) {
                        // we're going to delete this - check if it is a primary link we're going to need to reassign
                        if (!typesForWhichToReassignPrimaries.contains(input.getType().getClass())) {
                            final PrimaryRef primary = getPrimaryRef(localEntity.getKey(), getTypeId(localEntity.getType()), getTypeId(input.getType()));
                            if (primary.refersTo(input)) {
                                typesForWhichToReassignPrimaries.add(input.getType().getClass());
                            }
                        }
                        removedEntityLinks.add(input);
                        return false;
                    } else {
                        //this is not the link you are looking for, move along
                        return true;
                    }
                }).collect(toList());

                for (final Class<? extends EntityType> type : typesForWhichToReassignPrimaries) {
                    selectNewPrimary(localEntity.getKey(), localEntity.getType().getClass(), type, updatedLinks);
                }

                // Let's first clear all custom properties of every entity link.
                for (final EntityLink removedLink : removedEntityLinks) {
                    propertyService.getProperties(removedLink).removeAll();
                }

                // Now let's remove the entity links.
                setStoredEntityLinks(localEntity.getKey(), localEntity.getType().getClass(), updatedLinks);

                // Publish an EntityLinkDeletedEvent for every entity link we deleted.
                for (final EntityLink removedLink : removedEntityLinks) {
                    eventPublisher.publish(new EntityLinkDeletedEvent(removedLink, localEntity.getKey(), localEntity.getType().getClass()));
                }
            }
        }

        private List<EntityLink> getStoredEntityLinks(final String localKey, final Class<? extends EntityType> localType) {
            return getStoredEntityLinks(localKey, localType, PermissionMode.CHECK);
        }

        private List<EntityLink> getStoredEntityLinks(final String localKey, final Class<? extends EntityType> localType, final PermissionMode permissionMode) {
            requireNonNull(localKey, "localKey can't be null");
            requireNonNull(localType, "localType can't be null");

            switch (permissionMode) {
                case CHECK:
                    if (!internalHostApplication.doesEntityExist(localKey, localType)) {
                        LOG.error(String.format("No local entity with key '%s' and type '%s' exists", localKey, localType));
                        return Lists.newArrayList();
                    }
                    break;
                case NO_CHECK:
                    if (!internalHostApplication.doesEntityExistNoPermissionCheck(localKey, localType)) {
                        LOG.error(String.format("No local entity with key '%s' and type '%s' exists", localKey, localType));
                        return Lists.newArrayList();
                    }
                    break;
                default:
                    LOG.error("Unknown permission mode: " + permissionMode);
                    return Lists.newArrayList();
            }

            List<String> encodedLinks = getEncodedLinks(localKey, localType);
            if (encodedLinks == null) {
                encodedLinks = new ArrayList<String>();
            }

            final List<EntityLink> entityLinks = new ArrayList<EntityLink>();
            for (final String from : encodedLinks) {
                final JSONObject obj;
                final ApplicationId applicationId;
                try {
                    obj = new JSONObject(from);
                    applicationId = new ApplicationId(getRequiredJSONString(obj, APPLICATION_ID));
                } catch (JSONException e) {
                    throw new RuntimeException("Failed to decode stored entity link to JSON for local entity with key '" + localKey + "' and of type '" + localType + "'. Encoded string is: '" + from + "'", e);
                }

                final ApplicationLink applicationLink;
                try {
                    applicationLink = applicationLinkService.getApplicationLink(applicationId);
                } catch (TypeNotInstalledException e) {
                    LOG.warn(String.format("Couldn't load application link with id %s, type %s is not installed. " +
                            "All child entity links will be inaccessible.", applicationId, e.getType()));
                    continue;
                }

                if (applicationLink == null) {
                    LOG.debug("Skipping EntityLink [" + from + "] for ["
                            + localKey + "." + lookUpTypeId(localType) + "." + LINKED_ENTITIES
                            + "] because ApplicationLink with id [" + applicationId + "] was not found. It should be removed.");
                    continue;
                }

                final TypeId typeId = new TypeId(getRequiredJSONString(obj, TYPE));
                final EntityType type = typeAccessor.loadEntityType(typeId);

                if (type == null) {
                    LOG.warn(String.format("Couldn't load type %s for entity link (child of application link with id %s). Type is not installed? ",
                            typeId, applicationLink.getId()));
                    continue;
                }

                final String key = getRequiredJSONString(obj, KEY);
                PrimaryRef primaryRef = this.getPrimaryRef(localKey, lookUpTypeId(localType), getTypeId(type));
                final boolean isPrimary;
                if (primaryRef != null) {
                    isPrimary = primaryRef.refersTo(key, getTypeId(type), applicationLink.getId());
                } else {
                    isPrimary = false;
                }


                entityLinks.add(entityLinkBuilderFactory
                        .builder()
                        .key(key)
                        .type(type)
                        .name(getRequiredJSONString(obj, NAME))
                        .applicationLink(applicationLink)
                        .primary(isPrimary).build()
                );
            }
            return entityLinks;
        }

        @SuppressWarnings("unchecked")
        private List<String> getEncodedLinks(final String localKey, final Class<? extends EntityType> localType) {
            return (List<String>) propertyService.getLocalEntityProperties(localKey, lookUpTypeId(localType))
                    .getProperty(LINKED_ENTITIES);
        }

        private void setStoredEntityLinks(final String localKey, final Class<? extends EntityType> localType,
                                          final Iterable<? extends EntityLink> entities) {
            requireNonNull(localKey, "localKey can't be null");
            requireNonNull(localType, "localType can't be null");

            if (entities == null) {
                if (LOG.isDebugEnabled()) {
                    String message = String.format("Removing stored entity links for [%s] [%s] was [%s]", localKey, lookUpTypeId(localType),
                            propertyService.getLocalEntityProperties(localKey, lookUpTypeId(localType)).getProperty(LINKED_ENTITIES));
                    LOG.debug(message);
                }

                // remove stored entities
                propertyService.getLocalEntityProperties(localKey, lookUpTypeId(localType)).removeProperty(LINKED_ENTITIES);
                return;
            }

            final List<String> encodedEntities = Lists.newArrayList(
                    Iterables.transform(entities, new Function<EntityLink, String>() {
                        public String apply(final EntityLink from) {
                            final Map<String, String> propertyMap = new HashMap<String, String>();
                            propertyMap.put(KEY, from.getKey());
                            propertyMap.put(NAME, from.getName());
                            propertyMap.put(TYPE, getTypeId(from.getType()).get());
                            propertyMap.put(TYPE_I18N, from.getType().getI18nKey());
                            propertyMap.put(APPLICATION_ID, from.getApplicationLink().getId().get());
                            final StringWriter sw = new StringWriter();
                            try {
                                new JSONObject(propertyMap).write(sw);
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                            return sw.getBuffer().toString();
                        }
                    }));

            if (LOG.isDebugEnabled()) {
                String message = String.format("Setting stored entity links for [%s] [%s] as [%s]", localKey, lookUpTypeId(localType),
                        entities);
                LOG.debug(message);
            }

            propertyService.getLocalEntityProperties(localKey, lookUpTypeId(localType)).putProperty(LINKED_ENTITIES, encodedEntities);
        }

        private String getJSONString(final JSONObject obj, final String propertyKey) {
            try {
                return obj.isNull(propertyKey) ?
                        null :
                        (String) obj.get(propertyKey);
            } catch (JSONException je) {
                throw new RuntimeException(je);
            }
        }

        private String getRequiredJSONString(final JSONObject obj, final String propertyKey) throws NullPointerException {
            return assertNotNull(getJSONString(obj, propertyKey), propertyKey);
        }

        private <T> T assertNotNull(final T value, final String propertyKey) {
            return requireNonNull(value, EntityLink.class.getSimpleName() + " property '" + propertyKey + "' should not be null!");
        }

        private String getRequiredString(final Map map, final String propertyKey) {
            return assertNotNull((String) map.get(propertyKey), propertyKey);
        }

        public Iterable<EntityLink> getEntityLinksForKey(
                final String localKey, final Class<? extends EntityType> localType, final Class<? extends EntityType> typeOfRemoteEntities) {
            return getEntityLinksForKey(localKey, localType, typeOfRemoteEntities, PermissionMode.CHECK);
        }

        private Iterable<EntityLink> getEntityLinksForKey(
                final String localKey, final Class<? extends EntityType> localType, final Class<? extends EntityType> typeOfRemoteEntities, final PermissionMode permissionMode) {
            requireNonNull(localKey, "localKey can't be null");
            requireNonNull(localType, "localType can't be null");
            requireNonNull(typeOfRemoteEntities, "typeOfRemoteEntities can't be null");

            return Iterables.filter(this.getStoredEntityLinks(localKey, localType, permissionMode),
                    new Predicate<EntityLink>() {
                        public boolean apply(final EntityLink input) {
                            return typeOfRemoteEntities.isAssignableFrom(input.getType().getClass());
                        }
                    });
        }

        public Iterable<EntityLink> getEntityLinks(final Object entity, final Class<? extends EntityType> type) {
            requireNonNull(entity);

            final EntityReference entityRef = internalHostApplication.toEntityReference(entity);
            return getEntityLinksForKey(entityRef.getKey(), entityRef.getType().getClass(), type);
        }

        public Iterable<EntityLink> getEntityLinksForKey(final String localKey, final Class<? extends EntityType> localType) {
            return getEntityLinksForKey(localKey, localType, PermissionMode.CHECK);
        }

        private Iterable<EntityLink> getEntityLinksForKey(final String localKey, final Class<? extends EntityType> localType, final PermissionMode permissionMode) {
            requireNonNull(localKey, "localKey can't be null");
            requireNonNull(localType, "localType can't be null");

            return getStoredEntityLinks(localKey, localType, permissionMode);
        }

        public Iterable<EntityLink> getEntityLinks(final Object domainObject) {
            requireNonNull(domainObject);

            final EntityReference entityRef = internalHostApplication.toEntityReference(domainObject);
            return this.getEntityLinksForKey(entityRef.getKey(), entityRef.getType().getClass());
        }

        public Iterable<EntityLink> getEntityLinksNoPermissionCheck(final Object entity, final Class<? extends EntityType> type) {
            requireNonNull(entity);

            final EntityReference entityRef = internalHostApplication.toEntityReference(entity);
            return getEntityLinksForKey(entityRef.getKey(), entityRef.getType().getClass(), type, PermissionMode.NO_CHECK);
        }

        public Iterable<EntityLink> getEntityLinksNoPermissionCheck(final Object domainObject) {
            requireNonNull(domainObject);

            final EntityReference entityRef = internalHostApplication.toEntityReference(domainObject);
            return this.getEntityLinksForKey(entityRef.getKey(), entityRef.getType().getClass(), PermissionMode.NO_CHECK);
        }

        public EntityLink getPrimaryEntityLinkForKey(final String localKey, final Class<? extends EntityType> localType,
                                                     final Class<? extends EntityType> typeOfRemoteEntity) {
            requireNonNull(localKey, "localKey can't be null`");
            requireNonNull(localType, "localType can't be null");
            requireNonNull(typeOfRemoteEntity, "typeOfRemoteEntity can't be null");

            EntityLink primary = null;

            final PrimaryRef primaryRef = getPrimaryRef(localKey, lookUpTypeId(localType), lookUpTypeId(typeOfRemoteEntity));
            if (primaryRef != null) {
                for (final EntityLink entity : getEntityLinksForKey(localKey, localType)) {
                    if (primaryRef.refersTo(entity)) {
                        primary = entity;
                        break;
                    }
                }
            }

            return primary;
        }

        public EntityLink getPrimaryEntityLink(final Object domainObject, final Class<? extends EntityType> type) {
            requireNonNull(domainObject);

            final EntityReference entityRef = internalHostApplication.toEntityReference(domainObject);
            return getPrimaryEntityLinkForKey(entityRef.getKey(), entityRef.getType().getClass(), type);
        }

        public EntityLink getEntityLink(final String localKey, final Class<? extends EntityType> localType,
                                        final String remoteKey, final Class<? extends EntityType> remoteType, final ApplicationId applicationId) {
            EntityLink link = null;
            for (final EntityLink storedLink : getStoredEntityLinks(localKey, localType)) {
                if (equivalent(storedLink, remoteKey, remoteType, applicationId)) {
                    link = storedLink;
                    break;
                }
            }
            return link;
        }

        public Iterable<EntityLink> getEntityLinksForApplicationLink(final ApplicationLink applicationLink)
                throws TypeNotInstalledException {
            requireNonNull(applicationLink);
            final List<EntityLink> entityLinks = new ArrayList<EntityLink>();
            for (final EntityReference localEntity : internalHostApplication.getLocalEntities()) {
                final ArrayList<EntityLink> list = Lists.newArrayList(Iterables.filter(getStoredEntityLinks(localEntity.getKey(), localEntity.getType().getClass()), new Predicate<EntityLink>() {
                    public boolean apply(final EntityLink input) {
                        if (applicationLink.getId().equals(input.getApplicationLink().getId())) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }));
                entityLinks.addAll(list);
            }
            return entityLinks;
        }

        public EntityLink makePrimary(final String localKey, final Class<? extends EntityType> localType, final EntityLink newPrimary) {
            requireNonNull(localKey, "localKey can't be null");
            requireNonNull(localType, "localType can't be null");
            requireNonNull(newPrimary, "newPrimary can't be null");

            //confirm that the entity is actually linked to from the specified local entity
            if (getEntityLink(localKey, localType, newPrimary.getKey(), newPrimary.getType().getClass(),
                    newPrimary.getApplicationLink().getId()) == null) {
                throw new IllegalArgumentException(String.format(
                        "Can not make %s the new primary, not linked to from local entity %s:%s", newPrimary, localType, localKey));
            }

            return makePrimaryImpl(localKey, localType, newPrimary);
        }

        private static boolean equivalent(final EntityLink a, final EntityLink b) {
            return equivalent(a, b.getKey(), b.getType().getClass(), b.getApplicationLink().getId());
        }

        private static boolean equivalent(final EntityLink a, final String key, final Class<? extends EntityType> type, final ApplicationId applicationId) {
            return a.getKey().equals(key) &&
                    a.getType().getClass().equals(type) &&
                    a.getApplicationLink().getId().equals(applicationId);
        }

        private EntityLink makePrimaryImpl(final String localKey, final Class<? extends EntityType> localType, final EntityLink newEntity) {
            final String primaryPropertyKey = primaryPropertyKey(getTypeId(newEntity.getType()));
            final Properties primary = new Properties();
            primary.put(KEY, newEntity.getKey());
            primary.put(APPLICATION_ID, newEntity.getApplicationLink().getId().get());

            if (LOG.isDebugEnabled()) {
                String message = String.format("Set primary link for [%s] [%s] as [%s]", localKey, lookUpTypeId(localType),
                        primary);
                LOG.debug(message);
            }

            propertyService.getLocalEntityProperties(localKey, lookUpTypeId(localType)).putProperty(primaryPropertyKey, primary);

            //update the entity object to correspond with the changes made
            if (!newEntity.isPrimary()) {
                return entityLinkBuilderFactory.builder()
                        .applicationLink(newEntity.getApplicationLink())
                        .key(newEntity.getKey())
                        .type(newEntity.getType())
                        .name(newEntity.getName())
                        .primary(true)
                        .build();
            }
            return newEntity;
        }

        private PrimaryRef getPrimaryRef(final String key, final TypeId typeId, final TypeId typeOfRemoteEntity) {
            requireNonNull(key, "key can't be null");
            requireNonNull(typeId, "typeId can't be null");
            requireNonNull(typeOfRemoteEntity, "typeOfRemoteEntity can't be null");

            final Properties primaryProps = (Properties) propertyService.getLocalEntityProperties(key, typeId)
                    .getProperty(primaryPropertyKey(typeOfRemoteEntity));

            PrimaryRef primaryRef = null;
            if (primaryProps != null) {
                primaryRef = new PrimaryRef(
                        getRequiredString(primaryProps, KEY),
                        typeOfRemoteEntity,
                        new ApplicationId(getRequiredString(primaryProps, APPLICATION_ID))
                );
            }
            return primaryRef;
        }

        private static String primaryPropertyKey(final TypeId remoteType) {
            return String.format(PRIMARY_FMT, remoteType.get());
        }

        private static class PrimaryRef {
            private final String key;
            private final TypeId type;
            private final ApplicationId applicationId;

            private PrimaryRef(final String key, final TypeId type, final ApplicationId applicationId) {
                this.key = requireNonNull(key, "key can't be null");
                this.type = requireNonNull(type, "type can't be null");
                this.applicationId = requireNonNull(applicationId, "applicationId can't be null");
            }

            public String getKey() {
                return key;
            }

            public TypeId getType() {
                return type;
            }

            public ApplicationId getApplicationId() {
                return applicationId;
            }

            public boolean refersTo(final String key, final TypeId type, final ApplicationId applicationId) {
                return this.key.equals(key) &&
                        this.type.equals(type) &&
                        this.applicationId.equals(applicationId);
            }

            public boolean refersTo(final EntityLink link) {
                return refersTo(link.getKey(), getTypeId(link.getType()), link.getApplicationLink().getId());
            }
        }

        private enum PermissionMode {CHECK, NO_CHECK}
    }

}
