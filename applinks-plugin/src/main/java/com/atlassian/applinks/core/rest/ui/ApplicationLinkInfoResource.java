package com.atlassian.applinks.core.rest.ui;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.EntityType;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.core.rest.auth.AdminApplicationLinksInterceptor;
import com.atlassian.applinks.core.rest.context.ContextInterceptor;
import com.atlassian.applinks.core.rest.model.ApplicationLinkInfoEntity;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.applinks.spi.link.MutatingEntityLinkService;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.sal.api.message.I18nResolver;
import com.sun.jersey.spi.resource.Singleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.core.rest.util.RestUtil.notFound;
import static com.atlassian.applinks.core.rest.util.RestUtil.ok;
import static com.atlassian.applinks.spi.application.TypeId.getTypeId;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

/**
 * This rest end point provides additional information about configured authentication providers and entity links for an Application Link.
 *
 * @since 3.0
 */
@Path("applicationlinkInfo")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Singleton
@InterceptorChain({ContextInterceptor.class, AdminApplicationLinksInterceptor.class, NoCacheHeaderInterceptor.class})
public class ApplicationLinkInfoResource {
    private final PluginAccessor pluginAccessor;
    private final ApplicationLinkService applicationLinkService;
    private final I18nResolver i18nResolver;
    private final MutatingEntityLinkService entityLinkService;
    private final InternalHostApplication internalHostApplication;
    private final TypeAccessor typeAccessor;

    public ApplicationLinkInfoResource
            (final PluginAccessor pluginAccessor,
             final ApplicationLinkService applicationLinkService,
             final I18nResolver i18nResolver,
             final MutatingEntityLinkService entityLinkService,
             final InternalHostApplication internalHostApplication,
             final TypeAccessor typeAccessor) {
        this.pluginAccessor = pluginAccessor;
        this.applicationLinkService = applicationLinkService;
        this.i18nResolver = i18nResolver;
        this.entityLinkService = entityLinkService;
        this.internalHostApplication = internalHostApplication;
        this.typeAccessor = typeAccessor;
    }

    @GET
    @Path("id/{id}")
    public Response getConfiguredAuthenticationTypesAndEntityLinksForApplicationLink(@PathParam("id") final ApplicationId id) {
        final ApplicationLink applicationLink;
        final int entityCount;
        try {
            applicationLink = applicationLinkService.getApplicationLink(id);
            entityCount = (int) stream(entityLinkService.getEntityLinksForApplicationLink(applicationLink).spliterator(), false).count();
        } catch (TypeNotInstalledException e) {
            return notFound(i18nResolver.getText("applinks.type.not.installed", e.getType()));
        }

        if (applicationLink == null) {
            return notFound(i18nResolver.getText("applinks.notfound", id.get()));
        }
        final List<String> configuredAuthProviders = pluginAccessor.getEnabledModulesByClass(AuthenticationProviderPluginModule.class).stream()
                .filter(Objects::nonNull)
                .map(provider -> provider.getAuthenticationProviderClass().getName())
                .collect(toList());
        //Get all entities and filter only the ones that are hosted by this application.
        final List<EntityType> entityTypes = pluginAccessor.getEnabledModulesByClass(EntityType.class);
        List<String> hostAppEntityTypesAsString = filterEntityTypes(entityTypes, internalHostApplication.getType());
        //Now lets get the entities of the remote application
        ArrayList<String> remoteEntityTypesAsString = new ArrayList<>(filterEntityTypes(entityTypes, internalHostApplication.getType()));

        return ok(new ApplicationLinkInfoEntity(configuredAuthProviders,
                entityCount,
                hostAppEntityTypesAsString,
                remoteEntityTypesAsString));
    }

    private List<String> filterEntityTypes(List<EntityType> entityTypes, ApplicationType type) {
        return entityTypes.stream()
                .filter(entityType -> getTypeId(type).equals(getTypeId(typeAccessor.getApplicationType(entityType.getApplicationType()))))
                .map(from -> getTypeId(from).get())
                .collect(toList());
    }
}
