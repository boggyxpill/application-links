package com.atlassian.applinks.core.rest.ui;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.concurrent.ConcurrentExecutor;
import com.atlassian.applinks.core.rest.AbstractResource;
import com.atlassian.applinks.core.rest.auth.AdminApplicationLinksInterceptor;
import com.atlassian.applinks.core.rest.context.ContextInterceptor;
import com.atlassian.applinks.core.rest.context.CurrentContext;
import com.atlassian.applinks.core.rest.model.ApplicationLinkState;
import com.atlassian.applinks.core.rest.model.ApplicationLinkStateEntity;
import com.atlassian.applinks.core.rest.util.RestUtil;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ApplicationStatus;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.spi.resource.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.core.rest.util.RestUtil.ok;

/**
 * This rest end point is used when displaying application links to fetch application link states.
 *
 * @since 4.0
 */
@Path("listApplicationlinkstates")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Singleton
@InterceptorChain({ContextInterceptor.class, AdminApplicationLinksInterceptor.class, NoCacheHeaderInterceptor.class})
public class ApplicationLinkStatesUIResource extends AbstractResource {
    private static final Logger LOG = LoggerFactory.getLogger(ApplicationLinkStatesUIResource.class);

    private final MutatingApplicationLinkService applicationLinkService;
    private final ManifestRetriever manifestRetriever;
    private final I18nResolver i18nResolver;
    private final ConcurrentExecutor executor;

    public ApplicationLinkStatesUIResource
            (
                    final MutatingApplicationLinkService applicationLinkService,
                    final ManifestRetriever manifestRetriever,
                    final I18nResolver i18nResolver,
                    final RestUrlBuilder restUrlBuilder,
                    final RequestFactory requestFactory,
                    final InternalTypeAccessor typeAccessor,
                    final ConcurrentExecutor executor) {
        super(restUrlBuilder, typeAccessor, requestFactory, applicationLinkService);
        this.applicationLinkService = applicationLinkService;
        this.manifestRetriever = manifestRetriever;
        this.i18nResolver = i18nResolver;
        this.executor = executor;
    }

    @GET
    @Path("id/{id}")
    public Response getApplicationLinkState(@PathParam("id") final ApplicationId id) {
        try {
            final MutableApplicationLink applicationLink = applicationLinkService.getApplicationLink(id);
            if (applicationLink == null) {
                throw new Exception("Couldn't find application link");
            }
            Future<ApplicationLinkState> applicationLinkStateFuture = executor.submit(new CurrentContextAwareCallable<ApplicationLinkState>() {
                @Override
                public ApplicationLinkState callWithContext() throws Exception {
                    if (manifestRetriever.getApplicationStatus(applicationLink.getRpcUrl(), applicationLink.getType()) == ApplicationStatus.UNAVAILABLE) {
                        return ApplicationLinkState.OFFLINE;
                    } else {
                        try {
                            final Manifest manifest = manifestRetriever.getManifest(applicationLink.getRpcUrl(), applicationLink.getType());
                            if (!applicationLink.getId().equals(manifest.getId())) {
                                if (manifest.getAppLinksVersion() != null && manifest.getAppLinksVersion().getMajor() >= 3) {
                                    return ApplicationLinkState.UPGRADED_TO_UAL;
                                } else {
                                    return ApplicationLinkState.UPGRADED;
                                }
                            }
                        } catch (ManifestNotFoundException e) {
                            // unknown application type
                            LOG.error("The {} application type failed to produce a " +
                                            "Manifest for Application Link {}, so we cannot " +
                                            "determine the link status.", TypeId.getTypeId(
                                    applicationLink.getType()).toString(),
                                    applicationLink.getId().toString());
                        }
                        return ApplicationLinkState.OK;
                    }
                }
            });
            return ok(new ApplicationLinkStateEntity(applicationLinkStateFuture.get()));
        } catch (Exception e) {
            LOG.error("Error occurred when retrieving application link state", e);
            return RestUtil.serverError(i18nResolver.getText("applinks.error.retrieving.application.link.list", e.getMessage()));
        }
    }

    private abstract static class CurrentContextAwareCallable<T> implements Callable<T> {
        private final HttpContext httpContext;
        private final HttpServletRequest httpServletRequest;

        private CurrentContextAwareCallable() {
            this.httpContext = CurrentContext.getContext();
            this.httpServletRequest = CurrentContext.getHttpServletRequest();
        }

        public final T call() throws Exception {
            final HttpContext oldContext = CurrentContext.getContext();
            final HttpServletRequest oldRequest = CurrentContext.getHttpServletRequest();
            CurrentContext.setContext(httpContext);
            CurrentContext.setHttpServletRequest(httpServletRequest);
            try {
                return callWithContext();
            } finally {
                CurrentContext.setContext(oldContext);
                CurrentContext.setHttpServletRequest(oldRequest);
            }
        }

        public abstract T callWithContext() throws Exception;
    }
}
