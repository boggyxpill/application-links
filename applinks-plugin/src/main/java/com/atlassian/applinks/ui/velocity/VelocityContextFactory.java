package com.atlassian.applinks.ui.velocity;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.auth.OrphanedTrustDetector;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @since 3.0
 */
@Component
public class VelocityContextFactory {
    private final InternalHostApplication internalHostApplication;
    private final InternalTypeAccessor typeAccessor;
    private final I18nResolver i18nResolver;
    private final DocumentationLinker documentationLinker;
    private final OrphanedTrustDetector orphanedTrustDetector;
    private final ApplicationLinkService applicationLinkService;
    private final ManifestRetriever manifestRetriever;
    private final MessageFactory messageFactory;
    private final UserManager userManager;

    @Autowired
    public VelocityContextFactory(final InternalHostApplication internalHostApplication,
                                  final InternalTypeAccessor typeAccessor, final I18nResolver i18nResolver,
                                  final DocumentationLinker documentationLinker,
                                  @Qualifier("delegatingOrphanedTrustDetector") final OrphanedTrustDetector orphanedTrustDetector,
                                  final ApplicationLinkService applicationLinkService,
                                  final ManifestRetriever manifestRetriever, final MessageFactory messageFactory,
                                  final UserManager userManager) {
        this.internalHostApplication = internalHostApplication;
        this.typeAccessor = typeAccessor;
        this.i18nResolver = i18nResolver;
        this.documentationLinker = documentationLinker;
        this.orphanedTrustDetector = orphanedTrustDetector;
        this.applicationLinkService = applicationLinkService;
        this.manifestRetriever = manifestRetriever;
        this.messageFactory = messageFactory;
        this.userManager = userManager;
    }

    public ListApplicationLinksContext buildListApplicationLinksContext(final HttpServletRequest request) {
        final boolean isSysadmin = userManager.isSystemAdmin(userManager.getRemoteUserKey());
        return new ListApplicationLinksContext(internalHostApplication, typeAccessor,
                i18nResolver, documentationLinker, orphanedTrustDetector, request.getContextPath(), isSysadmin);
    }

    public ListEntityLinksContext buildListEntityLinksContext(final HttpServletRequest request,
                                                              final String entityTypeId, final String entityKey) {
        final boolean isAdmin = userManager.isAdmin(userManager.getRemoteUserKey(request));
        return new ListEntityLinksContext(applicationLinkService, manifestRetriever, internalHostApplication,
                documentationLinker, i18nResolver, messageFactory, typeAccessor, entityTypeId,
                entityKey, request.getContextPath(), getUsername(request), isAdmin);
    }

    private String getUsername(HttpServletRequest request) {
        UserProfile userProfile = userManager.getRemoteUser(request);

        return userProfile != null ? userProfile.getUsername() : null;
    }
}
