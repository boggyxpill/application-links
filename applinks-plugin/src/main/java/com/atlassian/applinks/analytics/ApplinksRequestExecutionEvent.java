package com.atlassian.applinks.analytics;

import com.atlassian.analytics.api.annotations.EventName;

import static java.util.Objects.requireNonNull;

/**
 * Event sent whenever a request is sent via applinks.
 *
 * @since 5.4.24
 */
@EventName("applinks.request.execution")
public class ApplinksRequestExecutionEvent {
    private final Long approxRequestSize;
    private final Long approxResponseSize;
    private final String remoteApplicationId;

    /**
     * Event sent for each applinks request.
     *
     * @param approxRequestSize Approximate size of the request in bytes (can't fetch
     *                          size of all headers).
     * @param approxResponseSize Approximate size of the response in bytes.
     * @param remoteApplicationId The id of the application to which the request
     *                            is sent.
     */
    public ApplinksRequestExecutionEvent(final Long approxRequestSize,
                                         final Long approxResponseSize,
                                         final String remoteApplicationId) {
        this.approxRequestSize = approxRequestSize;
        this.approxResponseSize = approxResponseSize;
        this.remoteApplicationId = requireNonNull(remoteApplicationId);
    }

    public Long getApproxRequestSize() {
        return approxRequestSize;
    }

    public Long getApproxResponseSize() {
        return approxResponseSize;
    }

    public String getRemoteApplicationId() {
        return remoteApplicationId;
    }
}
