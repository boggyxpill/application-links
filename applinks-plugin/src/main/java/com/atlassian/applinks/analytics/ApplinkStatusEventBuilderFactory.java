package com.atlassian.applinks.analytics;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.api.application.bamboo.BambooApplicationType;
import com.atlassian.applinks.api.application.bitbucket.BitbucketApplicationType;
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType;
import com.atlassian.applinks.api.application.crowd.CrowdApplicationType;
import com.atlassian.applinks.api.application.fecru.FishEyeCrucibleApplicationType;
import com.atlassian.applinks.api.application.generic.GenericApplicationType;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.applinks.api.application.refapp.RefAppApplicationType;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.CorsAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.core.ApplinkStatus;
import com.atlassian.applinks.core.ApplinkStatusService;
import com.atlassian.applinks.core.ElevatedPermissionsService;
import com.atlassian.applinks.core.auth.AuthenticatorAccessor;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.status.error.ApplinkError;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.sal.api.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;


public class ApplinkStatusEventBuilderFactory {
    private final ApplinkStatusService applinkStatusService;
    private final AppLinkPluginUtil appLinkPluginUtil;
    private final ApplicationProperties applicationProperties;
    private final InternalHostApplication internalHostApplication;
    private final ElevatedPermissionsService elevatedPermissions;
    private final AuthenticatorAccessor authenticatorAccessor;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;

    @Autowired
    public ApplinkStatusEventBuilderFactory(
            final ApplinkStatusService applinkStatusService,
            final AppLinkPluginUtil appLinkPluginUtil,
            final ApplicationProperties applicationProperties,
            final InternalHostApplication internalHostApplication,
            final ElevatedPermissionsService elevatedPermissions,
            final AuthenticatorAccessor authenticatorAccessor,
            final AuthenticationConfigurationManager authenticationConfigurationManager) {
        this.applinkStatusService = applinkStatusService;
        this.appLinkPluginUtil = appLinkPluginUtil;
        this.applicationProperties = applicationProperties;
        this.internalHostApplication = internalHostApplication;
        this.elevatedPermissions = elevatedPermissions;
        this.authenticatorAccessor = authenticatorAccessor;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
    }

    Builder createBuilder() {
        return new Builder(applinkStatusService, appLinkPluginUtil, applicationProperties, internalHostApplication, elevatedPermissions, authenticatorAccessor, authenticationConfigurationManager);
    }

    static public class Builder {

        private final Logger log = LoggerFactory.getLogger(Builder.class);

        private final ApplinkStatusService applinkStatusService;
        private final AppLinkPluginUtil appLinkPluginUtil;
        private final ApplicationProperties applicationProperties;
        private final InternalHostApplication internalHostApplication;
        private final ElevatedPermissionsService elevatedPermissions;
        private final AuthenticatorAccessor authenticatorAccessor;
        private final AuthenticationConfigurationManager authenticationConfigurationManager;

        private static final String BAMBOO_TYPE = "bamboo";
        private static final String BITBUCKET_TYPE = "bitbucket";
        private static final String CONFLUENCE_TYPE = "confluence";
        private static final String CROWD_TYPE = "crowd";
        private static final String FECRU_TYPE = "fecru";
        private static final String GENERIC_TYPE = "generic";
        private static final String JIRA_TYPE = "jira";
        private static final String REF_APP_TYPE = "refApp";
        private static final String UNKNOWN = "unknown";

        private int total = 0;
        private int working = 0;
        private int failing = 0;

        private final Map<String, Integer> occurenceCounter = new HashMap<>();
        private final List<ApplinkStatusApplinkEvent> applinksDetails = new ArrayList<>();

        Builder(ApplinkStatusService applinkStatusService,
                AppLinkPluginUtil appLinkPluginUtil,
                ApplicationProperties applicationProperties,
                InternalHostApplication internalHostApplication,
                ElevatedPermissionsService elevatedPermissions,
                AuthenticatorAccessor authenticatorAccessor,
                AuthenticationConfigurationManager authenticationConfigurationManager) {
            this.applinkStatusService = applinkStatusService;
            this.appLinkPluginUtil = appLinkPluginUtil;
            this.applicationProperties = applicationProperties;
            this.internalHostApplication = internalHostApplication;
            this.elevatedPermissions = elevatedPermissions;
            this.authenticatorAccessor = authenticatorAccessor;
            this.authenticationConfigurationManager = authenticationConfigurationManager;

            occurenceCounter.put(BAMBOO_TYPE, 0);
            occurenceCounter.put(BITBUCKET_TYPE, 0);
            occurenceCounter.put(CONFLUENCE_TYPE, 0);
            occurenceCounter.put(CROWD_TYPE, 0);
            occurenceCounter.put(FECRU_TYPE, 0);
            occurenceCounter.put(GENERIC_TYPE, 0);
            occurenceCounter.put(JIRA_TYPE, 0);
            occurenceCounter.put(REF_APP_TYPE, 0);
            occurenceCounter.put(UNKNOWN, 0);
        }

        void addApplink(ReadOnlyApplicationLink readOnlyApplicationLink) {
            final ApplinkStatus applinkStatus;
            try {
                applinkStatus = elevatedPermissions.executeAs(PermissionLevel.ADMIN, () -> applinkStatusService.getApplinkStatus(readOnlyApplicationLink.getId()));
            } catch (Exception e) {
                log.error("Failed to retrieve Applink-Status for " + readOnlyApplicationLink.getName(), e);
                return;
            }

            incrementTotalCount();
            incrementStatusCount(applinkStatus);

            final String type = getType(readOnlyApplicationLink.getType());

            incrementCountByType(type);
            applinksDetails.add(getApplinkDetailEvent(readOnlyApplicationLink, applinkStatus, applicationProperties.getPlatformId()));
        }

        ApplinkStatusMainEvent buildMainEvent() {
            return new ApplinkStatusMainEvent(
                    total,
                    working,
                    failing,
                    occurenceCounter,
                    applicationProperties.getBuildNumber(),
                    applicationProperties.getVersion(),
                    applicationProperties.getPlatformId(),
                    internalHostApplication.getId().get(),
                    appLinkPluginUtil.getVersion().toString()
            );
        }

        List<ApplinkStatusApplinkEvent> buildApplinkEvents() {
            return applinksDetails;
        }

        private void incrementCountByType(String type) {
            occurenceCounter.put(
                    type,
                    occurenceCounter.getOrDefault(type, 0) + 1
            );
        }

        private void incrementTotalCount() {
            total++;
        }

        private void incrementStatusCount(final ApplinkStatus applinkStatus) {
            if (applinkStatus.isWorking()) {
                working++;
            } else {
                failing++;
            }
        }

        private String getType(ApplicationType applicationType) {
            if (applicationType instanceof BambooApplicationType) {
                return BAMBOO_TYPE;
            } else if (applicationType instanceof BitbucketApplicationType) {
                return BITBUCKET_TYPE;
            } else if (applicationType instanceof ConfluenceApplicationType) {
                return CONFLUENCE_TYPE;
            } else if (applicationType instanceof CrowdApplicationType) {
                return CROWD_TYPE;
            } else if (applicationType instanceof FishEyeCrucibleApplicationType) {
                return FECRU_TYPE;
            } else if (applicationType instanceof GenericApplicationType) {
                return GENERIC_TYPE;
            } else if (applicationType instanceof JiraApplicationType) {
                return JIRA_TYPE;
            } else if (applicationType instanceof RefAppApplicationType) {
                return REF_APP_TYPE;
            }

            return UNKNOWN;
        }

        private ApplinkStatusApplinkEvent getApplinkDetailEvent(ReadOnlyApplicationLink readOnlyApplicationLink, ApplinkStatus applinkStatus, String product) {

            final Optional<ApplinkErrorType> error = getError(applinkStatus);
            return new ApplinkStatusApplinkEvent(
                    product,
                    internalHostApplication.getId().get(),
                    getType(readOnlyApplicationLink.getType()),
                    readOnlyApplicationLink.getId().get(),
                    applinkStatus.isWorking(),
                    error.map(e -> e.getCategory().name()).orElse(""),
                    error.map(Enum::name).orElse(""),
                    getConfiguredAuthTypes(readOnlyApplicationLink.getId())
            );
        }

        private Map<String, Boolean> getConfiguredAuthTypes(ApplicationId id) {
            final HashMap<String, Boolean> authTypes = new HashMap<>();
            authTypes.put("basic", false);
            authTypes.put("trusted", false);
            authTypes.put("twoLo", false);
            authTypes.put("twoLoi", false);
            authTypes.put("threeLo", false);
            authTypes.put("cors", false);
            authTypes.put("other", false);


            for (AuthenticationProviderPluginModule module : authenticatorAccessor.getAllAuthenticationProviderPluginModules()) {
                final Class<? extends AuthenticationProvider> providerClass = module.getAuthenticationProviderClass();
                if (authenticationConfigurationManager.isConfigured(id, providerClass)) {
                    if (BasicAuthenticationProvider.class.isAssignableFrom(providerClass)) {
                        authTypes.put("basic", true);
                    } else if (TrustedAppsAuthenticationProvider.class.isAssignableFrom(providerClass)) {
                        authTypes.put("trusted", true);
                    } else if (TwoLeggedOAuthAuthenticationProvider.class.isAssignableFrom(providerClass)) {
                        authTypes.put("twoLo", true);
                    } else if (TwoLeggedOAuthWithImpersonationAuthenticationProvider.class.isAssignableFrom(providerClass)) {
                        authTypes.put("twoLoi", true);
                    } else if (OAuthAuthenticationProvider.class.isAssignableFrom(providerClass)) {
                        authTypes.put("threeLo", true);
                    } else if (CorsAuthenticationProvider.class.isAssignableFrom(providerClass)) {
                        authTypes.put("cors", true);
                    } else {
                        authTypes.put("other", true);
                    }
                }
            }
            return authTypes;
        }

        private Optional<ApplinkErrorType> getError(ApplinkStatus applinkStatus) {
            return ofNullable(applinkStatus.getError()).map(ApplinkError::getType);
        }
    }

    @EventName("applinks.status.snapshot")
    static class ApplinkStatusMainEvent {

        private final int total;
        private final int working;
        private final int failing;
        private final Map<String, Integer> occurrence;
        private final String buildNumber;
        private final String productVersion;
        private final String product;
        private final String applicationId;
        private final String pluginVersion;

        public int getTotal() {
            return total;
        }

        public int getWorking() {
            return working;
        }

        public int getFailing() {
            return failing;
        }

        public Map<String, Integer> getOccurrence() {
            return occurrence;
        }

        public String getBuildNumber() {
            return buildNumber;
        }

        public String getProductVersion() {
            return productVersion;
        }

        public String getProduct() {
            return product;
        }

        public String getApplicationId() {
            return applicationId;
        }

        public String getPluginVersion() {
            return pluginVersion;
        }

        ApplinkStatusMainEvent(int total, int working, int failing, Map<String, Integer> occurrence, String buildNumber, String productVersion, String product, String applicationId, String pluginVersion) {
            this.total = total;
            this.working = working;
            this.failing = failing;
            this.occurrence = occurrence;
            this.buildNumber = buildNumber;
            this.productVersion = productVersion;
            this.product = product;
            this.applicationId = applicationId;
            this.pluginVersion = pluginVersion;
        }
    }

    @EventName("applinks.status.applink")
    @ParametersAreNonnullByDefault
    static class ApplinkStatusApplinkEvent {

        private final String product;
        private final String remoteProduct;
        private final String remoteApplicationId;
        private final boolean isWorking;
        private final String applicationId;
        private final String errorCategory;
        private final String errorDetail;
        private final Map<String, Boolean> auth;

        public boolean getIsWorking() {
            return isWorking;
        }

        @Nonnull
        public String getErrorCategory() {
            return errorCategory;
        }

        @Nonnull
        public String getErrorDetail() {
            return errorDetail;
        }

        @Nonnull
        public String getApplicationId() {
            return applicationId;
        }


        @Nonnull
        public String getProduct() {
            return product;
        }

        @Nonnull
        public String getRemoteProduct() {
            return remoteProduct;
        }

        @Nonnull
        public String getRemoteApplicationId() {
            return remoteApplicationId;
        }

        @Nonnull
        public Map<String, Boolean> getAuth() {
            return auth;
        }

        ApplinkStatusApplinkEvent(String product, String applicationId, String remoteProduct, String remoteApplicationId, boolean isWorking, String errorCategory, String errorDetail, Map<String, Boolean> auth) {
            this.product = product;
            this.remoteProduct = remoteProduct;
            this.remoteApplicationId = remoteApplicationId;
            this.isWorking = isWorking;
            this.applicationId = applicationId;
            this.errorCategory = errorCategory;
            this.errorDetail = errorDetail;
            this.auth = auth;
        }
    }
}
