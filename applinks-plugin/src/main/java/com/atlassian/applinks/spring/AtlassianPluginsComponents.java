package com.atlassian.applinks.spring;

import com.atlassian.applinks.analytics.ApplinkStatusJob;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ApplicationLinkUIService;
import com.atlassian.applinks.api.EntityLinkService;
import com.atlassian.applinks.api.ReadOnlyApplicationLinkService;
import com.atlassian.applinks.api.SubvertedEntityLinkService;
import com.atlassian.applinks.core.ApplinkStatusService;
import com.atlassian.applinks.core.DefaultTypeAccessor;
import com.atlassian.applinks.core.ElevatedPermissionsService;
import com.atlassian.applinks.core.ElevatedPermissionsServiceImpl;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.link.InternalEntityLinkService;
import com.atlassian.applinks.core.refapp.RefAppInternalHostApplication;
import com.atlassian.applinks.core.upgrade.FishEyeCrucibleHashAuthenticatorPropertiesUpgradeTask;
import com.atlassian.applinks.host.spi.HostApplication;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.link.EntityLinkBuilderFactory;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.link.MutatingEntityLinkService;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugins.osgi.javaconfig.conditions.product.FecruOnly;
import com.atlassian.plugins.osgi.javaconfig.conditions.product.RefappOnly;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

@Configuration
@SuppressWarnings("rawtypes")
public class AtlassianPluginsComponents {

    @Bean
    public ElevatedPermissionsService elevatedPermissionsService() {
        return new ElevatedPermissionsServiceImpl();
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportApplicationLinkService(
            final ApplicationLinkService applicationLinkService) {
        return exportOsgiService(applicationLinkService,
                as(ApplicationLinkService.class,
                        MutatingApplicationLinkService.class,
                        EntityLinkService.class,
                        MutatingEntityLinkService.class,
                        SubvertedEntityLinkService.class,
                        InternalEntityLinkService.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportApplicationLinkUIService(
            final ApplicationLinkUIService applicationLinkUIService) {
        return exportOsgiService(applicationLinkUIService, as(ApplicationLinkUIService.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportApplinkStatusJob(final ApplinkStatusJob applinkStatusJob) {
        return exportOsgiService(applinkStatusJob, as(LifecycleAware.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportApplinkStatusService(
            final ApplinkStatusService applinkStatusService) {
        return exportOsgiService(applinkStatusService, as(ApplinkStatusService.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportAuthenticationConfigurationManager(
            final AuthenticationConfigurationManager authenticationConfigurationManager) {
        return exportOsgiService(authenticationConfigurationManager, as(AuthenticationConfigurationManager.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportElevatedPermissionsService(
            final ElevatedPermissionsService elevatedPermissionsService) {
        return exportOsgiService(elevatedPermissionsService, as(ElevatedPermissionsService.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportEntityLinkBuilderFactory(
            final EntityLinkBuilderFactory entityLinkBuilderFactory) {
        return exportOsgiService(entityLinkBuilderFactory, as(EntityLinkBuilderFactory.class));
    }

    @Bean
    @Conditional(FecruOnly.class)
    public FactoryBean<ServiceRegistration> exportFishEyeCrucibleHashAuthenticatorPropertiesUpgradeTask(
            final FishEyeCrucibleHashAuthenticatorPropertiesUpgradeTask fishEyeCrucibleHashAuthenticatorPropertiesUpgradeTask) {
        return exportOsgiService(fishEyeCrucibleHashAuthenticatorPropertiesUpgradeTask, as(PluginUpgradeTask.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportManifestRetriever(final ManifestRetriever manifestRetriever) {
        return exportOsgiService(manifestRetriever, as(ManifestRetriever.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportReadOnlyApplicationLinkService(
            final ReadOnlyApplicationLinkService readOnlyApplicationLinkService) {
        return exportOsgiService(readOnlyApplicationLinkService, as(ReadOnlyApplicationLinkService.class));
    }

    @Bean
    @Conditional(RefappOnly.class)
    public FactoryBean<ServiceRegistration> exportRefappHostApplication(
            final RefAppInternalHostApplication hostApplication) {
        return exportOsgiService(hostApplication,
                as(HostApplication.class, InternalHostApplication.class, LifecycleAware.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportRemoteCapabilitiesService(
            final RemoteCapabilitiesService remoteCapabilitiesService) {
        return exportOsgiService(remoteCapabilitiesService, as(RemoteCapabilitiesService.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportTypeAccessor(final InternalTypeAccessor typeAccessor) {
        return exportOsgiService(typeAccessor, as(TypeAccessor.class, InternalTypeAccessor.class));
    }

    @Bean
    public InternalTypeAccessor internalTypeAccessor(PluginAccessor pluginAccessor, PluginEventManager eventManager) {
        return new DefaultTypeAccessor(pluginAccessor, eventManager);
    }
}
