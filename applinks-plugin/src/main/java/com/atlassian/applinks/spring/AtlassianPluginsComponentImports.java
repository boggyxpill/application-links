package com.atlassian.applinks.spring;

import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.plugins.osgi.javaconfig.conditions.product.BambooOnly;
import com.atlassian.plugins.osgi.javaconfig.conditions.product.BitbucketOnly;
import com.atlassian.plugins.osgi.javaconfig.conditions.product.ConfluenceOnly;
import com.atlassian.plugins.osgi.javaconfig.conditions.product.CrowdOnly;
import com.atlassian.plugins.osgi.javaconfig.conditions.product.FecruOnly;
import com.atlassian.plugins.osgi.javaconfig.conditions.product.JiraOnly;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.factoryBeanForOsgiService;

@Configuration
public class AtlassianPluginsComponentImports {

    @Bean
    @Conditional(JiraOnly.class)
    public FactoryBean<ApplicationRoleManager> applicationRoleManager() {
        return factoryBeanForOsgiService(ApplicationRoleManager.class);
    }

    @Bean
    @Conditional(BambooOnly.class)
    public FactoryBean<InternalHostApplication> bambooInternalHostApplication() {
        return factoryBeanForOsgiService(InternalHostApplication.class);
    }

    @Bean
    @Conditional(BitbucketOnly.class)
    public FactoryBean<InternalHostApplication> bitbucketInternalHostApplication() {
        return factoryBeanForOsgiService(InternalHostApplication.class);
    }

    @Bean
    @Conditional(ConfluenceOnly.class)
    public FactoryBean<InternalHostApplication> confluenceInternalHostApplication() {
        return factoryBeanForOsgiService(InternalHostApplication.class);
    }

    @Bean
    @Conditional(CrowdOnly.class)
    public FactoryBean<InternalHostApplication> crowdInternalHostApplication() {
        return factoryBeanForOsgiService(InternalHostApplication.class);
    }

    @Bean
    @Conditional(FecruOnly.class)
    public FactoryBean<InternalHostApplication> fecruInternalHostApplication() {
        return factoryBeanForOsgiService(InternalHostApplication.class);
    }

    @Bean
    @Conditional(JiraOnly.class)
    public FactoryBean<InternalHostApplication> jiraInternalHostApplication() {
        return factoryBeanForOsgiService(InternalHostApplication.class);
    }
}
