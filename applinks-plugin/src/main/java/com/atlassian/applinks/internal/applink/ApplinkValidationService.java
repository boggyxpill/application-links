package com.atlassian.applinks.internal.applink;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ReadOnlyApplicationLink;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ValidationException;
import com.atlassian.applinks.internal.common.permission.Unrestricted;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;

import javax.annotation.Nonnull;

/**
 * @since 5.1
 */
@Unrestricted("This is a non-mutating API and hence is safe to use by anyone. Clients using this API should enforce " +
        "appropriate permission levels suitable for their use case.")
public interface ApplinkValidationService {
    /**
     * Validate application link details for update.
     *
     * @param applicationId ID of the application link to update
     * @param details       details to validate
     * @throws ValidationException if the validation fails
     */
    void validateUpdate(@Nonnull ApplicationId applicationId, @Nonnull ApplicationLinkDetails details)
            throws ValidationException, NoSuchApplinkException;

    /**
     * Validate application link details for update.
     *
     * @param applink application link to update
     * @param details details to validate
     * @throws ValidationException if the validation fails
     */
    void validateUpdate(@Nonnull ReadOnlyApplicationLink applink, @Nonnull ApplicationLinkDetails details)
            throws ValidationException;
}
