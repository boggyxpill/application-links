
package com.atlassian.applinks.internal.rest.model.status;

import com.atlassian.applinks.core.ApplinkStatus;
import com.atlassian.applinks.internal.common.rest.model.applink.RestMinimalApplicationLink;
import com.atlassian.applinks.internal.rest.model.ApplinksRestRepresentation;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;

import static java.util.Objects.requireNonNull;


/**
 * JSON entity representing {@code ApplinkStatus}. This is a read-only entity (not used for updates).
 *
 * @since 4.3
 */
public class RestApplinkStatus extends ApplinksRestRepresentation {
    public static final String LINK = "link";
    public static final String WORKING = "working";
    public static final String ERROR = "error";
    public static final String LOCAL_AUTHENTICATION = "localAuthentication";
    public static final String REMOTE_AUTHENTICATION = "remoteAuthentication";

    private RestMinimalApplicationLink link;
    private boolean working;
    private RestApplinkOAuthStatus localAuthentication;
    private RestApplinkOAuthStatus remoteAuthentication;
    private RestApplinkError error;

    //for Jackson
    public RestApplinkStatus(){

    }

    @SuppressWarnings("ConstantConditions")
    public RestApplinkStatus(@Nonnull ApplinkStatus status) {
        this(status, null);
    }

    @SuppressWarnings("ConstantConditions")
    public RestApplinkStatus(@Nonnull ApplinkStatus status, @Nullable URI authorisationCallback) {
        requireNonNull(status, "status");
        link = new RestMinimalApplicationLink(status.getLink());
        working = status.isWorking();
        localAuthentication = new RestApplinkOAuthStatus(status.getLocalAuthentication());
        remoteAuthentication = status.getRemoteAuthentication() == null ? null : new RestApplinkOAuthStatus(status.getRemoteAuthentication());
        if (!status.isWorking()) {
            error = status.getError().accept(new RestApplinkError.Visitor(authorisationCallback));
        }
        else{
            error = null;
        }
    }
}

