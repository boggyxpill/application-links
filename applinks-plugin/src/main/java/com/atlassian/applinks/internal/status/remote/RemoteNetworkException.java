package com.atlassian.applinks.internal.status.remote;

import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.internal.status.error.ApplinkErrors.findCauseOfType;
import static com.atlassian.applinks.internal.status.error.ApplinkErrors.toDetails;
import static java.util.Objects.requireNonNull;

/**
 * Generic error raised if the remote connection required to fetch Applink status has failed due to an underlying
 * network problem.
 *
 * @since 4.3
 */
public class RemoteNetworkException extends ApplinkStatusException {
    private final ApplinkErrorType applinkErrorType;
    private final Class<? extends Throwable> underlyingErrorType;

    public RemoteNetworkException(@Nonnull ApplinkErrorType applinkErrorType,
                                  @Nonnull Class<? extends Throwable> underlyingErrorType,
                                  @Nullable String message) {
        super(message);
        this.applinkErrorType = requireNonNull(applinkErrorType, "applinkErrorType");
        this.underlyingErrorType = requireNonNull(underlyingErrorType, "underlyingErrorType");
    }

    public RemoteNetworkException(@Nonnull ApplinkErrorType applinkErrorType,
                                  @Nonnull Class<? extends Throwable> underlyingErrorType,
                                  @Nullable String message,
                                  @Nullable Throwable cause) {
        super(message, cause);
        this.applinkErrorType = requireNonNull(applinkErrorType, "applinkErrorType");
        this.underlyingErrorType = requireNonNull(underlyingErrorType, "underlyingErrorType");
    }

    @Nonnull
    @Override
    public final ApplinkErrorType getType() {
        return applinkErrorType;
    }

    @Nullable
    @Override
    public final String getDetails() {
        Throwable underlyingError = findCauseOfType(this, underlyingErrorType);
        if (underlyingError != null) {
            return toErrorDetails(underlyingError);
        } else {
            return null;
        }
    }

    @Override
    public String getMessage() {
        return super.getMessage() + ": " + internalToString();
    }

    @Nullable
    protected String toErrorDetails(Throwable underlyingError) {
        return toDetails(underlyingError);
    }

    private String internalToString() {
        StringBuilder sb = new StringBuilder(250);
        return sb.append("RemoteNetworkException{")
                .append("errorType=")
                .append(applinkErrorType).append(", ")
                .append("details=").append(getDetails()).append("}").toString();
    }
}
