package com.atlassian.applinks.internal.rest.applink.data;

import javax.annotation.Nonnull;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
public abstract class AbstractRestApplinkDataProvider implements RestApplinkDataProvider {
    protected final Set<String> supportedKeys;

    protected AbstractRestApplinkDataProvider(@Nonnull Set<String> supportedKeys) {
        this.supportedKeys = requireNonNull(supportedKeys, "supportedKeys");
    }

    @Override
    @Nonnull
    public final Set<String> getSupportedKeys() {
        return supportedKeys;
    }

}
