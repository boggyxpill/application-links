package com.atlassian.applinks.internal.status.remote;

import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkErrorVisitor;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.error.ResponseApplinkError;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;


/**
 * Abstract implementation of {@link ResponseApplinkError} exception.
 *
 * @since 5.0
 */
public class ResponseApplinkStatusException extends ApplinkStatusException
        implements ResponseApplinkError {
    private final ResponseApplinkError responseError;

    public ResponseApplinkStatusException(@Nonnull ResponseApplinkError responseError) {
        super(responseError.getDetails());
        this.responseError = requireNonNull(responseError, "responseError");
    }

    public ResponseApplinkStatusException(@Nonnull ResponseApplinkError responseError, @Nullable String message) {
        super(message);
        this.responseError = requireNonNull(responseError, "responseError");
    }

    public ResponseApplinkStatusException(@Nonnull ResponseApplinkError responseError, @Nullable String message,
                                          @Nullable Throwable cause) {
        super(message, cause);
        this.responseError = requireNonNull(responseError, "responseError");
    }

    public ResponseApplinkStatusException(@Nonnull ResponseApplinkError responseError, @Nullable Throwable cause) {
        this(responseError, responseError.getDetails(), cause);
    }

    @Nonnull
    @Override
    public ApplinkErrorType getType() {
        return responseError.getType();
    }

    @Nullable
    @Override
    public String getDetails() {
        return responseError.getDetails();
    }

    @Override
    public int getStatusCode() {
        return responseError.getStatusCode();
    }

    @Nullable
    @Override
    public String getBody() {
        return responseError.getBody();
    }

    @Nullable
    @Override
    public String getContentType() {
        return responseError.getContentType();
    }

    @Nullable
    @Override
    public <T> T accept(@Nonnull ApplinkErrorVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
