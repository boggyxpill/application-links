package com.atlassian.applinks.internal.feature;

/**
 * Describes features in Applinks that can be turned on and off. {@link #isSystem() system features} cannot be
 * manipulated using Applinks API.
 *
 * @since 4.3
 */
public enum ApplinksFeatures {
    /**
     * Turns on the Bitbucket re-brand. When enabled, all Stash and Bitbucket related entities will return i18n keys
     * pointing to Bitbucket. Otherwise the i18n keys will point to Stash.
     */
    BITBUCKET_REBRAND("fusion.bitbucket.rebrand", true, true),

    /**
     * Feature being used for the one-way applinks exploration in BSP-2122. This flag must not be
     * removed until production-ready frontend code has been written.
     */
    ONE_WAY_APPLINKS("applinks.one.way.applinks", true, false),

    /**
     * Switches the V3 "Status" UI on.
     * This is a system feature, enabled by default.
     */
    V3_UI("applinks.v3.ui", true, true),

    /**
     * Feature used for testing purposes only to keep unit/integration tests working.
     */
    TEST_NON_SYSTEM("applinks.test.non-system", false, false);

    final String featureKey; // feature key to manipulate
    private final boolean system;
    private final boolean defaultValue;

    ApplinksFeatures(String featureKey, boolean system, boolean defaultValue) {
        this.featureKey = featureKey;
        this.system = system;
        this.defaultValue = defaultValue;
    }

    /**
     * @return {@code true} if this is a system feature. System features cannot be manipulated by users.
     */
    public boolean isSystem() {
        return system;
    }

    /**
     * @return default value of the feature without prior user manipulation.
     */
    public boolean getDefaultValue() {
        return defaultValue;
    }
}