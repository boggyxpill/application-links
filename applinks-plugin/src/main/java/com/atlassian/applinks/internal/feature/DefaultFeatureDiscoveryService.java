package com.atlassian.applinks.internal.feature;

import com.atlassian.applinks.internal.common.exception.InvalidFeatureKeyException;
import com.atlassian.applinks.internal.common.exception.NotAuthenticatedException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.usersettings.UserSettings;
import com.atlassian.sal.api.usersettings.UserSettingsBuilder;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;

import static com.google.common.collect.Iterables.transform;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * @since 4.3
 */
public class DefaultFeatureDiscoveryService implements FeatureDiscoveryService {
    private static final int FEATURE_KEY_MAX_LENGTH = 100;
    private static final Pattern VALID_FEATURE_KEY = Pattern.compile("[\\w\\.\\-]+");

    private static final String FEATURE_DISCOVERY_PREFIX = "applinks.featurediscovery.";
    private static final Predicate<String> IS_FEATURE_DISCOVERY_KEY = key -> key.startsWith(FEATURE_DISCOVERY_PREFIX);
    private static final Function<String, String> REMOVE_FEATURE_DISCOVERY_PREFIX =
            key -> key.substring(FEATURE_DISCOVERY_PREFIX.length());

    private final PermissionValidationService permissionValidationService;
    private final ServiceExceptionFactory serviceExceptionFactory;
    private final UserManager userManager;
    private final UserSettingsService userSettingsService;

    @Autowired
    public DefaultFeatureDiscoveryService(PermissionValidationService permissionValidationService,
                                          ServiceExceptionFactory serviceExceptionFactory,
                                          UserManager userManager,
                                          UserSettingsService userSettingsService) {
        this.serviceExceptionFactory = serviceExceptionFactory;
        this.userManager = userManager;
        this.userSettingsService = userSettingsService;
        this.permissionValidationService = permissionValidationService;
    }

    @Override
    public boolean isDiscovered(@Nonnull String featureKey) throws NotAuthenticatedException, InvalidFeatureKeyException {
        requireNonNull(featureKey, "featureKey");
        permissionValidationService.validateAuthenticated();
        validateFeatureKey(featureKey);

        return getAllKeys().contains(FEATURE_DISCOVERY_PREFIX + featureKey.toLowerCase());
    }

    @Override
    public Set<String> getAllDiscoveredFeatureKeys() throws NotAuthenticatedException {
        permissionValidationService.validateAuthenticated();
        return getDiscoveredFeatureKeys();
    }

    @Override
    public void discover(@Nonnull String featureKey) throws NotAuthenticatedException, InvalidFeatureKeyException {
        requireNonNull(featureKey, "featureKey");
        permissionValidationService.validateAuthenticated();
        validateFeatureKey(featureKey);

        userSettingsService.updateUserSettings(userManager.getRemoteUserKey(), addDiscoveredFeature(featureKey));
    }

    private void validateFeatureKey(String featureKey) throws InvalidFeatureKeyException {
        if (isBlank(featureKey) || featureKey.length() > FEATURE_KEY_MAX_LENGTH || !hasLegalChars(featureKey)) {
            throw serviceExceptionFactory.raise(InvalidFeatureKeyException.class, featureKey);
        }
    }

    private Set<String> getDiscoveredFeatureKeys() {
        Set<String> applinksFeatureKeys = Sets.filter(getAllKeys(), IS_FEATURE_DISCOVERY_KEY);
        return ImmutableSet.copyOf(transform(applinksFeatureKeys, REMOVE_FEATURE_DISCOVERY_PREFIX));
    }

    private Set<String> getAllKeys() {
        return userSettingsService.getUserSettings(userManager.getRemoteUserKey()).getKeys();
    }

    private static boolean hasLegalChars(String featureKey) {
        return VALID_FEATURE_KEY.matcher(featureKey).matches();
    }

    private static Function<UserSettingsBuilder, UserSettings> addDiscoveredFeature(final String featureKey) {
        return settingsBuilder -> settingsBuilder.put(FEATURE_DISCOVERY_PREFIX + featureKey.toLowerCase(), true).build();
    }

}
