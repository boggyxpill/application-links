package com.atlassian.applinks.internal.capabilities;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.application.generic.GenericApplicationType;
import com.atlassian.applinks.api.event.ApplicationLinkAddedEvent;
import com.atlassian.applinks.api.event.ApplicationLinkDeletedEvent;
import com.atlassian.applinks.application.BuiltinApplinksType;
import com.atlassian.applinks.core.manifest.AppLinksManifestDownloader;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.cache.ApplinksRequestCache;
import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.internal.common.event.ManifestDownloadFailedEvent;
import com.atlassian.applinks.internal.common.event.ManifestDownloadedEvent;
import com.atlassian.applinks.internal.common.exception.InvalidArgumentException;
import com.atlassian.applinks.internal.common.exception.InvalidValueException;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.net.ResponsePreconditions;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.internal.status.error.ApplinkError;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.SimpleApplinkError;
import com.atlassian.applinks.internal.status.error.UnexpectedResponseError;
import com.atlassian.applinks.internal.util.remote.AnonymousApplinksResponseHandler;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheFactory;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicates;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ws.rs.core.Response.Status;
import java.net.URI;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.atlassian.applinks.internal.common.applink.ApplicationLinks.withRpcUrl;
import static com.atlassian.applinks.internal.common.lang.ApplinksEnums.fromNameSafe;
import static com.atlassian.applinks.internal.common.lang.ApplinksStreams.toStream;
import static com.atlassian.applinks.internal.rest.capabilities.ApplinksCapabilitiesResource.capabilitiesUrl;
import static com.atlassian.applinks.internal.rest.client.RestRequestBuilder.createAnonymousRequest;
import static com.atlassian.applinks.internal.status.error.NetworkErrorTranslator.toApplinkError;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
@Component
public class DefaultRemoteCapabilitiesService implements RemoteCapabilitiesService {
    @VisibleForTesting
    static final String REQUEST_CACHE_KEY = DefaultRemoteCapabilitiesService.class.getSimpleName();
    private static final Logger log = LoggerFactory.getLogger(DefaultRemoteCapabilitiesService.class);
    private static final int MAX_CACHE_SIZE = 100;

    private final AppLinksManifestDownloader manifestDownloader;
    private final ApplicationLinkService applicationLinkService;
    private final ApplinkHelper applinkHelper;
    private final ApplinksRequestCache applinksRequestCache;
    private final EventPublisher eventPublisher;
    private final PermissionValidationService permissionValidationService;
    private final ServiceExceptionFactory serviceExceptionFactory;

    private final Cache<ApplicationId, CachedCapabilities> capabilitiesCache;

    @Autowired
    @SuppressWarnings("squid:S00107") // Reducing the constructor parameters would need a significant refactor
    public DefaultRemoteCapabilitiesService(ApplicationLinkService applicationLinkService,
                                            ApplinkHelper applinkHelper,
                                            ApplinksRequestCache applinksRequestCache,
                                            EventPublisher eventPublisher,
                                            AppLinksManifestDownloader manifestDownloader,
                                            PermissionValidationService permissionValidationService,
                                            ServiceExceptionFactory serviceExceptionFactory,
                                            CacheFactory cacheFactory) {
        this.applicationLinkService = applicationLinkService;
        this.applinkHelper = applinkHelper;
        this.applinksRequestCache = applinksRequestCache;
        this.eventPublisher = eventPublisher;
        this.manifestDownloader = manifestDownloader;
        this.permissionValidationService = permissionValidationService;
        this.serviceExceptionFactory = serviceExceptionFactory;

        this.capabilitiesCache = cacheFactory.getCache("applinks.capabilities", null, new CacheSettingsBuilder()
                .local()
                .maxEntries(MAX_CACHE_SIZE)
                .build());
    }

    @PostConstruct
    public void register() {
        eventPublisher.register(this);
    }

    @PreDestroy
    public void unregister() {
        eventPublisher.unregister(this);
    }

    @PreDestroy
    public void purgeCache() {
        // purge the cache to prevent memory leaks
        capabilitiesCache.removeAll();
    }

    @Nonnull
    @Override
    public RemoteApplicationCapabilities getCapabilities(@Nonnull ApplicationLink applink) throws NoAccessException {
        permissionValidationService.validateAdmin();

        return getCapabilitiesUnrestricted(applink);
    }

    @Nonnull
    @Override
    public RemoteApplicationCapabilities getCapabilities(@Nonnull ApplicationId id)
            throws NoSuchApplinkException, NoAccessException {
        return getCapabilities(applinkHelper.getApplicationLink(id));
    }

    @Nonnull
    @Override
    public RemoteApplicationCapabilities getCapabilities(@Nonnull ApplicationLink applink, long maxAge,
                                                         @Nonnull TimeUnit units)
            throws InvalidArgumentException, NoAccessException {
        permissionValidationService.validateAdmin();

        validateMaxAge(maxAge);
        requireNonNull(units, "units");

        ApplinkErrorType linkValidationError = validateAtlassianApplink(applink);
        if (linkValidationError != null) {
            return new RemoteCapabilitiesError(new SimpleApplinkError(linkValidationError));
        }

        ApplinksRequestCache.Cache<ApplicationId, CachedCapabilities> requestCache = getRequestCache();
        CachedCapabilities cachedCapabilities = requestCache.get(applink.getId());
        if (cachedCapabilities != null && cachedCapabilities.isUpToDate(maxAge, units)) {
            return cachedCapabilities.capabilities;
        }

        cachedCapabilities = capabilitiesCache.get(applink.getId());
        if (cachedCapabilities != null && cachedCapabilities.isUpToDate(maxAge, units)) {
            requestCache.put(applink.getId(), cachedCapabilities);
            return cachedCapabilities.capabilities;
        } else {
            return updateAndGet(applink, cachedCapabilities);
        }
    }

    @Nonnull
    @Override
    public RemoteApplicationCapabilities getCapabilities(@Nonnull ApplicationId id, long maxAge,
                                                         @Nonnull TimeUnit units)
            throws InvalidArgumentException, NoSuchApplinkException, NoAccessException {
        return getCapabilities(applinkHelper.getApplicationLink(id), maxAge, units);
    }

    // piggyback off the manifest events to update the cache "for free"
    @EventListener
    public void onManifestDownloaded(@Nonnull ManifestDownloadedEvent manifestDownloaded) {
        Manifest manifest = manifestDownloaded.getManifest();
        ApplicationLink applink = getApplinkSafe(manifest);
        if (applink != null) {
            CachedCapabilities cachedCapabilities = capabilitiesCache.get(manifest.getId());
            log.debug("Updating cache for applink ID {}", applink.getId());
            updateAndGet(applink, manifest, cachedCapabilities);
        }
    }

    @EventListener
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public void onManifestDownloadFailed(@Nonnull ManifestDownloadFailedEvent manifestDownloadFailed) {
        ApplicationLink applink = findApplinkByUrl(manifestDownloadFailed.getUri());
        if (applink != null) {
            CachedCapabilities cachedCapabilities = capabilitiesCache.get(applink.getId());
            ApplinkError error = manifestDownloadFailed.getCause() != null ?
                    toApplinkError(manifestDownloadFailed.getCause(), "Failed to download manifest") :
                    new SimpleApplinkError(ApplinkErrorType.UNKNOWN);
            log.debug("Updating cache for applink ID {} with error {}", applink.getId(), error.getType());
            updateAndGet(applink.getId(), cachedCapabilities, withError(cachedCapabilities, error));
        }
    }

    // NOTE: because the private methods below are fairly complicated flows and rely heavily on
    // nullability/nonnullability, those invariants have been explicitly documented using @Nullable and @Nonnull
    // (which is not common for private methods)

    // clear cache entries for deleted applinks
    @EventListener
    public void onApplinkDeleted(ApplicationLinkDeletedEvent applinkDeleted) {
        ApplicationId id = applinkDeleted.getApplicationId();
        log.debug("Removing cache entry for deleted applink ID {}", id);
        capabilitiesCache.remove(id);
    }

    @EventListener
    public void onApplinkCreated(ApplicationLinkAddedEvent applinkCreated) {
        // in most cases by the time this event is raised the cache for that applink is already "warm", because
        // manifests have been downloaded. However when links are created programatically this might not be the case,
        // so this is the final guard to ensure each created applink has its capabilities cached
        getCapabilitiesUnrestricted(applinkCreated.getApplicationLink());
    }

    @VisibleForTesting
    Cache<ApplicationId, CachedCapabilities> getCapabilitiesCache() {
        return capabilitiesCache;
    }

    private RemoteApplicationCapabilities getCapabilitiesUnrestricted(@Nonnull ApplicationLink applink) {
        ApplinkErrorType linkValidationError = validateAtlassianApplink(applink);
        if (linkValidationError != null) {
            return new RemoteCapabilitiesError(new SimpleApplinkError(linkValidationError));
        }

        ApplinksRequestCache.Cache<ApplicationId, CachedCapabilities> requestCache = getRequestCache();
        CachedCapabilities cachedCapabilities = requestCache.get(applink.getId());
        if (cachedCapabilities != null) {
            return cachedCapabilities.capabilities;
        }

        cachedCapabilities = capabilitiesCache.get(applink.getId());
        if (cachedCapabilities != null) {
            requestCache.put(applink.getId(), cachedCapabilities);
            return cachedCapabilities.capabilities;
        } else {
            return updateAndGet(applink, null);
        }
    }

    private ApplinksRequestCache.Cache<ApplicationId, CachedCapabilities> getRequestCache() {
        return applinksRequestCache.getCache(REQUEST_CACHE_KEY, ApplicationId.class, CachedCapabilities.class);
    }

    @Nullable
    private ApplicationLink findApplinkByUrl(@Nonnull URI uri) {
        return toStream(applicationLinkService.getApplicationLinks()).filter(withRpcUrl(uri)).findFirst().orElse(null);
    }

    @Nullable
    private ApplicationLink getApplinkSafe(@Nonnull Manifest manifest) {
        try {
            return applinkHelper.getApplicationLink(manifest.getId());
        } catch (Exception e) {
            log.warn("Exception trying to get Applink for manifest with ID {}",
                    manifest.getId());
            log.debug("Stack trace for manifest with ID {}", manifest.getId(), e);
            return null;
        }
    }

    private void validateMaxAge(long maxAge) throws InvalidArgumentException {
        if (maxAge < 0) {
            throw serviceExceptionFactory.raise(InvalidValueException.class, "Max age", ">=0", maxAge);
        }
    }

    @Nonnull
    private RemoteApplicationCapabilities updateAndGet(@Nonnull ApplicationLink applink,
                                                       @Nullable CachedCapabilities currentCapabilities) {
        try {
            Manifest manifest = manifestDownloader.downloadNoEvent(applink.getRpcUrl());
            return updateAndGet(applink, manifest, currentCapabilities);
        } catch (ManifestNotFoundException e) {
            ApplinkError error = toApplinkError(e, "Failed to download manifest");
            return updateAndGet(applink.getId(), currentCapabilities, withError(currentCapabilities, error));
        }
    }

    @Nonnull
    private RemoteApplicationCapabilities updateAndGet(@Nonnull ApplicationLink applink,
                                                       @Nonnull Manifest manifest,
                                                       @Nullable CachedCapabilities currentCapabilities) {
        ApplicationVersion applicationVersion = parseVersion(applink.getId(), manifest.getVersion());
        ApplicationVersion applinksVersion = parseApplinksVersion(applink.getId(), manifest.getAppLinksVersion());
        if (applicationVersion == null || applinksVersion == null) {
            log.warn("Manifest for applink {} did not contain valid versions, skipping cache update", applink.getId());
            return updateAndGet(applink.getId(), currentCapabilities, withError(currentCapabilities,
                    createManifestInvalidError(manifest.getVersion(), manifest.getAppLinksVersion())));
        }

        CachedCapabilities updatedCapabilities;
        if (currentCapabilities != null) {
            // this should be the most common case: if applinks version did not change and there was no error
            // previously, no further requests are needed
            if (applinksVersion.equals(currentCapabilities.capabilities.getApplinksVersion())
                    && currentCapabilities.capabilities.getError() == null) {
                updatedCapabilities = currentCapabilities.updatedNow();
            } else {
                // versions have changed or there was an error previously: update versions and fetch latest capabilities
                updatedCapabilities = currentCapabilities.withVersions(applicationVersion, applinksVersion);
                updatedCapabilities = withRetrievedCapabilities(applink, applinksVersion, updatedCapabilities);
            }
        } else {
            // no capabilities in cache, create a new instance and fetch latest capabilities
            updatedCapabilities = new CachedCapabilities(new DefaultRemoteCapabilities.Builder()
                    .applicationVersion(applicationVersion)
                    .applinksVersion(applinksVersion)
                    .build());
            updatedCapabilities = withRetrievedCapabilities(applink, applinksVersion, updatedCapabilities);
        }

        return updateAndGet(applink.getId(), currentCapabilities, updatedCapabilities);
    }

    @Nonnull
    private RemoteApplicationCapabilities updateAndGet(@Nonnull ApplicationId id,
                                                       @Nullable CachedCapabilities currentCapabilities,
                                                       @Nonnull CachedCapabilities newCapabilities) {
        // these gymnastics are here to minimize cache access
        CachedCapabilities capabilitiesAnswer;
        if (currentCapabilities != null) {
            // previous value should exist in cache
            boolean successfulUpdate = capabilitiesCache.replace(id, currentCapabilities, newCapabilities);
            // if update was successful simply return newCapabilities, otherwise someone has updated the cache while we
            // were refreshing the value - grab those from cache
            capabilitiesAnswer = successfulUpdate ? newCapabilities : capabilitiesCache.get(id);
        } else {
            // previous capabilities did not exist, putIfAbsent to not override in case of concurrent write
            capabilitiesAnswer = capabilitiesCache.putIfAbsent(id, newCapabilities);
        }

        // final nonnull guard - in case values from cache were null for any reason, use newCapabilities as a return
        // value
        capabilitiesAnswer = capabilitiesAnswer == null ? newCapabilities : capabilitiesAnswer;
        getRequestCache().put(id, capabilitiesAnswer);
        return capabilitiesAnswer.capabilities;
    }

    @Nonnull
    private CachedCapabilities withRetrievedCapabilities(@Nonnull ApplicationLink applink,
                                                         @Nonnull ApplicationVersion applinksVersion,
                                                         @Nonnull CachedCapabilities toUpdate) {
        Set<ApplinksCapabilities> remoteCapabilities;
        CachedCapabilities answer = toUpdate;
        // First Applinks version that shipped with capabilities REST. This is for optimizing the
        // capabilities retrieval - no reason to try fetch capabilities from versions <
        // applinksWithCapabilitiesVersion.
        ApplicationVersion applinksWithCapabilitiesVersion = ApplicationVersion.parse("5.0.5");
        if (applinksVersion.compareTo(applinksWithCapabilitiesVersion) < 0) {
            // for applinks < 5.0.4 we know there's no remoteCapabilities
            // also override any previously existing error
            remoteCapabilities = EnumSet.noneOf(ApplinksCapabilities.class);
            answer = answer.withError(null);
        } else {
            ApplicationLinkRequest request = createAnonymousRequest(applink, capabilitiesUrl());
            CapabilitiesResponseHandler capabilitiesHandler = new CapabilitiesResponseHandler();
            remoteCapabilities = executeSafe(request, capabilitiesHandler);
            // set new error, this will reset error to null if the request was successful
            answer = answer.withError(capabilitiesHandler.error);
        }

        return answer.withRemoteCapabilities(remoteCapabilities);
    }

    @Nonnull
    private static CachedCapabilities withError(@Nullable CachedCapabilities currentCapabilities,
                                                @Nonnull ApplinkError newError) {
        return currentCapabilities != null ?
                currentCapabilities.withError(newError) :
                new CachedCapabilities(new RemoteCapabilitiesError(newError));
    }

    @Nonnull
    private static ApplinkError createManifestInvalidError(@Nullable String applicationVersion,
                                                           @Nullable Version applinksVersion) {
        return new SimpleApplinkError(ApplinkErrorType.UNEXPECTED_RESPONSE,
                format("applicationVersion=%s,applinksVersion=%s", applicationVersion, applinksVersion));
    }

    @Nonnull
    private static Set<ApplinksCapabilities> executeSafe(@Nonnull ApplicationLinkRequest request,
                                                         @Nonnull CapabilitiesResponseHandler capabilitiesHandler) {
        try {
            return request.execute(capabilitiesHandler);
        } catch (ResponseException e) {
            capabilitiesHandler.error = toApplinkError(e, "Failed to retrieve capabilities");
            return EnumSet.noneOf(ApplinksCapabilities.class);
        }
    }

    @Nullable
    private static ApplicationVersion parseVersion(@Nonnull ApplicationId id, @Nullable String version) {
        if (StringUtils.isBlank(version)) {
            return null;
        }
        try {
            return ApplicationVersion.parse(version);
        } catch (IllegalArgumentException e) {
            log.warn("Error when parsing application version {} for applink {}", version, id);
            log.debug("Stack trace: parsing application version {} for applink {}", version, id, e);
            return null;
        }
    }

    @Nullable
    private static ApplicationVersion parseApplinksVersion(@Nonnull ApplicationId id, @Nullable Version version) {
        if (version == null) {
            return null;
        }
        try {
            return ApplicationVersion.parse(version.toString());
        } catch (IllegalArgumentException e) {
            log.warn("Error when parsing applinks version {} for applink {}", version, id);
            log.debug("Stack trace: parsing applinks version {} for applink {}", version, id, e);
            return null;
        }
    }

    @Nullable
    private static ApplinkErrorType validateAtlassianApplink(@Nonnull ApplicationLink link) {
        if (link.getType() instanceof GenericApplicationType) {
            return ApplinkErrorType.GENERIC_LINK;
        } else if (!BuiltinApplinksType.class.isInstance(link.getType())) {
            return ApplinkErrorType.NON_ATLASSIAN;
        } else {
            return null;
        }
    }

    @VisibleForTesting
    static class CachedCapabilities {
        final RemoteApplicationCapabilities capabilities;
        final long lastUpdated;

        CachedCapabilities(RemoteApplicationCapabilities capabilities, long lastUpdated) {
            this.capabilities = capabilities;
            this.lastUpdated = lastUpdated;
        }

        CachedCapabilities(RemoteApplicationCapabilities capabilities) {
            this(capabilities, System.currentTimeMillis());
        }

        CachedCapabilities updatedNow() {
            return new CachedCapabilities(capabilities);
        }

        CachedCapabilities withError(ApplinkError error) {
            return new CachedCapabilities(new DefaultRemoteCapabilities.Builder(capabilities).error(error).build());
        }

        CachedCapabilities withVersions(ApplicationVersion applicationVersion, ApplicationVersion applinksVersion) {
            return new CachedCapabilities(new DefaultRemoteCapabilities.Builder(capabilities)
                    .applicationVersion(applicationVersion)
                    .applinksVersion(applinksVersion)
                    .build());
        }

        CachedCapabilities withRemoteCapabilities(Set<ApplinksCapabilities> remoteCapabilities) {
            return new CachedCapabilities(new DefaultRemoteCapabilities.Builder(capabilities)
                    .capabilities(remoteCapabilities)
                    .build());
        }

        boolean isUpToDate(long maxAge, TimeUnit units) {
            long acceptableLastUpdated = System.currentTimeMillis() - units.toMillis(maxAge);
            return lastUpdated >= acceptableLastUpdated;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final CachedCapabilities that = (CachedCapabilities) o;
            return Objects.equals(lastUpdated, that.lastUpdated) && Objects.equals(capabilities, that.capabilities);
        }

        @Override
        public int hashCode() {
            return Objects.hash(capabilities, lastUpdated);
        }
    }

    private static class CapabilitiesResponseHandler extends AnonymousApplinksResponseHandler<Set<ApplinksCapabilities>> {
        ApplinkError error;

        @Override
        public Set<ApplinksCapabilities> handle(Response response) throws ResponseException {
            // NOT_FOUND is the expected response if the capabilities resource is not running
            // if the status code is different thant that, something must have gone quite wrong
            ResponsePreconditions.checkStatus(response, Status.OK, Status.NOT_FOUND);

            Status status = Status.fromStatusCode(response.getStatusCode());
            if (status == Status.OK) {
                try {
                    return getCapabilities(response);
                } catch (Exception e) {
                    error = new UnexpectedResponseError(response);
                    return EnumSet.noneOf(ApplinksCapabilities.class);
                }
            } else {
                // NOT_FOUND
                return EnumSet.noneOf(ApplinksCapabilities.class);
            }
        }

        private Set<ApplinksCapabilities> getCapabilities(Response response) throws ResponseException {
            @SuppressWarnings("unchecked") Iterable<String> stringCapabilities = response.getEntity(List.class);
            Iterable<ApplinksCapabilities> enumCapabilities = transform(stringCapabilities,
                    fromNameSafe(ApplinksCapabilities.class));
            return Sets.newEnumSet(filter(enumCapabilities, Predicates.notNull()), ApplinksCapabilities.class);
        }
    }
}
