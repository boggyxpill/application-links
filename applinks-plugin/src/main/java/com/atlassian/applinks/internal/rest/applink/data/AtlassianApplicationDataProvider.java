package com.atlassian.applinks.internal.rest.applink.data;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.application.ApplicationTypes;
import com.atlassian.applinks.internal.common.exception.ServiceException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Provides a boolean flag indicating whether this link represents an Atlassian application.
 *
 * @since 5.2
 */
public class AtlassianApplicationDataProvider extends AbstractSingleKeyRestApplinkDataProvider {
    public static final String ATLASSIAN = "atlassian";

    public AtlassianApplicationDataProvider() {
        super(ATLASSIAN);
    }

    @Nullable
    @Override
    public Object doProvide(@Nonnull ApplicationLink applink) throws ServiceException {
        return ApplicationTypes.isAtlassian(applink.getType());
    }
}
