package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

class DisableTrustedApp extends TryWithAuthentication {
    private static final Logger LOGGER = LoggerFactory.getLogger(DisableTrustedApp.class);
    public static final TryWithAuthentication INSTANCE = new DisableTrustedApp();
    public static final String TRUSTED_AUTOCONFIG_PATH = "/plugins/servlet/applinks/auth/conf/trusted/autoconfig/";

    private DisableTrustedApp() {
    }

    @Override
    public boolean execute(@Nonnull final ApplicationLink applicationLink,
                           @Nonnull final ApplicationId applicationId,
                           @Nonnull final ApplicationLinkRequestFactory factory)
            throws IOException, CredentialsRequiredException, ResponseException {
        final String url = TRUSTED_AUTOCONFIG_PATH + applicationId.toString();
        final ApplicationLinkRequest request = factory.createRequest(Request.MethodType.DELETE, url);
        request.setHeader("X-Atlassian-Token", "no-check");
        RemoteActionHandler handler = new RemoteActionHandler();
        request.setConnectionTimeout((int) TimeUnit.SECONDS.toMillis(TryWithAuthentication.TIME_OUT_IN_SECONDS));
        request.setSoTimeout((int) TimeUnit.SECONDS.toMillis(TryWithAuthentication.TIME_OUT_IN_SECONDS));
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(Request.MethodType.DELETE.name() + " " + applicationLink.getRpcUrl() + url);
        }
        request.execute(handler);

        /*
         * Empty response from this resource indicates that we successfully removed Trusted Authentication remotely.
         * We cannot rely solely on the status code due to redirection proxy or websudo, neither of which actually removes Trusted Authentication remotely
         */
        return handler.isSuccessful() && handler.getResponse().map(String::isEmpty).orElse(false);
    }
}
