package com.atlassian.applinks.internal.status.remote;

import com.atlassian.applinks.internal.common.auth.oauth.OAuthMessageProblemException;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Raised when status fetch fails due to OAuth-related problem.
 *
 * @since 5.0
 */
public class RemoteOAuthException extends RemoteNetworkException {
    public RemoteOAuthException(@Nonnull ApplinkErrorType applinkErrorType,
                                @Nullable String message) {
        super(applinkErrorType, OAuthMessageProblemException.class, message);
    }

    public RemoteOAuthException(@Nonnull ApplinkErrorType applinkErrorType,
                                @Nullable String message,
                                @Nullable Throwable cause) {
        super(applinkErrorType, OAuthMessageProblemException.class, message, cause);
    }

    @Nullable
    @Override
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    protected String toErrorDetails(Throwable underlyingError) {
        return OAuthMessageProblemException.class.cast(underlyingError).getOAuthProblem();
    }
}
