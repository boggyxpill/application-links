package com.atlassian.applinks.internal.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.permission.Unrestricted;

import java.util.Set;
import javax.annotation.Nonnull;

/**
 * Provides access to the {@link ApplinksCapabilities capabilities} supported by this application.
 *
 * @see ApplinksCapabilities
 * @since 5.0
 */
@Unrestricted("No sensitive information exposed. Capabilities need to be anonymously accessible by linked applications")
public interface ApplinksCapabilitiesService {
    /**
     * @return all capabilities supported by this application
     */
    @Nonnull
    Set<ApplinksCapabilities> getCapabilities();
}
