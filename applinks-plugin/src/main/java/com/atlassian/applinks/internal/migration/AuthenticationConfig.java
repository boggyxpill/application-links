package com.atlassian.applinks.internal.migration;

import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.status.LegacyConfig;

import javax.annotation.Nonnull;
import java.util.Objects;

public final class AuthenticationConfig implements LegacyConfig {
    private final OAuthConfig oAuthConfig;
    private final boolean basicConfigured;
    private final boolean trustedConfigured;

    public AuthenticationConfig() {
        this(OAuthConfig.createDisabledConfig(), false, false);
    }

    public AuthenticationConfig(@Nonnull OAuthConfig oAuthConfig, boolean basicConfigured, boolean trustedConfigured) {
        this.oAuthConfig = Objects.requireNonNull(oAuthConfig, "oAuthConfig");
        this.basicConfigured = basicConfigured;
        this.trustedConfigured = trustedConfigured;
    }

    public boolean isOAuthConfigured() {
        return oAuthConfig.isEnabled();
    }

    @Override
    public boolean isBasicConfigured() {
        return basicConfigured;
    }

    @Override
    public boolean isTrustedConfigured() {
        return trustedConfigured;
    }

    @Nonnull
    public OAuthConfig getOAuthConfig() {
        return oAuthConfig;
    }

    public AuthenticationConfig trustedConfigured(boolean trustedConfigured) {
        return new AuthenticationConfig(oAuthConfig, basicConfigured, trustedConfigured);
    }

    public AuthenticationConfig basicConfigured(boolean basicConfigured) {
        return new AuthenticationConfig(oAuthConfig, basicConfigured, trustedConfigured);
    }

    public AuthenticationConfig oauth(OAuthConfig oAuthConfig) {
        return new AuthenticationConfig(oAuthConfig, basicConfigured, trustedConfigured);
    }
}


