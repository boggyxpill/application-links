package com.atlassian.applinks.internal.migration;

import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;

import javax.annotation.Nonnull;

/**
 * Utility to test whether OAuth is configured in the context of migration.
 *
 * @since 5.2
 */
public final class OAuthMigrationUtil {
    private OAuthMigrationUtil() {
        throw new UnsupportedOperationException("Do not instantiate.");
    }

    /**
     * Determines if OAuth is already configured for both directions (incoming/outgoing),
     * or if one direction with the other direction has no authentication.
     *
     * @param status the local Authentication status.
     * @return true if OAuth is considered configured.
     */
    public static boolean isOAuthConfigured(@Nonnull final AuthenticationStatus status) {
        final OAuthConfig outgoingOAuth = status.outgoing().getOAuthConfig();
        final OAuthConfig incomingOAuth = status.incoming().getOAuthConfig();
        boolean hasIncomingLegacyConfig = status.incoming().hasLegacy();
        boolean hasOutgoingLegacyConfig = status.outgoing().hasLegacy();

        boolean atLeastOneConnectionHasOAuth = incomingOAuth.isEnabled() || outgoingOAuth.isEnabled();
        boolean incomingOAuthConfigured = incomingOAuth.isEnabled() || isConnectionDisabled(incomingOAuth, hasIncomingLegacyConfig);
        boolean outgoingOAuthConfigured = outgoingOAuth.isEnabled() || isConnectionDisabled(outgoingOAuth, hasOutgoingLegacyConfig);
        return incomingOAuthConfigured && outgoingOAuthConfigured && atLeastOneConnectionHasOAuth;
    }

    /**
     * Determines if the connection (incoming or outgoing) has no authentication.
     *
     * This occurs for one-way connection when a product can be a consumer to the other, but not a producer.
     *
     * @param oauthConfig     OAuth configuration of an incoming or an outgoing connection
     * @param hasLegacyConfig whether the connection has trusted or basic authentication configured.
     * @return true if there is no authentication, oauth or legacy.
     */
    private static boolean isConnectionDisabled(OAuthConfig oauthConfig, boolean hasLegacyConfig) {
        return !oauthConfig.isEnabled() && !hasLegacyConfig;
    }

}
