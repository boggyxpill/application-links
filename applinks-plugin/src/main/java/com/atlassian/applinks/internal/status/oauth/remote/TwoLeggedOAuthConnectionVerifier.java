package com.atlassian.applinks.internal.status.oauth.remote;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.common.net.ResponsePreconditions;
import com.atlassian.applinks.internal.rest.client.RestRequestBuilder;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.error.NetworkErrorTranslator;
import com.atlassian.applinks.internal.status.error.SimpleApplinkStatusException;
import com.atlassian.applinks.internal.util.remote.AnonymousApplinksResponseHandler;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.core.rest.ManifestResource.manifestUrl;
import static java.util.Objects.requireNonNull;

/**
 * Implementation of {@link OAuthConnectionVerifier} that performs a 2LO request to the remote application to force any OAuth problems to
 * surface.
 *
 * @since 5.0
 */
public class TwoLeggedOAuthConnectionVerifier implements OAuthConnectionVerifier {
    private final AuthenticationConfigurationManager authenticationConfigurationManager;

    @Autowired
    public TwoLeggedOAuthConnectionVerifier(AuthenticationConfigurationManager authenticationConfigurationManager) {
        this.authenticationConfigurationManager = authenticationConfigurationManager;
    }

    @Override
    public void verifyOAuthConnection(@Nonnull ApplicationLink link) throws ApplinkStatusException {
        requireNonNull(link, "link");
        if (!is2LoConfigured(link)) {
            // no check if there's no OAuth available to perform it
            return;
        }

        try {
            // verify by issuing a 2LO request to the manifest resource (guaranteed to be available anonymously)
            ApplicationLinkRequest request = new RestRequestBuilder(link)
                    .methodType(Request.MethodType.GET)
                    .url(manifestUrl())
                    .accept(MediaType.APPLICATION_JSON)
                    .authentication(TwoLeggedOAuthAuthenticationProvider.class)
                    .buildAnonymous();
            request.execute(new OAuthEchoResponseHandler());
        } catch (ResponseException e) {
            throw NetworkErrorTranslator.toApplinkErrorException(e, "2LO OAuth request failed");
        }
    }

    private boolean is2LoConfigured(ApplicationLink link) {
        return authenticationConfigurationManager.isConfigured(link.getId(), TwoLeggedOAuthAuthenticationProvider.class);
    }

    private static class OAuthEchoResponseHandler extends AnonymousApplinksResponseHandler<Void> {
        @Override
        public Void handle(Response response) throws ResponseException {
            // at this stage if there was any OAuth configuration problem, it would have been reported as oauth_problem
            // and intercepted by the OAuth applinks request framework
            // otherwise we either get OK response or UNAUTHORIZED, which means 2LO is not enabled at the other end
            ResponsePreconditions.checkStatus(response, Status.OK, Status.UNAUTHORIZED);
            if (response.getStatusCode() == Status.UNAUTHORIZED.getStatusCode()) {
                if (ApplinksOAuth.isAuthLevelDisabled(response)) {
                    throw new SimpleApplinkStatusException(ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED,
                            "Received 401 from remote application, indicating that 2LO is not enabled");
                } else {
                    // if it's 401 unrelated to OAuth, treat it as unexpected status
                    ResponsePreconditions.fail(response);
                }
            }

            return null;
        }
    }
}
