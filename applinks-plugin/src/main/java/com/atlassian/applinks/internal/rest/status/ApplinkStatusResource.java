package com.atlassian.applinks.internal.rest.status;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.core.ApplinkStatusService;
import com.atlassian.applinks.core.rest.context.ContextInterceptor;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.rest.interceptor.RestRepresentationInterceptor;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.internal.rest.RestUrlBuilder;
import com.atlassian.applinks.internal.rest.RestVersion;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.internal.rest.interceptor.ServiceExceptionInterceptor;
import com.atlassian.applinks.internal.rest.model.status.RestApplinkOAuthStatus;
import com.atlassian.applinks.internal.rest.model.status.RestApplinkStatus;
import com.atlassian.applinks.internal.status.oauth.OAuthStatusService;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import javax.annotation.Nonnull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

import static com.atlassian.applinks.core.rest.util.RestUtil.badRequest;
import static com.atlassian.applinks.core.rest.util.RestUtil.noContent;
import static com.atlassian.applinks.core.rest.util.RestUtil.ok;
import static com.atlassian.applinks.internal.common.rest.util.RestApplicationIdParser.parseApplicationId;

@Api
@Path(ApplinkStatusResource.CONTEXT)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@AnonymousAllowed
@InterceptorChain({
        ContextInterceptor.class,
        ServiceExceptionInterceptor.class,
        RestRepresentationInterceptor.class,
        NoCacheHeaderInterceptor.class})
public class ApplinkStatusResource {
    public static final String CONTEXT = "status";

    public static final RestUrl STATUS_PATH = RestUrl.forPath(CONTEXT);
    public static final RestUrl OAUTH_PATH = RestUrl.forPath("oauth");

    private static final String AUTHORISATION_CALLBACK = "authorisationCallback";

    @Nonnull
    public static RestUrlBuilder statusUrl(@Nonnull ApplicationId id) {
        return new RestUrlBuilder()
                .version(RestVersion.V3)
                .addPath(STATUS_PATH)
                .addApplicationId(id);
    }

    @Nonnull
    public static RestUrlBuilder oAuthStatusUrl(@Nonnull ApplicationId id) {
        return statusUrl(id).addPath(OAUTH_PATH);
    }

    private final ApplinkHelper applinkHelper;
    private final ApplinkStatusService applinkStatusService;
    private final OAuthStatusService oAuthStatusService;

    public ApplinkStatusResource(ApplinkHelper applinkHelper,
                                 ApplinkStatusService applinkStatusService,
                                 OAuthStatusService oAuthStatusService) {
        this.applinkHelper = applinkHelper;
        this.applinkStatusService = applinkStatusService;
        this.oAuthStatusService = oAuthStatusService;
    }

    @ApiOperation(value = "Returns the status of the Applink with a given id",
            authorizations = @Authorization("Admin"),
            response = RestApplinkStatus.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "User does not have Administrator access" ),
            @ApiResponse(code = 404, message = "No Applink with the given ID exists on this server"),
    })
    @GET
    @Path("{id}")
    public Response getStatus(@PathParam("id") String id,
                              @ApiParam(hidden = true) @QueryParam(AUTHORISATION_CALLBACK) String authorisationCallback)
            throws NoAccessException, NoSuchApplinkException {
        URI uriAuthorisationCallback = parseAuthorisationCallback(authorisationCallback);
        return ok(new RestApplinkStatus(applinkStatusService.getApplinkStatus(parseApplicationId(id)),
                uriAuthorisationCallback));
    }

    @ApiOperation(
            value = "Get the Applink Oauth status for an applink with the given id",
            response = RestApplinkOAuthStatus.class)

    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 404, message = "No Applink with the given ID exists on this server"),
    })
    @GET
    @Path("{id}/oauth")
    public Response getOAuthStatus(@PathParam("id") String id) throws ServiceException {
        return ok(new RestApplinkOAuthStatus(oAuthStatusService.getOAuthStatus(parseApplicationId(id))));
    }

    @ApiOperation(
            value = "Update the Applink Oauth status for an applink with the given id",
            authorizations = @Authorization("Admin"),
            notes = "NOTE: Enabling Two-legged OAuth with Impersonation requires Sysadmin access",
            response = RestApplinkOAuthStatus.class)
    @ApiResponses({
            @ApiResponse(code=204, message = "Update successful"),
            @ApiResponse(code = 401, message = "User does not have Administrator access" ),
            @ApiResponse(code = 404, message = "No Applink with the given ID exists on this server"),
            @ApiResponse(code = 500, message = "Malformed json body (this will hopefully be fixed in a future API release)"),
            @ApiResponse(code = 409, message = "Public key for remote host unavailable and no cached credenitals exist")
    })
    @PUT
    @Path("{id}/oauth")
    public Response updateOAuthStatus(@PathParam("id") String id, RestApplinkOAuthStatus restOAuthStatus)
            throws ServiceException {
        ApplicationLink link = applinkHelper.getApplicationLink(parseApplicationId(id));
        oAuthStatusService.updateOAuthStatus(link, restOAuthStatus.asDomain());

        return noContent();
    }

    private static URI parseAuthorisationCallback(String authorisationCallback) {
        if (authorisationCallback == null) {
            return null;
        }
        try {
            URI callback = new URI(authorisationCallback);
            if (callback.isAbsolute()) {
                return callback;
            } else {
                throw new WebApplicationException(badRequest(AUTHORISATION_CALLBACK + " must be absolute"));
            }
        } catch (URISyntaxException e) {
            throw new WebApplicationException(badRequest(e.getMessage()));
        }
    }
}
