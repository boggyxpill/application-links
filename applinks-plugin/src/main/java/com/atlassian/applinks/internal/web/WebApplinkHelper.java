package com.atlassian.applinks.internal.web;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.permission.Unrestricted;
import com.atlassian.applinks.internal.common.exception.InvalidApplicationIdException;
import com.atlassian.applinks.internal.common.exception.InvalidRequestException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ServiceException;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

/**
 * Retrieves application links from IDs in servlet paths, throwing {@link ServiceException} in case of failure.
 *
 * @since 5.1
 */
@Unrestricted("Clients using this component are responsible for enforcing appropriate permissions")
public interface WebApplinkHelper {
    /**
     * Retrieve application link from ID presumed to be embedded in the {@code request}'s path info,
     *
     * @param request request to examine
     * @return application link
     */
    @Nonnull
    ApplicationLink getApplicationLink(@Nonnull HttpServletRequest request)
            throws InvalidRequestException, InvalidApplicationIdException, NoSuchApplinkException;
}
