package com.atlassian.applinks.internal.status.oauth.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.sal.api.net.ResponseException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Describes a way to fetch remote OAuth status
 *
 * @since 5.0
 */
public interface OAuthStatusFetchStrategy {
    @Nullable
    ApplinkOAuthStatus fetch(@Nonnull ApplicationId localId, @Nonnull ApplicationLink applink)
            throws ApplinkStatusException, ResponseException;
}
