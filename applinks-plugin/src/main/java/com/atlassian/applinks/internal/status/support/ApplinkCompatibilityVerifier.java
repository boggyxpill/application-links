package com.atlassian.applinks.internal.status.support;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Verifies compatibility of the examined applinks. Incompatible Applinks cannot be further diagnosed by the Status
 * API.
 *
 * @see com.atlassian.applinks.internal.status.error.ApplinkErrorCategory#INCOMPATIBLE
 * @since 4.3
 */
public interface ApplinkCompatibilityVerifier {
    /**
     * Examine {@code applicationLink} for local compatibility (without contacting the remote end).
     *
     * @param applicationLink applink to examine
     * @return specific compatibility error found, or {@code null} if the application link's local configuration is
     * compatible
     */
    @Nullable
    ApplinkErrorType verifyLocalCompatibility(@Nonnull ApplicationLink applicationLink);
}
