package com.atlassian.applinks.internal.rest.applink.data;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.exception.ServiceException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.lang.String.format;
import static java.util.Collections.singleton;
import static java.util.Objects.requireNonNull;

/**
 * @since 5.2
 */
public abstract class AbstractSingleKeyRestApplinkDataProvider extends AbstractRestApplinkDataProvider
        implements RestApplinkDataProvider {
    protected AbstractSingleKeyRestApplinkDataProvider(@Nonnull String supportedKey) {
        super(singleton(requireNonNull(supportedKey, "supportedKey")));
    }

    @Nullable
    @Override
    public Object provide(@Nonnull String key, @Nonnull ApplicationLink applink)
            throws ServiceException, IllegalArgumentException {
        if (!supportedKeys.contains(key)) {
            throw new IllegalArgumentException(format("Unsupported key: '%s'", key));
        }
        return doProvide(applink);
    }

    @Nullable
    protected abstract Object doProvide(@Nonnull ApplicationLink applink) throws ServiceException;
}
