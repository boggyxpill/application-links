package com.atlassian.applinks.internal.rest.applink.data;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType;
import com.atlassian.applinks.internal.common.application.ApplicationTypes;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.net.Uris;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Provides config URL for an applink, which goes straight to the Applinks config for Atlassian applications, and
 * otherwise points to the display URL.
 *
 * @since 5.2
 */
public class ConfigUrlDataProvider extends AbstractSingleKeyRestApplinkDataProvider {
    public static final String CONFIG_URL = "configUrl";

    public ConfigUrlDataProvider() {
        super(CONFIG_URL);
    }

    @Nullable
    @Override
    public Object doProvide(@Nonnull ApplicationLink applink) throws ServiceException {
        if (ApplicationTypes.isAtlassian(applink.getType())) {
            return getAtlassianConfigUrl(applink);
        } else {
            return applink.getDisplayUrl();
        }
    }

    private Object getAtlassianConfigUrl(ApplicationLink applink) {
        // backwards compatibility with the older Applinks version, where theconfig screen was only exposed as a
        // Confluence action;
        // also see ConfluenceLegacyApplicationLinksUrlFilter
        if (applink.getType() instanceof ConfluenceApplicationType) {
            return Uris.uncheckedConcatenate(applink.getDisplayUrl(), "/admin/listapplicationlinks.action");
        } else {
            return Uris.uncheckedConcatenate(applink.getDisplayUrl(), "/plugins/servlet/applinks/listApplicationLinks");
        }
    }
}
