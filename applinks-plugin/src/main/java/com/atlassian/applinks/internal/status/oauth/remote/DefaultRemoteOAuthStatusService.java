package com.atlassian.applinks.internal.status.oauth.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.auth.Anonymous;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.internal.common.exception.InvalidArgumentException;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.error.NetworkErrorTranslator;
import com.atlassian.applinks.internal.status.error.SimpleApplinkStatusException;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.remote.NoOutgoingAuthenticationException;
import com.atlassian.applinks.internal.status.remote.NoRemoteApplinkException;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.sal.api.net.ResponseException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.net.URI;
import java.util.EnumSet;
import java.util.concurrent.TimeUnit;

/**
 * @since 4.3
 */
public class DefaultRemoteOAuthStatusService implements RemoteOAuthStatusService {
    // applink errors to short-circuit for - we know we won't be able to get status from them
    private static final EnumSet<ApplinkErrorType> INCOMPATIBLE_LINKS = EnumSet.of(
            ApplinkErrorType.GENERIC_LINK,
            ApplinkErrorType.NON_ATLASSIAN,
            ApplinkErrorType.REMOTE_VERSION_INCOMPATIBLE
    );

    private static final String FAILED_TO_FETCH_OAUTH_STATUS_MESSAGE = "Failed to fetch OAuth status";

    private final ApplinkHelper applinkHelper;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final InternalHostApplication internalHostApplication;
    private final OAuthConnectionVerifier oAuthConnectionVerifier;
    private final PermissionValidationService permissionValidationService;
    private final RemoteCapabilitiesService remoteCapabilitiesService;

    @Autowired
    public DefaultRemoteOAuthStatusService(ApplinkHelper applinkHelper,
                                           AuthenticationConfigurationManager authenticationConfigurationManager,
                                           InternalHostApplication internalHostApplication,
                                           OAuthConnectionVerifier oAuthConnectionVerifier,
                                           PermissionValidationService permissionValidationService,
                                           RemoteCapabilitiesService remoteCapabilitiesService) {
        this.applinkHelper = applinkHelper;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.internalHostApplication = internalHostApplication;
        this.oAuthConnectionVerifier = oAuthConnectionVerifier;
        this.permissionValidationService = permissionValidationService;
        this.remoteCapabilitiesService = remoteCapabilitiesService;
    }

    @Nonnull
    @Override
    public ApplinkOAuthStatus fetchOAuthStatus(@Nonnull ApplicationId id)
            throws NoSuchApplinkException, NoAccessException, ApplinkStatusException {
        return fetchOAuthStatus(applinkHelper.getApplicationLink(id));
    }

    @Nonnull
    @Override
    public ApplinkOAuthStatus fetchOAuthStatus(@Nonnull ApplicationLink link)
            throws ApplinkStatusException, NoAccessException {
        permissionValidationService.validateAdmin();

        try {
            return fetchInternal(link);
        } catch (ResponseException e) {
            throw NetworkErrorTranslator.toApplinkErrorException(e, FAILED_TO_FETCH_OAUTH_STATUS_MESSAGE);
        }
    }

    private ApplinkOAuthStatus fetchInternal(@Nonnull ApplicationLink applink)
            throws ResponseException, NoRemoteApplinkException, NoAccessException {

        RemoteApplicationCapabilities capabilities = getCapabilities(applink, 1, TimeUnit.HOURS);
        OAuthStatusFetchStrategy fetchStrategy = getFetchStrategy(applink, capabilities);

        // try "default" local ID first, then try fallback (see generateFallbackId)
        ApplinkOAuthStatus status = fetchStrategy.fetch(internalHostApplication.getId(), applink);
        if (status != null) {
            return status;
        }

        // then try by fallback ID
        status = fetchStrategy.fetch(generateFallbackId(), applink);
        if (status != null) {
            return status;
        } else {
            throw new NoRemoteApplinkException(applink.getRpcUrl() +
                    " does not have Application Link to the local application");
        }
    }

    private RemoteApplicationCapabilities getCapabilities(@Nonnull ApplicationLink applink, long maxAge, TimeUnit unit)
            throws NoAccessException {
        try {
            return remoteCapabilitiesService.getCapabilities(applink, maxAge, unit);
        } catch (InvalidArgumentException e) {
            throw new AssertionError("Unexpected InvalidArgumentException when getting capabilities", e);
        }
    }

    @SuppressWarnings("ConstantConditions")
    private OAuthStatusFetchStrategy getFetchStrategy(ApplicationLink applink,
                                                      RemoteApplicationCapabilities capabilities) {
        if (isIncompatible(capabilities)) {
            // don't attempt to retrieve status for applinks that we know don't support any of the required REST APIs
            throw new SimpleApplinkStatusException(capabilities.getError().getType());
        }
        if (capabilities.getCapabilities().contains(ApplinksCapabilities.STATUS_API)) {
            return new StatusApiOAuthFetchStrategy(Anonymous.class);
        } else {
            Class<? extends AuthenticationProvider> provider = getAuthProvider(applink);
            if (capabilities.getApplinksVersion() != null && capabilities.getApplinksVersion().getMajor() >= 5) {
                // Applinks 5.x
                return new ApplinkAuthenticationOAuthFetchStrategy.For5x(provider, oAuthConnectionVerifier);
            } else if (capabilities.getApplinksVersion() != null && capabilities.getApplinksVersion().getMajor() == 4) {
                // Applinks 4.x
                return new ApplinkAuthenticationOAuthFetchStrategy.For4x(provider, oAuthConnectionVerifier);
            } else if (capabilities.getApplinksVersion() != null) {
                // incompatible version
                throw new SimpleApplinkStatusException(ApplinkErrorType.REMOTE_VERSION_INCOMPATIBLE);
            } else {
                // Applinks version not available - try both 4.x and 5.x
                return new OAuthStatusFetchStrategyChain(
                        new ApplinkAuthenticationOAuthFetchStrategy.For5x(provider, oAuthConnectionVerifier),
                        new ApplinkAuthenticationOAuthFetchStrategy.For4x(provider, oAuthConnectionVerifier)
                );
            }
        }
    }


    @SuppressWarnings("ConstantConditions")
    private static boolean isIncompatible(RemoteApplicationCapabilities capabilities) {
        return capabilities.hasError() && INCOMPATIBLE_LINKS.contains(capabilities.getError().getType());
    }

    /**
     * @return fallback application ID that is based on the base URL rather than provided via product SPI (which is
     * usually based on server ID). This may be used in the Atlassian Cloud environment instead of the default ID
     * obtained via {@code InternalHostApplication.getId}.
     * @see InternalHostApplication#getId()
     * @see ApplicationIdUtil#generate(URI)
     */
    private ApplicationId generateFallbackId() {
        return ApplicationIdUtil.generate(internalHostApplication.getBaseUrl());
    }

    /**
     * @param link applink to connect to
     * @return auth provider to use to make an authenticated request - try pick one of the impersonating OAuth
     * providers, otherwise bail
     */
    private Class<? extends AuthenticationProvider> getAuthProvider(ApplicationLink link) {
        if (authenticationConfigurationManager.isConfigured(link.getId(),
                TwoLeggedOAuthWithImpersonationAuthenticationProvider.class)) {
            return TwoLeggedOAuthWithImpersonationAuthenticationProvider.class;
        } else if (authenticationConfigurationManager.isConfigured(link.getId(),
                OAuthAuthenticationProvider.class)) {
            return OAuthAuthenticationProvider.class;
        } else {
            // unable to make a request, outgoing OAuth disabled
            throw new NoOutgoingAuthenticationException("Neither 3LO nor 2LOi auth configured");
        }
    }
}
