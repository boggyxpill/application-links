package com.atlassian.applinks.internal.status.error;

import com.atlassian.applinks.internal.status.remote.ApplinkStatusAccessException;
import com.atlassian.applinks.internal.status.remote.ResponseApplinkStatusException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Creates exceptions matching instances of {@link ApplinkError}.
 *
 * @since 5.0
 */
public class ApplinkErrorExceptionFactory implements ApplinkErrorVisitor<ApplinkStatusException> {
    @Nullable
    @Override
    public ApplinkStatusException visit(@Nonnull ApplinkError error) {
        return error instanceof ApplinkStatusException ?
                (ApplinkStatusException) error :
                new SimpleApplinkStatusException(error, asCause(error));
    }

    @Nullable
    @Override
    public ApplinkStatusException visit(@Nonnull AuthorisationUriAwareApplinkError error) {
        return error instanceof ApplinkStatusAccessException ?
                (ApplinkStatusAccessException) error :
                new ApplinkStatusAccessException(error, asCause(error));
    }

    @Nullable
    @Override
    public ApplinkStatusException visit(@Nonnull ResponseApplinkError responseError) {
        return responseError instanceof ResponseApplinkStatusException ?
                (ResponseApplinkStatusException) responseError :
                new ResponseApplinkStatusException(responseError, asCause(responseError));
    }

    private static Throwable asCause(@Nonnull Object error) {
        return error instanceof Throwable ? (Throwable) error : null;
    }
}
