<!DOCTYPE html>
#* @vtlvariable name="i18n" type="com.atlassian.sal.api.message.I18nResolver" *#
#* @vtlvariable name="username" type="java.lang.String" *#
#parse("/com/atlassian/applinks/core/common/_help_link.vm")
#parse("/com/atlassian/applinks/core/common/_configured_status.vm")
#parse("/com/atlassian/applinks/core/common/_xsrf_token_element.vm")
<html>
<head>
    <script type="text/javascript">
        // force disable TZ update flag
        window.__tzTesting = true;
    </script>
    ${webResources.get()}
    ## The pane made visible on page load:
    <meta name="view" content="$view">
    ## The pane that will be activated by the Cancel button:
    <meta name="cancel" content="#if($username)enabled#{else}disabled#end">
</head>
<body class="auth-config aui-layout aui-theme-default">
    ## The pane that is rendered when auth is disabled.
    <div class="auth-basic-view">
        <form id="auth-basic-access" class="aui ajs-dirty-warning-exempt" method="POST">
            #if ($configured)
                <div class="aui-message aui-message-info">
                    <p>
                        ${i18n.getText("auth.basic.config.configured", $remoteApplicationName, $remoteApplicationType)}
                        #help('applinks.docs.configuring.auth.basic')
                    </p>
                </div>
                #status($configured)
                <div class="field-group">
                    <label>${i18n.getText("auth.basic.config.label.username")}</label>
                    <span class="field-value">$!username</span>
                </div>
                ## TODO: add a test button: https://studio.atlassian.com/browse/APL-200
                <div class="buttons-container">
                    <div class="buttons">
                        <input type="submit" value="${i18n.getText("auth.basic.config.button.disable")}" class="button" />
                        <input type="hidden" name="method" value="DELETE" />## forms don't support DELETE
                    </div>
                </div>
            #else
                <div class="aui-message aui-message-info">
                    <p>
                        ${i18n.getText("auth.basic.config.label.edit", $localApplicationName, $localApplicationType, $remoteApplicationName, $remoteApplicationType)}
                        #help('applinks.docs.configuring.auth.basic')
                    </p>
                </div>
                #status($configured)
                <div class="field-group">
                    <label for="basicUsername">${i18n.getText("auth.basic.config.label.username")}<span class="aui-icon icon-required"></span></label>
                    <input id="basicUsername" type="text" name="username" class="text medium-field" value="#if($usernameInput)$!usernameInput#{else}$!username#end" />
                </div>
                <div class="field-group">
                    <label for="password1">${i18n.getText("auth.basic.config.label.password.new")}<span class="aui-icon icon-required"></span></label>
                    <input type="password" class="password medium-field" id="password1" name="password1" />
                </div>
                <div class="field-group">
                    <label for="password2">${i18n.getText("auth.basic.config.label.password.confirm")}<span class="aui-icon icon-required"></span></label>
                    <input type="password" class="password medium-field" name="password2" id="password2" />
                </div>
                <div class="field-group">
                    <div class="error">$!error</div>
                </div>
                <div class="buttons-container">
                    <div class="buttons">
                        <input type="submit" value="${i18n.getText("auth.basic.config.button.enable")}" class="button" />
                    </div>
                </div>
                <input type="hidden" name="method" value="PUT">## PUT doesn't support www-form-encoded content
            #end
            #xsrfTokenElement()
        </form>
    </div>
</body>
</html>
