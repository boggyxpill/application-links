package com.atlassian.applinks.api.application.stash;

import com.atlassian.applinks.api.ApplicationType;

/**
 * @since 3.8
 * @deprecated Stash has been renamed to Bitbucket. Please use
 * {@link com.atlassian.applinks.api.application.bitbucket.BitbucketApplicationType} instead.
 */
@Deprecated
public interface StashApplicationType extends ApplicationType {
}
