package com.atlassian.applinks.api.application.crowd;

import com.atlassian.applinks.api.ApplicationType;

/**
 * Application type for Crowd
 *
 * @since v3.11
 */
public interface CrowdApplicationType extends ApplicationType {
}
