package com.atlassian.applinks.api.event;

import com.atlassian.applinks.api.ApplicationLink;

import javax.annotation.Nonnull;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * This event is broadcast after an application link's details (e.g., name or display URL)
 * have changed.
 *
 * @since 3.2
 */
public class ApplicationLinkDetailsChangedEvent extends ApplicationLinkEvent {

    private final ApplicationLink oldApplicationLink;

    /**
     * @param applicationLink The updated {@link ApplicationLink}.
     * @deprecated for removal in 8.0. Use {@link #ApplicationLinkDetailsChangedEvent(ApplicationLink, ApplicationLink)}
     * and specify the older application link as well
     */
    @Deprecated
    public ApplicationLinkDetailsChangedEvent(final ApplicationLink applicationLink) {
        this(applicationLink, null);
    }

    public ApplicationLinkDetailsChangedEvent(@Nonnull final ApplicationLink applicationLink,
                                              final ApplicationLink oldApplicationLink) {
        super(requireNonNull(applicationLink, "applicationLink"));
        this.oldApplicationLink = oldApplicationLink;
    }

    @Nonnull
    public Optional<ApplicationLink> getOldApplicationLink() {
        return Optional.ofNullable(oldApplicationLink);
    }
}