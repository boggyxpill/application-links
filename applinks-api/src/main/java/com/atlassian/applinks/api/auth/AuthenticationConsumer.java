package com.atlassian.applinks.api.auth;

/**
 * Represents the consuming end of an Authentication mechanism
 *
 * @since SHPXXVII-43
 */
public interface AuthenticationConsumer {
}
