package com.atlassian.applinks.ui.validators;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.ui.AbstractApplinksServlet;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.stream.StreamSupport;

public class CallbackParameterValidator {
    private final MessageFactory messageFactory;
    private final InternalHostApplication internalHostApplication;
    private final ApplicationLinkService applicationLinkService;

    @Autowired
    public CallbackParameterValidator(final MessageFactory messageFactory,
                                      final InternalHostApplication internalHostApplication,
                                      final ApplicationLinkService applicationLinkService) {
        this.messageFactory = messageFactory;
        this.internalHostApplication = internalHostApplication;
        this.applicationLinkService = applicationLinkService;
    }

    public boolean isCallbackUrlValid(String callbackUrl) {
        try {
            validate(callbackUrl);
            return true;
        } catch (AbstractApplinksServlet.BadRequestException e) {
            return false;
        }
    }

    public void validate(final String redirectUri)
            throws AbstractApplinksServlet.BadRequestException {
        final URI baseUrl = internalHostApplication.getBaseUrl();

        final URI callbackUri;
        try {
            callbackUri = new URI(redirectUri);
        } catch (URISyntaxException e) {
            throw createBadRequestException(redirectUri);
        }

        // if it's an absolute url, it should start with application base url
        // absolute = has scheme or is schemaless
        if (redirectUri.startsWith("//")) {
            throw createBadRequestException(redirectUri);
        }

        if (!callbackUri.isAbsolute() && !redirectUri.startsWith(baseUrl.getPath())) {
            throw createBadRequestException(redirectUri);
        }

        if (!callbackUri.isAbsolute()) {
            return;
        }

        if (!validateAbsoluteUri(baseUrl, callbackUri) && !isPointingToAnyApplinkUrl(callbackUri)) {
            throw createBadRequestException(redirectUri);
        }
    }

    private boolean isPointingToAnyApplinkUrl(final URI callbackUri) {
        return StreamSupport.stream(applicationLinkService.getApplicationLinks().spliterator(), false)
                .anyMatch(applicationLink -> validateCallbackUriAgainstApplink(callbackUri, applicationLink));
    }

    public boolean validateCallbackUriAgainstApplink(URI callbackUri, ApplicationLink applicationLink) {
        final boolean isValidByDisplayUrl = validateAbsoluteUri(applicationLink.getDisplayUrl(), callbackUri);
        final boolean isValidByRpcUrl = validateAbsoluteUri(applicationLink.getRpcUrl(), callbackUri);
        return isValidByDisplayUrl || isValidByRpcUrl;
    }

    public boolean validateAbsoluteUri(URI baseUri, URI targetUri) {
        if (!baseUri.getScheme().equals(targetUri.getScheme())) {
            return false;
        }

        if (!baseUri.getHost().equals(targetUri.getHost())) {
            return false;
        }

        if (baseUri.getPort() != targetUri.getPort()) {
            return false;
        }

        if (!targetUri.getPath().startsWith(baseUri.getPath())) {
            return false;
        }

        return true;
    }

    private AbstractApplinksServlet.BadRequestException createBadRequestException(final String redirectUri) {
        return new AbstractApplinksServlet.BadRequestException(messageFactory.newI18nMessage("auth.config.parameter.callback.invalid", redirectUri));
    }
}
