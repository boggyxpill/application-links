package com.atlassian.applinks.ui;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.applinks.core.util.Message;

public class UnauthorizedException extends RequestException {
    public UnauthorizedException() {
        this(null);
    }

    public UnauthorizedException(final Message message) {
        super(HttpServletResponse.SC_UNAUTHORIZED, message);
    }

    public UnauthorizedException(final Message message, final Throwable cause) {
        super(HttpServletResponse.SC_UNAUTHORIZED, message, cause);
    }

    @Override
    public final String getTemplate() {
        return "com/atlassian/applinks/ui/no_admin_privileges.vm";
    }
}
