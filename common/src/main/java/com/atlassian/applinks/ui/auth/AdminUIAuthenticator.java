package com.atlassian.applinks.ui.auth;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * Component for use in servlet filters and interceptors to limit AppLinks end-point access to administrators.
 */
public class AdminUIAuthenticator {
    /**
     * Username request parameter
     */
    public static final String ADMIN_USERNAME = "al_username";
    /**
     * Password request parameter
     */
    public static final String ADMIN_PASSWORD = "al_password";

    private static final String ADMIN_SESSION_KEY = "al_auth";
    private static final String ADMIN = "admin";
    private static final String SYSADMIN = "sysadmin";

    private final UserManager userManager;

    @Autowired
    public AdminUIAuthenticator(final UserManager userManager) {
        this.userManager = userManager;
    }

    /**
     * Check the supplied username and password, and set a session flag if valid.
     *
     * @param username       the username of the admin user to authenticate as
     * @param password       the password of the admin user to authenticate as
     * @param sessionHandler a {@link SessionHandler} for mutating the user's current session
     * @return true if the supplied username/password are not null and they match an admin user; or
     *         true if the supplied username/password are null, and the currently logged in user is an admin; or
     *         false otherwise.
     * @see #checkAdminUIAccessBySessionOrCurrentUser
     */
    public boolean checkAdminUIAccessBySessionOrPasswordAndActivateAdminSession(final String username, final String password, final SessionHandler sessionHandler) {
        if (isAdminSession(sessionHandler)) {
            return true;
        }

        if (checkAdminUIAccessByPasswordOrCurrentUser(username, password)) {
            sessionHandler.set(ADMIN_SESSION_KEY, ADMIN);
            return true;
        }

        return false;
    }

    /**
     * Check the supplied username and password, and set a session flag if valid.
     *
     * @param username       the username of the sysadmin user77 to authenticate as
     * @param password       the password of the sysadmin user to authenticate as
     * @param sessionHandler a {@link SessionHandler} for mutating the user's current session
     * @return true if the supplied username/password are not null and they match a sysadmin user; or
     *         true if the supplied username/password are null, and the currently logged in user is a sysadmin; or
     *         false otherwise.
     */
    public boolean checkSysadminUIAccessBySessionOrPasswordAndActivateSysadminSession(final String username, final String password, final SessionHandler sessionHandler) {
        if (isSysadminSession(sessionHandler)) {
            return true;
        }

        if (checkSysadminUIAccessByPasswordOrCurrentUser(username, password)) {
            sessionHandler.set(ADMIN_SESSION_KEY, SYSADMIN);
            return true;
        }

        return false;
    }

    /**
     * @param username the username of the admin user to authenticate as, can be null
     * @param password the password of the admin user to authenticate as, can be null
     * @return true if the supplied username/password are not null and they match an admin user; or
     *         true if the supplied username/password are null, and the currently logged in user is an admin; or
     *         false otherwise.
     */
    public boolean checkAdminUIAccessByPasswordOrCurrentUser(final String username, final String password) {
        if (username != null & password != null) {
            return userManager.authenticate(username, password) && userManager.isAdmin(new UserKey(username));
        } else {
            return isCurrentUserAdmin();
        }
    }

    /**
     * @param username the username of the admin user to authenticate as, can be null
     * @param password the password of the admin user to authenticate as, can be null
     * @return true if the supplied username/password are not null and they match an admin user; or
     *         true if the supplied username/password are null, and the currently logged in user is an admin; or
     *         false otherwise.
     */
    public boolean checkSysadminUIAccessByPasswordOrCurrentUser(final String username, final String password) {
        if (username != null & password != null) {
            return userManager.authenticate(username, password) && userManager.isSystemAdmin(new UserKey(username));
        } else {
            return isCurrentUserSysadmin();
        }
    }

    public boolean checkAdminUIAccessBySessionOrCurrentUser(final HttpServletRequest request) {
        UserKey userKey = userManager.getRemoteUserKey();
        return isAdminSession(request) || isAdmin(userKey);
    }

    public boolean checkSysadminUIAccessBySessionOrCurrentUser(final HttpServletRequest request) {
        UserKey userKey = userManager.getRemoteUserKey();
        return isSysadminSession(request) || isSysadmin(userKey);
    }

    public boolean isCurrentUserAdmin() {
        return isAdmin(userManager.getRemoteUserKey());
    }

    public boolean isCurrentUserSysadmin() {
        return isSysadmin(userManager.getRemoteUserKey());
    }

    private boolean isAdmin(final UserKey userKey) {
        return userKey != null && (userManager.isAdmin(userKey) || userManager.isSystemAdmin(userKey));
    }

    private boolean isSysadmin(final UserKey userKey) {
        return userKey != null && userManager.isSystemAdmin(userKey);
    }

    private boolean isAdminSession(HttpServletRequest request) {
        return isAdminSession(new ServletSessionHandler(request));
    }

    private boolean isAdminSession(SessionHandler sessionHandler) {
        return ADMIN.equals(sessionHandler.get(ADMIN_SESSION_KEY)) || SYSADMIN.equals(sessionHandler.get(ADMIN_SESSION_KEY));
    }

    private boolean isSysadminSession(HttpServletRequest request) {
        return isSysadminSession(new ServletSessionHandler(request));
    }

    private boolean isSysadminSession(SessionHandler sessionHandler) {
        return SYSADMIN.equals(sessionHandler.get(ADMIN_SESSION_KEY));
    }

    public interface SessionHandler {
        void set(String key, Object value);

        Object get(String key);
    }
}
