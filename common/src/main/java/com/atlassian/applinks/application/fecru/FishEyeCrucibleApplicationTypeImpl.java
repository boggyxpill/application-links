package com.atlassian.applinks.application.fecru;

import com.atlassian.applinks.api.ApplicationTypeVisitor;
import com.atlassian.applinks.api.application.fecru.FishEyeCrucibleApplicationType;
import com.atlassian.applinks.application.BuiltinApplinksType;
import com.atlassian.applinks.application.HiResIconizedIdentifiableType;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since 3.0
 */
public class FishEyeCrucibleApplicationTypeImpl extends HiResIconizedIdentifiableType
        implements FishEyeCrucibleApplicationType, NonAppLinksApplicationType, BuiltinApplinksType {
    static final TypeId TYPE_ID = new TypeId("fecru");

    public FishEyeCrucibleApplicationTypeImpl(AppLinkPluginUtil pluginUtil, WebResourceUrlProvider webResourceUrlProvider) {
        super(pluginUtil, webResourceUrlProvider);
    }

    @Nonnull
    public TypeId getId() {
        return TYPE_ID;
    }

    @Nonnull
    public String getI18nKey() {
        return "applinks.fecru";
    }

    @Nullable
    @Override
    public <T> T accept(@Nonnull ApplicationTypeVisitor<T> visitor) {
        return visitor.visit(this);
    }
}