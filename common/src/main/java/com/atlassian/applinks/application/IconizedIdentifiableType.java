package com.atlassian.applinks.application;

import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.spi.application.IdentifiableType;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Base class that generates a local icon URI based on the {@link com.atlassian.applinks.spi.application.TypeId}
 * name of the sub class.
 *
 * @since 3.1
 */
public abstract class IconizedIdentifiableType implements IdentifiableType {
    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

    protected final WebResourceUrlProvider webResourceUrlProvider;
    protected final AppLinkPluginUtil pluginUtil;

    public IconizedIdentifiableType(final AppLinkPluginUtil pluginUtil,
                                    final WebResourceUrlProvider webResourceUrlProvider) {
        this.pluginUtil = pluginUtil;
        this.webResourceUrlProvider = webResourceUrlProvider;
    }

    @Nullable
    public URI getIconUrl() {
        try {
            return new URI(webResourceUrlProvider.getStaticPluginResourceUrl(pluginUtil.getPluginKey() +
                    ":applinks-images", "images", UrlMode.ABSOLUTE) + "/types/16" + getIconKey() + ".png");
        } catch (URISyntaxException e) {
            LOG.warn("Unable to find the icon for this application type.", e);
            return null;
        }
    }

    @Override
    public String toString() {
        return getId().toString();
    }

    @Nonnull
    protected String getIconKey() {
        return getId().get();
    }
}
