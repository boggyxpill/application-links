package com.atlassian.applinks.application.generic;

import com.atlassian.applinks.api.ApplicationTypeVisitor;
import com.atlassian.applinks.api.application.generic.GenericApplicationType;
import com.atlassian.applinks.application.BuiltinApplinksType;
import com.atlassian.applinks.application.HiResIconizedIdentifiableType;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * The generic application type supports all "out-of-the-box" authentication types that UAL ships with.
 * This application type can be used to authenticate to an application for which UAL does not have a specific
 * application type, but the application supports one or more of UAL's authentication providers.
 *
 * @since 3.0
 */
public class GenericApplicationTypeImpl extends HiResIconizedIdentifiableType
        implements GenericApplicationType, NonAppLinksApplicationType, BuiltinApplinksType {
    static final TypeId TYPE_ID = new TypeId("generic");

    public GenericApplicationTypeImpl(AppLinkPluginUtil pluginUtil, WebResourceUrlProvider webResourceUrlProvider) {
        super(pluginUtil, webResourceUrlProvider);
    }

    @Nonnull
    public TypeId getId() {
        return TYPE_ID;
    }

    @Nonnull
    public String getI18nKey() {
        return "applinks.generic";
    }

    @Nullable
    @Override
    public <T> T accept(@Nonnull ApplicationTypeVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
