package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nonnull;

/**
 * @since 5.1
 */
public interface DetailedErrors {
    @Nonnull
    Iterable<DetailedError> getErrors();

    boolean hasErrors();
}
