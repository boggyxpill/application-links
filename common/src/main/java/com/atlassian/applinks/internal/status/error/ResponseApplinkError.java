package com.atlassian.applinks.internal.status.error;

import javax.annotation.Nullable;

/**
 * {@link ApplinkError} that was triggered by an invalid or unexpected response during the process of retrieving of
 * Applink status. In addition to the standard information this error contains information about the HTTP response that
 * has caused the error.
 *
 * @see ApplinkErrorType#UNEXPECTED_RESPONSE
 * @see ApplinkErrorType#UNEXPECTED_RESPONSE_STATUS
 * @since 5.0
 */
public interface ResponseApplinkError extends ApplinkError {
    /**
     * @return HTTP status code of the response that triggered this error
     */
    int getStatusCode();

    /**
     * Body of the response, represented as a list of lines. May have been subjected to truncating for performance
     * reasons.
     *
     * @return response body, or {@code null} if there was no body
     */
    @Nullable
    String getBody();

    /**
     * @return media type extracted from the {@code Content-Type} header of the response, or {@code null} if there was
     * no body
     */
    @Nullable
    String getContentType();
}
