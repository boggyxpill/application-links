package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since 5.1
 */
public interface DetailedError {
    @Nullable
    String getContext();

    @Nonnull
    String getSummary();

    @Nullable
    String getDetails();
}
