package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Base exception for all service exceptions in Applinks.
 *
 * @since 4.3
 */
public abstract class ServiceException extends Exception {
    // _Both_ constructors should be provided in subclasses for ServiceExceptionFactory to work
    // NOTE: if you're extending this class:
    // - you may want to update mappings in ServiceExceptionHttpMapper
    // - add DEFAULT_MESSAGE field in your subclass with a valid i18n key (see ServiceExceptionFactory)

    protected ServiceException(@Nullable String message) {
        super(message);
    }

    protected ServiceException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
