package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Raised when OAuth config cannot be updated because the remote consumer information is not available.
 *
 * @since 4.3
 */
public class ConsumerInformationUnavailableException extends EntityUpdateException {
    public static final String DEFAULT_MESSAGE = "applinks.service.error.entity.oauth.consumernotavailable";

    public ConsumerInformationUnavailableException(@Nullable String message) {
        super(message);
    }

    public ConsumerInformationUnavailableException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
