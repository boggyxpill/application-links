package com.atlassian.applinks.internal.common.net;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

/**
 * @since 5.0
 */
public final class HttpUtils {

    private HttpUtils() {
        // do not instantiate
    }

    /**
     * @param statusCode status code to convert
     * @return string representation of the status code incl. reason phrase where possible, e.g. "400: Bad request"
     */
    @Nonnull
    public static String toStatusString(int statusCode) {
        Response.Status status = Response.Status.fromStatusCode(statusCode);
        return status != null ? statusCode + ": " + status.getReasonPhrase() : Integer.toString(statusCode);
    }
}
