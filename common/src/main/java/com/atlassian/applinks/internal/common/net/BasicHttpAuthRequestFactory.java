package com.atlassian.applinks.internal.common.net;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * A {@link RequestFactory} decorator that applies Basic HTTP Authentication to
 * the {@link Request} objects it creates.
 *
 * @since 3.0
 */
public class BasicHttpAuthRequestFactory<T extends Request<?, ?>> implements RequestFactory<T> {
    private final RequestFactory<T> requestFactory;
    private final String username;
    private final String password;

    public BasicHttpAuthRequestFactory(final RequestFactory<T> requestFactory,
                                       final String username,
                                       final String password) {
        this.password = password;
        this.requestFactory = requestFactory;
        this.username = username;
    }

    public T createRequest(final Request.MethodType methodType, final String url) {
        final URI uri;
        try {
            uri = new URI(url);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("invalid url '" + url + "'", e);
        }

        final T request = requestFactory.createRequest(methodType, url);
        request.addBasicAuthentication(uri.getHost(), username, password);
        return request;
    }

    public boolean supportsHeader() {
        return requestFactory.supportsHeader();
    }
}
