package com.atlassian.applinks.internal.rest.model;

/**
 * Marker interface for REST entities that represent a specific domain object of type {@code T}, but only for
 * serialization (marshalling) purposes.
 * <p>
 * REST entities implementing this interface are required provide a "copy" constructor accepting an instance of
 * {@code T} to turn {@code T} into its REST representation for serialization.
 * </p>
 *
 * @param <T> type parameter of the domain object
 * @since 5.0
 */
public interface ReadOnlyRestRepresentation<T> {
}
