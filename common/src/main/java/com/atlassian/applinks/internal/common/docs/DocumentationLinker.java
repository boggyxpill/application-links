package com.atlassian.applinks.internal.common.docs;

import java.net.URI;
import java.util.Map;
import javax.annotation.Nonnull;

/**
 * Provides URIs for UAL documentation.
 *
 * @since 3.0
 */
public interface DocumentationLinker {

    /**
     * @param pageKey a key matching a <strong>help page</strong> property defined in ual-help-paths.properties
     * @return a URI targeting the specified help page in the host application's documentation space
     */
    @Nonnull
    URI getLink(String pageKey);

    /**
     * @param pageKey    a key matching a <strong>help page</strong> property defined in ual-help-paths.properties
     * @param sectionKey a confluence anchor name on the page to be linked to
     * @return a URI targeting the specified help page in the host application's documentation space
     */
    @Nonnull
    URI getLink(String pageKey, String sectionKey);

    /**
     * @return baseURL for applinks documentation for this version
     * (e.g https://confluence.atlassian.com/display/APPLINKS-043/)
     */
    @Nonnull
    URI getDocumentationBaseUrl();

    /*
     * @return a map of all of the documentation page keys to their corresponding relative URLs in the documentation
     * space
     */
    @Nonnull
    Map<String, String> getAllLinkMappings();
}
