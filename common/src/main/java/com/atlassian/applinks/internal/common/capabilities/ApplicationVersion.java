package com.atlassian.applinks.internal.common.capabilities;

import com.google.common.collect.ComparisonChain;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
public class ApplicationVersion implements Comparable<ApplicationVersion> {
    private static final Pattern VERSION_PATTERN = Pattern.compile("(\\d+)(\\.\\d+)?(\\.\\d+)?(.*)");

    private final String versionString;
    private final int major;
    private final int minor;
    private final int bugfix;
    private final String suffix;

    private ApplicationVersion(String versionString, int major, int minor, int bugfix, String suffix) {
        this.versionString = versionString;
        this.major = major;
        this.minor = minor;
        this.bugfix = bugfix;
        this.suffix = suffix;
    }

    @Nonnull
    public static ApplicationVersion parse(@Nonnull String versionString) throws IllegalArgumentException {
        requireNonNull(versionString, "versionString");

        versionString = versionString.trim();
        Matcher matcher = VERSION_PATTERN.matcher(versionString);
        if (matcher.matches() && matcher.groupCount() == 4) {
            return new ApplicationVersion(versionString,
                    parseVersionInt(matcher, 1),
                    parseVersionInt(matcher, 2),
                    parseVersionInt(matcher, 3),
                    matcher.group(matcher.groupCount())); // last group will always be suffix, even if empty string
        } else {
            throw new IllegalArgumentException("Invalid version: " + versionString);
        }
    }

    public int getMajor() {
        return major;
    }

    public int getMinor() {
        return minor;
    }

    public int getBugfix() {
        return bugfix;
    }

    @Nonnull
    public String getSuffix() {
        return suffix;
    }

    /**
     * @return full version string, may include suffixes signifying milestones, release candidates etc.
     */
    @Nonnull
    public String getVersionString() {
        return versionString;
    }

    @Override
    public String toString() {
        return getVersionString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ApplicationVersion that = (ApplicationVersion) o;
        return Objects.equals(major, that.major) &&
                Objects.equals(minor, that.minor) &&
                Objects.equals(bugfix, that.bugfix) &&
                Objects.equals(suffix, that.suffix);
    }

    @Override
    public int hashCode() {
        return Objects.hash(major, minor, bugfix, suffix);
    }

    @Override
    public int compareTo(@Nonnull ApplicationVersion that) {
        return ComparisonChain.start()
                .compare(major, that.major)
                .compare(minor, that.minor)
                .compare(bugfix, that.bugfix)
                .compare(suffix, that.suffix)
                .result();
    }

    private static int parseVersionInt(Matcher matcher, int group) {
        String value = matcher.group(group);
        if (value != null) {
            return Integer.parseInt(StringUtils.remove(value, '.'));
        } else {
            return 0;
        }

    }
}
