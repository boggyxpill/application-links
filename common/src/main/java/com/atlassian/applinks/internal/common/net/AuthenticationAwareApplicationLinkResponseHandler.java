package com.atlassian.applinks.internal.common.net;

import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since 4.3
 */
public interface AuthenticationAwareApplicationLinkResponseHandler<R> extends ApplicationLinkResponseHandler<R> {
    /**
     * <p>
     * Triggered when a call to a remote server failed because the caller does
     * not have an established session with the server and will need to
     * authorize requests first by visiting the authorization URL provided by
     * {@link com.atlassian.applinks.api.AuthorisationURIGenerator}.
     * </p>
     * <p>
     * In this callback method the user still gets access to the full response
     * the server returned.
     * </p>
     * <p>
     * Note that for any request, only one callback method will ever be
     * invoked. Either this one, or {@link #handle(com.atlassian.sal.api.net.Response)}.
     * </p>
     * <p>
     * If your implementation of this callback method produces a result object,
     * you can parameterise the handler accordingly and return your result. If
     * you are not returning anything, you can parameterise with {@link java.lang.Void}
     * and return {@code null}.
     *
     * @param response      a response object. Never null.
     * @param problem       a actual problem that caused the rejections\
     * @param problemAdvice further details about the problem that caused the rejection
     * @return the result produced by this handler.
     * @throws com.atlassian.sal.api.net.ResponseException can be thrown by
     *                                                     when there was a problem processing the response.
     */
    @Nonnull
    R credentialsRequired(@Nonnull Response response, @Nullable String problem, @Nullable String problemAdvice)
            throws ResponseException;

    /**
     * Triggered when a call to a remote server failed because the caller provided invalid authentication details.
     *
     * @param response      a response object. Never null.
     * @param problem       a actual problem that caused the rejection
     * @param problemAdvice further details about the problem that caused the rejection
     * @return the result produced by this handler.
     * @throws com.atlassian.sal.api.net.ResponseException for unexpected errors
     */
    @Nonnull
    R authenticationFailed(@Nonnull Response response, @Nullable String problem, @Nullable String problemAdvice)
            throws ResponseException;
}
