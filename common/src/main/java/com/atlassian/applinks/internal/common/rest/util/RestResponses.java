package com.atlassian.applinks.internal.common.rest.util;

import com.atlassian.applinks.internal.rest.model.RestError;
import com.atlassian.applinks.internal.rest.model.RestErrors;

import java.net.URI;
import java.util.Arrays;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * @since 5.0
 */
public final class RestResponses {
    private RestResponses() {
        throw new AssertionError("Do not instantiate " + RestResponses.class.getSimpleName());
    }

    @Nonnull
    public static Response error(@Nonnull Status status, @Nonnull RestError... errors) {
        return Response.status(status).entity(new RestErrors(status, Arrays.asList(errors))).build();
    }

    @Nonnull
    public static Response error(@Nonnull Status status, @Nonnull String errorMessage) {
        return error(status, new RestError(errorMessage));
    }

    @Nonnull
    public static Response error(@Nonnull Status status, @Nullable String context, @Nonnull String errorMessage) {
        return error(status, new RestError(context, errorMessage, null));
    }

    @Nonnull
    public static Response badRequest(@Nullable String context, @Nonnull String errorMessage) {
        return error(Status.BAD_REQUEST, context, errorMessage);
    }

    @Nonnull
    public static Response badRequest(@Nonnull String errorMessage) {
        return error(Status.BAD_REQUEST, errorMessage);
    }

    @Nonnull
    public static Response noContent() {
        return Response.noContent().build();
    }

    @Nonnull
    public static Response notFound(@Nullable String context, @Nonnull String errorMessage) {
        return error(Status.NOT_FOUND, context, errorMessage);
    }

    @Nonnull
    public static Response notFound(@Nonnull String errorMessage) {
        return notFound(null, errorMessage);
    }

    /**
     * {@code 201 Created} response applicable only for {@code PUT} requests to a URI that matches the {@code location}
     * header of the newly created resource.
     *
     * @param entity response entity
     * @return {@code Created} response with {@code entity} and {@code location} header set to the currently requested
     * URI
     */
    @Nonnull
    public static Response created(@Nonnull Object entity) {
        return Response.created(URI.create("")).entity(entity).build();
    }
}
