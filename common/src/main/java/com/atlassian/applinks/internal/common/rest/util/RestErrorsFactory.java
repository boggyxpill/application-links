package com.atlassian.applinks.internal.common.rest.util;

import com.atlassian.applinks.internal.common.exception.DetailedError;
import com.atlassian.applinks.internal.common.exception.DetailedErrors;
import com.atlassian.applinks.internal.rest.model.RestError;
import com.atlassian.applinks.internal.rest.model.RestErrors;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.1
 */
public final class RestErrorsFactory {
    private RestErrorsFactory() {
        // do not instantiate
    }

    @Nonnull
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public static RestErrors fromException(@Nonnull Response.Status status, @Nonnull Exception exception) {
        requireNonNull(status, "status");
        requireNonNull(exception, "exception");

        if (exception instanceof DetailedErrors) {
            return new RestErrors(status, DetailedErrors.class.cast(exception));
        } else if (exception instanceof DetailedError) {
            return new RestErrors(status, new RestError(DetailedError.class.cast(exception)));
        } else {
            return new RestErrors(status, exception);
        }
    }
}
