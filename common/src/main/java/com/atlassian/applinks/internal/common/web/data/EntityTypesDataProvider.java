package com.atlassian.applinks.internal.common.web.data;

import com.atlassian.applinks.api.EntityType;
import com.atlassian.applinks.internal.common.json.JacksonJsonableMarshaller;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import static com.atlassian.applinks.internal.common.application.ApplicationTypes.resolveEntityTypeId;

/**
 * Injects entity types i18n (singular and plural) into the page for client-side usage.
 *
 * @see <a href="https://developer.atlassian.com/docs/advanced-topics/adding-data-providers-to-your-plugin">
 * Data provider documentation</a>
 * @since 5.2
 */
public class EntityTypesDataProvider implements WebResourceDataProvider {
    private final I18nResolver i18nResolver;
    private final TypeAccessor typeAccessor;

    public EntityTypesDataProvider(I18nResolver i18nResolver, TypeAccessor typeAccessor) {
        this.i18nResolver = i18nResolver;
        this.typeAccessor = typeAccessor;
    }

    @Override
    public Jsonable get() {
        return JacksonJsonableMarshaller.INSTANCE.marshal(getAllTypes());
    }

    private BaseRestEntity getAllTypes() {
        BaseRestEntity.Builder singularTypes = new BaseRestEntity.Builder();
        BaseRestEntity.Builder pluralTypes = new BaseRestEntity.Builder();
        for (EntityType entityType : typeAccessor.getEnabledEntityTypes()) {
            String id = resolveEntityTypeId(entityType);
            singularTypes.add(id, i18nResolver.getText(entityType.getI18nKey()));
            pluralTypes.add(id, i18nResolver.getText(entityType.getPluralizedI18nKey()));
        }
        return new BaseRestEntity.Builder()
                .add("singular", singularTypes.build())
                .add("plural", pluralTypes.build())
                .build();
    }
}
