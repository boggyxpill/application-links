package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * @since 5.0
 */
public class InvalidValueException extends InvalidArgumentException {
    public static final String DEFAULT_MESSAGE = "applinks.service.error.invalidvalue";

    public InvalidValueException(@Nullable String message) {
        super(message);
    }

    public InvalidValueException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
