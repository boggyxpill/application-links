/**
 * Contains REST map entities for V1/V2 application link authentication objects
 *
 * @since 5.0
 */
package com.atlassian.applinks.internal.rest.model.auth.compatibility;