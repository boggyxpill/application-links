package com.atlassian.applinks.internal.common.capabilities;

import com.atlassian.applinks.internal.status.error.ApplinkError;

import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Represents remote capabilities of an applinked application. For any applink, either the capabilities are available,
 * in which case {@link #getApplicationVersion() application version}, {@link #getApplinksVersion() Applinks version}
 * will be non-{@code null}, or not due to any number of
 * {@link com.atlassian.applinks.internal.status.error.ApplinkErrorType errors}, in which case error details will be
 * available via {@link #getError()}. {@link #getCapabilities()} is always non-{@code null}, but may be empty in case
 * of error or if the connected application does not support any capabilities.
 * <p>
 * If remote versions were previously retrieved, but the latest attempt to retrieve fresh remote capabilities was not
 * successful, both the <i>latest successfully retrieved</i> versions, as well as the latest {@link #getError() error}
 * will be available.
 * </p>
 * <p>
 * For non-Atlassian applications, this object will always be in error status:
 * {@link com.atlassian.applinks.internal.status.error.ApplinkErrorType#NON_ATLASSIAN}. Otherwise the most commonly
 * any errors will be caused by
 * {@link com.atlassian.applinks.internal.status.error.ApplinkErrorCategory#NETWORK_ERROR network connectivity}.
 * </p>
 *
 * @since 5.0
 */
public interface RemoteApplicationCapabilities {
    @Nullable
    ApplicationVersion getApplicationVersion();

    @Nullable
    ApplicationVersion getApplinksVersion();

    @Nonnull
    Set<ApplinksCapabilities> getCapabilities();

    @Nullable
    ApplinkError getError();

    boolean hasError();
}
