package com.atlassian.applinks.internal.rest.model;

import com.atlassian.applinks.internal.common.exception.DetailedError;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

/**
 * JSON representation of an application error.
 *
 * @since 4.3
 */
public class RestError extends BaseRestEntity implements ReadOnlyRestRepresentation<DetailedError> {
    public static final String CONTEXT = "context";
    public static final String SUMMARY = "summary";
    public static final String DETAILS = "details";

    public RestError(@Nonnull String summary) {
        this(null, summary, null);
    }

    public RestError(@Nullable String context, @Nonnull String summary, @Nullable Object details) {
        requireNonNull(summary, "summary");
        put(SUMMARY, summary);
        putIfNotNull(CONTEXT, context);
        putIfNotNull(DETAILS, details);
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public RestError(@Nonnull Exception javaError) {
        this(null, requireNonNull(javaError, "javaError").getLocalizedMessage(), javaError.toString());
    }

    public RestError(@Nonnull DetailedError error) {
        this(error.getContext(), error.getSummary(), splitLines(error));
    }

    private static Iterable<String> splitLines(@Nonnull DetailedError error) {
        return error.getDetails() != null ?
                ImmutableList.copyOf(Splitter.on(System.lineSeparator()).split(error.getDetails())) : null;
    }
}
