package com.atlassian.applinks.internal.common.exception;

import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Raised when business constraints are not met.
 * <p>
 * NOTE: this class is not compatible with {@code ServiceExceptionFactory}, use {@link ValidationExceptionBuilder}
 * instead.
 * </p>
 *
 * @since 5.1
 */
public class ValidationException extends InvalidArgumentException implements DetailedErrors {
    private final Object origin;
    private final List<DetailedError> errors;

    public ValidationException(@Nonnull Object origin, @Nonnull Iterable<DetailedError> errors,
                               @Nullable String message) {
        this(origin, errors, message, null);
    }

    public ValidationException(@Nonnull Object origin, @Nonnull Iterable<DetailedError> errors,
                               @Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
        this.origin = requireNonNull(origin, "origin");
        this.errors = ImmutableList.copyOf(errors);
    }

    @Nonnull
    public Object getOrigin() {
        return origin;
    }

    @Nonnull
    @Override
    public Iterable<DetailedError> getErrors() {
        return errors;
    }

    @Override
    public boolean hasErrors() {
        return !errors.isEmpty();
    }
}
