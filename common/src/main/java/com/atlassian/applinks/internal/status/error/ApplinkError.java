package com.atlassian.applinks.internal.status.error;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Represents a specific Application Link error. This includes {@link #getType() the type of error}, as well as
 * (optionally) human-readable {@link #getDetails() details of the problem}.
 *
 * @since 4.3
 */
public interface ApplinkError {
    /**
     * @return specific type of this error
     */
    @Nonnull
    ApplinkErrorType getType();

    /**
     * @return optional low-level error details, e.g. details of the runtime exception that initiated the error
     */
    @Nullable
    String getDetails();

    /**
     * Accept a visitor. Depending on the actual error, the {@code visitor} will gain access to further details about
     * this error via one of its {@code visit} methods.
     *
     * @param visitor visitor to accept
     */
    @Nullable
    <T> T accept(@Nonnull ApplinkErrorVisitor<T> visitor);
}
