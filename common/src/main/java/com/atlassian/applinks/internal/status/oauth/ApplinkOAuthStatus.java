package com.atlassian.applinks.internal.status.oauth;

import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.google.common.base.Objects;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDefaultOAuthConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDisabledConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createOAuthWithImpersonationConfig;
import static java.util.Objects.requireNonNull;

/**
 * Represents OAuth configuration status for an Application Link. There are separate OAuth configurations for
 * incoming and outgoing authentication of any given applink.
 *
 * @since 4.3
 */
public final class ApplinkOAuthStatus {
    /**
     * "Default" {@code ApplinkOAuthStatus} object with both incoming and outgoing config set to default
     *
     * @see OAuthConfig#createDefaultOAuthConfig()
     */
    public static final ApplinkOAuthStatus DEFAULT = new ApplinkOAuthStatus(
            createDefaultOAuthConfig(),
            createDefaultOAuthConfig()
    );

    /**
     * {@code ApplinkOAuthStatus} object with both incoming and outgoing config set to "Impersonation".
     *
     * @see OAuthConfig#createOAuthWithImpersonationConfig()
     */
    public static final ApplinkOAuthStatus IMPERSONATION = new ApplinkOAuthStatus(
            createOAuthWithImpersonationConfig(),
            createOAuthWithImpersonationConfig()
    );

    /**
     * "Null" {@code ApplinkOAuthStatus} object with both incoming and outgoing config disabled
     */
    public static final ApplinkOAuthStatus OFF = new ApplinkOAuthStatus(createDisabledConfig(), createDisabledConfig());

    private final OAuthConfig incoming;
    private final OAuthConfig outgoing;

    public ApplinkOAuthStatus(@Nonnull OAuthConfig incoming, @Nonnull OAuthConfig outgoing) {
        this.incoming = requireNonNull(incoming, "incoming");
        this.outgoing = requireNonNull(outgoing, "outgoing");
    }

    @Nonnull
    public OAuthConfig getIncoming() {
        return incoming;
    }

    @Nonnull
    public OAuthConfig getOutgoing() {
        return outgoing;
    }

    /**
     * Determine if this incoming is the same as other's outgoing and this outgoing equals other's incoming.
     *
     * @param other the other status
     * @return true if and only if this incoming equals other's outgoing and this outgoing equals other's incoming.
     */
    public boolean matches(@Nonnull final ApplinkOAuthStatus other) {
        requireNonNull(other, "other");
        return incoming.equals(other.getOutgoing()) && outgoing.equals(other.getIncoming());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ApplinkOAuthStatus status = (ApplinkOAuthStatus) o;
        return Objects.equal(incoming, status.incoming) &&
                Objects.equal(outgoing, status.outgoing);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(incoming, outgoing);
    }
}
