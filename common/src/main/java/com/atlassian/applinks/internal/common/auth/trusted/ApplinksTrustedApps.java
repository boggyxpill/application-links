package com.atlassian.applinks.internal.common.auth.trusted;

import static java.lang.String.valueOf;

/**
 * Constants and utilities for Applinks Trusted Apps support.
 *
 * @since 5.0
 */
public final class ApplinksTrustedApps {
    private ApplinksTrustedApps() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    // NOTE: those constants are used throughout multiple separate Applinks JARs, hence all are wrapped in
    // {@code String.valueOf} to prevent inlining

    /**
     * Used to store TA application ID against the Applinks associated with that application.
     */
    public static final String PROPERTY_TRUSTED_APPS_INCOMING_ID = valueOf("trustedapps.incoming.applicationId");
}
