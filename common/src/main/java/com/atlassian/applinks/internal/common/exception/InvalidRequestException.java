package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Thrown when a web or service request was invalid.
 *
 * @since 5.1
 */
public class InvalidRequestException extends InvalidArgumentException {
    public static final String DEFAULT_MESSAGE = "applinks.service.error.request.invalid";

    public InvalidRequestException(@Nullable String message) {
        super(message);
    }

    public InvalidRequestException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
