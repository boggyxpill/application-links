package com.atlassian.applinks.internal.common.rest.util;

import com.atlassian.applinks.internal.rest.model.RestError;
import com.atlassian.sal.api.message.I18nResolver;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class RestEnumParser<E extends Enum<E>> {
    public static final String DEFAULT_MESSAGE_KEY = "applinks.rest.enum.notfound";

    private final I18nResolver i18nResolver;

    private final Response.Status errorStatus;
    private final String errorMessageKey;
    private final Class<E> enumType;

    public RestEnumParser(@Nonnull Class<E> enumType, @Nonnull I18nResolver i18nResolver,
                          @Nonnull String errorMessageKey, @Nullable Response.Status errorStatus) {
        this.enumType = requireNonNull(enumType, "enumType");
        this.i18nResolver = requireNonNull(i18nResolver, "i18nResolver");
        this.errorStatus = errorStatus != null ? errorStatus : Response.Status.BAD_REQUEST;
        this.errorMessageKey = requireNonNull(errorMessageKey, "errorMessageKey");
    }

    public RestEnumParser(@Nonnull Class<E> enumType,
                          @Nonnull I18nResolver i18nResolver,
                          @Nonnull String errorMessageKey) {
        this(enumType, i18nResolver, errorMessageKey, null);
    }

    public RestEnumParser(@Nonnull Class<E> enumType, @Nonnull I18nResolver i18nResolver) {
        this(enumType, i18nResolver, DEFAULT_MESSAGE_KEY);
    }

    @Nonnull
    public E parseEnumParameter(@Nonnull String name, @Nonnull String context) {
        requireNonNull(name, "value");
        try {
            return Enum.valueOf(enumType, name.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new WebApplicationException(badEnumParam(name, context));
        }
    }

    @Nonnull
    @SuppressWarnings("ConstantConditions")
    public E parseEnumParameter(@Nullable String name, @Nonnull E defaultValue, @Nonnull String context) {
        return isNotEmpty(name) ? parseEnumParameter(name, context) : defaultValue;
    }

    private Response badEnumParam(String name, String context) {
        return RestResponses.error(errorStatus,
                new RestError(context, i18nResolver.getText(errorMessageKey, name, context), null));
    }
}
