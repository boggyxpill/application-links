package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * @since 4.3
 */
public abstract class InvalidArgumentException extends ServiceException {
    protected InvalidArgumentException(@Nullable String message) {
        super(message);
    }

    protected InvalidArgumentException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
