package com.atlassian.applinks.internal.common.exception;

import com.atlassian.applinks.internal.common.i18n.I18nKey;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

/**
 * Thrown when an application ID string contains an invalid value that cannot be parsed into {@code ApplicationId}.
 *
 * @see com.atlassian.applinks.api.ApplicationId
 * @since 5.1
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
public class InvalidApplicationIdException extends InvalidValueException {
    public static final String DEFAULT_MESSAGE = "applinks.service.error.invalidvalue.applinkid";

    public InvalidApplicationIdException(@Nullable String message) {
        super(message);
    }

    public InvalidApplicationIdException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }

    @Nonnull
    public static I18nKey invalidIdI18nKey(@Nullable String invalidIdValue) {
        Optional<String> invalidIdVal = Optional.ofNullable(invalidIdValue);
        return I18nKey.newI18nKey(DEFAULT_MESSAGE, invalidIdVal.orElse(""));
    }
}
