package com.atlassian.applinks.core;

import com.atlassian.applinks.core.util.RendererContextBuilder;
import com.atlassian.applinks.core.util.WebResources;
import com.atlassian.applinks.ui.validators.CallbackParameterValidator;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

public class RedirectController {
    private static final Logger log = LoggerFactory.getLogger(RedirectController.class);
    public static final String REDIRECT_WARNING_TEMPLATE = "com/atlassian/applinks/ui/auth/invalidRedirectUrl.vm";
    private final CallbackParameterValidator callbackParameterValidator;
    private final TemplateRenderer templateRenderer;
    private final WebResourceManager webResourceManager;

    @Autowired
    public RedirectController(final CallbackParameterValidator callbackParameterValidator, final TemplateRenderer templateRenderer, final WebResourceManager webResourceManager) {
        this.callbackParameterValidator = callbackParameterValidator;
        this.templateRenderer = templateRenderer;
        this.webResourceManager = webResourceManager;
    }

    public void redirectOrPrintRedirectionWarning(final HttpServletResponse response, String redirectUrl) throws IOException {
        final boolean callbackUrlValid = callbackParameterValidator.isCallbackUrlValid(redirectUrl);

        if (callbackUrlValid) {
            response.sendRedirect(redirectUrl);
        } else {
            log.warn("Prevented redirect to an invalid url: {}", redirectUrl);
            printRedirectWarningMessage(response);
        }
    }

    private void printRedirectWarningMessage(final HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        final Map<String, Object> context = buildMessageContext();
        templateRenderer.render(REDIRECT_WARNING_TEMPLATE, context, response.getWriter());
    }

    private Map<String, Object> buildMessageContext() {
        final RendererContextBuilder builder = new RendererContextBuilder();
        webResourceManager.requireResource("com.atlassian.applinks.applinks-plugin:oauth-dance");
        final StringWriter stringWriter = new StringWriter();
        webResourceManager.includeResources(stringWriter, UrlMode.RELATIVE);
        final WebResources webResources = new WebResources();
        webResources.setIncludedResources(stringWriter.getBuffer().toString());
        builder.put("webResources", webResources);
        return builder.build();
    }

}
    