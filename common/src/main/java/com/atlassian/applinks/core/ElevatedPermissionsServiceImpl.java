package com.atlassian.applinks.core;

import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.common.permission.Unrestricted;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.concurrent.Callable;

import static java.util.Objects.requireNonNull;


/**
 * Allows to elevate permissions for blocks of code that need it (only when using {@code PermissionValidationService}).
 * NOTE: Previously it was a strictly internal component. But that did not allow to perform a lot of internal operations
 * (such as getting the applink status) from outside of the plugin.
 *
 * @since 5.3
 */
@Unrestricted("The goal of this component is to allow anonymous code to execute with elevated permissions")
public class ElevatedPermissionsServiceImpl implements ElevatedPermissionsService {
    private final ThreadLocal<PermissionLevel> permissionLevelsContext = new ThreadLocal<>();

    @Nullable
    @Override
    public <T> T executeAs(@Nonnull PermissionLevel level, @Nonnull Callable<T> closure) throws Exception {
        requireNonNull(level, "level");
        requireNonNull(closure, "closure");

        permissionLevelsContext.set(level);
        try {
            return closure.call();
        } finally {
            permissionLevelsContext.remove();
        }
    }

    @Override
    public boolean isElevatedTo(@Nonnull PermissionLevel level) {
        requireNonNull(level, "level");
        PermissionLevel current = permissionLevelsContext.get();
        return current != null && current.ordinal() >= level.ordinal();
    }
}
