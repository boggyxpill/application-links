package com.atlassian.applinks.core;

import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.EntityType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.util.TypeAccessor;

public interface InternalTypeAccessor extends TypeAccessor {
    ApplicationType loadApplicationType(String typeClassName);

    EntityType loadEntityType(String typeClassName);

    Iterable<? extends EntityType> getEntityTypesForApplicationType(TypeId typeId);
}
