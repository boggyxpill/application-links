package com.atlassian.applinks.core.util;

import com.atlassian.applinks.internal.common.net.Uris;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import javax.annotation.Nonnull;

import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.StringUtils.stripEnd;

public final class URIUtil {
    // Hide implicit public constructor
    private URIUtil() {}

    /**
     * @param base  base
     * @param paths paths
     * @return concatenated URI
     * @deprecated use {@link Uris#concatenate(String, String...)} instead
     */
    @Deprecated
    public static String concatenate(final String base, final String... paths) {
        return stripEnd(base, "/") +
                removeRedundantSlashes("/" + join(paths, "/"));
    }

    /**
     * @param base  base
     * @param paths paths
     * @return concatenated URI
     * @throws URISyntaxException if the resulting URI is invalid
     * @deprecated use {@link Uris#concatenate(URI, String...)} instead
     */
    @Deprecated
    public static URI concatenate(final URI base, final String... paths) throws URISyntaxException {
        return new URI(concatenate(base.toASCIIString(), paths));
    }

    @SuppressWarnings("squid:S1075")
    public static URI concatenate(final URI base, final URI... paths) {
        try {
            final String[] pathStrings = Iterables.toArray(
                    Lists.transform(Lists.newArrayList(paths), URI::toASCIIString),
                    String.class);
            return new URI(stripEnd(base.toASCIIString(), "/") +
                    Uris.removeRedundantSlashes("/" + join(pathStrings, "/")));
        } catch (URISyntaxException e) {
            throw new URIUtilException("Failed to concatenate URIs", e);
        }
    }

    /**
     * Equivalent to {@code URLEncoder.encode(string, "UTF-8");}
     *
     * @deprecated use {@link Uris#utf8Encode(String)} instead
     */
    @Deprecated
    public static String utf8Encode(final String string) {
        return Uris.utf8Encode(string);
    }

    public static String utf8Decode(final String string) {
        try {
            return URLDecoder.decode(string, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new URIUtilException("UTF-8 not installed!?", e);
        }
    }

    /**
     * Equivalent to {@code URLEncoder.encode(uri.toASCIIString(), "UTF-8")}
     *
     * @deprecated use {@link Uris#utf8Encode(URI)} instead
     */
    @Deprecated
    public static String utf8Encode(final URI uri) {
        return Uris.utf8Encode(uri);
    }

    /**
     * @param uri URI string
     * @return new URI
     * @deprecated use {@link URI#create(String)} instead
     */
    @Deprecated
    @Nonnull
    public static URI uncheckedToUri(final String uri) {
        try {
            return new URI(uri);
        } catch (URISyntaxException e) {
            throw new URIUtilException(String.format("Failed to convert %s to URI (%s)", uri, e.getReason()), e);
        }
    }

    /**
     * ONLY to be used if the caller is <strong>certain</strong> that the base and path will form a valid URI when concatenated.
     */
    public static URI uncheckedConcatenateAndToUri(final String base, final String... paths) {
        String uri = Uris.concatenate(base, paths);
        try {
            return new URI(uri);
        } catch (URISyntaxException e) {
            throw new URIUtilException(String.format("Failed to convert %s to URI (%s)", uri, e.getReason()), e);
        }
    }

    /**
     * ONLY to be used if the caller is <strong>certain</strong> that the base and path will form a valid URI when
     * concatenated.
     *
     * @param base  base
     * @param paths paths
     * @return concatenated URI
     * @deprecated use {@link Uris#uncheckedConcatenate(URI, String...)} instead
     */
    @Deprecated
    public static URI uncheckedConcatenate(final URI base, final String... paths) {
        try {
            return concatenate(base, paths);
        } catch (URISyntaxException e) {
            throw new URIUtilException(String.format("Failed to concatenate %s to form URI (%s)", base, e.getReason()), e);
        }
    }

    /**
     * ONLY to be used if the caller is <strong>certain</strong> that the supplied String is a valid URI.
     */
    public static URI uncheckedCreate(final String uri) {
        try {
            return new URI(uri);
        } catch (URISyntaxException e) {
            throw new URIUtilException(String.format("%s is not a valid URI (%s)", uri, e.getReason()), e);
        }
    }

    /**
     * <p>
     * Reduces sequences of more than one consecutive forward slash ("/") to a
     * single slash (see: https://studio.atlassian.com/browse/PLUG-597).
     * </p>
     *
     * @param path any string, including {@code null} (e.g. {@code "foo//bar"})
     * @return the input string, with all sequences of more than one
     * consecutive slash removed (e.g. {@code "foo/bar"})
     * @deprecated use {@link Uris#removeRedundantSlashes(String)} instead
     */
    @Deprecated
    public static String removeRedundantSlashes(final String path) {
        return Uris.removeRedundantSlashes(path);
    }

    public static URI copyOf(final URI uri) {
        if (uri == null) {
            return null;
        }

        try {
            return new URI(uri.toASCIIString());
        } catch (URISyntaxException e) {
            //this should never happen, but is there a better way to copy URIs?
            throw new URIUtilException("Failed to copy URI: " + uri.toASCIIString());
        }
    }

    public static class URIUtilException extends RuntimeException {
        public URIUtilException(String message) {
            super(message);
        }

        public URIUtilException(String message, Exception e) {
            super(message, e);
        }
    }
}
