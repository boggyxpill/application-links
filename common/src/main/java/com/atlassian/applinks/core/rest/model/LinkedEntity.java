package com.atlassian.applinks.core.rest.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.atlassian.plugins.rest.common.Link;

/**
 * Base class for entities. Provides links.
 */
@XmlSeeAlso({ApplicationLinkEntity.class, AuthenticationProviderEntity.class,
        ConsumerEntity.class
})
public class LinkedEntity {
    @XmlElement(name = "link")
    private List<Link> links;

    @SuppressWarnings("unused")
    public LinkedEntity() {

    }

    protected LinkedEntity(final List<Link> links) {
        if (links != null) {
            for (Link link : links) {
                this.addLink(link);
            }
        }
    }

    public LinkedEntity(final Link... links) {
        if (links != null) {
            for (Link link : links) {
                this.addLink(link);
            }
        }
    }

    public void addLink(Link link) {
        if (links == null) {
            links = new ArrayList<Link>();
        }
        links.add(link);
    }

    protected List<Link> getLinks() {
        return links;
    }

}

