package com.atlassian.applinks.core;

import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.common.permission.Unrestricted;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.concurrent.Callable;

/**
 * Allows to elevate permissions for blocks of code that need it (only when using {@code PermissionValidationService}).
 * NOTE: Previously it was a strictly internal component. It should be used very carefully and only if elevating
 * permissions will not result in security vulnerabilities.
 * This component has been exposed as a part of the external API to allow execution of some operations
 * (such as getting the applink status) from outside of the plugin.
 *
 * @since 5.3
 */
@Unrestricted("The goal of this component is to allow the code with non-authenticated context to be executed with elevated permissions")
public interface ElevatedPermissionsService {
    /**
     * Execute {@code closure} with the given permission {@code level}. This propagates any exceptions thrown by the
     * {@code closure}
     *
     * @param level   permission level
     * @param closure code to execute
     * @param <T>     type of return value
     * @return the return from {@code closure}
     * @throws Exception any exception from the callable
     */
    @Nullable
    <T> T executeAs(@Nonnull PermissionLevel level, @Nonnull Callable<T> closure) throws Exception;

    /**
     * Allows to check if the permission is elevated to the certain level
     *
     * @param level permission level
     * @return true if the permission is elevated to the level
     */
    boolean isElevatedTo(@Nonnull PermissionLevel level);
}
