package com.atlassian.applinks.core.manifest;

import com.atlassian.annotations.Internal;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.core.rest.ManifestResource;
import com.atlassian.applinks.core.rest.context.CurrentContext;
import com.atlassian.applinks.core.rest.model.ManifestEntity;
import com.atlassian.applinks.core.rest.util.RestUtil;
import com.atlassian.applinks.core.util.Holder;
import com.atlassian.applinks.internal.common.event.ManifestDownloadFailedEvent;
import com.atlassian.applinks.internal.common.event.ManifestDownloadedEvent;
import com.atlassian.applinks.internal.common.net.ResponseContentException;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.util.ChainingClassLoader;
import com.atlassian.plugin.util.ClassLoaderUtils;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.lang3.ObjectUtils;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import static com.atlassian.applinks.internal.common.net.ResponsePreconditions.checkStatusOk;

/**
 * Non-public component that is used to download manifests for AppLinks peers.
 *
 * @since 3.0
 */
public class AppLinksManifestDownloader {
    private static final Logger LOG = LoggerFactory.getLogger(AppLinksManifestDownloader.class);

    private static final String CACHE_KEY = "ManifestRequestCache";
    private static final int CONNECTION_TIMEOUT = 10000; // 10 seconds

    private final RequestFactory requestFactory;
    private final EventPublisher eventPublisher;
    private final TypeAccessor typeAccessor;
    private final RestUrlBuilder restUrlBuilder;

    @Autowired
    public AppLinksManifestDownloader(RequestFactory requestFactory, EventPublisher eventPublisher,
                                      TypeAccessor typeAccessor, RestUrlBuilder restUrlBuilder) {
        this.requestFactory = requestFactory;
        this.eventPublisher = eventPublisher;
        this.typeAccessor = typeAccessor;
        this.restUrlBuilder = restUrlBuilder;
    }

    /**
     * Downloads the manifest. Upon successfully downloading the manifest (or failing to do so) this method will raise
     * an appropriate event.
     *
     * @param url the <strong>baseurl</strong> of the remote application
     *            instance from which to retrieve a manifest.
     * @return never {@code null}. If the remote app does not seem to be an AppLinks
     * app (possibly due to a 404), {@link ManifestNotFoundException} is thrown
     * @throws ManifestNotFoundException when no manifest could be obtained.
     * @see ManifestDownloadedEvent
     * @see ManifestDownloadFailedEvent
     */
    @Nonnull
    public Manifest download(final URI url) throws ManifestNotFoundException {
        return downloadInternal(url, true);
    }

    /**
     * Same as {@link #download(URI)} but won't raise events. Only use for components that listen to and rely on
     * manifest download events but also may want to download the manifest directly at times. <i>Most likely</i> you
     * will want to use {@link #download(URI)} instead.
     *
     * @param url the <strong>baseurl</strong> of the remote application instance from which to retrieve a manifest.
     * @return never {@code null}. If the remote app does not seem to be an AppLinks
     * app (possibly due to a 404), {@link ManifestNotFoundException} is thrown
     * @throws ManifestNotFoundException when no manifest could be obtained.
     */
    @Internal
    @Nonnull
    public Manifest downloadNoEvent(final URI url) throws ManifestNotFoundException {
        return downloadInternal(url, false);
    }

    private Manifest downloadInternal(URI url, boolean raiseEvents) throws ManifestNotFoundException {
        final LoadingCache<URI, DownloadResult> resultMap = getDownloadCache(raiseEvents);
        return resultMap != null ?
                resultMap.getUnchecked(url).get() :
                doDownload(url, raiseEvents);
    }

    @SuppressWarnings("unchecked")
    private LoadingCache<URI, DownloadResult> getDownloadCache(final boolean raiseEvents) {
        final HttpServletRequest request = CurrentContext.getHttpServletRequest();
        if (request != null) {
            LoadingCache<URI, DownloadResult> cache = (LoadingCache<URI, DownloadResult>) request.getAttribute(CACHE_KEY);
            if (cache == null) {
                if ((cache = (LoadingCache<URI, DownloadResult>) request.getAttribute(CACHE_KEY)) == null) {
                    request.setAttribute(CACHE_KEY, cache = CacheBuilder.newBuilder().build(
                            new CacheLoader<URI, DownloadResult>() {
                                @Override
                                public DownloadResult load(@Nonnull final URI uri) throws Exception {
                                    return new DownloadResult() {
                                        Manifest manifest = null;
                                        ManifestNotFoundException exception = null;

                                        {
                                            try {
                                                manifest = doDownload(uri, raiseEvents);
                                            } catch (ManifestNotFoundException e) {
                                                exception = e;
                                            }
                                        }

                                        public Manifest get() throws ManifestNotFoundException {
                                            if (manifest == null) {
                                                LOG.debug("Throwing cached ManifestNotFoundException for: " + uri.toString());
                                                throw exception;
                                            } else {
                                                LOG.debug("Returning cached manifest for: " + uri.toString());
                                                return manifest;
                                            }
                                        }
                                    };
                                }
                            }));
                }
            }
            return cache;
        }
        return null;
    }

    private Manifest doDownload(final URI url, boolean raiseEvent) throws ManifestNotFoundException {
        final Holder<Manifest> manifestHolder = new Holder<>();
        final Holder<Throwable> exception = new Holder<>();

        final ClassLoader currentContextClassloader = Thread.currentThread().getContextClassLoader();

        ClassLoader[] classLoaders = Stream.of(
                ManifestEntity.class.getClassLoader(),
                ClassLoaderUtils.class.getClassLoader(),
                ClassLoader.getSystemClassLoader())
                .filter(Objects::nonNull)
                .toArray(ClassLoader[]::new);

        final ChainingClassLoader chainingClassLoader = new ChainingClassLoader(classLoaders);
        try {
            Thread.currentThread().setContextClassLoader(chainingClassLoader); // APL-833
            requestFactory.createRequest(Request.MethodType.GET, appLinksManifestUrl(url))
                    .setConnectionTimeout(CONNECTION_TIMEOUT)
                    .setSoTimeout(CONNECTION_TIMEOUT)
                    .setFollowRedirects(false)
                    .execute(new ResponseHandler<Response>() {
                        public void handle(Response response) throws ResponseException {
                            // this means the response indicates a redirection.
                            if (response.getStatusCode() >= 300 && response.getStatusCode() < 400) {
                                final String location = response.getHeader("Location");
                                if (location == null) {
                                    throw new ResponseException("manifest not found");
                                }

                                LOG.info("Manifest request got redirected to '" + location + "'.");
                                exception.set(new ManifestGotRedirectedException("manifest got redirected", location));
                            } else {
                                checkStatusOk(response);
                                try {
                                    manifestHolder.set(asManifest(response.getEntity(ManifestEntity.class)));
                                } catch (Exception ex) {
                                    exception.set(new ResponseContentException(response, ex));
                                }
                            }
                        }
                    });
        } catch (ResponseException re) {
            exception.set((Throwable) ObjectUtils.defaultIfNull(re.getCause(), re));
        } finally {
            Thread.currentThread().setContextClassLoader(currentContextClassloader);
        }
        if (manifestHolder.get() == null) {
            if (raiseEvent) {
                eventPublisher.publish(new ManifestDownloadFailedEvent(url, exception.get()));
            }
            if (exception.get() == null) {
                throw new ManifestNotFoundException(url.toString());
            } else {
                throw new ManifestNotFoundException(url.toString(), exception.get());
            }
        }
        if (raiseEvent) {
            eventPublisher.publish(new ManifestDownloadedEvent(manifestHolder.get()));
        }
        return manifestHolder.get();

    }

    @VisibleForTesting
    protected String appLinksManifestUrl(final URI baseUri) {
        return restUrlBuilder.getUrlFor(RestUtil.getBaseRestUri(baseUri), ManifestResource.class).getManifest()
                .toString();
    }

    private Manifest asManifest(final ManifestEntity manifest) {
        return new Manifest() {
            public ApplicationId getId() {
                return manifest.getId();
            }

            public String getName() {
                return manifest.getName();
            }

            public TypeId getTypeId() {
                return manifest.getTypeId();
            }

            public Long getBuildNumber() {
                return manifest.getBuildNumber();
            }

            public String getVersion() {
                return manifest.getVersion();
            }

            public URI getUrl() {
                return manifest.getUrl();
            }

            @Override
            public URI getIconUrl() {
                return manifest.getIconUrl();
            }

            @Override
            public URI getIconUri() {
                return manifest.getIconUri();
            }


            public Version getAppLinksVersion() {
                return manifest.getApplinksVersion();
            }

            public Boolean hasPublicSignup() {
                return manifest.hasPublicSignup();
            }

            public Set<Class<? extends AuthenticationProvider>> getInboundAuthenticationTypes() {
                return loadTypes(manifest.getInboundAuthenticationTypes());
            }

            public Set<Class<? extends AuthenticationProvider>> getOutboundAuthenticationTypes() {
                return loadTypes(manifest.getOutboundAuthenticationTypes());
            }

            private Set<Class<? extends AuthenticationProvider>> loadTypes(final Set<String> classNames) {
                final Set<Class<? extends AuthenticationProvider>> types = new HashSet<Class<? extends AuthenticationProvider>>();
                if (classNames == null) {
                    return types;
                }

                for (final String name : classNames) {
                    final Class<? extends AuthenticationProvider> c = typeAccessor.getAuthenticationProviderClass(name);
                    if (c != null) {
                        types.add(c);
                    } else {
                        LOG.info(String.format("Authenticator %s specified by remote application %s is not " +
                                "installed locally, and will not be used.", name, getId()));
                    }
                }
                return types;
            }
        };
    }

    private interface DownloadResult {
        Manifest get() throws ManifestNotFoundException;
    }

    public static class ManifestGotRedirectedException extends Exception {
        private String newLocation;

        public ManifestGotRedirectedException(String message, String newLocation) {
            super(message);
            this.newLocation = newLocation;
        }

        public String getNewLocation() {
            return newLocation;
        }

        public String newLocationBaseUrl() {
            if (newLocation.indexOf("rest/applinks/") > 0) {
                return newLocation.substring(0, newLocation.indexOf("rest/applinks/"));
            }

            return newLocation;
        }
    }
}
