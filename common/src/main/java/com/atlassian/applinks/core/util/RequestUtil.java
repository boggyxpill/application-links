package com.atlassian.applinks.core.util;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.ui.BadRequestException;
import com.atlassian.applinks.ui.NotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import javax.servlet.http.HttpServletRequest;

public class RequestUtil {
    private static final Logger logger = LoggerFactory.getLogger(RequestUtil.class);

    private static final int HTTP_DEFAULT_PORT = 80;
    private static final int HTTPS_DEFAULT_PORT = 443;
    public static final String HTTP_SCHEME = "http";
    public static final String HTTPS_SCHEME = "https";

    /**
     * Returns the base url for a given request.
     *
     * @param request the original http request.
     * @param baseUrl the base URL to return if it fails to build the base URL from the request.
     * @return the base url of this request.
     */
    public static URI getBaseURLFromRequest(HttpServletRequest request, final URI baseUrl) {
        try {
            final StringBuilder urlBuilder = new StringBuilder();
            final String scheme = request.getScheme();
            urlBuilder.append(scheme);
            urlBuilder.append("://");
            urlBuilder.append(request.getServerName());
            final int port = request.getServerPort();
            if (!isStandardPort(scheme, port)) {
                urlBuilder.append(":");
                urlBuilder.append(port);
            }
            urlBuilder.append(request.getContextPath());
            return new URI(urlBuilder.toString());
        } catch (Exception ex) {
            return baseUrl;
        }
    }

    /**
     * Returns the default port for the specified scheme.
     *
     * @param scheme the scheme (such as http or https)
     * @return the default port, or -1 if the default port for the scheme is not known
     * @since 3.7
     */
    public static int getDefaultPort(String scheme) {
        if (scheme.equalsIgnoreCase(HTTP_SCHEME)) {
            return HTTP_DEFAULT_PORT;
        }
        if (scheme.equalsIgnoreCase(HTTPS_SCHEME)) {
            return HTTPS_DEFAULT_PORT;
        }
        return -1;
    }

    private static boolean isStandardPort(String scheme, int port) {
        if (scheme.equalsIgnoreCase(HTTP_SCHEME) && port == HTTP_DEFAULT_PORT) {
            return true;
        }
        if (scheme.equalsIgnoreCase(HTTPS_SCHEME) && port == HTTPS_DEFAULT_PORT) {
            return true;
        }
        return false;
    }

    /**
     * Extracts the {@link com.atlassian.applinks.api.ApplicationLink} from the request url of auth servlet.
     *
     * @param applicationLinkService application link service
     * @param messageFactory         message factory
     * @param request                http request
     * @return application link, never null
     * @throws NotFoundException   if the link doesn't exist
     * @throws BadRequestException if the request url is not of auth servlet
     */
    public static ApplicationLink getApplicationLink(final ApplicationLinkService applicationLinkService,
                                                     final MessageFactory messageFactory,
                                                     final HttpServletRequest request)
            throws NotFoundException, BadRequestException {
        // remove any excess slashes
        final String pathInfo = URI.create(request.getPathInfo()).normalize().toString();
        final String[] elements = StringUtils.split(pathInfo, '/');
        if (elements.length > 0) {
            final ApplicationId id = new ApplicationId(elements[0]);
            try {
                final ApplicationLink link = applicationLinkService.getApplicationLink(id);
                if (link != null) {
                    return link;
                } else {
                    final NotFoundException exception = new NotFoundException();
                    exception.setTemplate("com/atlassian/applinks/ui/auth/applink-missing.vm");
                    throw exception;
                }
            } catch (TypeNotInstalledException e) {
                logger.warn(String.format("Unable to load ApplicationLink %s due to uninstalled type definition (%s).", id.toString(), e.getType()), e);
            }
            throw new NotFoundException(messageFactory.newI18nMessage("auth.config.applink.notfound", id.toString()));
        }
        throw new BadRequestException(messageFactory.newI18nMessage("auth.config.applinkpath.missing"));
    }

    public static String getRequiredParameter(final HttpServletRequest request, final MessageFactory messageFactory, final String name)
            throws BadRequestException {
        final String value = request.getParameter(name);
        if (StringUtils.isBlank(value)) {
            throw new BadRequestException(messageFactory.newI18nMessage("auth.config.parameter.missing", name));
        } else {
            return value;
        }
    }
}
