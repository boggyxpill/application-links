package com.atlassian.applinks.core.util;

import com.atlassian.sal.api.message.I18nResolver;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

import static java.util.Objects.requireNonNull;


/**
 * @since v3.0
 */
public class MessageFactory {
    private final I18nResolver resolver;

    @Autowired
    public MessageFactory(I18nResolver resolver) {
        this.resolver = requireNonNull(resolver, "resolver");
    }

    public Message newI18nMessage(final String key, final Serializable... params) {
        requireNonNull(resolver, "resolver");
        requireNonNull(key, "key");
        requireNonNull(params, "params");
        return new Message() {
            public String toString() {
                return resolver.getText(key, params);
            }
        };
    }

    public Message newLocalizedMessage(final String message) {
        return new Message() {
            public String toString() {
                return message;
            }
        };
    }
}