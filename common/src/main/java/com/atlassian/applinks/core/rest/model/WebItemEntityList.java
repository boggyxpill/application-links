package com.atlassian.applinks.core.rest.model;

import java.util.List;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "webItems")
public class WebItemEntityList {
    @XmlElement(name = "webItem")
    private List<WebItemEntity> items;

    public WebItemEntityList() {
    }

    public WebItemEntityList(final List<WebItemEntity> items) {
        this.items = items;
    }

    @Nullable
    public List<WebItemEntity> getItems() {
        return items;
    }
}