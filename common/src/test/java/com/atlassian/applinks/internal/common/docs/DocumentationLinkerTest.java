package com.atlassian.applinks.internal.common.docs;

import com.atlassian.applinks.core.AppLinkPluginUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.Version;

import java.net.URI;
import java.net.URISyntaxException;

import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.Silent.class)
public class DocumentationLinkerTest {
    @Mock
    private AppLinkPluginUtil applinkPluginUtil;

    private DocumentationLinker documentationLinker;

    @Test
    public void testDocumentationLinkerSingleDigitMajorVersion() throws URISyntaxException {
        initDocumtentationLinker(new Version(4, 3, 1));

        assertEquals(new URI("https://confluence.atlassian.com/display/APPLINKS-043/"),
                documentationLinker.getDocumentationBaseUrl());
    }

    @Test
    public void testDocumentationLinkerDoubleDigitMajorVersion() throws URISyntaxException {
        initDocumtentationLinker(new Version(10, 3, 1));

        assertEquals(new URI("https://confluence.atlassian.com/display/APPLINKS-103/"),
                documentationLinker.getDocumentationBaseUrl());
    }

    @Test
    public void testGetHelpLink() throws URISyntaxException {
        initDefaultLinker();

        assertEquals(new URI("https://confluence.atlassian.com/display/APPLINKS-050/Application+Links+User+Guide"),
                documentationLinker.getLink("applinks.docs.administration.guide"));
    }

    @Test
    public void testGetHelpLinkWithSectionKey() throws URISyntaxException {
        initDefaultLinker();

        assertEquals(
                new URI("https://confluence.atlassian.com/display/APPLINKS-050/Application+Links+User+Guide#ApplicationLinksUserGuide-one"),
                documentationLinker.getLink("applinks.docs.administration.guide", "one"));
    }

    @Test
    public void getAllMappings() throws URISyntaxException {
        initDefaultLinker();

        assertEquals("Expected 3 links in the test help paths", 3, documentationLinker.getAllLinkMappings().size());
        assertThat(documentationLinker.getAllLinkMappings(), hasEntry("applinks.docs.test.link.one", "Test+Link+One"));
        assertThat(documentationLinker.getAllLinkMappings(), hasEntry("applinks.docs.test.link.two", "Test+Link+Two"));
    }

    private void initDefaultLinker() {
        initDocumtentationLinker(new Version(5, 0, 0));
    }

    private void initDocumtentationLinker(Version version) {
        when(applinkPluginUtil.getVersion()).thenReturn(version);
        documentationLinker = new DefaultDocumentationLinker(applinkPluginUtil,
                "com/atlassian/applinks/internal/common/docs/test-help-paths.properties");
    }
}
