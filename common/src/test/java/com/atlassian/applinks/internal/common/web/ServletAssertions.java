package com.atlassian.applinks.internal.common.web;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @since 5.1
 */
public final class ServletAssertions {
    private ServletAssertions() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    public static void assertClickjackingPrevention(@Nonnull HttpServletResponse response) {
        assertThat(response.getHeader("X-Frame-Options"), equalTo("SAMEORIGIN"));
    }
}
