package com.atlassian.applinks.internal.common.test.mock;


import com.atlassian.applinks.internal.common.exception.DefaultServiceExceptionFactory;
import com.atlassian.applinks.test.mock.MockI18nResolver;

public class SimpleServiceExceptionFactory extends DefaultServiceExceptionFactory {
    public SimpleServiceExceptionFactory() {
        super(new MockI18nResolver());
    }

    public SimpleServiceExceptionFactory(boolean appendArgs) {
        super(new MockI18nResolver(appendArgs));
    }
}
