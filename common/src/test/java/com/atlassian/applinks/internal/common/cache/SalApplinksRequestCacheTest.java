package com.atlassian.applinks.internal.common.cache;

import com.atlassian.sal.api.web.context.HttpContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SalApplinksRequestCacheTest {
    @Mock
    private HttpContext httpContext;

    @InjectMocks
    private SalApplinksRequestCache applinksRequestCache;

    private MockHttpServletRequest mockRequest = new MockHttpServletRequest();

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void getCacheUsingNullCacheName() {
        applinksRequestCache.getCache(null, String.class, String.class);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void getAsUsingNullKeyType() {
        applinksRequestCache.getCache("cache", null, String.class);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void getAsUsingNullValueType() {
        applinksRequestCache.getCache("cache", String.class, null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void getUsingNullKey() {
        getCache().get(null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void putUsingNullKey() {
        getCache().put(null, "value");
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void putUsingNullValue() {
        getCache().put("key", null);
    }

    @Test
    public void getGivenNoExistingRequestContext() {
        assertNull(getCache().get("key"));
    }

    @Test
    public void putAndGetGivenNoExistingRequestContext() {
        getCache().put("key", "value");

        assertNull(getCache().get("key"));
    }

    @Test
    public void getGivenExistingRequestContextNoCacheValue() {
        setUpExistingRequest();

        assertNull(getCache().get("key"));
    }

    @Test
    public void putAndGetGivenExistingRequest() {
        setUpExistingRequest();
        getCache().put("key", "value");

        assertEquals("value", getCache().get("key"));
    }

    @SuppressWarnings("unused")
    @Test(expected = ClassCastException.class)
    public void getAsGivenWrongValueType() {
        setUpExistingRequest();
        getCache().put("key", "value");

        Integer value = applinksRequestCache.getCache("cache", String.class, Integer.class).get("key");
    }

    private ApplinksRequestCache.Cache<String, String> getCache() {
        return applinksRequestCache.getCache("cache", String.class, String.class);
    }

    private void setUpExistingRequest() {
        when(httpContext.getRequest()).thenReturn(mockRequest);
    }
}
