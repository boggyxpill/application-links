package com.atlassian.applinks.internal.common.manifest;


import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.core.manifest.AppLinksManifestDownloader;
import com.atlassian.applinks.core.rest.model.ManifestEntity;
import com.atlassian.applinks.internal.common.event.ManifestDownloadFailedEvent;
import com.atlassian.applinks.internal.common.event.ManifestDownloadedEvent;
import com.atlassian.applinks.internal.common.net.ResponseContentException;
import com.atlassian.applinks.internal.common.net.Uris;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.applinks.test.mock.MockApplicationLinkResponse;
import com.atlassian.applinks.test.mock.TestApplinkIds;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseStatusException;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.Response.Status;
import java.net.URI;

import static com.atlassian.applinks.test.mock.MockRequestAnswer.withMockRequests;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ApplinksManifestDownloaderTest {
    private static final String TEST_URL = "http://test.com";
    private static final String TEST_MANIFEST_URL = TEST_URL + "/rest/applinks/1.0/manifest";
    private static final ApplicationId TEST_ID = TestApplinkIds.DEFAULT_ID.applicationId();

    @Mock
    private RequestFactory<?> requestFactory;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private TypeAccessor typeAccessor;
    @Mock
    private RestUrlBuilder restUrlBuilder;

    @Captor
    private ArgumentCaptor<ManifestDownloadedEvent> successfulEventCaptor;
    @Captor
    private ArgumentCaptor<ManifestDownloadFailedEvent> failedEventCaptor;

    @InjectMocks
    private MockManifestDownloader downloader;

    @Test
    public void eventForDirectDownloadSuccessful() throws ManifestNotFoundException {
        ManifestEntity manifestEntity = new ManifestEntity(testManifest());
        when(requestFactory.createRequest(Request.MethodType.GET, TEST_MANIFEST_URL))
                .thenAnswer(withMockRequests(okManifestResponse(manifestEntity)));

        downloader.download(URI.create(TEST_URL));

        verify(eventPublisher).publish(successfulEventCaptor.capture());
        assertNotNull(successfulEventCaptor.getValue());
        assertEquals(TEST_ID, successfulEventCaptor.getValue().getManifest().getId());
    }

    @Test
    public void eventForDirectDownloadFailedWithException() throws ManifestNotFoundException {
        ResponseException failure = new ResponseException("Test");
        when(requestFactory.createRequest(Request.MethodType.GET, TEST_MANIFEST_URL))
                .thenAnswer(withMockRequests(failedManifestResponse(failure)));

        downloadExpectingError();

        verify(eventPublisher).publish(failedEventCaptor.capture());
        assertNotNull(failedEventCaptor.getValue());
        assertEquals(TEST_URL, failedEventCaptor.getValue().getUri().toString());
        assertEquals(failure, failedEventCaptor.getValue().getCause());
    }

    @Test
    public void eventForDirectDownloadFailedWith404() throws ManifestNotFoundException {
        when(requestFactory.createRequest(Request.MethodType.GET, TEST_MANIFEST_URL))
                .thenAnswer(withMockRequests(failedManifestResponse404()));

        downloadExpectingError();

        verify(eventPublisher).publish(failedEventCaptor.capture());
        assertNotNull(failedEventCaptor.getValue());
        assertThat(failedEventCaptor.getValue().getCause(), Matchers.instanceOf(ResponseStatusException.class));
        assertEquals(TEST_URL, failedEventCaptor.getValue().getUri().toString());
    }

    @Test
    public void eventForDirectDownloadFailedWithUnexpectedResponseBody() throws ManifestNotFoundException {
        ManifestEntity manifestEntity = new ManifestEntity(testManifest());
        when(requestFactory.createRequest(Request.MethodType.GET, TEST_MANIFEST_URL))
                .thenAnswer(withMockRequests(okManifestResponse(manifestEntity)
                        .setResponseBody("Surprise!")
                        .setReadException(new ResponseException("Boom"))));

        downloadExpectingError();

        verify(eventPublisher).publish(failedEventCaptor.capture());
        assertNotNull(failedEventCaptor.getValue());
        assertThat(failedEventCaptor.getValue().getCause(), Matchers.instanceOf(ResponseContentException.class));
        assertEquals(TEST_URL, failedEventCaptor.getValue().getUri().toString());
    }

    @Test
    public void eventNotRaisedForDirectDownloadSuccessful() throws ManifestNotFoundException {
        ManifestEntity manifestEntity = new ManifestEntity(testManifest());
        when(requestFactory.createRequest(Request.MethodType.GET, TEST_MANIFEST_URL))
                .thenAnswer(withMockRequests(okManifestResponse(manifestEntity)));

        downloader.downloadNoEvent(URI.create(TEST_URL));

        verifyZeroInteractions(eventPublisher);
    }

    @Test
    public void eventNotRaisedForDirectDownloadFailedWithException() throws ManifestNotFoundException {
        ResponseException failure = new ResponseException("Test");
        when(requestFactory.createRequest(Request.MethodType.GET, TEST_MANIFEST_URL))
                .thenAnswer(withMockRequests(failedManifestResponse(failure)));

        downloadNoEventExpectingError();

        verifyZeroInteractions(eventPublisher);
    }

    private void downloadExpectingError() throws ManifestNotFoundException {
        try {
            downloader.download(URI.create(TEST_URL));
            fail("Expected manifest exception");
        } catch (ManifestNotFoundException ignored) {
        }
    }

    private void downloadNoEventExpectingError() throws ManifestNotFoundException {
        try {
            downloader.downloadNoEvent(URI.create(TEST_URL));
            fail("Expected manifest exception");
        } catch (ManifestNotFoundException ignored) {
        }
    }

    private static MockApplicationLinkResponse okManifestResponse(ManifestEntity manifest) {
        return new MockApplicationLinkResponse()
                .setStatus(Status.OK)
                .setEntity(manifest);
    }

    private static MockApplicationLinkResponse failedManifestResponse(ResponseException e) {
        return new MockApplicationLinkResponse().setResponseException(e);
    }

    private static MockApplicationLinkResponse failedManifestResponse404() {
        return new MockApplicationLinkResponse().setStatus(Status.NOT_FOUND);
    }

    private static Manifest testManifest() {
        Manifest manifest = mock(Manifest.class);
        when(manifest.getId()).thenReturn(TEST_ID);
        return manifest;
    }


    public static class MockManifestDownloader extends AppLinksManifestDownloader {

        public MockManifestDownloader(RequestFactory requestFactory, EventPublisher eventPublisher,
                                      TypeAccessor typeAccessor) {
            super(requestFactory, eventPublisher, typeAccessor, null);
        }

        @Override
        protected String appLinksManifestUrl(URI baseUri) {
            return Uris.uncheckedConcatenate(baseUri, "rest", "applinks", "1.0", "manifest").toString();
        }
    }
}
