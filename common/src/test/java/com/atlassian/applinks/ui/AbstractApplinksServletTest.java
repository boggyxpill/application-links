package com.atlassian.applinks.ui;

import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AbstractApplinksServletTest {
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private MessageFactory messageFactory;
    @Mock
    private TemplateRenderer templateRenderer;
    @Mock
    private WebResourceManager webResourceManager;
    @Mock
    private DocumentationLinker documentationLinker;
    @Mock
    private LoginUriProvider loginUriProvider;
    @Mock
    private InternalHostApplication internalHostApplication;
    @Mock
    private AdminUIAuthenticator adminUIAuthenticator;
    @Mock
    private XsrfTokenAccessor xsrfTokenAccessor;
    @Mock
    private XsrfTokenValidator xsrfTokenValidator;
    @InjectMocks
    private ConcreteApplinksServletForTest concreteApplinksServletForTest;

    @Test
    public void applinksServletShouldValidateXsrfTokenForPostRequests() throws IOException, ServletException {
        HttpServletRequest req = createHttp11RequestMock("POST");
        PrintWriter writer = mock(PrintWriter.class);
        HttpServletResponse resp = response(writer);
        when(xsrfTokenValidator.validateFormEncodedToken(eq(req))).thenReturn(false);
        concreteApplinksServletForTest.service(req, resp);

        verify(xsrfTokenValidator).validateFormEncodedToken(req);
        verify(templateRenderer).render(eq(AbstractApplinksServlet.XSRF_AUTH_TEMPLATE), any(), eq(writer));
    }

    @Test
    public void applinksServletShouldValidateXsrfTokenForPutRequests() throws IOException, ServletException {
        HttpServletRequest req = createHttp11RequestMock("PUT");
        PrintWriter writer = mock(PrintWriter.class);
        HttpServletResponse resp = response(writer);
        when(xsrfTokenValidator.validateFormEncodedToken(eq(req))).thenReturn(false);
        concreteApplinksServletForTest.service(req, resp);

        verify(xsrfTokenValidator).validateFormEncodedToken(req);
        verify(templateRenderer).render(eq(AbstractApplinksServlet.XSRF_AUTH_TEMPLATE), any(), eq(writer));
    }

    @Test
    public void applinksServletShouldValidateXsrfTokenForDeleteRequests() throws IOException, ServletException {
        HttpServletRequest req = createHttp11RequestMock("DELETE");
        PrintWriter writer = mock(PrintWriter.class);
        HttpServletResponse resp = response(writer);
        when(xsrfTokenValidator.validateFormEncodedToken(eq(req))).thenReturn(false);
        concreteApplinksServletForTest.service(req, resp);

        verify(xsrfTokenValidator).validateFormEncodedToken(req);
        verify(templateRenderer).render(eq(AbstractApplinksServlet.XSRF_AUTH_TEMPLATE), any(), eq(writer));
    }

    @Test
    public void applinksServletShouldNotValidateXsrfTokenForGetRequests() throws IOException, ServletException {
        HttpServletRequest req = createHttp11RequestMock("GET");
        PrintWriter writer = mock(PrintWriter.class);
        HttpServletResponse resp = response(writer);
        concreteApplinksServletForTest.service(req, resp);

        verify(xsrfTokenValidator, never()).validateFormEncodedToken(req);
        verify(templateRenderer, never()).render(eq(AbstractApplinksServlet.XSRF_AUTH_TEMPLATE), any(), eq(writer));
    }

    private static HttpServletRequest createHttp11RequestMock(String method) {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getProtocol()).thenReturn("HTTP/1.1");
        when(req.getMethod()).thenReturn(method);
        return req;
    }

    private static HttpServletResponse response(PrintWriter writer) throws IOException {
        HttpServletResponse resp = mock(HttpServletResponse.class);
        when(resp.getWriter()).thenReturn(writer);
        return resp;
    }

    private static class ConcreteApplinksServletForTest extends AbstractApplinksServlet {

        public ConcreteApplinksServletForTest(I18nResolver i18nResolver, MessageFactory messageFactory, 
                                              TemplateRenderer templateRenderer, WebResourceManager webResourceManager, 
                                              DocumentationLinker documentationLinker, LoginUriProvider loginUriProvider, 
                                              InternalHostApplication internalHostApplication, AdminUIAuthenticator adminUIAuthenticator,
                                              XsrfTokenAccessor xsrfTokenAccessor, XsrfTokenValidator xsrfTokenValidator) {
            super(i18nResolver, messageFactory, templateRenderer, webResourceManager, documentationLinker, loginUriProvider, 
                    internalHostApplication, adminUIAuthenticator, xsrfTokenAccessor, xsrfTokenValidator);
        }
    }

}