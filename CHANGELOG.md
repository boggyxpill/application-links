# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [8.1.0] - Unreleased

- [BSP-2396] Upgrade vulnerable dependencies. 

## [8.0.0] - 2020-06-22

### Updates

- [MNSTR-4178] AUI9 support. Changed icon-close class to aui-close-button. Changed "a" buttons to "button" buttons (mainly in dialogs + feedback collector).
Switch "aria-hidden" to "hidden". Add "aria-label" to inline dialogs.

## [7.2.0] - 2020-05-15

### Updates
- [QUICK-1549] Added a new field to `ApplicationLinkDetailsChangedEvent` that contains the `ApplicationLink` before it was updated.
The `ApplicationLink` object in `ApplicationLinkDeletedEvent` now contains all details of the `ApplicationLink` that was deleted.

## [7.1.5] - 2020-04-27

### Updated
- [BSP-1221] Converted all plugins to be transformerless.

## [7.1.4] - 2020-03-04

### Fixed
- [BSP-1095] Removed space before URL in title help link href.

## [7.1.3] - 2020-01-20

### Updates
- [BSP-832] Add CSRF Protection on Applinks endpoint.

## [7.0.2] - 2020-01-20

### Updates
- [BSP-832] Add CSRF Protection on Applinks endpoint.

## [6.0.12] - 2020-01-20

### Updates
- [BSP-832] Add CSRF Protection on Applinks endpoint.

## [5.4.21] - 2020-01-20

### Updates
- [BSP-832] Add CSRF Protection on Applinks endpoint.

## [5.4.20] - 2019-12-19

### Updates
- [RAID-1640] Enabled WebSudo protection for EditAppLinksServlet.

## [7.0.1] - 2019-12-19

### Updates
- [RAID-1640] Enabled WebSudo protection for EditAppLinksServlet.


## [7.1.2] - 2019-12-17

### Updates
- [RAID-1640] Enabled WebSudo protection for EditAppLinksServlet.

## [7.1.1] - 2019-11-14

### Updates
- [MNSTR-3176] merged applinks-7.0.x (AUI8) branch with current master (6.1.x - some BBS react rework).
- [BSP-661] Updated Platform POM to 5.0.15.

## [7.0.0] - 2019-11-14

### Updates
- [MNSTR-2473]: Support for AUI 8 and newer, changes in wrm dependencies, html markup, js.
Cleaned some old aui versions from pom.xml file.
Cleaned amps configuration so refapps/jira/confluence no longer uses amps to supply auiplugin in some version by <pluginArtifacts>.
Lower auiplugin versions didn't work for Jira/Confluence (they did work for refapp) - but for me it wasn't testing with original products just product with auiplugin in some version.
If you want to test product compatibility with some auiplugin version then use product version that is shipped with that auiplugin version 
eg. jira 8.3.0 for auiplugin 8.0.3, jira 8.4.0 for auiplugin 8.5.0 etc.
- [BSP-661] Updated Platform POM to 5.0.15.

### Fixed
- Some buggy tests (pageobjects)

[MNSTR-2473]: https://bulldog.internal.atlassian.com/browse/MNSTR-2473

## [6.1.0] - 2019-08-22

### Added
 - Entity links frontend support and web-items for Bitbucket Server.
 - PageObjects for the renewed entity links page.
 - Webpack for a select amount of pages (entitylink pages).
 - AtlasKit dependencies for entity links

### Changed
 - Renewed the front-end for entity links
 - Updated the webdriver tests
 
### Fixed
 - Creating an entitylink in the service will return correctly if the new entity link is primary or not.
 
## [6.0.11] - 2019-11-14

### Fixed
- APL-1383 Upgrade jackson-databind dependency to address CVE-2019-16942, CVE-2019-16943, CVE-2019-17531

## [6.0.10] - 2019-09-10

### Fixed
- BSP-483 Bump Jackson to fix CVE-2019-12814

## [6.0.9] - 2019-07-31

### Fixed
- APL-1378 Allow GET requests for `ListEntityLinksAction`

## [6.0.8] - 2019-07-23

### Fixed
- CONFSRVDEV-11346 Remove duplicate outdated icon

## [6.0.7] - 2019-06-27

### Fixed
- BSP-362 Add XSRF protection for applinks

## [6.0.6] - 2019-05-26

### Fixed
- APL-1375 Bump jackson-databind to fix CVE-2018-14721 vulnerability
- BSP-430 Fix NPE when applinks concurrently added and deleted

## [6.0.5] - 2019-04-26

### Fixed
- BSP-269 Fix compatibility with atlassian-util-concurrent provided by pre Platform 5 Confluence
- BSP-354 Verify user is admin when listing application links in project

## [6.0.4] - 2019-02-06

### Fixed
- APL-1370 Fix reflected XSS vulnerability

## [6.0.3] - 2019-01-18

### Fixed
- BSP-269 Fix integration tests

## [6.0.2] - 2019-01-15

### Fixed
- MNSTR-2469 Add missing web-resource dependency upon `com.atlassian.auiplugin:aui-template`
- BBSDEV-17096 Update applinks for changed Bitbucket internal resources

## [6.0.1] - 2018-12-12

### Fixed
- BSERV-11456 Ignore invalid origins for CORS checks

## [6.0.0] - 2018-12-12

### Changed
- BSP-39 Support Platform 5 and Java 11
 


