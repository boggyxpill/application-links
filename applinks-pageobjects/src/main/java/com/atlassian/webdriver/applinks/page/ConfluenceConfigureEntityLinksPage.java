package com.atlassian.webdriver.applinks.page;

import com.atlassian.applinks.api.EntityType;
import com.atlassian.applinks.api.application.confluence.ConfluenceSpaceEntityType;

/**
 * Represents the Confluence entity links configuration screen, which is backed by a Confluence action.
 *
 * @since 4.3
 */
public class ConfluenceConfigureEntityLinksPage extends ConfigureEntityLinksPage {
    public ConfluenceConfigureEntityLinksPage(Class<? extends EntityType> entityType, String key) {
        super(entityType, key);
    }

    public ConfluenceConfigureEntityLinksPage(String spaceKey) {
        super(ConfluenceSpaceEntityType.class, spaceKey);
    }

    @Override
    public String getUrl() {
        return String.format("/spaces/listentitylinks.action?key=%s&typeId=%s", key, entityType.getName());
    }
}