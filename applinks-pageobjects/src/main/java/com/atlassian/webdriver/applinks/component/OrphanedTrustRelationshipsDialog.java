package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;
import javax.inject.Inject;

public class OrphanedTrustRelationshipsDialog extends AbstractApplinkChainedComponent<ListApplicationLinkPage> {
    @Inject
    protected PageElementFinder elementFinder;
    @Inject
    protected PageBinder pageBinder;

    @ElementBy(id = "orphaned-trust-certificates-dialog")
    protected PageElement dialog;

    public OrphanedTrustRelationshipsDialog(final ListApplicationLinkPage nextPage) {
        super(nextPage);
    }

    public OrphanedTrustRelationshipsDialog() {
        super(null);
    }

    @Nonnull
    public DeleteOrphanedTrustRelationshipDialog deleteFirstEntry() {
        return deleteFirstEntryFoundInPageElement(dialog);
    }

    @Nonnull
    public TimedCondition isOpen() {
        return dialog.timed().isVisible();
    }

    private DeleteOrphanedTrustRelationshipDialog deleteFirstEntryFoundInPageElement(final PageElement container) {
        final PageElement deleteLink = container.find(By.cssSelector("a.orphaned-trust-delete"));
        if (!deleteLink.isVisible()) {
            throw new IllegalStateException("there is no delete link visible to the user");
        }
        deleteLink.click();
        return pageBinder.bind(DeleteOrphanedTrustRelationshipDialog.class);
    }

    public ListApplicationLinkPage close() {
        PageElement closeLink = dialog.find(By.cssSelector("a.applinks-cancel-link"));
        if (!closeLink.isVisible()) {
            throw new IllegalStateException("the link for closing the orphaned trust relationship dialog is not visible to the user");
        }
        closeLink.click();
        return nextPage;
    }
}
