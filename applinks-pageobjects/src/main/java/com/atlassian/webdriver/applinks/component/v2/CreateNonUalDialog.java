package com.atlassian.webdriver.applinks.component.v2;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.apache.commons.lang3.Validate;
import org.openqa.selenium.By;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Represents the AUI dialog for creating a non-UAL applink in the new workflow.
 *
 * @since v4.0.0
 */
public class CreateNonUalDialog extends AbstractCreationDialog {
    @ElementBy(id = "applinks-create-nonual-form")
    protected PageElement createForm;

    @ElementBy(id = "application-name-new")
    protected PageElement nameTextBox;

    @ElementBy(id = "application-types-new")
    protected PageElement typeSelect;

    @ElementBy(id = "serviceProvider")
    PageElement serviceProviderTextBox;

    @ElementBy(id = "consumerKey")
    PageElement consumerKeyTextBox;

    @ElementBy(id = "sharedSecret")
    PageElement sharedSecretTextBox;

    @ElementBy(id = "requestTokenUrl")
    PageElement requestTokenUrlTextBox;

    @ElementBy(id = "accessTokenUrl")
    PageElement accessTokenUrlTextBox;

    @ElementBy(id = "authorizeUrl")
    PageElement authorizeUrlTextBox;

    @ElementBy(id = "createIncoming")
    PageElement createIncomingCheckBox;

    @ElementBy(className = "createNonUalLinkDialogError")
    PageElement createError;

    @ElementBy(className = "error")
    PageElement errors;

    @Override
    public TimedQuery<Boolean> isAt() {
        return createForm.timed().isVisible();
    }

    public CreateNonUalDialog setName(String name) {
        Poller.waitUntilTrue(nameTextBox.timed().isVisible());

        nameTextBox.clear();
        nameTextBox.type(name);
        return this;
    }

    public PauseDialog clickContinueToCreateNonUalLink() {
        Poller.waitUntilTrue(continueButton.timed().isVisible());

        Validate.isTrue(!createIncomingCheckBox.isPresent() || !createIncomingCheckBox.isSelected(),
                "For incoming links use clickContinueToCreateNonUalIncomingLink instead");

        continueButton.click();
        Poller.waitUntilFalse(continueButton.timed().isVisible());
        return pageBinder.bind(PauseDialog.class);
    }

    public CreateNonUalDialog clickContinueExpectingAnError() {
        Poller.waitUntilTrue(continueButton.timed().isVisible());

        Validate.isTrue(!createIncomingCheckBox.isPresent() || !createIncomingCheckBox.isSelected(),
                "For incoming links use clickContinueToCreateNonUalIncomingLink instead");
        

        continueButton.click();
        Poller.waitUntilTrue(continueButton.timed().isVisible());
        return this;
    }

    public CreateNonUalIncomingDialog clickContinueToCreateNonUalIncomingLink() {
        Poller.waitUntilTrue(continueButton.timed().isVisible());

        Validate.isTrue(!createIncomingCheckBox.isPresent() || createIncomingCheckBox.isSelected(),
                "For outgoing only links use clickContinueToCreateNonUalLink instead");

        continueButton.click();
        return this.getCreateNonUalIncomingDialog();
    }

    public String getHeadlineError() {
        Poller.waitUntilTrue(createError.timed().isVisible());
        return createError.getText();
    }

    public String getServiceProvider() {
        return this.getFieldValue(serviceProviderTextBox);
    }

    public CreateNonUalDialog setServiceProvider(String value) {
        return this.setTextBox(serviceProviderTextBox, value);
    }

    public String getConsumerKey() {
        return this.getFieldValue(consumerKeyTextBox);
    }

    public CreateNonUalDialog setConsumerKey(String value) {
        return this.setTextBox(consumerKeyTextBox, value);
    }

    public String getSharedSecret() {
        return this.getFieldValue(sharedSecretTextBox);
    }

    public CreateNonUalDialog setSharedSecret(String value) {
        return this.setTextBox(sharedSecretTextBox, value);
    }

    public String getRequestTokenUrl() {
        return this.getFieldValue(requestTokenUrlTextBox);
    }

    public CreateNonUalDialog setRequestTokenUrl(String value) {
        return this.setTextBox(requestTokenUrlTextBox, value);
    }

    public String getAccessTokenUrl() {
        return this.getFieldValue(accessTokenUrlTextBox);
    }

    public CreateNonUalDialog setAccessTokenUrl(String value) {
        return this.setTextBox(accessTokenUrlTextBox, value);
    }

    public String getAuthorizeUrl() {
        return this.getFieldValue(authorizeUrlTextBox);
    }

    public CreateNonUalDialog setAuthorizeUrl(String value) {
        return this.setTextBox(authorizeUrlTextBox, value);
    }

    public CreateNonUalDialog checkCreateIncomingCheckBox() {
        Poller.waitUntilTrue(createIncomingCheckBox.timed().isVisible());
        if (!createIncomingCheckBox.isSelected()) {
            createIncomingCheckBox.click();
        }

        return this;
    }

    public CreateNonUalDialog setTextBox(PageElement field, String value) {
        setFieldValue(field, value);
        return this;
    }

    public Iterable<String> getVisibleErrors() {
        List<PageElement> elementErrors = this.elementFinder.findAll(By.className("error"));

        return elementErrors.stream()
                .map(PageElement::getText)
                .collect(toList());
    }

    public TimedElement getServiceProviderTextBoxTimed() {
        return this.serviceProviderTextBox.timed();
    }

    public TimedElement getConsumerKeyTextBoxTimed() {
        return this.consumerKeyTextBox.timed();
    }

    public TimedElement getSharedSecretTextBoxTimed() {
        return this.sharedSecretTextBox.timed();
    }

    public TimedElement getRequestTokenUrlTextBoxTimed() {
        return this.requestTokenUrlTextBox.timed();
    }

    public TimedElement getAccessTokenUrlTextBoxTimed() {
        return this.accessTokenUrlTextBox.timed();
    }

    public TimedElement getAuthorizeUrlTextBoxTimed() {
        return this.authorizeUrlTextBox.timed();
    }

    public TimedElement getCreateIncomingCheckBoxTimed() {
        return this.createIncomingCheckBox.timed();
    }

    public TimedElement getApplicationNameTextBoxTimed() {
        return this.nameTextBox.timed();
    }

    public TimedElement getApplicationTypeSelectTimed() {
        return this.typeSelect.timed();
    }
}
