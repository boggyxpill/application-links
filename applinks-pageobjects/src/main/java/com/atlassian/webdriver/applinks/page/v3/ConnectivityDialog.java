package com.atlassian.webdriver.applinks.page.v3;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;

public abstract class ConnectivityDialog {
    public ConnectivityDialog() {
    }

    protected abstract PageElement getDialog();

    public TimedQuery<String> getContents() {
        return getDialog().find(By.className("oauth-mismatch-dialog-contents")).timed().getText();
    }

    public TimedQuery<Boolean> isVisible() {
        return getDialog().timed().isVisible();
    }


    // check whether the dialog is visible before dismissing as it doesn't reliably open in tests
    public void dismiss() {
        if (isVisible().now()) {
            getDialog().find(By.className("aui-button")).click();
            waitUntilFalse(isVisible());
        }
    }
}
