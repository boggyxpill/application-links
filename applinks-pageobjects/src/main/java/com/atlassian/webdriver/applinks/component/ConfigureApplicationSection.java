package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;

public abstract class ConfigureApplicationSection extends AbstractAuthenticationSection {
    public static final String FRAME_ID_BASIC_ACCESS = "BasicAuthenticationProviderPluginModule";
    public static final String FRAME_ID_CORS = "corsAuthenticationProviderPluginModule";
    public static final String FRAME_ID_INCOMING = "incoming-auth";
    public static final String FRAME_ID_OAUTH = "OAuthAuthenticatorProviderPluginModule";
    public static final String FRAME_ID_OUTGOING = "outgoing-auth";
    public static final String FRAME_ID_TRUSTED_APPLICATIONS = "trustedAppsAuthenticationProviderPluginModule";

    public static final String MENU_ID_APPLICATION_DETAILS = "menu-applinkDetailsPage";
    public static final String MENU_ID_INCOMING = "menu-incoming-authentication-page";
    public static final String MENU_ID_OUTGOING = "menu-outgoing-authentication-page";

    public static final String SUBMENU_CLASS_OAUTH = "OAuthAuthenticationProvider";
    public static final String SUBMENU_CLASS_TRUSTED_APPLICATIONS = "TrustedAppsAuthenticationProvider";
    public static final String SUBMENU_TA_SELECTOR = ".auth-config-tab-link.TrustedAppsAuthenticationProvider";
    public static final String SUBMENU_OAUTH_SELECTOR = ".auth-config-tab-link.OAuthAuthenticationProvider";
    public static final String SUBMENU_BASIC_SELECTOR = ".auth-config-tab-link.BasicAuthenticationProvider";
    public static final String SUBMENU_CORS_SELECTOR = ".auth-config-tab-link.CorsAuthenticationProvider";

    public ApplicationDetailsSection openApplicationDetails() {
        topFrame();
        elementFinder.find(By.id(MENU_ID_APPLICATION_DETAILS)).click();

        return pageBinder.bind(ApplicationDetailsSection.class);
    }

    /**
     * This assumes the creation of a link which starts on the Trusted Applications tab when configuring Incoming
     * Authentication, where the tabs shown are Trusted Applications, OAuth and Basic Access (in that order).
     *
     * @return -
     */
    public BasicAccessAuthenticationSection openIncomingBasicAccess() {
        return navigateTo(MENU_ID_INCOMING, SUBMENU_BASIC_SELECTOR, FRAME_ID_INCOMING, FRAME_ID_BASIC_ACCESS, BasicAccessAuthenticationSection.class);
    }

    /**
     * This assumes the creation of a link between two applications which support the CORS authentication type, such
     * as two reference apps. It is assumed that the tabs for Trusted Applications, OAuth, Basic Access and CORS will
     * all be present, with the CORS tab presented last.
     *
     * @return -
     */
    public CorsAuthenticationSection openIncomingCors() {
        return navigateTo(MENU_ID_INCOMING, SUBMENU_CORS_SELECTOR, FRAME_ID_INCOMING, FRAME_ID_CORS, CorsAuthenticationSection.class);
    }

    /**
     * This assumes the creation of a link which <i>only</i> shows the OAuth tab. If configuring Incoming Authentication
     * starts on the Trusted Applications tab, the section returned will fail in strange ways.
     *
     * @return -
     */
    public OauthIncomingAuthenticationSection openIncomingOauth() {
        return navigateToSubFrame(MENU_ID_INCOMING, SUBMENU_CLASS_OAUTH,
                FRAME_ID_INCOMING, FRAME_ID_OAUTH, OauthIncomingAuthenticationSection.class);
    }

    /**
     * This assumes the creation of a link which <i>only</i> shows the OAuth tab with auto config servlet in the active frame.
     *
     * @return -
     */
    public OauthIncomingAuthenticationWithAutoConfigSection openIncomingOauthExpectingAutoConfig() {
        return navigateTo(MENU_ID_INCOMING, SUBMENU_OAUTH_SELECTOR, FRAME_ID_INCOMING, FRAME_ID_OAUTH, OauthIncomingAuthenticationWithAutoConfigSection.class);
    }

    /**
     * This assumes the creation of a link which starts on the Trusted Applications tab when configuring Incoming
     * Authentication. Whether the other tabs are present or not does not affect the functionality of this method.
     *
     * @return -
     */
    public TrustedApplicationAuthenticationSection openIncomingTrustedApplications() {
        return navigateTo(MENU_ID_INCOMING, FRAME_ID_INCOMING, FRAME_ID_TRUSTED_APPLICATIONS, TrustedApplicationAuthenticationSection.class);
    }

    /**
     * This assumes the creation of a link which includes the Trusted Applications tab, but not necessarily as the first, when configuring Incoming
     * Authentication. Whether the other tabs are present or not does not affect the functionality of this method.
     *
     * @return -
     */
    public TrustedApplicationAuthenticationSection openIncomingTrustedApplicationsSubFrame() {
        return navigateToSubFrame(MENU_ID_INCOMING, SUBMENU_CLASS_TRUSTED_APPLICATIONS,
                FRAME_ID_INCOMING, FRAME_ID_TRUSTED_APPLICATIONS, TrustedApplicationAuthenticationSection.class);
    }

    /**
     * This assumes the creation of a link which starts on the Trusted Applications tab when configuring Outgoing
     * Authentication, where the tabs shown are Trusted Applications, OAuth and Basic Access (in that order).
     *
     * @return -
     */
    public BasicAccessAuthenticationSection openOutgoingBasicAccess() {
        return navigateTo(MENU_ID_OUTGOING, SUBMENU_BASIC_SELECTOR, FRAME_ID_OUTGOING, FRAME_ID_BASIC_ACCESS, BasicAccessAuthenticationSection.class);
    }

    /**
     * Open the manual configuration of OAuth screen.
     */
    public OauthOutgoingAuthenticationEditModeSection openOutgoingOauth() {
        return navigateTo(MENU_ID_OUTGOING, SUBMENU_OAUTH_SELECTOR, FRAME_ID_OUTGOING, FRAME_ID_OAUTH, OauthOutgoingAuthenticationEditModeSection.class);
    }

    /**
     * This assumes the creation of a link which <i>only</i> shows the OAuth tab. If configuring Outgoing Authentication
     * starts on the Trusted Applications tab, the section returned will fail in strange ways.
     *
     * @return -
     */
    public OauthOutgoingAuthenticationWithAutoConfigSection openOutgoingOauthExpectingAutoConfig() {
        return navigateTo(MENU_ID_OUTGOING, SUBMENU_OAUTH_SELECTOR, FRAME_ID_OUTGOING, FRAME_ID_OAUTH, OauthOutgoingAuthenticationWithAutoConfigSection.class);
    }

    /**
     * This assumes the creation of a link which starts on the Trusted Applications tab when configuring Outgoing
     * Authentication. Whether the other tabs are present or not does not affect the functionality of this method.
     *
     * @return -
     */
    public TrustedApplicationAuthenticationSection openOutgoingTrustedApplications() {
        return navigateTo(MENU_ID_OUTGOING, FRAME_ID_OUTGOING, FRAME_ID_TRUSTED_APPLICATIONS, TrustedApplicationAuthenticationSection.class);

    }

    /**
     * This assumes the creation of a link which includes the Trusted Applications tab, but not necessarily as the first, when configuring Outgoing
     * Authentication. Whether the other tabs are present or not does not affect the functionality of this method.
     *
     * @return -
     */
    public TrustedApplicationAuthenticationSection openOutgoingTrustedApplicationsSubFrame() {
        return navigateToSubFrame(MENU_ID_OUTGOING, SUBMENU_CLASS_TRUSTED_APPLICATIONS,
                FRAME_ID_OUTGOING, FRAME_ID_TRUSTED_APPLICATIONS, TrustedApplicationAuthenticationSection.class);

    }

    private <T> T navigateTo(String menuId, String frame, String subframe, Class<T> sectionClass) {
        return navigateTo(menuId, null, frame, subframe, sectionClass);
    }

    private <T> T navigateTo(String menuId, String subMenuCssSelector, String frame, String subframe, Class<T> sectionClass) {
        topFrame();

        elementFinder.find(By.id(menuId)).click();
        webDriver.switchTo().frame(frame);

        // wait until the frame is fully loaded before continue.
        PageElement authContainerFooter = elementFinder.find(By.id("auth_container_footer"));
        Poller.waitUntilTrue(authContainerFooter.timed().isPresent());

        if (subMenuCssSelector != null) {
            PageElement subMenu = elementFinder.find(By.cssSelector(subMenuCssSelector));
            if (!subMenu.isPresent()) {
                throw new IllegalStateException("submenu [" + subMenuCssSelector + "] is not present as expected.");
            }
            subMenu.click();
        }

        if (!elementFinder.find(By.name(subframe)).isPresent()) {
            throw new IllegalStateException("subframe [" + subframe + "] is not present as expected.");
        }
        webDriver.switchTo().frame(subframe);

        return pageBinder.bind(sectionClass);
    }

    /**
     * Navigates to the passes subframe be click menu and submenu. While the menu is identified by an id, the
     * <strong>submenu is identified by class</strong>. This way, the method should be able to activate the proper
     * subframe, even in case of a different submenu layouts.
     */
    private <T> T navigateToSubFrame(String menuId, String subMenuClass, String frame, String subframe, Class<T> sectionClass) {
        topFrame();

        elementFinder.find(By.id(menuId)).click();
        webDriver.switchTo().frame(frame);

        elementFinder.find(By.className(subMenuClass)).click();
        webDriver.switchTo().frame(subframe);

        return pageBinder.bind(sectionClass);
    }

    /**
     * Get the value from a UI field that is expected to be present.
     */
    public String getFieldValue(PageElement field) {
        Poller.waitUntilTrue(field.timed().isVisible());
        return field.getText();
    }
}