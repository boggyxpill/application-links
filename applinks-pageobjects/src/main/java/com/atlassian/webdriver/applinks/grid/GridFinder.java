package com.atlassian.webdriver.applinks.grid;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;
import org.hamcrest.Matcher;
import org.openqa.selenium.By;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.notNullValue;

/**
 * Declarative interface for finding elements in a grid-like structure, eg a table. Generally uses a column-value match to find a row,
 * then finds an element (eg a different cell / link / button)
 * <pre>
 *     {@code
 *     PageElement root = elementFinder.find(By.id("my-table-id"))
 *     new GridFinderFactory(root).inRow("A Column Name", "My Cell Value").find(By.linkText("Delete")).click();
 *     }
 * </pre>
 * <p>&nbsp;</p>
 */
public class GridFinder<R extends GridFinderRow> {
    private final Timeouts timeouts;
    private final PageElement root;
    private final By columnNameLocator;
    private final By rowLocator;
    private final By cellLocator;
    private final GridFinderRowCreator<R> rowCreator;

    public GridFinder(Timeouts timeouts, PageElement root, By columnNameLocator, By rowLocator, By cellLocator,
                      GridFinderRowCreator<R> rowCreator) {
        this.timeouts = timeouts;
        this.root = root;
        this.columnNameLocator = columnNameLocator;
        this.rowLocator = rowLocator;
        this.cellLocator = cellLocator;
        this.rowCreator = rowCreator;
    }

    public List<R> allRows() {
        return allRowsSupplier().get();
    }

    public TimedQuery<List<R>> allRowsTimed() {
        return Queries.forSupplier(timeouts, allRowsSupplier());
    }

    public TimedQuery<List<R>> allRowsTimed(TimeoutType timeoutType) {
        return Queries.forSupplier(timeouts, allRowsSupplier(), timeoutType);
    }


    public Supplier<List<R>> allRowsSupplier() {
        return new Supplier<List<R>>() {
            public List<R> get() {
                if (!root.isPresent()) {
                    return Collections.emptyList();
                }
                List<PageElement> allRows = root.findAll(rowLocator);
                return Lists.transform(allRows, new Function<PageElement, R>() {
                    public R apply(PageElement pageElement) {
                        return rowCreator.create(GridFinder.this, pageElement);
                    }
                });
            }
        };
    }

    public List<R> inRows(int columnIndex, Matcher<String> cellMatcher) {
        List<R> rows = new LinkedList<R>();
        for (R row : allRows()) {
            List<PageElement> cells = row.allCells();
            if (cells.size() > columnIndex) {
                String cellText = cells.get(columnIndex).getText();
                if (cellMatcher.matches(cellText)) {
                    rows.add(row);
                }
            }
        }
        return rows;
    }

    public List<R> inRows(String columnName, Matcher<String> cellMatcher) {
        Integer columnIndex = findColumnIndex(columnName);
        if (null == columnIndex) {
            return new LinkedList<R>();
        }
        return inRows(columnIndex, cellMatcher);
    }


    public R inRow(int columnIndex, Matcher<String> cellMatcher) {
        List<R> rows = inRows(columnIndex, cellMatcher);
        if (rows.isEmpty()) {
            return null;
        }
        return rows.get(0);
    }

    @SuppressWarnings("unchecked")
    public TimedCondition hasRow(String columnName, Matcher<String> cellMatcher) {
        return Conditions.forMatcher(getRow(columnName, cellMatcher), (Matcher) notNullValue());
    }


    public TimedQuery<R> getRow(String columnName, Matcher<String> cellMatcher) {
        return Queries.forSupplier(timeouts, rowSupplier(columnName, cellMatcher));
    }

    @SuppressWarnings("unchecked")
    public TimedCondition hasRow(int columnIndex, Matcher<String> cellMatcher) {
        return Conditions.forMatcher(getRow(columnIndex, cellMatcher), (Matcher) notNullValue());
    }


    public TimedQuery<R> getRow(int columnIndex, Matcher<String> cellMatcher) {
        return Queries.forSupplier(timeouts, rowSupplier(columnIndex, cellMatcher));
    }

    private Supplier<R> rowSupplier(final int columnIndex, final Matcher<String> cellMatcher) {
        return new Supplier<R>() {
            public R get() {
                List<R> rows = inRows(columnIndex, cellMatcher);
                if (rows.isEmpty()) {
                    return null;
                }
                return rows.get(0);
            }
        };

    }

    public R inRow(String columnName, Matcher<String> cellMatcher) {
        return rowSupplier(columnName, cellMatcher).get();
    }

    private Supplier<R> rowSupplier(final String columnName, final Matcher<String> cellMatcher) {
        return new Supplier<R>() {
            public R get() {
                List<R> rows = inRows(columnName, cellMatcher);
                if (rows.isEmpty()) {
                    return null;
                }
                return rows.get(0);
            }
        };

    }

    protected Integer findColumnIndex(String columnName) {
        return getColumnIndices().get(columnName);
    }

    protected Map<String, Integer> getColumnIndices() {
        if (null == this.columnNameLocator) {
            throw new ColumnNameLocatorNotSpecifiedException();
        }
        Map<String, Integer> indices = new HashMap<String, Integer>();
        int i = 0;
        for (PageElement element : root.findAll(columnNameLocator)) {
            String text = element.getAttribute("id");
            if (text != null && text.length() > 0) {
                indices.put(text, i);
            }
            i += 1;
        }
        return indices;
    }

    protected By getCellLocator() {
        return cellLocator;
    }
}
