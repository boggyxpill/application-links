package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.function.Predicate;

import static com.atlassian.webdriver.applinks.util.ApplinksElements.TAG_ANCHOR;
import static java.util.Objects.requireNonNull;

/**
 * Represents a single item in the drop-down menu in the actions section of an applink in the applinks configuration
 * screen
 *
 * @since 4.3
 */
public class DropDownItem {
    private final PageElement itemContainer;

    /**
     * Predicate for checking if the item's data-action attribute matches a value
     */
    public static Predicate<DropDownItem> hasAction(@Nonnull final String action) {
        requireNonNull(action, "action");
        return item -> action.equals(item.getAction());
    }

    public DropDownItem(@Nonnull PageElement container) {
        this.itemContainer = requireNonNull(container, "container");
    }

    @Nullable
    public String getName() {
        return itemContainer.getText();
    }

    @Nullable
    public String getAction() {
        return getAttribute("data-action");
    }

    @Nullable
    public String getAttribute(@Nonnull String attributeName) {
        return itemContainer.find(By.tagName(TAG_ANCHOR)).getAttribute(attributeName);
    }

    public void click() {
        itemContainer.find(By.tagName(TAG_ANCHOR)).click();
    }
}
