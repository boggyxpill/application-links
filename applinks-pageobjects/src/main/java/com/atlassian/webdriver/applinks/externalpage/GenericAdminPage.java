package com.atlassian.webdriver.applinks.externalpage;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import static java.util.Objects.requireNonNull;

/**
 * Represents any admin page and allows for finding admin links by ID and visit the target pages, as well as finding and
 * visiting Applinks admin pages.
 *
 * @since 4.3
 */
public class GenericAdminPage implements Page {
    public static final String CONFIGURE_APPLINKS_LINK_ID = "configure-application-links";

    @Inject
    protected PageBinder pageBinder;

    @Inject
    protected PageElementFinder pageElementFinder;

    private final String url;

    public GenericAdminPage(String url) {
        this.url = url;
    }

    public GenericAdminPage() {
        this.url = "/admin";
    }

    @Override
    public String getUrl() {
        return url;
    }

    public boolean hasConfigureApplinksLink() {
        return hasLink(CONFIGURE_APPLINKS_LINK_ID);
    }

    @Nonnull
    public <P> P goToConfigureApplinks(@Nonnull Class<P> applinksAdminPageClass) {
        return followLink(CONFIGURE_APPLINKS_LINK_ID, applinksAdminPageClass);
    }

    public boolean hasLink(@Nonnull String linkId) {
        return findLink(linkId).isPresent();
    }

    @Nonnull
    public <P> P followLink(@Nonnull String linkId, @Nonnull Class<P> targetPageClass, @Nonnull Object... args) {
        requireNonNull(targetPageClass, "targetPageClass");
        requireNonNull(args, "args");

        findLink(linkId).click();
        return pageBinder.bind(targetPageClass, args);
    }

    protected PageElement findLink(String linkId) {
        return pageElementFinder.find(By.id(requireNonNull(linkId, "linkId")));
    }
}
