package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

public class OauthOutgoingAuthenticationEditModeSection extends ConfigureApplicationSection {
    @ElementBy(id = "name")
    private PageElement serviceProviderNameTextBox;

    @ElementBy(id = "consumerKey")
    private PageElement consumerKeyTextBox;

    @ElementBy(id = "sharedSecret")
    private PageElement sharedSecretTextBox;

    @ElementBy(id = "description")
    private PageElement descriptionTextBox;

    @ElementBy(id = "requestTokenUrl")
    private PageElement requestTokenUrlTextBox;

    @ElementBy(id = "accessTokenUrl")
    private PageElement accessTokenUrlTextBox;

    @ElementBy(id = "authorizeUrl")
    private PageElement authorizeUrlTextBox;

    @ElementBy(id = "save")
    PageElement saveButton;

    @ElementBy(id = "auth-oauth-action-disable")
    PageElement disableButton;

    public OauthOutgoingAuthenticationEditModeSection setServiceProviderName(String serviceProviderName) {
        serviceProviderNameTextBox.clear();
        serviceProviderNameTextBox.type(serviceProviderName);
        return this;
    }

    public OauthOutgoingAuthenticationEditModeSection setConsumerKey(String consumerKey) {
        consumerKeyTextBox.clear();
        consumerKeyTextBox.type(consumerKey);
        return this;
    }

    public OauthOutgoingAuthenticationEditModeSection setSharedSecret(String sharedSecret) {
        sharedSecretTextBox.clear();
        sharedSecretTextBox.type(sharedSecret);
        return this;
    }

    public OauthOutgoingAuthenticationEditModeSection setDescription(String description) {
        descriptionTextBox.clear();
        descriptionTextBox.type(description);
        return this;
    }

    public OauthOutgoingAuthenticationEditModeSection setRequestTokenUrl(String requestTokenUrl) {
        requestTokenUrlTextBox.clear();
        requestTokenUrlTextBox.type(requestTokenUrl);
        return this;
    }

    public OauthOutgoingAuthenticationEditModeSection setAccessTokenUrl(String accessTokenUrl) {
        accessTokenUrlTextBox.clear();
        accessTokenUrlTextBox.type(accessTokenUrl);
        return this;
    }

    public OauthOutgoingAuthenticationEditModeSection setAuthorizeUrl(String authorizeUrl) {
        authorizeUrlTextBox.clear();
        authorizeUrlTextBox.type(authorizeUrl);
        return this;
    }

    public OauthOutgoingAuthenticationViewModeSection save() {
        Poller.waitUntilTrue(saveButton.timed().isVisible());
        saveButton.click();
        return pageBinder.bind(OauthOutgoingAuthenticationViewModeSection.class);
    }

    public OauthOutgoingAuthenticationEditModeSection disable() {
        Poller.waitUntilTrue(disableButton.timed().isVisible());
        disableButton.click();
        return this;
    }

    public String getServiceProvider() {
        return this.getFieldValue(serviceProviderNameTextBox);
    }

    public String getConsumerKey() {
        return this.getFieldValue(consumerKeyTextBox);
    }

    public String getSharedSecret() {
        return this.getFieldValue(sharedSecretTextBox);
    }

    public String getRequestTokenUrl() {
        return this.getFieldValue(requestTokenUrlTextBox);
    }

    public String getAccessTokenUrl() {
        return this.getFieldValue(accessTokenUrlTextBox);
    }

    public String getAuthorizeUrl() {
        return this.getFieldValue(authorizeUrlTextBox);
    }
}