package com.atlassian.webdriver.applinks.component.v2;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.startsWith;

/**
 * Represents the dialog shown while work is happening in the v2 workflow.
 *
 * @since v4.0.0
 */
public class PauseDialog extends AbstractDialog {
    private static final String APPLINKS_PAUSE_DIALOG_ID = "applinksPauseDialog";

    @ElementBy(cssSelector = "button.applinks-cancel")
    protected PageElement closeLink;

    public TimedQuery<Boolean> isAt() {
        return getDialogContainer().timed().isPresent();
    }

    public ListApplicationLinkPage clickClose() {
        waitUntilTrue(closeLink.timed().isVisible());
        closeLink.click();
        return pageBinder.bind(ListApplicationLinkPage.class);
    }

    /**
     * handle the redirection process to a remote application.
     * Checks the browser is pointing at somewhere on the remote application before continuing
     */
    public void handleRedirectionToRemoteApplication(String remoteBaseUrl) {
        waitUntilFalse(closeLink.timed().isVisible());
        waitUntil(this.currentUrl(), startsWith(remoteBaseUrl));
    }

    /**
     * handle the redirection process to a remote application.
     * Checks the browser is pointing at somewhere on the remote application before continuing
     */
    public CreateUalDialog handleRedirectionToRemoteApplicationCreateUalDialog(String remoteBaseUrl) {
        waitUntilFalse(closeLink.timed().isVisible());
        waitUntil(this.currentUrl(), startsWith(remoteBaseUrl));
        return pageBinder.bind(CreateUalDialog.class);
    }

    public CreateUalDialog handleRedirectionToRemoteApplicationNoClose() {
        waitUntilFalse(getDialogContainer().timed().isVisible());
        return pageBinder.bind(CreateUalDialog.class);
    }

    protected PageElement getDialogContainer() {
        return elementFinder.find(By.id(APPLINKS_PAUSE_DIALOG_ID));
    }
}
