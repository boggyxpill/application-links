package com.atlassian.webdriver.applinks.page.adg.entitylinks;

import com.atlassian.applinks.api.EntityType;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

public class ConfigureEntityLinksPage implements Page {

    @Inject
    protected PageElementFinder elementFinder;
    @Inject
    protected PageBinder pageBinder;

    @ElementBy(className = "add-link")
    private PageElement addLink;

    @ElementBy(cssSelector = ".droplist > span")
    private PageElement dropdownButton;

    @ElementBy(className = "delete-link-action")
    private PageElement deleteLink;

    private final Class<? extends EntityType> entityType;
    private final String key;

    public ConfigureEntityLinksPage(Class<? extends EntityType> entityType, String key) {
        this.entityType = entityType;
        this.key = key;
    }

    public String getUrl() {
        return "/plugins/servlet/applinks/listEntityLinks/" + entityType.getName() + "/" + key;
    }

    public CreateProjectLinkDialog clickAddLink() {
        addLink.click();
        return pageBinder.bind(CreateProjectLinkDialog.class, entityType, key);
    }

    public List<EntityLinkRow> getRows() {
        return elementFinder.find(By.cssSelector(".explanation-container + div > table ")).find(By.tagName("tbody")).findAll(By.tagName("tr"))
                .stream()
                .map(EntityLinkRow::new)
                .collect(Collectors.toList());
    }

    public boolean hasNoAppLinksMessage() {
        return elementFinder.find(By.className("no-app-links-empty-state")).isPresent();
    }

    public boolean isEmptyState() {
        return elementFinder.find(By.className("no-links-empty-state")).isPresent();
    }

    public boolean hasAddLink() {
        return addLink.isPresent();
    }

    public class EntityLinkRow {

        private final PageElement pageElement;

        EntityLinkRow(PageElement pageElement) {
            this.pageElement = pageElement;
        }

        public DeleteProjectLinkDialog delete() {
            dropdownButton.click();
            deleteLink.click();
            return pageBinder.bind(DeleteProjectLinkDialog.class, entityType, key);
        }

        public String getName() {
            return pageElement.find(By.className("entity-link-name")).getText();
        }

        public boolean isPrimary() {
            return pageElement.find(By.className("name-container")).getText().contains("PRIMARY");
        }
    }
}
