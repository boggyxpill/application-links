package com.atlassian.webdriver.applinks.page;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.stream.Stream;

public abstract class ApplinkAbstractPage implements Page {
    @Inject
    protected WebDriver driver;
    @Inject
    protected PageElementFinder elementFinder;
    @Inject
    protected PageBinder pageBinder;

    /**
     * @return timed condition that there's a generic AJAX error dialog currently on the page
     */
    @Nonnull
    public final TimedCondition hasAjaxErrorDialog() {
        return elementFinder.find(By.id("applinks-error-dialog")).timed().isPresent();
    }

    public void clearDialogs() {
        //Find all "helpful" messages on the page and close them as they might get in the way of clicking other things
        Stream.of(
                "div.aui-message.closeable span.icon-close",
                "div.aui-message.closeable button.icon-close",
                "div.aui-message.closeable .aui-close-button",
                "button.helptip-close"
        )
                .flatMap(cssCloseButtonSelector ->
                        elementFinder.findAll(By.cssSelector(cssCloseButtonSelector)).stream()
                )
                .filter(PageElement::isPresent)
                .filter(PageElement::isVisible)
                .forEach(element -> element.javascript().mouse().click());
    }
}