package com.atlassian.webdriver.applinks.externalcomponent;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * * Page Object for UPM web sudo
 */
public class WebSudoPage {
    @Inject
    private PageElementFinder elementFinder;

    @ElementBy(id = "os_password")
    private PageElement passwordField;

    @ElementBy(id = "websudo")
    private PageElement webSudoButton;

    public WebSudoPage handleIfRequired(String password) {
        String wholePageText = elementFinder.find(By.tagName("body")).getText();
        if (wholePageText.contains("validate your credentials")) {
            passwordField.type(password);
            webSudoButton.click();
        }
        return this;
    }
}
