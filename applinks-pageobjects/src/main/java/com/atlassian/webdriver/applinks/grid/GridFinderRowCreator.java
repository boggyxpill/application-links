package com.atlassian.webdriver.applinks.grid;

import com.atlassian.pageobjects.elements.PageElement;

/**
 * Interface for creating grid finder rows
 */
public interface GridFinderRowCreator<R extends GridFinderRow> {
    public R create(GridFinder<R> gridFinder, PageElement pageElement);
}
