package com.atlassian.webdriver.applinks.page.adg.entitylinks;

import com.atlassian.applinks.api.EntityType;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.applinks.externalcomponent.OAuthConfirmPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.inject.Inject;

import static org.hamcrest.Matchers.is;

public class CreateProjectLinkDialog extends AbstractAtlaskitModalDialog {

    @Inject
    protected PageElementFinder elementFinder;
    @Inject
    protected PageBinder pageBinder;

    private final Class<? extends EntityType> entityType;
    private final String key;

    public CreateProjectLinkDialog(Class<? extends EntityType> entityType, String key) {
        this.entityType = entityType;
        this.key = key;
    }

    public CreateProjectLinkDialog selectApplication(String start) {
        selectValue("applselect", start);
        return this;
    }

    public OAuthConfirmPage<CreateProjectLinkDialog> authorize() {
        elementFinder.find(By.className("auth-button")).click();
        return pageBinder.bind(OAuthConfirmPage.class, this);
    }

    public CreateProjectLinkDialog selectProject(String project) {
        selectValue("projectselect", project);
        return this;
    }

    public ConfigureEntityLinksPage create() {
        elementFinder.find(By.className("create-link")).click();
        waitUntilDialogClosed();
        return pageBinder.bind(ConfigureEntityLinksPage.class, entityType, key);
    }

    private void selectValue(String selectId, String start) {
        elementFinder.find(By.id(selectId)).find(By.tagName("input"))
                .type(start)
                .type(Keys.RETURN);
    }
}
