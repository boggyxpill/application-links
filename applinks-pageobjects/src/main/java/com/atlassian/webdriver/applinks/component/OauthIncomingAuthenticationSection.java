package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.google.common.base.Supplier;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class OauthIncomingAuthenticationSection extends ConfigureApplicationSection {
    private static final String CONFIGURED = "Configured";
    private static final String NOT_CONFIGURED = "Not Configured";

    @Inject
    protected PageElementFinder elementFinder;
    @Inject
    protected Timeouts timeouts;

    @ElementBy(cssSelector = ".status-configured")
    protected PageElement configuredStatus;

    @ElementBy(cssSelector = ".status-not-configured")
    protected PageElement notConfiguredStatus;

    @ElementBy(id = "key")
    PageElement consumerKeyField;

    @ElementBy(id = "consumerName")
    PageElement consumerNameField;

    @ElementBy(id = "description")
    PageElement descriptionField;

    @ElementBy(id = "publicKey")
    PageElement publicKeyField;

    @ElementBy(id = "publicKey")
    PageElement consumerCallbackUrlField;

    @ElementBy(id = "2lo-option-group")
    PageElement twoLOSection;

    @ElementBy(id = "two-lo-enabled")
    CheckboxElement twoLOAllowedField;

    @ElementBy(id = "two-lo-execute-as")
    PageElement executingTwoLOUserField;

    @ElementBy(id = "two-lo-impersonation-enabled")
    CheckboxElement twoLOImpersonationAllowedField;

    @ElementBy(id = "incoming_oauth_manual_footer")
    PageElement footerDiv;

    @ElementBy(id = "save")
    PageElement saveButton;

    @ElementBy(id = "delete")
    PageElement deleteButton;

    @ElementBy(id = "auth-oauth-action-enable")
    PageElement enableButton;

    @ElementBy(id = "auth-oauth-action-disable")
    PageElement disableButton;

    @ElementBy(id = "confirm-dialog")
    PageElement confirmDeleteDialog;


    public TimedCondition isConfiguredTimed() {
        return configuredStatus.timed().isVisible();
    }

    public boolean isConfigured() {
        return isConfiguredTimed().now();
    }

    public TimedCondition isNotConfiguredTimed() {
        return notConfiguredStatus.timed().isVisible();
    }

    public boolean isNotConfigured() {
        return isNotConfiguredTimed().now();
    }

    public OauthIncomingAuthenticationSection setConsumerKey(String consumerKey) {
        waitUntilTrue(publicKeyField.timed().isVisible());
        consumerKeyField.clear();
        consumerKeyField.type(consumerKey);
        return this;
    }

    public OauthIncomingAuthenticationSection setConsumerName(String consumerName) {
        consumerNameField.clear();
        consumerNameField.type(consumerName);
        return this;
    }

    public OauthIncomingAuthenticationSection setDescription(String description) {
        descriptionField.clear();
        descriptionField.type(description);
        return this;
    }

    public OauthIncomingAuthenticationSection setPublicKey(String publicKey) {
        publicKeyField.clear();
        publicKeyField.type(publicKey);
        return this;
    }


    public OauthIncomingAuthenticationSection setConsumerCallbackUrl(String consumerCallbackUrl) {
        consumerCallbackUrlField.clear();
        consumerCallbackUrlField.type(consumerCallbackUrl);
        return this;
    }

    public boolean isTwoLOSectionPresent() {
        return twoLOSection.isPresent();
    }

    public OauthIncomingAuthenticationSection setTwoLOAllowed(boolean twoLOAllowed) {
        if (twoLOAllowed) {
            twoLOAllowedField.check();
        } else {
            twoLOAllowedField.uncheck();
        }
        return this;
    }

    public boolean getTwoLOAllowed() {
        return isTwoLOAllowedSupplier().get();
    }

    public TimedCondition isTwoLOAllowedTimed() {
        return Conditions.forSupplier(timeouts, isTwoLOAllowedSupplier());
    }

    public Supplier<Boolean> isTwoLOAllowedSupplier() {
        return new Supplier<Boolean>() {
            public Boolean get() {
                final String twoLo = twoLOAllowedField.getText();
                return StringUtils.isNotEmpty(twoLo) && Boolean.parseBoolean(twoLo);
            }
        };
    }

    public OauthIncomingAuthenticationSection setExecutingTwoLOUser(String executingTwoLOUser) {
        executingTwoLOUserField.clear();
        executingTwoLOUserField.type(executingTwoLOUser);
        return this;
    }

    public String getExecutingTwoLOUser() {
        return executingTwoLOUserField.getText();
    }

    public void ensureExecutingTwoLOUserErrorVisible() {
        PageElement message = elementFinder.find(By.id("two-lo-execute-as-error"));
        waitUntilTrue(message.timed().isPresent());
    }

    public OauthIncomingAuthenticationSection setTwoLOImpersonationAllowed(boolean twoLOImpersonationAllowed) {
        if (twoLOImpersonationAllowed) {
            twoLOImpersonationAllowedField.check();
        } else {
            twoLOImpersonationAllowedField.uncheck();
        }
        return this;
    }

    public boolean getTwoLOImpersonationAllowed() {
        return Boolean.parseBoolean(twoLOImpersonationAllowedField.getText());
    }

    public OauthIncomingAuthenticationSection save() {
        waitUntilTrue(saveButton.timed().isVisible());
        saveButton.click();
        return pageBinder.bind(OauthIncomingAuthenticationSection.class);
    }

    public OauthIncomingAuthenticationSection delete() {
        waitUntilTrue(deleteButton.timed().isVisible());
        deleteButton.click();
        confirmDelete();
        return pageBinder.bind(OauthIncomingAuthenticationSection.class);
    }

    /**
     * Click confirm on the confirm delete dialog that is displayed once the 'delete' button is clicked
     * for an existing incoming authentication. The dialog must be already open.
     *
     * @see #isConfirmDeleteDialogOpen()
     */
    private void confirmDelete() {
        waitUntilTrue(isConfirmDeleteDialogOpen());
        whyIntegrationTestingSux();

    }

    private void whyIntegrationTestingSux() {
        // START HACK because WebDriver in its infinite wisdom thinks that the whole confirm dialog is not visible
        // (not that we are making it easy for WebDriver to figure out things, by opening a dialog inside
        // an iframe and what not)
        // it will not allow to click that freakin' button - obviously - because user couldn't click it. So
        // considerate of you WebDriver, thanks. We have to deal with this in a well-established way
        // of using Javascript to do the job (back to Selenium1, yaaay)
        confirmDeleteDialog.find(By.className("confirm")).javascript().execute("jQuery(arguments[0]).click();");
        // END HACK <crying>
    }


    public TimedCondition isConfirmDeleteDialogOpen() {
        return confirmDeleteDialog.timed().hasClass("aui-dialog-content-ready");
    }

    public OauthIncomingAuthenticationSection enable() {
        waitUntilTrue(enableButton.timed().isVisible());
        enableButton.click();
        return this;
    }

    public OauthIncomingAuthenticationSection disable() {
        waitUntilTrue(disableButton.timed().isVisible());
        disableButton.click();
        return this;
    }

    public OauthIncomingAuthenticationSection login(final String userName, final String password) {
        elementFinder.find(By.id("al_username")).type(userName);
        elementFinder.find(By.id("al_password")).type(password);
        elementFinder.find(By.id("login-btn1")).click();
        // web sudo
        final PageElement passwordField = elementFinder.find(By.id("os_password"));
        waitUntilTrue(passwordField.timed().isVisible());
        passwordField.type(password);
        elementFinder.find(By.id("websudo")).click();
        return this;
    }

    public String getConsumerKey() {
        return this.getFieldValue(consumerKeyField);
    }

    public String getConsumerName() {
        return this.getFieldValue(consumerNameField);
    }

    public String getPublicKey() {
        return this.getFieldValue(publicKeyField);
    }
}
