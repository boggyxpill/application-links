package com.atlassian.webdriver.applinks.component.v3;

import com.atlassian.pageobjects.elements.PageElement;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Arrays.stream;

/**
 * Maps to the OAuth authentication levels rendered in the applinks edit page authentication section.
 * IDs/Texts should match the implementation inside oauth-picker-model.js.
 *
 * @since 5.2
 */
public enum UiOAuthConfig {
    OAUTH("1", "OAuth", "OAuth"),
    OAUTH_IMPERSONATION("2", "OAuth (impersonation)", "OAuth "),
    OAUTH_DISABLED("0", "Disabled", "Disabled");

    UiOAuthConfig(@Nonnull String id, @Nonnull String text, @Nonnull String typingText) {
        this.id = id;
        this.text = text;
        this.typingText = typingText;
    }

    private final String id;
    private final String text;
    /**
     * When typing the oauth in the input text via webdriver the `(` is missed for OAUTH_IMPERSONATION due to a bug in
     * Selenium and therefore no valid option is selected. To avoid that, we need this workaround.
     *
     * See https://github.com/seleniumhq/selenium-google-code-issue-archive/issues/6822
     */
    private final String typingText;

    @Nullable
    public static UiOAuthConfig fromText(@Nullable String text) {
        return stream(values()).filter(elem -> elem.getText().equals(text)).findFirst().orElse(null);
    }

    @Nullable
    public static UiOAuthConfig fromText(@Nonnull PageElement element) {
        return fromText(element.getText());
    }

    @Nonnull
    public String getId() {
        return id;
    }

    @Nonnull
    public String getText() {
        return text;
    }

    @Nonnull
    String typingText() {
        return typingText;
    }
}
