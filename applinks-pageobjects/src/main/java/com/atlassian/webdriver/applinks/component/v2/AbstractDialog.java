package com.atlassian.webdriver.applinks.component.v2;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.query.webdriver.GenericWebDriverTimedQuery;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.stream.Collectors;

/**
 * Represents an abstract instance of the new workflow AUI dialogs.
 *
 * @since v4.0.0
 */
public abstract class AbstractDialog {
    @Inject
    protected AtlassianWebDriver driver;

    @Inject
    protected PageElementFinder elementFinder;

    @Inject
    protected PageBinder pageBinder;

    @ElementBy(cssSelector = "button[class = 'button-panel-button aui-button aui-button-primary']")
    protected PageElement continueButton;

    @ElementBy(cssSelector = "button[class = 'button-panel-button applinks-cancel']")
    protected PageElement cancelLink;

    @ElementBy(className = "aui-dialog")
    protected PageElement dialog;

    public TimedQuery<Boolean> isAt() {
        return dialog.timed().isPresent();
    }

    public boolean singleDialogIsPresent() {
        Poller.waitUntilTrue(this.isAt());
        Iterable<PageElement> visibleDialogs =
                elementFinder.findAll(By.className("aui-dialog")).stream()
                        .filter(input -> input.isPresent() && input.isVisible())
                        .collect(Collectors.toList());
        return Iterables.size(visibleDialogs) <= 1;
    }

    public TimedQuery<Boolean> cancelIsVisible() {
        return cancelLink.timed().isPresent();
    }

    public TimedQuery<Boolean> continueIsVisible() {
        return continueButton.timed().isPresent();
    }

    public ListApplicationLinkPage clickCancel() {
        Poller.waitUntilTrue(cancelLink.timed().isPresent());
        cancelLink.click();
        Poller.waitUntilFalse(cancelLink.timed().isPresent());
        return pageBinder.bind(ListApplicationLinkPage.class);

    }

    public void clickContinue() {
        Poller.waitUntilTrue(continueButton.timed().isPresent());
        Poller.waitUntilTrue(continueButton.timed().isEnabled());
        continueButton.click();
    }


    public void setFieldValue(PageElement field, String value) {
        Poller.waitUntilTrue(field.timed().isPresent());

        field.clear();
        field.type(value);
    }

    public String getFieldValue(PageElement field) {
        Poller.waitUntilTrue(field.timed().isPresent());
        return field.getText();
    }

    public TimedQuery<String> currentUrl() {
        return new GenericWebDriverTimedQuery<>(() -> driver.getCurrentUrl(), 10_000);
    }

    public TimedElement getContinueButtonTimed() {
        return continueButton.timed();
    }

    public TimedElement getCancelLinkTimed() {
        return cancelLink.timed();
    }
}
